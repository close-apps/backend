
/*
 * Java dto for entity table regle_conversion
 * Created on 2020-08-17 ( Time 14:14:49 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._RegleConversionDto;

import lombok.*;
/**
 * DTO for table "regle_conversion"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class RegleConversionDto extends _RegleConversionDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     valeurAcheter        ;
	/*
	 * 
	 */
    private String     pointGagner          ;
	/*
	 * 
	 */
    private String     sommeCorrespondant   ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    programmeFideliteCarteId ;
	/*
	 * 
	 */
    private Integer    produitId            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String produitLibelle;
	private String programmeFideliteCarteLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   valeurAcheterParam    ;                     
	private SearchParam<String>   pointGagnerParam      ;                     
	private SearchParam<String>   sommeCorrespondantParam;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  programmeFideliteCarteIdParam;                     
	private SearchParam<Integer>  produitIdParam        ;                     
	private SearchParam<String>   produitLibelleParam   ;                     
	private SearchParam<String>   programmeFideliteCarteLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		RegleConversionDto other = (RegleConversionDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
