
/*
 * Java dto for entity table point_de_vente
 * Created on 2021-06-29 ( Time 01:39:32 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._PointDeVenteDto;

import lombok.*;
/**
 * DTO for table "point_de_vente"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class PointDeVenteDto extends _PointDeVenteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private String     code                 ;
	/*
	 * 
	 */
    private String     libelleCommune       ;
	/*
	 * 
	 */
    private String     ville                ;
	/*
	 * 
	 */
    private Boolean    isAllCarteDipso      ;
	/*
	 * 
	 */
    private Boolean    isAllTamponDipso     ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;
	/*
	 * 
	 */
    private Integer    communeId            ;
	/*
	 * 
	 */
    private Integer    gpsPointDeVenteId    ;
	/*
	 * 
	 */
    private Integer    userId               ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String communeLibelle;
	private String communeCode;
	private String userNom;
	private String userPrenoms;
	private String userLogin;
	private String enseigneNom;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   codeParam             ;                     
	private SearchParam<String>   libelleCommuneParam   ;                     
	private SearchParam<String>   villeParam            ;                     
	private SearchParam<Boolean>  isAllCarteDipsoParam  ;                     
	private SearchParam<Boolean>  isAllTamponDipsoParam ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<Integer>  communeIdParam        ;                     
	private SearchParam<Integer>  gpsPointDeVenteIdParam;                     
	private SearchParam<Integer>  userIdParam           ;                     
	private SearchParam<String>   communeLibelleParam   ;                     
	private SearchParam<String>   communeCodeParam      ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomsParam      ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   enseigneNomParam      ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PointDeVenteDto other = (PointDeVenteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
