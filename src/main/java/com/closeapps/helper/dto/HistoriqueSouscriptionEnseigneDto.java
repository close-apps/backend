
/*
 * Java dto for entity table historique_souscription_enseigne
 * Created on 2021-12-16 ( Time 11:03:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._HistoriqueSouscriptionEnseigneDto;

import lombok.*;
/**
 * DTO for table "historique_souscription_enseigne"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class HistoriqueSouscriptionEnseigneDto extends _HistoriqueSouscriptionEnseigneDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

  	/*
	 * 
	 */
    private Integer    souscriptionId       ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;
	/*
	 * 
	 */
    private Integer    nbreSouscription     ;
	/*
	 * 
	 */
    private Integer    nbreUtilisateurMobile ;
	/*
	 * 
	 */
    private String     commentaire          ;
	/*
	 * 
	 */
    private Integer    prixSpecifique       ;
	/*
	 * 
	 */
    private Integer    modePaiementId       ;
	/*
	 * 
	 */
    private Integer    etatSouscriptionId   ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isActive             ;
	/*
	 * 
	 */
	private String     dateExpiration       ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String enseigneNom;
	private String modePaiementLibelle;
	private String modePaiementCode;
	private String etatSouscriptionLibelle;
	private String etatSouscriptionCode;


	// Search param
	private SearchParam<Integer>  souscriptionIdParam   ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  nbreSouscriptionParam ;                     
	private SearchParam<Integer>  nbreUtilisateurMobileParam;                     
	private SearchParam<String>   commentaireParam      ;                     
	private SearchParam<Integer>  prixSpecifiqueParam   ;                     
	private SearchParam<Integer>  modePaiementIdParam   ;                     
	private SearchParam<Integer>  etatSouscriptionIdParam;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isActiveParam         ;                     

		private SearchParam<String>   dateExpirationParam   ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   enseigneNomParam      ;                     
	private SearchParam<String>   modePaiementLibelleParam;                     
	private SearchParam<String>   modePaiementCodeParam ;                     
	private SearchParam<String>   etatSouscriptionLibelleParam;                     
	private SearchParam<String>   etatSouscriptionCodeParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		HistoriqueSouscriptionEnseigneDto other = (HistoriqueSouscriptionEnseigneDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
