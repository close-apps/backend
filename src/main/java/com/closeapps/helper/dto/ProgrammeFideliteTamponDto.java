
/*
 * Java dto for entity table programme_fidelite_tampon
 * Created on 2020-11-29 ( Time 21:02:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._ProgrammeFideliteTamponDto;

import lombok.*;
/**
 * DTO for table "programme_fidelite_tampon"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class ProgrammeFideliteTamponDto extends _ProgrammeFideliteTamponDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     libelle              ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private String     conditionsDutilisation ;
	/*
	 * 
	 */
    private Integer    nbreTampon           ;
	/*
	 * 
	 */
    private Integer    nbrePallier          ;
	/*
	 * 
	 */
    private String     urlImage             ;
	/*
	 * 
	 */
    private String     gain                 ;
	/*
	 * 
	 */
    private Integer    valeurMaxGain        ;
	/*
	 * 
	 */
    private Boolean    isValeurMaxGain      ;
	/*
	 * 
	 */
    private Integer    tamponObtenuApresSouscription ;
	/*
	 * 
	 */
    private String     codeBarre            ;
	/*
	 * 
	 */
    private String     qrCode               ;
	/*
	 * 
	 */
    private Boolean    isDispoInAllPdv      ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private Boolean    isDesactived         ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String enseigneNom;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   libelleParam          ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   conditionsDutilisationParam;                     
	private SearchParam<Integer>  nbreTamponParam       ;                     
	private SearchParam<Integer>  nbrePallierParam      ;                     
	private SearchParam<String>   urlImageParam         ;                     
	private SearchParam<String>   gainParam             ;                     
	private SearchParam<Integer>  valeurMaxGainParam    ;                     
	private SearchParam<Boolean>  isValeurMaxGainParam  ;                     
	private SearchParam<Integer>  tamponObtenuApresSouscriptionParam;                     
	private SearchParam<String>   codeBarreParam        ;                     
	private SearchParam<String>   qrCodeParam           ;                     
	private SearchParam<Boolean>  isDispoInAllPdvParam  ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isDesactivedParam     ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<String>   enseigneNomParam      ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ProgrammeFideliteTamponDto other = (ProgrammeFideliteTamponDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
