
/*
 * Java dto for entity table souscription
 * Created on 2021-12-16 ( Time 10:39:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._SouscriptionDto;

import lombok.*;
/**
 * DTO for table "souscription"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class SouscriptionDto extends _SouscriptionDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    pallierForfaitId     ;
	/*
	 * 
	 */
    private Integer    dureeForfaitId       ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private String     urlImage             ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private String     prix                 ;
	/*
	 * 
	 */
    private Integer    nbreMaxiUser         ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String pallierForfaitLibelle;
	private String pallierForfaitCode;
	private String dureeForfaitLibelle;
	private String dureeForfaitCode;


	// Search param
	private SearchParam<Integer>  pallierForfaitIdParam ;                     
	private SearchParam<Integer>  dureeForfaitIdParam   ;                     
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   urlImageParam         ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   prixParam             ;                     
	private SearchParam<Integer>  nbreMaxiUserParam     ;                     
	private SearchParam<String>   pallierForfaitLibelleParam;                     
	private SearchParam<String>   pallierForfaitCodeParam;                     
	private SearchParam<String>   dureeForfaitLibelleParam;                     
	private SearchParam<String>   dureeForfaitCodeParam ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		SouscriptionDto other = (SouscriptionDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
