
/*
 * Java dto for entity table information_regle_securite_carte
 * Created on 2021-08-14 ( Time 17:43:18 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._InformationRegleSecuriteCarteDto;

import lombok.*;
/**
 * DTO for table "information_regle_securite_carte"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class InformationRegleSecuriteCarteDto extends _InformationRegleSecuriteCarteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    plafondSommeACrediter ;
	/*
	 * 
	 */
    private Integer    nbreFoisCrediterParPdv ;
	/*
	 * 
	 */
    private Integer    nbreFoisCrediterAllPdv ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    programmeFideliteCarteId ;
	/*
	 * 
	 */
    private Integer    uniteTempsId         ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String programmeFideliteCarteLibelle;
	private String uniteTempsLibelle;
	private String uniteTempsCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  plafondSommeACrediterParam;                     
	private SearchParam<Integer>  nbreFoisCrediterParPdvParam;                     
	private SearchParam<Integer>  nbreFoisCrediterAllPdvParam;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  programmeFideliteCarteIdParam;                     
	private SearchParam<Integer>  uniteTempsIdParam     ;                     
	private SearchParam<String>   programmeFideliteCarteLibelleParam;                     
	private SearchParam<String>   uniteTempsLibelleParam;                     
	private SearchParam<String>   uniteTempsCodeParam   ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		InformationRegleSecuriteCarteDto other = (InformationRegleSecuriteCarteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
