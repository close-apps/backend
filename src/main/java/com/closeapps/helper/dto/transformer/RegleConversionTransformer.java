

/*
 * Java transformer for entity table regle_conversion 
 * Created on 2020-08-17 ( Time 14:14:50 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "regle_conversion"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface RegleConversionTransformer {

	RegleConversionTransformer INSTANCE = Mappers.getMapper(RegleConversionTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.produit.id", target="produitId"),
				@Mapping(source="entity.produit.libelle", target="produitLibelle"),
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
				@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
	})
	RegleConversionDto toDto(RegleConversion entity) throws ParseException;

    List<RegleConversionDto> toDtos(List<RegleConversion> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.valeurAcheter", target="valeurAcheter"),
		@Mapping(source="dto.pointGagner", target="pointGagner"),
		@Mapping(source="dto.sommeCorrespondant", target="sommeCorrespondant"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="produit", target="produit"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
	})
    RegleConversion toEntity(RegleConversionDto dto, Produit produit, ProgrammeFideliteCarte programmeFideliteCarte) throws ParseException;

    //List<RegleConversion> toEntities(List<RegleConversionDto> dtos) throws ParseException;

}
