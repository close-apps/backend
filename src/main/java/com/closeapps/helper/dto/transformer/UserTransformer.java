

/*
 * Java transformer for entity table user 
 * Created on 2020-12-30 ( Time 09:35:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "user"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface UserTransformer {

	UserTransformer INSTANCE = Mappers.getMapper(UserTransformer.class);

	@Mappings({

		@Mapping(source="entity.password", ignore = true, target="password"),

		@Mapping(source="entity.dateExpirationToken", dateFormat="dd/MM/yyyy",target="dateExpirationToken"),

		@Mapping(source="entity.passwordGenerate", ignore = true, target="passwordGenerate"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
				@Mapping(source="entity.enseigne.isActive", target="enseigneIsActive"), //AJOUTER
				@Mapping(source="entity.enseigne.etatSouscription.code", target="etatSouscriptionEnseigne"), //AJOUTER
				@Mapping(source="entity.enseigne.souscription", target="enseigneSouscription"), //AJOUTER
				@Mapping(source="entity.enseigne.dateExpirationSouscription", target="enseigneDateExpiration"), //AJOUTER
		@Mapping(source="entity.role.id", target="roleId"),
				@Mapping(source="entity.role.libelle", target="roleLibelle"),
				@Mapping(source="entity.role.code", target="roleCode"),
	})
	UserDto toDto(User entity) throws ParseException;

    List<UserDto> toDtos(List<User> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.matricule", target="matricule"),
		@Mapping(source="dto.token", target="token"),
		@Mapping(source="dto.dateExpirationToken", dateFormat="dd/MM/yyyy",target="dateExpirationToken"),
		@Mapping(source="dto.isTokenValide", target="isTokenValide"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isFirstlyConnexion", target="isFirstlyConnexion"),
		@Mapping(source="dto.isAdminEnseigne", target="isAdminEnseigne"),
		@Mapping(source="dto.byFirebaseUser", target="byFirebaseUser"),
		@Mapping(source="dto.emailGoogle", target="emailGoogle"),
		@Mapping(source="dto.telephoneGoogle", target="telephoneGoogle"),
		@Mapping(source="dto.emailFacebook", target="emailFacebook"),
		@Mapping(source="dto.telephoneFacebook", target="telephoneFacebook"),
		@Mapping(source="dto.passwordGenerate", target="passwordGenerate"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="enseigne", target="enseigne"),
		@Mapping(source="role", target="role"),
	})
    User toEntity(UserDto dto, Enseigne enseigne, Role role) throws ParseException;

    //List<User> toEntities(List<UserDto> dtos) throws ParseException;

}
