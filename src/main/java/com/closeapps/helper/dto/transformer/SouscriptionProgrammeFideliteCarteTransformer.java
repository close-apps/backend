

/*
 * Java transformer for entity table souscription_programme_fidelite_carte 
 * Created on 2020-09-17 ( Time 10:12:19 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "souscription_programme_fidelite_carte"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface SouscriptionProgrammeFideliteCarteTransformer {

	SouscriptionProgrammeFideliteCarteTransformer INSTANCE = Mappers.getMapper(SouscriptionProgrammeFideliteCarteTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.carte.id", target="carteId"),
		@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
		@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
		@Mapping(source="entity.programmeFideliteCarte.pourXAcheter", target="programmeFideliteCartePourXAcheter"),// AJOUT
		@Mapping(source="entity.programmeFideliteCarte.nbrePointObtenu", target="programmeFideliteCarteNbrePointObtenu"),// AJOUT
		@Mapping(source="entity.programmeFideliteCarte.correspondanceNbrePoint", target="programmeFideliteCarteCorrespondanceNbrePoint"),// AJOUT
		
		@Mapping(source="entity.programmeFideliteCarte.enseigne.urlLogo", target="enseigneUrlLogo"), // ajouter
		@Mapping(source="entity.programmeFideliteCarte.enseigne.couleur", target="enseigneCouleur"), // ajouter
	})
	SouscriptionProgrammeFideliteCarteDto toDto(SouscriptionProgrammeFideliteCarte entity) throws ParseException;

	List<SouscriptionProgrammeFideliteCarteDto> toDtos(List<SouscriptionProgrammeFideliteCarte> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.raisonLocked", target="raisonLocked"),
		@Mapping(source="dto.isNotifed", target="isNotifed"),
		@Mapping(source="dto.messageNotifed", target="messageNotifed"),
		@Mapping(source="dto.unsubscribe", target="unsubscribe"),
		@Mapping(source="dto.isShowLastNotification", target="isShowLastNotification"),
		@Mapping(source="dto.emailParrain", target="emailParrain"),
		@Mapping(source="dto.telephoneParrain", target="telephoneParrain"),
		@Mapping(source="dto.nbrePointActuelle", target="nbrePointActuelle"),
		@Mapping(source="dto.nbrePointPrecedent", target="nbrePointPrecedent"),
		@Mapping(source="dto.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="carte", target="carte"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
	})
	SouscriptionProgrammeFideliteCarte toEntity(SouscriptionProgrammeFideliteCarteDto dto, Carte carte, ProgrammeFideliteCarte programmeFideliteCarte) throws ParseException;

	//List<SouscriptionProgrammeFideliteCarte> toEntities(List<SouscriptionProgrammeFideliteCarteDto> dtos) throws ParseException;

}
