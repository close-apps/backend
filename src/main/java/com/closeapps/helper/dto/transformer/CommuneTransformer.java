

/*
 * Java transformer for entity table commune 
 * Created on 2020-12-30 ( Time 09:34:29 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "commune"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface CommuneTransformer {

	CommuneTransformer INSTANCE = Mappers.getMapper(CommuneTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.ville.id", target="villeId"),
				@Mapping(source="entity.ville.libelle", target="villeLibelle"),
				@Mapping(source="entity.ville.code", target="villeCode"),
	})
	CommuneDto toDto(Commune entity) throws ParseException;

    List<CommuneDto> toDtos(List<Commune> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="ville", target="ville"),
	})
    Commune toEntity(CommuneDto dto, Ville ville) throws ParseException;

    //List<Commune> toEntities(List<CommuneDto> dtos) throws ParseException;

}
