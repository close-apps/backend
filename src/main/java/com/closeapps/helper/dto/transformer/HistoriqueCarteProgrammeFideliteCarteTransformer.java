

/*
 * Java transformer for entity table historique_carte_programme_fidelite_carte 
 * Created on 2021-08-03 ( Time 11:17:28 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "historique_carte_programme_fidelite_carte"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface HistoriqueCarteProgrammeFideliteCarteTransformer {

	HistoriqueCarteProgrammeFideliteCarteTransformer INSTANCE = Mappers.getMapper(HistoriqueCarteProgrammeFideliteCarteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),

		//@Mapping(source="entity.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),

		@Mapping(source="entity.heureAction", dateFormat="dd/MM/yyyy",target="heureAction"),
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
		@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
		@Mapping(source="entity.programmeFideliteCarte.pourXAcheter", target="programmeFideliteCartePourXAcheter"),// AJOUT
		@Mapping(source="entity.programmeFideliteCarte.nbrePointObtenu", target="programmeFideliteCarteNbrePointObtenu"),// AJOUT
		@Mapping(source="entity.programmeFideliteCarte.correspondanceNbrePoint", target="programmeFideliteCarteCorrespondanceNbrePoint"),// AJOUT
		@Mapping(source="entity.dateAction", dateFormat="dd/MM/yyyy HH:mm:ss",target="dateAction"), // ajouter

		@Mapping(source="entity.typeAction.id", target="typeActionId"),
		@Mapping(source="entity.typeAction.libelle", target="typeActionLibelle"),
		@Mapping(source="entity.typeAction.code", target="typeActionCode"),
		@Mapping(source="entity.carte.id", target="carteId"),
		@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.pointDeVente.id", target="pointDeVenteId"),
		@Mapping(source="entity.pointDeVente.nom", target="pointDeVenteNom"),
		@Mapping(source="entity.pointDeVente.code", target="pointDeVenteCode"),
	})
	HistoriqueCarteProgrammeFideliteCarteDto toDto(HistoriqueCarteProgrammeFideliteCarte entity) throws ParseException;

	List<HistoriqueCarteProgrammeFideliteCarteDto> toDtos(List<HistoriqueCarteProgrammeFideliteCarte> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.nbrePointOperation", target="nbrePointOperation"),
		@Mapping(source="dto.nbrePointActuel", target="nbrePointActuel"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),
		@Mapping(source="dto.heureAction", dateFormat="dd/MM/yyyy",target="heureAction"),
		@Mapping(source="dto.nbrePointAvantAction", target="nbrePointAvantAction"),
		@Mapping(source="dto.sommeOperation", target="sommeOperation"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
		@Mapping(source="typeAction", target="typeAction"),
		@Mapping(source="carte", target="carte"),
		@Mapping(source="pointDeVente", target="pointDeVente"),
	})
	HistoriqueCarteProgrammeFideliteCarte toEntity(HistoriqueCarteProgrammeFideliteCarteDto dto, ProgrammeFideliteCarte programmeFideliteCarte, TypeAction typeAction, Carte carte, PointDeVente pointDeVente) throws ParseException;

	//List<HistoriqueCarteProgrammeFideliteCarte> toEntities(List<HistoriqueCarteProgrammeFideliteCarteDto> dtos) throws ParseException;

}
