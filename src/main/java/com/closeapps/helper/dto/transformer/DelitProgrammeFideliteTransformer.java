

/*
 * Java transformer for entity table delit_programme_fidelite 
 * Created on 2021-08-16 ( Time 17:55:17 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "delit_programme_fidelite"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface DelitProgrammeFideliteTransformer {

	DelitProgrammeFideliteTransformer INSTANCE = Mappers.getMapper(DelitProgrammeFideliteTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateDelit", dateFormat="dd/MM/yyyy",target="dateDelit"),

		@Mapping(source="entity.heureDelit", dateFormat="dd/MM/yyyy",target="heureDelit"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.informationRegleSecuriteTampon.id", target="informationRegleSecuriteTamponId"),
		@Mapping(source="entity.informationRegleSecuriteCarte.id", target="informationRegleSecuriteCarteId"),
		@Mapping(source="entity.pointDeVente.id", target="pointDeVenteId"),
		@Mapping(source="entity.pointDeVente.nom", target="pointDeVenteNom"),
		@Mapping(source="entity.pointDeVente.code", target="pointDeVenteCode"),
		@Mapping(source="entity.carte.id", target="carteId"),
		@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.carte.user.id", target="userId"), // ajouter
		@Mapping(source="entity.carte.user.nom", target="userNom"), // ajouter
		@Mapping(source="entity.carte.user.prenoms", target="userPrenoms"), // ajouter
		@Mapping(source="entity.carte.user.email", target="userEmail"), // ajouter
		@Mapping(source="entity.carte.user.login", target="userLogin"), // ajouter
		@Mapping(source="entity.carte.user.telephone", target="userTelephone"), // ajouter
		@Mapping(source="entity.informationRegleSecuriteTampon.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"), // ajouter
		@Mapping(source="entity.informationRegleSecuriteCarte.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"), // ajouter
		@Mapping(source="entity.informationRegleSecuriteTampon.uniteTemps.libelle", target="uniteTempsTamponLibelle"), // ajouter
		@Mapping(source="entity.informationRegleSecuriteCarte.uniteTemps.libelle", target="uniteTempsCarteLibelle"), // ajouter


	})
	DelitProgrammeFideliteDto toDto(DelitProgrammeFidelite entity) throws ParseException;

	List<DelitProgrammeFideliteDto> toDtos(List<DelitProgrammeFidelite> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateDelit", dateFormat="dd/MM/yyyy",target="dateDelit"),
		@Mapping(source="dto.heureDelit", dateFormat="dd/MM/yyyy",target="heureDelit"),
		@Mapping(source="dto.numeroDelit", target="numeroDelit"),
		@Mapping(source="dto.messageNotifed", target="messageNotifed"),
		@Mapping(source="dto.messageLocked", target="messageLocked"),
		@Mapping(source="dto.isNotifed", target="isNotifed"),
		@Mapping(source="dto.isApplicateSanction", target="isApplicateSanction"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="informationRegleSecuriteTampon", target="informationRegleSecuriteTampon"),
		@Mapping(source="informationRegleSecuriteCarte", target="informationRegleSecuriteCarte"),
		@Mapping(source="pointDeVente", target="pointDeVente"),
		@Mapping(source="carte", target="carte"),
	})
	DelitProgrammeFidelite toEntity(DelitProgrammeFideliteDto dto, InformationRegleSecuriteTampon informationRegleSecuriteTampon, InformationRegleSecuriteCarte informationRegleSecuriteCarte, PointDeVente pointDeVente, Carte carte) throws ParseException;

	//List<DelitProgrammeFidelite> toEntities(List<DelitProgrammeFideliteDto> dtos) throws ParseException;

}
