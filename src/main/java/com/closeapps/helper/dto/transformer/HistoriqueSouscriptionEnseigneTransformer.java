

/*
 * Java transformer for entity table historique_souscription_enseigne 
 * Created on 2021-12-16 ( Time 11:03:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "historique_souscription_enseigne"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface HistoriqueSouscriptionEnseigneTransformer {

	HistoriqueSouscriptionEnseigneTransformer INSTANCE = Mappers.getMapper(HistoriqueSouscriptionEnseigneTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),

		@Mapping(source="entity.dateExpiration", dateFormat="dd/MM/yyyy",target="dateExpiration"),
		@Mapping(source="entity.souscription.id", target="souscriptionId"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
		@Mapping(source="entity.modePaiement.id", target="modePaiementId"),
				@Mapping(source="entity.modePaiement.libelle", target="modePaiementLibelle"),
				@Mapping(source="entity.modePaiement.code", target="modePaiementCode"),
		@Mapping(source="entity.etatSouscription.id", target="etatSouscriptionId"),
				@Mapping(source="entity.etatSouscription.libelle", target="etatSouscriptionLibelle"),
			@Mapping(source = "entity.etatSouscription.code", target = "etatSouscriptionCode"),
				
				@Mapping(source="entity.enseigne.telephone", target="enseigneTelephone"), // AJOUTER
				@Mapping(source="entity.enseigne.email", target="enseigneEmail"), // AJOUTER
				@Mapping(source="entity.enseigne.nomAdmin", target="enseigneNomAdmin"), // AJOUTER
			@Mapping(source = "entity.enseigne.prenomsAdmin", target = "enseignePrenomsAdmin"), // AJOUTER
				
			@Mapping(source="entity.souscription.description", target="souscriptionDescription"), // AJOUTER
			@Mapping(source="entity.souscription.pallierForfait.libelle", target="pallierForfaitLibelle"), // AJOUTER


	})
	HistoriqueSouscriptionEnseigneDto toDto(HistoriqueSouscriptionEnseigne entity) throws ParseException;

    List<HistoriqueSouscriptionEnseigneDto> toDtos(List<HistoriqueSouscriptionEnseigne> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nbreSouscription", target="nbreSouscription"),
		@Mapping(source="dto.nbreUtilisateurMobile", target="nbreUtilisateurMobile"),
		@Mapping(source="dto.commentaire", target="commentaire"),
		@Mapping(source="dto.prixSpecifique", target="prixSpecifique"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isActive", target="isActive"),
		@Mapping(source="dto.dateExpiration", dateFormat="dd/MM/yyyy",target="dateExpiration"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="souscription", target="souscription"),
		@Mapping(source="enseigne", target="enseigne"),
		@Mapping(source="modePaiement", target="modePaiement"),
		@Mapping(source="etatSouscription", target="etatSouscription"),
	})
    HistoriqueSouscriptionEnseigne toEntity(HistoriqueSouscriptionEnseigneDto dto, Souscription souscription, Enseigne enseigne, ModePaiement modePaiement, EtatSouscription etatSouscription) throws ParseException;

    //List<HistoriqueSouscriptionEnseigne> toEntities(List<HistoriqueSouscriptionEnseigneDto> dtos) throws ParseException;

}
