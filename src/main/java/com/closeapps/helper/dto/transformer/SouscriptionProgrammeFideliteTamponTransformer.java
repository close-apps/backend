

/*
 * Java transformer for entity table souscription_programme_fidelite_tampon 
 * Created on 2020-09-17 ( Time 10:11:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "souscription_programme_fidelite_tampon"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface SouscriptionProgrammeFideliteTamponTransformer {

	SouscriptionProgrammeFideliteTamponTransformer INSTANCE = Mappers.getMapper(SouscriptionProgrammeFideliteTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
				@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.carte.id", target="carteId"),
				@Mapping(source="entity.carte.code", target="carteCode"),

				@Mapping(source="entity.programmeFideliteTampon.enseigne.urlLogo", target="enseigneUrlLogo"), // ajouter
				@Mapping(source="entity.programmeFideliteTampon.enseigne.couleur", target="enseigneCouleur"), // ajouter

	})
	SouscriptionProgrammeFideliteTamponDto toDto(SouscriptionProgrammeFideliteTampon entity) throws ParseException;

    List<SouscriptionProgrammeFideliteTamponDto> toDtos(List<SouscriptionProgrammeFideliteTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.raisonLocked", target="raisonLocked"),
		@Mapping(source="dto.isNotifed", target="isNotifed"),
		@Mapping(source="dto.messageNotifed", target="messageNotifed"),
		@Mapping(source="dto.unsubscribe", target="unsubscribe"),
		@Mapping(source="dto.raisonUnsubscribe", target="raisonUnsubscribe"),
		@Mapping(source="dto.isShowLastNotification", target="isShowLastNotification"),
		@Mapping(source="dto.nbreTamponActuelle", target="nbreTamponActuelle"),
		@Mapping(source="dto.nbreTamponPrecedent", target="nbreTamponPrecedent"),
		@Mapping(source="dto.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="carte", target="carte"),
	})
    SouscriptionProgrammeFideliteTampon toEntity(SouscriptionProgrammeFideliteTamponDto dto, ProgrammeFideliteTampon programmeFideliteTampon, Carte carte) throws ParseException;

    //List<SouscriptionProgrammeFideliteTampon> toEntities(List<SouscriptionProgrammeFideliteTamponDto> dtos) throws ParseException;

}
