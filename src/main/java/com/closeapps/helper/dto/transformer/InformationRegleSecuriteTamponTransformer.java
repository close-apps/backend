

/*
 * Java transformer for entity table information_regle_securite_tampon 
 * Created on 2021-08-11 ( Time 09:26:15 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "information_regle_securite_tampon"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface InformationRegleSecuriteTamponTransformer {

	InformationRegleSecuriteTamponTransformer INSTANCE = Mappers.getMapper(InformationRegleSecuriteTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
				@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.uniteTemps.id", target="uniteTempsId"),
				@Mapping(source="entity.uniteTemps.libelle", target="uniteTempsLibelle"),
				@Mapping(source="entity.uniteTemps.code", target="uniteTempsCode"),
	})
	InformationRegleSecuriteTamponDto toDto(InformationRegleSecuriteTampon entity) throws ParseException;

    List<InformationRegleSecuriteTamponDto> toDtos(List<InformationRegleSecuriteTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nbreMaxiParPdv", target="nbreMaxiParPdv"),
		@Mapping(source="dto.nbreMaxiAllPdv", target="nbreMaxiAllPdv"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="uniteTemps", target="uniteTemps"),
	})
    InformationRegleSecuriteTampon toEntity(InformationRegleSecuriteTamponDto dto, ProgrammeFideliteTampon programmeFideliteTampon, UniteTemps uniteTemps) throws ParseException;

    //List<InformationRegleSecuriteTampon> toEntities(List<InformationRegleSecuriteTamponDto> dtos) throws ParseException;

}
