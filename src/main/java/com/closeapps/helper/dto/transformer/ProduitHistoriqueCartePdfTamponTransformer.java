

/*
 * Java transformer for entity table produit_historique_carte_pdf_tampon 
 * Created on 2020-08-17 ( Time 14:14:49 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "produit_historique_carte_pdf_tampon"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface ProduitHistoriqueCartePdfTamponTransformer {

	ProduitHistoriqueCartePdfTamponTransformer INSTANCE = Mappers.getMapper(ProduitHistoriqueCartePdfTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.historiqueCarteProgrammeFideliteTampon.id", target="historiqueCarteProgrammeFideliteTamponId"),
		@Mapping(source="entity.produit.id", target="produitId"),
				@Mapping(source="entity.produit.libelle", target="produitLibelle"),
	})
	ProduitHistoriqueCartePdfTamponDto toDto(ProduitHistoriqueCartePdfTampon entity) throws ParseException;

    List<ProduitHistoriqueCartePdfTamponDto> toDtos(List<ProduitHistoriqueCartePdfTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="historiqueCarteProgrammeFideliteTampon", target="historiqueCarteProgrammeFideliteTampon"),
		@Mapping(source="produit", target="produit"),
	})
    ProduitHistoriqueCartePdfTampon toEntity(ProduitHistoriqueCartePdfTamponDto dto, HistoriqueCarteProgrammeFideliteTampon historiqueCarteProgrammeFideliteTampon, Produit produit) throws ParseException;

    //List<ProduitHistoriqueCartePdfTampon> toEntities(List<ProduitHistoriqueCartePdfTamponDto> dtos) throws ParseException;

}
