

/*
 * Java transformer for entity table point_de_vente_programme_fidelite_tampon 
 * Created on 2020-08-17 ( Time 14:14:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "point_de_vente_programme_fidelite_tampon"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PointDeVenteProgrammeFideliteTamponTransformer {

	PointDeVenteProgrammeFideliteTamponTransformer INSTANCE = Mappers.getMapper(PointDeVenteProgrammeFideliteTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
		@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.pointDeVente.id", target="pointDeVenteId"),
		@Mapping(source="entity.pointDeVente.nom", target="pointDeVenteNom"),
		@Mapping(source="entity.pointDeVente.code", target="pointDeVenteCode"),

		@Mapping(source="entity.pointDeVente.gpsPointDeVente.longititude", target="longitude"),// AJOUTER
		@Mapping(source="entity.pointDeVente.gpsPointDeVente.latitude", target="latitude"),// AJOUTER
		@Mapping(source="entity.pointDeVente.commune.ville.libelle", target="villePointDeVente"), // AJOUTER
		@Mapping(source="entity.pointDeVente.commune.libelle", target="communePointDeVente"), // AJOUTER

	})
	PointDeVenteProgrammeFideliteTamponDto toDto(PointDeVenteProgrammeFideliteTampon entity) throws ParseException;

	List<PointDeVenteProgrammeFideliteTamponDto> toDtos(List<PointDeVenteProgrammeFideliteTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="pointDeVente", target="pointDeVente"),
	})
	PointDeVenteProgrammeFideliteTampon toEntity(PointDeVenteProgrammeFideliteTamponDto dto, ProgrammeFideliteTampon programmeFideliteTampon, PointDeVente pointDeVente) throws ParseException;

	//List<PointDeVenteProgrammeFideliteTampon> toEntities(List<PointDeVenteProgrammeFideliteTamponDto> dtos) throws ParseException;

}
