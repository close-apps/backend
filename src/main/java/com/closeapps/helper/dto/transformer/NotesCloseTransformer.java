

/*
 * Java transformer for entity table notes_close 
 * Created on 2021-08-04 ( Time 01:38:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "notes_close"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface NotesCloseTransformer {

	NotesCloseTransformer INSTANCE = Mappers.getMapper(NotesCloseTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.carte.id", target="carteId"),
				@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.user.id", target="userId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"),
	})
	NotesCloseDto toDto(NotesClose entity) throws ParseException;

    List<NotesCloseDto> toDtos(List<NotesClose> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.commentaire", target="commentaire"),
		@Mapping(source="dto.isValiderByClose", target="isValiderByClose"),
		@Mapping(source="dto.retourClose", target="retourClose"),
		@Mapping(source="dto.nbreEtoile", target="nbreEtoile"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="carte", target="carte"),
		@Mapping(source="user", target="user"),
	})
    NotesClose toEntity(NotesCloseDto dto, Carte carte, User user) throws ParseException;

    //List<NotesClose> toEntities(List<NotesCloseDto> dtos) throws ParseException;

}
