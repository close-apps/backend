

/*
 * Java transformer for entity table historique_carte_programme_fidelite_tampon 
 * Created on 2021-09-11 ( Time 11:16:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "historique_carte_programme_fidelite_tampon"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface HistoriqueCarteProgrammeFideliteTamponTransformer {

	HistoriqueCarteProgrammeFideliteTamponTransformer INSTANCE = Mappers.getMapper(HistoriqueCarteProgrammeFideliteTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(source="entity.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),
		@Mapping(source="entity.dateAction", dateFormat="dd/MM/yyyy HH:mm:ss",target="dateAction"), // ajouter
		@Mapping(source="entity.heureAction", dateFormat="dd/MM/yyyy",target="heureAction"),
		@Mapping(source="entity.carte.id", target="carteId"),
		@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.pointDeVente.id", target="pointDeVenteId"),
		@Mapping(source="entity.pointDeVente.nom", target="pointDeVenteNom"),
		@Mapping(source="entity.pointDeVente.code", target="pointDeVenteCode"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
		@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.typeAction.id", target="typeActionId"),
		@Mapping(source="entity.typeAction.libelle", target="typeActionLibelle"),
		@Mapping(source="entity.typeAction.code", target="typeActionCode"),
		@Mapping(source="entity.souscriptionProgrammeFideliteTampon.id", target="souscriptionProgrammeFideliteTamponId"),
		@Mapping(source="entity.souscriptionProgrammeFideliteTampon.code", target="souscriptionProgrammeFideliteTamponCode"),
		@Mapping(source="entity.souscriptionProgrammeFideliteTampon.libelle", target="souscriptionProgrammeFideliteTamponLibelle"),
	})
	HistoriqueCarteProgrammeFideliteTamponDto toDto(HistoriqueCarteProgrammeFideliteTampon entity) throws ParseException;

	List<HistoriqueCarteProgrammeFideliteTamponDto> toDtos(List<HistoriqueCarteProgrammeFideliteTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.nbreTamponObtenu", target="nbreTamponObtenu"),
		@Mapping(source="dto.nbreTamponActuel", target="nbreTamponActuel"),
		@Mapping(source="dto.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),
		@Mapping(source="dto.heureAction", dateFormat="dd/MM/yyyy",target="heureAction"),
		@Mapping(source="dto.nbreTamponAvantAction", target="nbreTamponAvantAction"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.chiffreDaffaire", target="chiffreDaffaire"),
		@Mapping(source="carte", target="carte"),
		@Mapping(source="pointDeVente", target="pointDeVente"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="typeAction", target="typeAction"),
		@Mapping(source="souscriptionProgrammeFideliteTampon", target="souscriptionProgrammeFideliteTampon"),
	})
	HistoriqueCarteProgrammeFideliteTampon toEntity(HistoriqueCarteProgrammeFideliteTamponDto dto, Carte carte, PointDeVente pointDeVente, ProgrammeFideliteTampon programmeFideliteTampon, TypeAction typeAction, SouscriptionProgrammeFideliteTampon souscriptionProgrammeFideliteTampon) throws ParseException;

	//List<HistoriqueCarteProgrammeFideliteTampon> toEntities(List<HistoriqueCarteProgrammeFideliteTamponDto> dtos) throws ParseException;

}
