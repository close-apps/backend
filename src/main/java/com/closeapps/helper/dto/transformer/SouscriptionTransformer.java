

/*
 * Java transformer for entity table souscription 
 * Created on 2021-12-16 ( Time 10:39:34 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "souscription"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface SouscriptionTransformer {

	SouscriptionTransformer INSTANCE = Mappers.getMapper(SouscriptionTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.pallierForfait.id", target="pallierForfaitId"),
				@Mapping(source="entity.pallierForfait.libelle", target="pallierForfaitLibelle"),
				@Mapping(source="entity.pallierForfait.code", target="pallierForfaitCode"),
		@Mapping(source="entity.dureeForfait.id", target="dureeForfaitId"),
				@Mapping(source="entity.dureeForfait.libelle", target="dureeForfaitLibelle"),
				@Mapping(source="entity.dureeForfait.code", target="dureeForfaitCode"),
	})
	SouscriptionDto toDto(Souscription entity) throws ParseException;

    List<SouscriptionDto> toDtos(List<Souscription> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.urlImage", target="urlImage"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.prix", target="prix"),
		@Mapping(source="dto.nbreMaxiUser", target="nbreMaxiUser"),
		@Mapping(source="pallierForfait", target="pallierForfait"),
		@Mapping(source="dureeForfait", target="dureeForfait"),
	})
    Souscription toEntity(SouscriptionDto dto, PallierForfait pallierForfait, DureeForfait dureeForfait) throws ParseException;

    //List<Souscription> toEntities(List<SouscriptionDto> dtos) throws ParseException;

}
