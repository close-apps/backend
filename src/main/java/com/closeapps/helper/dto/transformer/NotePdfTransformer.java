

/*
 * Java transformer for entity table note_pdf 
 * Created on 2021-08-26 ( Time 17:44:08 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "note_pdf"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface NotePdfTransformer {

	NotePdfTransformer INSTANCE = Mappers.getMapper(NotePdfTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.souscriptionProgrammeFideliteTampon.id", target="souscriptionProgrammeFideliteTamponId"),
				@Mapping(source="entity.souscriptionProgrammeFideliteTampon.code", target="souscriptionProgrammeFideliteTamponCode"),
				@Mapping(source="entity.souscriptionProgrammeFideliteTampon.libelle", target="souscriptionProgrammeFideliteTamponLibelle"),
		@Mapping(source="entity.user.id", target="userId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.souscriptionProgrammeFideliteCarte.id", target="souscriptionProgrammeFideliteCarteId"),
				@Mapping(source="entity.souscriptionProgrammeFideliteCarte.code", target="souscriptionProgrammeFideliteCarteCode"),
				@Mapping(source="entity.souscriptionProgrammeFideliteCarte.libelle", target="souscriptionProgrammeFideliteCarteLibelle"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
				@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
				@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
	})
	NotePdfDto toDto(NotePdf entity) throws ParseException;

    List<NotePdfDto> toDtos(List<NotePdf> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.note", target="note"),
		@Mapping(source="dto.commentaire", target="commentaire"),
		@Mapping(source="dto.retourEnseigne", target="retourEnseigne"),
		@Mapping(source="dto.dateAction", dateFormat="dd/MM/yyyy",target="dateAction"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="souscriptionProgrammeFideliteTampon", target="souscriptionProgrammeFideliteTampon"),
		@Mapping(source="user", target="user"),
		@Mapping(source="souscriptionProgrammeFideliteCarte", target="souscriptionProgrammeFideliteCarte"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="enseigne", target="enseigne"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
	})
    NotePdf toEntity(NotePdfDto dto, SouscriptionProgrammeFideliteTampon souscriptionProgrammeFideliteTampon, User user, SouscriptionProgrammeFideliteCarte souscriptionProgrammeFideliteCarte, ProgrammeFideliteTampon programmeFideliteTampon, Enseigne enseigne, ProgrammeFideliteCarte programmeFideliteCarte) throws ParseException;

    //List<NotePdf> toEntities(List<NotePdfDto> dtos) throws ParseException;

}
