

/*
 * Java transformer for entity table enseigne 
 * Created on 2021-12-16 ( Time 10:39:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "enseigne"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface EnseigneTransformer {

	EnseigneTransformer INSTANCE = Mappers.getMapper(EnseigneTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),

		@Mapping(source="entity.dateExpirationSouscription", dateFormat="dd/MM/yyyy",target="dateExpirationSouscription"),
		@Mapping(source="entity.role.id", target="roleGerantId"),
				@Mapping(source="entity.role.libelle", target="roleLibelle"),
				@Mapping(source="entity.role.code", target="roleCode"),
		@Mapping(source="entity.etatSouscription.id", target="etatSouscriptionId"),
				@Mapping(source="entity.etatSouscription.libelle", target="etatSouscriptionLibelle"),
				@Mapping(source="entity.etatSouscription.code", target="etatSouscriptionCode"),
		@Mapping(source="entity.souscription.id", target="souscriptionId"),
		@Mapping(source="entity.modePaiement.id", target="modePaiementId"),
				@Mapping(source="entity.modePaiement.libelle", target="modePaiementLibelle"),
				@Mapping(source="entity.modePaiement.code", target="modePaiementCode"),
	})
	EnseigneDto toDto(Enseigne entity) throws ParseException;

    List<EnseigneDto> toDtos(List<Enseigne> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.urlLogo", target="urlLogo"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.facebook", target="facebook"),
		@Mapping(source="dto.instagram", target="instagram"),
		@Mapping(source="dto.twitter", target="twitter"),
		@Mapping(source="dto.siteInternet", target="siteInternet"),
		@Mapping(source="dto.couleur", target="couleur"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isAccepted", target="isAccepted"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.nomAdmin", target="nomAdmin"),
		@Mapping(source="dto.prenomsAdmin", target="prenomsAdmin"),
		@Mapping(source="dto.emailAdmin", target="emailAdmin"),
		@Mapping(source="dto.telephoneAdmin", target="telephoneAdmin"),
		@Mapping(source="dto.chiffreDaffaire", target="chiffreDaffaire"),
		@Mapping(source="dto.nbreTransaction", target="nbreTransaction"),
		@Mapping(source="dto.recompenseEngagerCarte", target="recompenseEngagerCarte"),
		@Mapping(source="dto.recompenseEngagerTampon", target="recompenseEngagerTampon"),
		@Mapping(source="dto.recompenseRembouserCarte", target="recompenseRembouserCarte"),
		@Mapping(source="dto.recompenseRembouserTampon", target="recompenseRembouserTampon"),
		@Mapping(source="dto.nbreMaxiUserSouscription", target="nbreMaxiUserSouscription"),
		@Mapping(source="dto.nbreUtilisateurMobile", target="nbreUtilisateurMobile"),
		@Mapping(source="dto.nbreSouscription", target="nbreSouscription"),
		@Mapping(source="dto.prixSpecifique", target="prixSpecifique"),
		@Mapping(source="dto.dateExpirationSouscription", dateFormat="dd/MM/yyyy",target="dateExpirationSouscription"),
		@Mapping(source="dto.isActive", target="isActive"),
		@Mapping(source="role", target="role"),
		@Mapping(source="etatSouscription", target="etatSouscription"),
		@Mapping(source="souscription", target="souscription"),
		@Mapping(source="modePaiement", target="modePaiement"),
	})
    Enseigne toEntity(EnseigneDto dto, Role role, EtatSouscription etatSouscription, Souscription souscription, ModePaiement modePaiement) throws ParseException;

    //List<Enseigne> toEntities(List<EnseigneDto> dtos) throws ParseException;

}
