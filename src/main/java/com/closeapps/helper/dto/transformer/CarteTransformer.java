

/*
 * Java transformer for entity table carte 
 * Created on 2020-09-17 ( Time 11:23:03 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "carte"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface CarteTransformer {

	CarteTransformer INSTANCE = Mappers.getMapper(CarteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.user.id", target="userId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"), 
				@Mapping(source="entity.user.matricule", target="userMatricule"), // AJOUTER
				@Mapping(source="entity.user.telephone", target="userTelephone"), // AJOUTER
				@Mapping(source="entity.user.email", target="userEmail"), // AJOUTER
				
	})
	CarteDto toDto(Carte entity) throws ParseException;

    List<CarteDto> toDtos(List<Carte> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.codeBarre", target="codeBarre"),
		@Mapping(source="dto.qrCode", target="qrCode"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.urlImage", target="urlImage"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="user", target="user"),
	})
    Carte toEntity(CarteDto dto, User user) throws ParseException;

    //List<Carte> toEntities(List<CarteDto> dtos) throws ParseException;

}
