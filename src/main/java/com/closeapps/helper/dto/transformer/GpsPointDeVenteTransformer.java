

/*
 * Java transformer for entity table gps_point_de_vente 
 * Created on 2020-08-17 ( Time 14:14:45 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "gps_point_de_vente"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface GpsPointDeVenteTransformer {

	GpsPointDeVenteTransformer INSTANCE = Mappers.getMapper(GpsPointDeVenteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	GpsPointDeVenteDto toDto(GpsPointDeVente entity) throws ParseException;

    List<GpsPointDeVenteDto> toDtos(List<GpsPointDeVente> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.longititude", target="longititude"),
		@Mapping(source="dto.latitude", target="latitude"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    GpsPointDeVente toEntity(GpsPointDeVenteDto dto) throws ParseException;

    //List<GpsPointDeVente> toEntities(List<GpsPointDeVenteDto> dtos) throws ParseException;

}
