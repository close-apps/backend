

/*
 * Java transformer for entity table user_enseigne_point_de_vente 
 * Created on 2020-11-29 ( Time 21:02:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "user_enseigne_point_de_vente"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface UserEnseignePointDeVenteTransformer {

	UserEnseignePointDeVenteTransformer INSTANCE = Mappers.getMapper(UserEnseignePointDeVenteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.pointDeVente.id", target="pointDeVenteId"),
				@Mapping(source="entity.pointDeVente.nom", target="pointDeVenteNom"),
				@Mapping(source="entity.pointDeVente.code", target="pointDeVenteCode"),
		@Mapping(source="entity.user.id", target="userId"),
				@Mapping(source="entity.user.nom", target="userNom"),
				@Mapping(source="entity.user.prenoms", target="userPrenoms"),
				@Mapping(source="entity.user.login", target="userLogin"),
	})
	UserEnseignePointDeVenteDto toDto(UserEnseignePointDeVente entity) throws ParseException;

    List<UserEnseignePointDeVenteDto> toDtos(List<UserEnseignePointDeVente> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="pointDeVente", target="pointDeVente"),
		@Mapping(source="user", target="user"),
	})
    UserEnseignePointDeVente toEntity(UserEnseignePointDeVenteDto dto, PointDeVente pointDeVente, User user) throws ParseException;

    //List<UserEnseignePointDeVente> toEntities(List<UserEnseignePointDeVenteDto> dtos) throws ParseException;

}
