

/*
 * Java transformer for entity table secteur_activite_enseigne 
 * Created on 2020-08-17 ( Time 14:14:50 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "secteur_activite_enseigne"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface SecteurActiviteEnseigneTransformer {

	SecteurActiviteEnseigneTransformer INSTANCE = Mappers.getMapper(SecteurActiviteEnseigneTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
		@Mapping(source="entity.secteurActivite.id", target="secteurActiviteId"),
				@Mapping(source="entity.secteurActivite.libelle", target="secteurActiviteLibelle"),
				@Mapping(source="entity.secteurActivite.code", target="secteurActiviteCode"),
	})
	SecteurActiviteEnseigneDto toDto(SecteurActiviteEnseigne entity) throws ParseException;

    List<SecteurActiviteEnseigneDto> toDtos(List<SecteurActiviteEnseigne> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="enseigne", target="enseigne"),
		@Mapping(source="secteurActivite", target="secteurActivite"),
	})
    SecteurActiviteEnseigne toEntity(SecteurActiviteEnseigneDto dto, Enseigne enseigne, SecteurActivite secteurActivite) throws ParseException;

    //List<SecteurActiviteEnseigne> toEntities(List<SecteurActiviteEnseigneDto> dtos) throws ParseException;

}
