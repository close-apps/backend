

/*
 * Java transformer for entity table point_de_vente 
 * Created on 2021-06-29 ( Time 01:40:00 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "point_de_vente"
 * 
 * @author Back-End developper
 *
 */
@Mapper(componentModel="spring")
public interface PointDeVenteTransformer {

	PointDeVenteTransformer INSTANCE = Mappers.getMapper(PointDeVenteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.gpsPointDeVente.id", target="gpsPointDeVenteId"),
		@Mapping(source="entity.commune.id", target="communeId"),
		@Mapping(source="entity.commune.libelle", target="communeLibelle"),
		@Mapping(source="entity.commune.code", target="communeCode"),
		@Mapping(source="entity.user.id", target="userId"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenoms", target="userPrenoms"),
		@Mapping(source="entity.user.login", target="userLogin"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
		@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
		@Mapping(source="entity.commune.ville.libelle", target="villeLibelle"), //AJOUTER

	})
	PointDeVenteDto toDto(PointDeVente entity) throws ParseException;

	List<PointDeVenteDto> toDtos(List<PointDeVente> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelleCommune", target="libelleCommune"),
		@Mapping(source="dto.ville", target="ville"),
		@Mapping(source="dto.isAllCarteDipso", target="isAllCarteDipso"),
		@Mapping(source="dto.isAllTamponDipso", target="isAllTamponDipso"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="gpsPointDeVente", target="gpsPointDeVente"),
		@Mapping(source="commune", target="commune"),
		@Mapping(source="user", target="user"),
		@Mapping(source="enseigne", target="enseigne"),
	})
	PointDeVente toEntity(PointDeVenteDto dto, GpsPointDeVente gpsPointDeVente, Commune commune, User user, Enseigne enseigne) throws ParseException;

	//List<PointDeVente> toEntities(List<PointDeVenteDto> dtos) throws ParseException;

}
