

/*
 * Java transformer for entity table souscription_programme_fidelite 
 * Created on 2021-06-23 ( Time 06:52:38 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "souscription_programme_fidelite"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface SouscriptionProgrammeFideliteTransformer {

	SouscriptionProgrammeFideliteTransformer INSTANCE = Mappers.getMapper(SouscriptionProgrammeFideliteTransformer.class);

	@Mappings({

		@Mapping(source="entity.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.carte.id", target="carteId"),
				@Mapping(source="entity.carte.code", target="carteCode"),
		@Mapping(source="entity.programmeFideliteTampon.id", target="programmeFideliteTamponId"),
				@Mapping(source="entity.programmeFideliteTampon.libelle", target="programmeFideliteTamponLibelle"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
		@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
		@Mapping(source="entity.enseigne.urlLogo", target="enseigneUrlLogo"), // ajouter
		@Mapping(source="entity.enseigne.couleur", target="enseigneCouleur"), // ajouter
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
				@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
	})
	SouscriptionProgrammeFideliteDto toDto(SouscriptionProgrammeFidelite entity) throws ParseException;

    List<SouscriptionProgrammeFideliteDto> toDtos(List<SouscriptionProgrammeFidelite> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateSouscription", dateFormat="dd/MM/yyyy",target="dateSouscription"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="carte", target="carte"),
		@Mapping(source="programmeFideliteTampon", target="programmeFideliteTampon"),
		@Mapping(source="enseigne", target="enseigne"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
	})
    SouscriptionProgrammeFidelite toEntity(SouscriptionProgrammeFideliteDto dto, Carte carte, ProgrammeFideliteTampon programmeFideliteTampon, Enseigne enseigne, ProgrammeFideliteCarte programmeFideliteCarte) throws ParseException;

    //List<SouscriptionProgrammeFidelite> toEntities(List<SouscriptionProgrammeFideliteDto> dtos) throws ParseException;

}
