

/*
 * Java transformer for entity table programme_fidelite_tampon 
 * Created on 2020-11-29 ( Time 21:02:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "programme_fidelite_tampon"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface ProgrammeFideliteTamponTransformer {

	ProgrammeFideliteTamponTransformer INSTANCE = Mappers.getMapper(ProgrammeFideliteTamponTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
				@Mapping(source="entity.enseigne.urlLogo", target="enseigneUrlLogo"), // ajouter
				@Mapping(source="entity.enseigne.couleur", target="enseigneCouleur"), // ajouter
				
	})
	ProgrammeFideliteTamponDto toDto(ProgrammeFideliteTampon entity) throws ParseException;

    List<ProgrammeFideliteTamponDto> toDtos(List<ProgrammeFideliteTampon> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.conditionsDutilisation", target="conditionsDutilisation"),
		@Mapping(source="dto.nbreTampon", target="nbreTampon"),
		@Mapping(source="dto.nbrePallier", target="nbrePallier"),
		@Mapping(source="dto.urlImage", target="urlImage"),
		@Mapping(source="dto.gain", target="gain"),
		@Mapping(source="dto.valeurMaxGain", target="valeurMaxGain"),
		@Mapping(source="dto.isValeurMaxGain", target="isValeurMaxGain"),
		@Mapping(source="dto.tamponObtenuApresSouscription", target="tamponObtenuApresSouscription"),
		@Mapping(source="dto.codeBarre", target="codeBarre"),
		@Mapping(source="dto.qrCode", target="qrCode"),
		@Mapping(source="dto.isDispoInAllPdv", target="isDispoInAllPdv"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.isDesactived", target="isDesactived"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="enseigne", target="enseigne"),
	})
    ProgrammeFideliteTampon toEntity(ProgrammeFideliteTamponDto dto, Enseigne enseigne) throws ParseException;

    //List<ProgrammeFideliteTampon> toEntities(List<ProgrammeFideliteTamponDto> dtos) throws ParseException;

}
