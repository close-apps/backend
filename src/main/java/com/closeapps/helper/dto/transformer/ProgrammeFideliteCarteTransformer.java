

/*
 * Java transformer for entity table programme_fidelite_carte 
 * Created on 2020-11-29 ( Time 21:02:52 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "programme_fidelite_carte"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface ProgrammeFideliteCarteTransformer {

	ProgrammeFideliteCarteTransformer INSTANCE = Mappers.getMapper(ProgrammeFideliteCarteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.typeProgrammeFideliteCarte.id", target="typeProgrammeFideliteCarteId"),
				@Mapping(source="entity.typeProgrammeFideliteCarte.libelle", target="typeProgrammeFideliteCarteLibelle"),
				@Mapping(source="entity.typeProgrammeFideliteCarte.code", target="typeProgrammeFideliteCarteCode"),
		@Mapping(source="entity.enseigne.id", target="enseigneId"),
				@Mapping(source="entity.enseigne.nom", target="enseigneNom"),
				@Mapping(source="entity.enseigne.urlLogo", target="enseigneUrlLogo"), // ajouter
				@Mapping(source="entity.enseigne.couleur", target="enseigneCouleur"), // ajouter
				
				
	})
	ProgrammeFideliteCarteDto toDto(ProgrammeFideliteCarte entity) throws ParseException;

    List<ProgrammeFideliteCarteDto> toDtos(List<ProgrammeFideliteCarte> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.conditionsDutilisation", target="conditionsDutilisation"),
		@Mapping(source="dto.pourXAcheter", target="pourXAcheter"),
		@Mapping(source="dto.nbrePointObtenu", target="nbrePointObtenu"),
		@Mapping(source="dto.correspondanceNbrePoint", target="correspondanceNbrePoint"),
		@Mapping(source="dto.isDispoInAllPdv", target="isDispoInAllPdv"),
		@Mapping(source="dto.isLocked", target="isLocked"),
		@Mapping(source="dto.pointObtenuParParrain", target="pointObtenuParParrain"),
		@Mapping(source="dto.pointObtenuParFilleul", target="pointObtenuParFilleul"),
		@Mapping(source="dto.pointObtenuApresSouscription", target="pointObtenuApresSouscription"),
		@Mapping(source="dto.nbreDeSouscrit", target="nbreDeSouscrit"),
		@Mapping(source="dto.isDesactived", target="isDesactived"),
		@Mapping(source="dto.urlCarte", target="urlCarte"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="typeProgrammeFideliteCarte", target="typeProgrammeFideliteCarte"),
		@Mapping(source="enseigne", target="enseigne"),
	})
    ProgrammeFideliteCarte toEntity(ProgrammeFideliteCarteDto dto, TypeProgrammeFideliteCarte typeProgrammeFideliteCarte, Enseigne enseigne) throws ParseException;

    //List<ProgrammeFideliteCarte> toEntities(List<ProgrammeFideliteCarteDto> dtos) throws ParseException;

}
