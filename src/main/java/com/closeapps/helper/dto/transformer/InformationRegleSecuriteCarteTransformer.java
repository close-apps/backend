

/*
 * Java transformer for entity table information_regle_securite_carte 
 * Created on 2021-08-14 ( Time 17:43:18 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;	
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.closeapps.helper.dto.*;
import com.closeapps.dao.entity.*;


/**
TRANSFORMER for table "information_regle_securite_carte"
 * 
* @author Back-End developper
   *
 */
@Mapper(componentModel="spring")
public interface InformationRegleSecuriteCarteTransformer {

	InformationRegleSecuriteCarteTransformer INSTANCE = Mappers.getMapper(InformationRegleSecuriteCarteTransformer.class);

	@Mappings({

		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),

		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),

		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="entity.programmeFideliteCarte.id", target="programmeFideliteCarteId"),
				@Mapping(source="entity.programmeFideliteCarte.libelle", target="programmeFideliteCarteLibelle"),
		@Mapping(source="entity.uniteTemps.id", target="uniteTempsId"),
				@Mapping(source="entity.uniteTemps.libelle", target="uniteTempsLibelle"),
				@Mapping(source="entity.uniteTemps.code", target="uniteTempsCode"),
	})
	InformationRegleSecuriteCarteDto toDto(InformationRegleSecuriteCarte entity) throws ParseException;

    List<InformationRegleSecuriteCarteDto> toDtos(List<InformationRegleSecuriteCarte> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.plafondSommeACrediter", target="plafondSommeACrediter"),
		@Mapping(source="dto.nbreFoisCrediterParPdv", target="nbreFoisCrediterParPdv"),
		@Mapping(source="dto.nbreFoisCrediterAllPdv", target="nbreFoisCrediterAllPdv"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="programmeFideliteCarte", target="programmeFideliteCarte"),
		@Mapping(source="uniteTemps", target="uniteTemps"),
	})
    InformationRegleSecuriteCarte toEntity(InformationRegleSecuriteCarteDto dto, ProgrammeFideliteCarte programmeFideliteCarte, UniteTemps uniteTemps) throws ParseException;

    //List<InformationRegleSecuriteCarte> toEntities(List<InformationRegleSecuriteCarteDto> dtos) throws ParseException;

}
