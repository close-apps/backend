
/*
 * Java dto for entity table user
 * Created on 2020-12-30 ( Time 09:35:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._UserDto;

import lombok.*;
/**
 * DTO for table "user"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class UserDto extends _UserDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     prenoms              ;
	/*
	 * 
	 */
    private String     email                ;
	/*
	 * 
	 */
    private String     telephone            ;
	/*
	 * 
	 */
    private String     login                ;
	/*
	 * 
	 */
    private String     password             ;
	/*
	 * 
	 */
    private String     matricule            ;
	/*
	 * 
	 */
    private String     token                ;
	/*
	 * 
	 */
	private String     dateExpirationToken  ;
	/*
	 * 
	 */
    private Boolean    isTokenValide        ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * Pour savoir si c'est la premiere connexion du user
	 */
    private Boolean    isFirstlyConnexion   ;
	/*
	 * Pour indiquer si ce user est l'admin de l'enseigne
	 */
    private Boolean    isAdminEnseigne      ;
	/*
	 * Pour voir si le user a fait sa creation de compte par Firebase
	 */
    private Boolean    byFirebaseUser       ;
	/*
	 * 
	 */
    private String     emailGoogle          ;
	/*
	 * 
	 */
    private String     telephoneGoogle      ;
	/*
	 * 
	 */
    private String     emailFacebook        ;
	/*
	 * 
	 */
    private String     telephoneFacebook    ;
	/*
	 * 
	 */
    private String     passwordGenerate     ;
	/*
	 * 
	 */
    private Integer    roleId               ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String enseigneNom;
	private String roleLibelle;
	private String roleCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   prenomsParam          ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<String>   matriculeParam        ;                     
	private SearchParam<String>   tokenParam            ;                     

		private SearchParam<String>   dateExpirationTokenParam;                     
	private SearchParam<Boolean>  isTokenValideParam    ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isFirstlyConnexionParam;                     
	private SearchParam<Boolean>  isAdminEnseigneParam  ;                     
	private SearchParam<Boolean>  byFirebaseUserParam   ;                     
	private SearchParam<String>   emailGoogleParam      ;                     
	private SearchParam<String>   telephoneGoogleParam  ;                     
	private SearchParam<String>   emailFacebookParam    ;                     
	private SearchParam<String>   telephoneFacebookParam;                     
	private SearchParam<String>   passwordGenerateParam ;                     
	private SearchParam<Integer>  roleIdParam           ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   enseigneNomParam      ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
	private SearchParam<String>   roleCodeParam         ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		UserDto other = (UserDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
