
/*
 * Java dto for entity table historique_carte_programme_fidelite_carte
 * Created on 2021-08-26 ( Time 17:47:23 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._HistoriqueCarteProgrammeFideliteCarteDto;

import lombok.*;
/**
 * DTO for table "historique_carte_programme_fidelite_carte"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class HistoriqueCarteProgrammeFideliteCarteDto extends _HistoriqueCarteProgrammeFideliteCarteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    carteId              ;
	/*
	 * 
	 */
    private Integer    programmeFideliteCarteId ;
	/*
	 * 
	 */
    private Integer    typeActionId         ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    pointDeVenteId       ;
	/*
	 * 
	 */
    private Integer    nbrePointOperation   ;
	/*
	 * 
	 */
    private Integer    nbrePointActuel      ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
	private String     dateAction           ;
	/*
	 * 
	 */
	private String     heureAction          ;
	/*
	 * 
	 */
    private Integer    nbrePointAvantAction ;
	/*
	 * 
	 */
    private Integer    sommeOperation       ;
	/*
	 * 
	 */
    private Integer    chiffreDaffaire      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String programmeFideliteCarteLibelle;
	private String typeActionLibelle;
	private String typeActionCode;
	private String carteCode;
	private String pointDeVenteNom;
	private String pointDeVenteCode;


	// Search param
	private SearchParam<Integer>  carteIdParam          ;                     
	private SearchParam<Integer>  programmeFideliteCarteIdParam;                     
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  typeActionIdParam     ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  pointDeVenteIdParam   ;                     
	private SearchParam<Integer>  nbrePointOperationParam;                     
	private SearchParam<Integer>  nbrePointActuelParam  ;                     
	private SearchParam<String>   descriptionParam      ;                     

		private SearchParam<String>   dateActionParam       ;                     

		private SearchParam<String>   heureActionParam      ;                     
	private SearchParam<Integer>  nbrePointAvantActionParam;                     
	private SearchParam<Integer>  sommeOperationParam   ;                     
	private SearchParam<Integer>  chiffreDaffaireParam  ;                     
	private SearchParam<String>   programmeFideliteCarteLibelleParam;                     
	private SearchParam<String>   typeActionLibelleParam;                     
	private SearchParam<String>   typeActionCodeParam   ;                     
	private SearchParam<String>   carteCodeParam        ;                     
	private SearchParam<String>   pointDeVenteNomParam  ;                     
	private SearchParam<String>   pointDeVenteCodeParam ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		HistoriqueCarteProgrammeFideliteCarteDto other = (HistoriqueCarteProgrammeFideliteCarteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
