
/*
 * Java dto for entity table note_pdf
 * Created on 2021-08-26 ( Time 17:44:07 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._NotePdfDto;

import lombok.*;
/**
 * DTO for table "note_pdf"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class NotePdfDto extends _NotePdfDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    note                 ;
	/*
	 * 
	 */
    private String     commentaire          ;
	/*
	 * 
	 */
    private String     retourEnseigne       ;
	/*
	 * 
	 */
	private String     dateAction           ;
	/*
	 * 
	 */
    private Integer    userId               ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;
	/*
	 * 
	 */
    private Integer    programmeFideliteCarteId ;
	/*
	 * 
	 */
    private Integer    programmeFideliteTamponId ;
	/*
	 * 
	 */
    private Integer    souscriptionProgrammeFideliteCarteId ;
	/*
	 * 
	 */
    private Integer    souscriptionProgrammeFideliteTamponId ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String souscriptionProgrammeFideliteTamponCode;
	private String souscriptionProgrammeFideliteTamponLibelle;
	private String userNom;
	private String userPrenoms;
	private String userLogin;
	private String souscriptionProgrammeFideliteCarteCode;
	private String souscriptionProgrammeFideliteCarteLibelle;
	private String programmeFideliteTamponLibelle;
	private String enseigneNom;
	private String programmeFideliteCarteLibelle;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  noteParam             ;                     
	private SearchParam<String>   commentaireParam      ;                     
	private SearchParam<String>   retourEnseigneParam   ;                     

		private SearchParam<String>   dateActionParam       ;                     
	private SearchParam<Integer>  userIdParam           ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<Integer>  programmeFideliteCarteIdParam;                     
	private SearchParam<Integer>  programmeFideliteTamponIdParam;                     
	private SearchParam<Integer>  souscriptionProgrammeFideliteCarteIdParam;                     
	private SearchParam<Integer>  souscriptionProgrammeFideliteTamponIdParam;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   souscriptionProgrammeFideliteTamponCodeParam;                     
	private SearchParam<String>   souscriptionProgrammeFideliteTamponLibelleParam;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomsParam      ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<String>   souscriptionProgrammeFideliteCarteCodeParam;                     
	private SearchParam<String>   souscriptionProgrammeFideliteCarteLibelleParam;                     
	private SearchParam<String>   programmeFideliteTamponLibelleParam;                     
	private SearchParam<String>   enseigneNomParam      ;                     
	private SearchParam<String>   programmeFideliteCarteLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		NotePdfDto other = (NotePdfDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
