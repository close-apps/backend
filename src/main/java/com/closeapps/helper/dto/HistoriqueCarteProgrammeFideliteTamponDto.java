
/*
 * Java dto for entity table historique_carte_programme_fidelite_tampon
 * Created on 2021-09-11 ( Time 11:16:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._HistoriqueCarteProgrammeFideliteTamponDto;

import lombok.*;
/**
 * DTO for table "historique_carte_programme_fidelite_tampon"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class HistoriqueCarteProgrammeFideliteTamponDto extends _HistoriqueCarteProgrammeFideliteTamponDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    carteId              ;
	/*
	 * 
	 */
    private Integer    programmeFideliteTamponId ;
	/*
	 * 
	 */
    private Integer    typeActionId         ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    pointDeVenteId       ;
	/*
	 * 
	 */
    private Integer    souscriptionProgrammeFideliteTamponId ;
	/*
	 * 
	 */
    private Integer    nbreTamponObtenu     ;
	/*
	 * 
	 */
    private Integer    nbreTamponActuel     ;
	/*
	 * 
	 */
	private String     dateAction           ;
	/*
	 * 
	 */
	private String     heureAction          ;
	/*
	 * 
	 */
    private Integer    nbreTamponAvantAction ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private Integer    chiffreDaffaire      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String carteCode;
	private String pointDeVenteNom;
	private String pointDeVenteCode;
	private String programmeFideliteTamponLibelle;
	private String typeActionLibelle;
	private String typeActionCode;
	private String souscriptionProgrammeFideliteTamponCode;
	private String souscriptionProgrammeFideliteTamponLibelle;


	// Search param
	private SearchParam<Integer>  carteIdParam          ;                     
	private SearchParam<Integer>  programmeFideliteTamponIdParam;                     
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  typeActionIdParam     ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  pointDeVenteIdParam   ;                     
	private SearchParam<Integer>  souscriptionProgrammeFideliteTamponIdParam;                     
	private SearchParam<Integer>  nbreTamponObtenuParam ;                     
	private SearchParam<Integer>  nbreTamponActuelParam ;                     

		private SearchParam<String>   dateActionParam       ;                     

		private SearchParam<String>   heureActionParam      ;                     
	private SearchParam<Integer>  nbreTamponAvantActionParam;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<Integer>  chiffreDaffaireParam  ;                     
	private SearchParam<String>   carteCodeParam        ;                     
	private SearchParam<String>   pointDeVenteNomParam  ;                     
	private SearchParam<String>   pointDeVenteCodeParam ;                     
	private SearchParam<String>   programmeFideliteTamponLibelleParam;                     
	private SearchParam<String>   typeActionLibelleParam;                     
	private SearchParam<String>   typeActionCodeParam   ;                     
	private SearchParam<String>   souscriptionProgrammeFideliteTamponCodeParam;                     
	private SearchParam<String>   souscriptionProgrammeFideliteTamponLibelleParam;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		HistoriqueCarteProgrammeFideliteTamponDto other = (HistoriqueCarteProgrammeFideliteTamponDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
