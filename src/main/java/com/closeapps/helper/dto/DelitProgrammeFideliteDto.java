
/*
 * Java dto for entity table delit_programme_fidelite
 * Created on 2021-08-17 ( Time 23:11:14 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._DelitProgrammeFideliteDto;

import lombok.*;
/**
 * DTO for table "delit_programme_fidelite"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class DelitProgrammeFideliteDto extends _DelitProgrammeFideliteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
	private String     dateDelit            ;
	/*
	 * 
	 */
	private String     heureDelit           ;
	/*
	 * 
	 */
    private String     numeroDelit          ;
	/*
	 * 
	 */
    private Boolean    isNotifed            ;
	/*
	 * 
	 */
    private String     messageNotifed       ;
	/*
	 * 
	 */
    private String     messageLocked        ;
	/*
	 * 
	 */
    private Boolean    isApplicateSanction  ;
	/*
	 * 
	 */
    private Integer    carteId              ;
	/*
	 * 
	 */
    private Integer    informationRegleSecuriteCarteId ;
	/*
	 * 
	 */
    private Integer    informationRegleSecuriteTamponId ;
	/*
	 * 
	 */
    private Integer    pointDeVenteId       ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String pointDeVenteNom;
	private String pointDeVenteCode;
	private String carteCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     

		private SearchParam<String>   dateDelitParam        ;                     

		private SearchParam<String>   heureDelitParam       ;                     
	private SearchParam<String>   numeroDelitParam      ;                     
	private SearchParam<Boolean>  isNotifedParam        ;                     
	private SearchParam<String>   messageNotifedParam   ;                     
	private SearchParam<String>   messageLockedParam    ;                     
	private SearchParam<Boolean>  isApplicateSanctionParam;                     
	private SearchParam<Integer>  carteIdParam          ;                     
	private SearchParam<Integer>  informationRegleSecuriteCarteIdParam;                     
	private SearchParam<Integer>  informationRegleSecuriteTamponIdParam;                     
	private SearchParam<Integer>  pointDeVenteIdParam   ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   pointDeVenteNomParam  ;                     
	private SearchParam<String>   pointDeVenteCodeParam ;                     
	private SearchParam<String>   carteCodeParam        ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		DelitProgrammeFideliteDto other = (DelitProgrammeFideliteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
