
/*
 * Java dto for entity table enseigne
 * Created on 2021-12-16 ( Time 10:39:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._EnseigneDto;

import lombok.*;
/**
 * DTO for table "enseigne"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class EnseigneDto extends _EnseigneDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     nom                  ;
	/*
	 * 
	 */
    private String     telephone            ;
	/*
	 * 
	 */
    private String     email                ;
	/*
	 * 
	 */
    private String     urlLogo              ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private String     facebook             ;
	/*
	 * 
	 */
    private String     instagram            ;
	/*
	 * 
	 */
    private String     twitter              ;
	/*
	 * 
	 */
    private String     siteInternet         ;
	/*
	 * 
	 */
    private String     couleur              ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * Pour verifier si la demande a ete accepter
	 */
    private Boolean    isAccepted           ;
	/*
	 * pour connaitre le role du gérant pour des raisons de statistique
	 */
    private Integer    roleGerantId         ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private String     nomAdmin             ;
	/*
	 * 
	 */
    private String     prenomsAdmin         ;
	/*
	 * 
	 */
    private String     emailAdmin           ;
	/*
	 * 
	 */
    private String     telephoneAdmin       ;
	/*
	 * 
	 */
    private Long       chiffreDaffaire      ;
	/*
	 * 
	 */
    private Long       nbreTransaction      ;
	/*
	 * 
	 */
    private Long       recompenseEngagerCarte ;
	/*
	 * 
	 */
    private Long       recompenseEngagerTampon ;
	/*
	 * 
	 */
    private Long       recompenseRembouserCarte ;
	/*
	 * 
	 */
    private Long       recompenseRembouserTampon ;
	/*
	 * 
	 */
    private Integer    nbreMaxiUserSouscription ;
	/*
	 * 
	 */
    private Integer    nbreUtilisateurMobile ;
	/*
	 * 
	 */
    private Integer    nbreSouscription     ;
	/*
	 * 
	 */
    private Integer    etatSouscriptionId   ;
	/*
	 * 
	 */
    private Integer    modePaiementId       ;
	/*
	 * 
	 */
    private Integer    souscriptionId       ;
	/*
	 * 
	 */
    private Integer    prixSpecifique       ;
	/*
	 * 
	 */
	private String     dateExpirationSouscription ;
	/*
	 * 
	 */
    private Boolean    isActive             ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String roleLibelle;
	private String roleCode;
	private String etatSouscriptionLibelle;
	private String etatSouscriptionCode;
	private String modePaiementLibelle;
	private String modePaiementCode;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<String>   urlLogoParam          ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   facebookParam         ;                     
	private SearchParam<String>   instagramParam        ;                     
	private SearchParam<String>   twitterParam          ;                     
	private SearchParam<String>   siteInternetParam     ;                     
	private SearchParam<String>   couleurParam          ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Boolean>  isAcceptedParam       ;                     
	private SearchParam<Integer>  roleGerantIdParam     ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   nomAdminParam         ;                     
	private SearchParam<String>   prenomsAdminParam     ;                     
	private SearchParam<String>   emailAdminParam       ;                     
	private SearchParam<String>   telephoneAdminParam   ;                     
	private SearchParam<Long>     chiffreDaffaireParam  ;                     
	private SearchParam<Long>     nbreTransactionParam  ;                     
	private SearchParam<Long>     recompenseEngagerCarteParam;                     
	private SearchParam<Long>     recompenseEngagerTamponParam;                     
	private SearchParam<Long>     recompenseRembouserCarteParam;                     
	private SearchParam<Long>     recompenseRembouserTamponParam;                     
	private SearchParam<Integer>  nbreMaxiUserSouscriptionParam;                     
	private SearchParam<Integer>  nbreUtilisateurMobileParam;                     
	private SearchParam<Integer>  nbreSouscriptionParam ;                     
	private SearchParam<Integer>  etatSouscriptionIdParam;                     
	private SearchParam<Integer>  modePaiementIdParam   ;                     
	private SearchParam<Integer>  souscriptionIdParam   ;                     
	private SearchParam<Integer>  prixSpecifiqueParam   ;                     

		private SearchParam<String>   dateExpirationSouscriptionParam;                     
	private SearchParam<Boolean>  isActiveParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   etatSouscriptionLibelleParam;                     
	private SearchParam<String>   etatSouscriptionCodeParam;                     
	private SearchParam<String>   modePaiementLibelleParam;                     
	private SearchParam<String>   modePaiementCodeParam ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		EnseigneDto other = (EnseigneDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
