
/*
 * Java dto for entity table souscription_programme_fidelite_tampon
 * Created on 2021-08-18 ( Time 16:55:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._SouscriptionProgrammeFideliteTamponDto;

import lombok.*;
/**
 * DTO for table "souscription_programme_fidelite_tampon"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class SouscriptionProgrammeFideliteTamponDto extends _SouscriptionProgrammeFideliteTamponDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private Integer    carteId              ;
	/*
	 * 
	 */
    private Integer    programmeFideliteTamponId ;
	/*
	 * pour distinguer les users chez le client
	 */
    private String     code                 ;
	/*
	 * 
	 */
    private String     libelle              ;
	/*
	 * 
	 */
    private Boolean    isShowLastNotification ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private String     raisonLocked         ;
	/*
	 * 
	 */
    private Boolean    unsubscribe          ;
	/*
	 * 
	 */
    private String     raisonUnsubscribe    ;
	/*
	 * 
	 */
    private Boolean    isNotifed            ;
	/*
	 * 
	 */
    private String     messageNotifed       ;
	/*
	 * 
	 */
    private Integer    nbreTamponActuelle   ;
	/*
	 * 
	 */
    private Integer    nbreTamponPrecedent  ;
	/*
	 * 
	 */
	private String     dateSouscription     ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String programmeFideliteTamponLibelle;
	private String carteCode;


	// Search param
	private SearchParam<Integer>  carteIdParam          ;                     
	private SearchParam<Integer>  programmeFideliteTamponIdParam;                     
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   codeParam             ;                     
	private SearchParam<String>   libelleParam          ;                     
	private SearchParam<Boolean>  isShowLastNotificationParam;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<String>   raisonLockedParam     ;                     
	private SearchParam<Boolean>  unsubscribeParam      ;                     
	private SearchParam<String>   raisonUnsubscribeParam;                     
	private SearchParam<Boolean>  isNotifedParam        ;                     
	private SearchParam<String>   messageNotifedParam   ;                     
	private SearchParam<Integer>  nbreTamponActuelleParam;                     
	private SearchParam<Integer>  nbreTamponPrecedentParam;                     

		private SearchParam<String>   dateSouscriptionParam ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   programmeFideliteTamponLibelleParam;                     
	private SearchParam<String>   carteCodeParam        ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		SouscriptionProgrammeFideliteTamponDto other = (SouscriptionProgrammeFideliteTamponDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
