
/*
 * Java dto for entity table programme_fidelite_carte
 * Created on 2020-11-29 ( Time 21:02:51 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;
import com.closeapps.helper.dto.customize._ProgrammeFideliteCarteDto;

import lombok.*;
/**
 * DTO for table "programme_fidelite_carte"
 *
* @author Back-End developper
   */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class ProgrammeFideliteCarteDto extends _ProgrammeFideliteCarteDto implements Cloneable{

	/*
	 * 
	 */
    private Integer    id                   ; // Primary Key

	/*
	 * 
	 */
    private String     libelle              ;
	/*
	 * 
	 */
    private String     description          ;
	/*
	 * 
	 */
    private String     conditionsDutilisation ;
	/*
	 * 
	 */
    private Integer    pourXAcheter         ;
	/*
	 * 
	 */
    private Integer    nbrePointObtenu      ;
	/*
	 * 
	 */
    private Integer    correspondanceNbrePoint ;
	/*
	 * 
	 */
    private Boolean    isDispoInAllPdv      ;
	/*
	 * 
	 */
    private Boolean    isLocked             ;
	/*
	 * 
	 */
    private Integer    pointObtenuParParrain ;
	/*
	 * 
	 */
    private Integer    pointObtenuParFilleul ;
	/*
	 * 
	 */
    private Integer    pointObtenuApresSouscription ;
	/*
	 * 
	 */
    private Integer    nbreDeSouscrit       ;
	/*
	 * 
	 */
    private Boolean    isDesactived         ;
	/*
	 * 
	 */
    private String     urlCarte             ;
	/*
	 * 
	 */
    private Integer    createdBy            ;
	/*
	 * 
	 */
	private String     createdAt            ;
	/*
	 * 
	 */
    private Integer    updatedBy            ;
	/*
	 * 
	 */
	private String     updatedAt            ;
	/*
	 * 
	 */
    private Integer    deletedBy            ;
	/*
	 * 
	 */
	private String     deletedAt            ;
	/*
	 * 
	 */
    private Boolean    isDeleted            ;
	/*
	 * 
	 */
    private Integer    enseigneId           ;
	/*
	 * 
	 */
    private Integer    typeProgrammeFideliteCarteId ;



    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String typeProgrammeFideliteCarteLibelle;
	private String typeProgrammeFideliteCarteCode;
	private String enseigneNom;


	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   libelleParam          ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   conditionsDutilisationParam;                     
	private SearchParam<Integer>  pourXAcheterParam     ;                     
	private SearchParam<Integer>  nbrePointObtenuParam  ;                     
	private SearchParam<Integer>  correspondanceNbrePointParam;                     
	private SearchParam<Boolean>  isDispoInAllPdvParam  ;                     
	private SearchParam<Boolean>  isLockedParam         ;                     
	private SearchParam<Integer>  pointObtenuParParrainParam;                     
	private SearchParam<Integer>  pointObtenuParFilleulParam;                     
	private SearchParam<Integer>  pointObtenuApresSouscriptionParam;                     
	private SearchParam<Integer>  nbreDeSouscritParam   ;                     
	private SearchParam<Boolean>  isDesactivedParam     ;                     
	private SearchParam<String>   urlCarteParam         ;                     
	private SearchParam<Integer>  createdByParam        ;                     

		private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     

		private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     

		private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  enseigneIdParam       ;                     
	private SearchParam<Integer>  typeProgrammeFideliteCarteIdParam;                     
	private SearchParam<String>   typeProgrammeFideliteCarteLibelleParam;                     
	private SearchParam<String>   typeProgrammeFideliteCarteCodeParam;                     
	private SearchParam<String>   enseigneNomParam      ;                     





	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ProgrammeFideliteCarteDto other = (ProgrammeFideliteCarteDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

 
}
