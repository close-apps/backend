
/*
 * Java dto for entity table user 
 * Created on 2020-08-17 ( Time 14:14:53 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;

import com.closeapps.dao.entity.Souscription;
import com.closeapps.helper.dto.CarteDto;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.PointDeVenteDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "user"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _UserDto {
	
	protected  String enseigneDateExpiration ;
	protected  String etatSouscriptionEnseigne ;

	
	protected Boolean enseigneIsActive;
	
	protected  Souscription enseigneSouscription ;

	protected  String newPassword ;
	protected Integer carteId ;
	protected CarteDto dataCarte ;
	//protected Boolean byGoogleUser ;
	protected Boolean isUserMobile ;
	protected Boolean isUserPDV ;
	

	
	protected Integer firstPointDeVenteId ;
	protected EnseigneDto dataEnseigne ;
	
	protected List<PointDeVenteDto> datasPointDeVente ;
	
	
	// for Stats
	protected String  dateDebut ;
	protected String  dateFin ;
	protected Integer  souscriptionId ;

	protected Integer nbreTotalEnseigne ;
	protected Double gainTotal ;
	protected Integer nbreAllUtilisateur ;
	

	// bande
	protected List<Double> nbreEnseigne ;
	protected List<Double> nbreNewEnseigne ;
	protected List<Double> nbreContratArreter ;
	protected List<Double> nbreEnseigneParContrat ;
	protected List<Double> gain ;
	protected List<String> abscisseGraphe ;
	
	protected List<EnseigneDto> itemsEnseigne ;
	

}
