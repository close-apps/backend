
/*
 * Java dto for entity table carte 
 * Created on 2020-08-17 ( Time 14:14:43 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import com.closeapps.dao.entity.ProgrammeFideliteCarte;
import com.closeapps.dao.entity.ProgrammeFideliteTampon;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * DTO customize for table "carte"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _RetourGroupByDto {
	
	private ProgrammeFideliteCarte programmeFideliteCarte ;
	//private ProgrammeFideliteTampon programmeFideliteTampon ;
	private Long chiffreDaffaire ;
	

}
