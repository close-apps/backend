
/*
 * Java dto for entity table information_regle_securite_tampon 
 * Created on 2020-08-17 ( Time 14:14:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import com.closeapps.dao.entity.ProgrammeFideliteTampon;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "information_regle_securite_tampon"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _InformationRegleSecuriteTamponDto {
	
	protected ProgrammeFideliteTampon entityProgrammeFideliteTampon ;

}
