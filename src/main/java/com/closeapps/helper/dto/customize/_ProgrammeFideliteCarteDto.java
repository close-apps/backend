
/*
 * Java dto for entity table programme_fidelite_carte 
 * Created on 2020-08-17 ( Time 14:50:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;

import com.closeapps.helper.contract.SearchParam;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.InformationRegleSecuriteCarteDto;
import com.closeapps.helper.dto.PointDeVenteDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "programme_fidelite_carte"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _ProgrammeFideliteCarteDto {
	
	protected String enseigneUrlLogo ;
	protected String enseigneCouleur ;
	protected Integer    nbrePDV ;
	protected Integer    nbreUtilisateur ;
	
	protected String    urlLogo ;
	protected String    nomFichier ;
	protected String    fichierBase64 ;

	protected Boolean    isActiveEnseigne ;
	protected SearchParam<Boolean>    isActiveEnseigneParam ;


	protected EnseigneDto dataEnseigne ;
	protected List<PointDeVenteDto>  datasPointDeVente ;
	protected List<InformationRegleSecuriteCarteDto> datasInformationRegleSecuriteCarte ;

}
