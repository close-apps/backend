
/*
 * Java dto for entity table point_de_vente 
 * Created on 2020-09-13 ( Time 11:28:11 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;

import com.closeapps.helper.contract.SearchParam;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.GpsPointDeVenteDto;
import com.closeapps.helper.dto.ProgrammeFideliteCarteDto;
import com.closeapps.helper.dto.ProgrammeFideliteTamponDto;
import com.closeapps.helper.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "point_de_vente"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _PointDeVenteDto {
	
	
	protected Integer userEnseigneId ;
	protected Integer oldPointDeVenteId ;
	protected Integer newPointDeVenteId ;
	
	protected Integer nbreProgrammeFideliteTampon ;
	protected Integer nbreProgrammeFideliteCarte ;
	protected Integer nbreUserEnseigne ;
	
	protected String villeLibelle ;
	
	//protected Boolean    isDispoTamponAllPdv            ;
	//protected Boolean    isDispoCarteAllPdv            ;

	protected Boolean    isActiveEnseigne ;
	protected SearchParam<Boolean>    isActiveEnseigneParam ;

	
	protected GpsPointDeVenteDto dataGpsPointDeVente ;
	protected UserDto dataUserEnseigne ;
	protected List<UserDto> datasUserEnseigne ;
	protected EnseigneDto dataEnseigne ;
	protected List<ProgrammeFideliteTamponDto> datasProgrammeFideliteTampon  ;
	protected List<ProgrammeFideliteCarteDto> datasProgrammeFideliteCarte  ;
	
	

}
