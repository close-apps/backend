
/*
 * Java dto for entity table carte 
 * Created on 2020-08-17 ( Time 14:14:43 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;

import com.closeapps.dao.entity.User;
import com.closeapps.helper.dto.ProgrammeFideliteCarteDto;
import com.closeapps.helper.dto.ProgrammeFideliteTamponDto;
import com.closeapps.helper.dto.SouscriptionProgrammeFideliteCarteDto;
import com.closeapps.helper.dto.SouscriptionProgrammeFideliteTamponDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "carte"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _CarteDto {
	
	protected User entityUser ;
	protected String contenuScanner ;
	protected String description ;
	protected Boolean isScanManuel ;
	
	protected String userMatricule ;
	protected String userTelephone ;
	protected String userEmail ;
	
	protected String typeActionCode ;
	protected Integer somme ;
	protected Long chiffreDaffaire ;
	
	protected Integer programmeFideliteCarteId  ;
	protected Integer pointDeVenteId  ;
	protected Integer programmeFideliteTamponId  ;
	
	
	
	
	
	protected List<ProgrammeFideliteCarteDto>  datasProgrammeFideliteCarte ;
	protected List<ProgrammeFideliteTamponDto>  datasProgrammeFideliteTampon ;
	
	protected List<SouscriptionProgrammeFideliteCarteDto>  datasSouscriptionProgrammeFideliteCarte ;
	protected List<SouscriptionProgrammeFideliteTamponDto>  datasSouscriptionProgrammeFideliteTampon ;
	

}
