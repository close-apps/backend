
/*
 * Java dto for entity table historique_carte_programme_fidelite_tampon 
 * Created on 2020-08-17 ( Time 14:14:46 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "historique_carte_programme_fidelite_tampon"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _HistoriqueCarteProgrammeFideliteTamponDto {
	
	protected Integer     userId          ;
	protected SearchParam<Integer>   userIdParam      ;                     

	protected String     currentDateAction          ;
	protected SearchParam<String>   currentDateActionParam      ;                     

}
