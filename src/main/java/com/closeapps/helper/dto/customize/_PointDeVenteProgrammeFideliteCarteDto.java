
/*
 * Java dto for entity table point_de_vente_programme_fidelite_carte 
 * Created on 2020-08-17 ( Time 14:14:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "point_de_vente_programme_fidelite_carte"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _PointDeVenteProgrammeFideliteCarteDto {
	protected String villePointDeVente ;
	protected String communePointDeVente ;
	protected Double longitude ;
	protected Double latitude ;
	protected Integer userPDVId ;
	private SearchParam<Integer>  userPDVIdParam        ;                     

	protected Boolean    isActiveEnseigne ;
	protected SearchParam<Boolean>    isActiveEnseigneParam ;


}
