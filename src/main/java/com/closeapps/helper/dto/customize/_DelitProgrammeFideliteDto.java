
/*
 * Java dto for entity table delit_programme_fidelite 
 * Created on 2021-08-16 ( Time 17:55:17 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "delit_programme_fidelite"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _DelitProgrammeFideliteDto {


    protected Integer    programmeFideliteCarteId           ;
    protected SearchParam<Integer>    programmeFideliteCarteIdParam           ;
    protected Integer    programmeFideliteTamponId             ;
    protected SearchParam<Integer>    programmeFideliteTamponIdParam             ;
   
    protected Integer    userId              ;
    protected SearchParam<Integer>    userIdParam              ;
    
    protected Boolean isBlocageUserInPDFAndPDV ;
    protected String    userNom             ;
    protected String    userPrenoms             ;
    
    protected String    userEmail             ;
    protected String    userLogin             ;
    protected String    userTelephone             ;
    protected String    programmeFideliteCarteLibelle            ;
    protected String    programmeFideliteTamponLibelle             ;
    protected String    uniteTempsTamponLibelle             ;
    protected String    uniteTempsCarteLibelle             ;
    
    
    
    


}
