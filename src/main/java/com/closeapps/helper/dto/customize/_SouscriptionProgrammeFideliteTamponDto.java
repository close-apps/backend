
/*
 * Java dto for entity table souscription_programme_fidelite_tampon 
 * Created on 2020-09-17 ( Time 10:11:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import com.closeapps.helper.contract.SearchParam;
import com.closeapps.helper.dto.CarteDto;
import com.closeapps.helper.dto.ProgrammeFideliteTamponDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "souscription_programme_fidelite_tampon"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _SouscriptionProgrammeFideliteTamponDto {
	
	protected String enseigneUrlLogo ;
	protected String enseigneCouleur ;
	
	protected Integer userId ;
	private SearchParam<Integer>  userIdParam          ;   
	protected String urlLogo ;
                 

	protected CarteDto dataCarte ;
	protected ProgrammeFideliteTamponDto dataProgrammeFideliteTampon ;
	

}
