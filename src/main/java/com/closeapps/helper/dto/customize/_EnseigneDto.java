
/*
 * Java dto for entity table enseigne 
 * Created on 2020-09-01 ( Time 02:39:21 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;
import java.util.Map;

import com.closeapps.helper.dto.EtatInscripitionDto;
import com.closeapps.helper.dto.SecteurActiviteDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "enseigne"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _EnseigneDto {
	
	protected List<SecteurActiviteDto> datasSecteurActivite ;
	protected List<EtatInscripitionDto> datasEtatInscripition ;
	protected String  etatInscripitionCode ;
	protected String  etatInscripitionLibelle ;
	protected String  dateDebut ;
	protected String  dateFin ;
	protected Integer  souscriptionId ;
	
	
	protected String    nomFichier ;
	protected String    fichierBase64 ;

	
//	protected Long  nbreUserMobileEnseigne ;
	protected Integer  nbrePointDeVente ;
	protected Integer  nbreProgrammeFideliteTampon ;
	protected Integer  nbreProgrammeFideliteCarte ;
	protected Integer  programmeFideliteTamponId ;
	protected Integer  programmeFideliteCarteId ;
	protected Integer  pointDeVenteId ;
	
	

	
	// varibles stats pousse 
	
	// basic
	protected Integer  nbreTotalTransaction  ;
	protected Integer  nbreTransactionParPdfCarte  ;
	protected Integer  nbreTransactionParPdfTampon  ;
	protected Integer  nbreTransactionPointDeVente  ;
	
	// bande 
	protected List<Double> recompenseEngagePdfTampon ;
	protected List<Double> recompenseEngagePdfCarte ;
	protected List<Double> recompenseRemboursePdfTampon ;
	protected List<Double> recompenseRemboursePdfCarte ;
	protected List<Double> recompenseEnAttentePdfTampon ;
	protected List<Double> recompenseEnAttentePdfCarte ;
	protected List<Double> npsPdfTampon ;
	protected List<Double> npsPdfCarte ;
	protected List<Double> tauxRetentioPdfTampon ;
	protected List<Double> tauxRetentioPdfCarte ;
	protected List<Double> tauxAttrioPdfTampon ;
	protected List<Double> tauxAttrioPdfCarte ;
	protected List<Double> tauxReachatPdfTampon ;
	protected List<Double> tauxReachatPdfCarte ;
	protected List<Integer> nbreInscritPdfTampon ;
	protected List<Integer> nbreInscritPdfCarte ;
	protected List<Double> chiffreDaffairePdfCarte ;
	protected List<String> abscisseGraphe ;

	// circulaire 
	protected List<_GraphCirculaireDto> chiffreDaffaireParPdfCarte ;
	protected List<String> legendCaPdfCarte;
	protected List<_GraphCirculaireDto> inscritParPdfTampon;
	protected List<String> legendPdfTampon;
	protected List<_GraphCirculaireDto> inscritParPdfCarte ;
	protected List<String> legendPdfCarte;


	
	

}
