
/*
 * Java dto for entity table historique_souscription_enseigne 
 * Created on 2021-12-16 ( Time 11:03:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "historique_souscription_enseigne"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _HistoriqueSouscriptionEnseigneDto {
  
  protected String enseignePrenomsAdmin;
  protected String souscriptionDescription;
  protected String enseigneTelephone;
  protected String enseigneNomAdmin;
  protected String enseigneEmail;
protected String pallierForfaitLibelle ;
}
