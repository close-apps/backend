
/*
 * Java dto for entity table souscription_programme_fidelite_carte 
 * Created on 2020-08-31 ( Time 10:15:45 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.List;

import com.closeapps.dao.entity.User;
import com.closeapps.helper.contract.SearchParam;
import com.closeapps.helper.dto.CarteDto;
import com.closeapps.helper.dto.ProgrammeFideliteCarteDto;
import com.closeapps.helper.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * DTO customize for table "souscription_programme_fidelite_carte"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _SouscriptionProgrammeFideliteCarteDto {
	
	
	protected String enseigneUrlLogo ;
	protected String enseigneCouleur ;
	
	
	protected List<UserDto> datasUser ;
	protected Integer userId ;
	protected Integer enseigneId ;
	protected Integer pointPartager ;
	
	protected String urlLogo ;
	protected String telephoneReceveur ;
	protected String description ;
	protected String emailReceveur ;
	protected SearchParam<Integer>  userIdParam          ;       
	protected SearchParam<Integer>  enseigneIdParam          ;       
	
	protected Boolean    isActiveEnseigne ;
	protected SearchParam<Boolean>    isActiveEnseigneParam ;

	
	protected CarteDto dataCarte ;
	protected ProgrammeFideliteCarteDto dataProgrammeFideliteCarte ;
	
	protected Integer programmeFideliteCartePourXAcheter ;
	protected Integer programmeFideliteCarteNbrePointObtenu ;
	protected Integer programmeFideliteCarteCorrespondanceNbrePoint ;

	
	

}
