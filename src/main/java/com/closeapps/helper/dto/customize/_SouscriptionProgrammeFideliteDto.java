
/*
 * Java dto for entity table souscription_programme_fidelite 
 * Created on 2021-06-18 ( Time 09:17:11 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "souscription_programme_fidelite"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _SouscriptionProgrammeFideliteDto {
	
	protected String enseigneUrlLogo ;
	protected String enseigneCouleur ;
	protected Integer pointDeVenteId ;
	protected SearchParam<Integer>  pointDeVenteIdParam        ;                     

	protected Integer enseigneIdForTampon ;
	protected SearchParam<Integer>  enseigneIdForTamponParam        ;                     

	protected Integer enseigneIdForCarte ;
	protected SearchParam<Integer>  enseigneIdForCarteParam        ;                     

	
	protected Boolean    isActiveEnseigne ;
	protected SearchParam<Boolean>    isActiveEnseigneParam ;

	
}
