
/*
 * Java dto for entity table commune 
 * Created on 2020-12-30 ( Time 09:34:29 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.closeapps.dao.entity.Ville;
import com.closeapps.helper.contract.*;

import lombok.*;
/**
 * DTO customize for table "commune"
 * 
* @author Back-End developper
   *
 */
@Data // getters setters
//@AllArgsConstructor // constructeur avec arguments
//@NoArgsConstructor // constructeur sans arguments
@JsonInclude(Include.NON_NULL)
public class _CommuneDto {
	
	protected Ville entityVille ;

}
