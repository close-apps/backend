
/*
 * Created on 2020-08-17 ( Time 14:15:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.contract;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Response
 * 
 * @author Back-End developper
  *
 */
@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class Response<T> extends ResponseBase {

	protected List<T> items;

	// ----------------------------------------------------------------------
	// GETTER(S) & SETTER(S) FOR DATA FIELDS
	// ----------------------------------------------------------------------

	/**
	 * Get the "items" field value
	 * 
	 * @return the field value
	 */
	public List<T> getItems() {
		return items;
	}

	/**
	 * Set the "items" field value
	 * 
	 * @param items
	 */
	public void setItems(List<T> items) {
		this.items = items;
	}

}
