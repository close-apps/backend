
/*
 * Created on 2020-08-17 ( Time 14:15:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.contract;

import java.util.Locale;

/**
 * IBasic Business
 * 
 * @author Back-End developper
  *
 */
public interface IBasicBusiness<T, K> {

	/**
	 * create Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K create(T request, Locale locale);

	/**
	 * update Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K update(T request, Locale locale);

	/**
	 * delete Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K delete(T request, Locale locale);

	/**
	 * get a List of Object by using T as criteria object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K getByCriteria(T request, Locale locale);
}
