
/*
 * Created on 2020-08-17 ( Time 14:15:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
* Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.contract;

/**
 * Search Param
 * 
  * @author Back-End developper
 *
 */
public class SearchParam<T> {

	String	operator;
	T		start;
	T		end;

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @return the start
	 */
	public T getStart() {
		return start;
	}

	/**
	 * @return the end
	 */
	public T getEnd() {
		return end;
	}

	/**
	 * @param operator
	 *            the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(T start) {
		this.start = start;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(T end) {
		this.end = end;
	}

}
