/*
 * Created on 2020-08-17 ( Time 14:15:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
* Copyright 2020 SalaamSolutions. All Rights Reserved.
  */

package com.closeapps.helper.contract;

import lombok.*;

/**
 * Request Base
 * 
 * @author Back-End developper
  *
 */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class RequestBase {

	protected String	sessionUser;
	protected Integer	size;
	protected Integer	index;
	protected String	lang;
	protected String	businessLineCode;
	protected String	caseEngine;
	protected Boolean	isAnd;
	protected boolean	stat;
	protected Integer	user;


}