
/*
 * Created on 2020-08-17 ( Time 14:15:33 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
* Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.helper.contract;

import com.closeapps.helper.Status;
import lombok.*;

/**
 * Response Base
 * 
 * @author Back-End developper
  *
 */
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class ResponseBase {

	protected Status	status;
	protected boolean	hasError;
	protected String	sessionUser;
	protected Long		count;

}
