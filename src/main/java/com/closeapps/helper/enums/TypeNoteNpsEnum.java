package com.closeapps.helper.enums;

public class TypeNoteNpsEnum {
	
	public static final String	DETRACTORS				= "DETRACTORS";
	public static final String	PROMOTERS				= "PROMOTERS";
	public static final String	PASSIVES					= "PASSIVES";
}

