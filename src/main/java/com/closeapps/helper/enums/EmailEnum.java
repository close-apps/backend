package com.closeapps.helper.enums;

public class EmailEnum {
	public static final String	codeOffre								= "codeOffre";
	public static final String	codeAppelDoffres						= "codeAppelDoffres";
	public static final String	date									= "date";
	public static final String	nomUser									= "nomUser";
	public static final String	appLink									= "appLink";
	public static final String	userName									= "userName";
	public static final String	password									= "password";
	public static final String	nomEntreprise							= "nomEntreprise";
	public static final String	bienvenueEntreprise							= "bienvenueEntreprise";
	public static final String	bienvenue						= "Bienvenue ";
	public static final String	contenuMail						= "contenuMail";
	
	
	public static final String	nomEntrepriseCliente					= "nomEntrepriseCliente";
	public static final String	nomEntrepriseOffre						= "nomEntrepriseOffre";
	public static final String	domaineEntreprise						= "domaineEntreprise";
	public static final String	NOM_ENTREPRISE							= "CLOSE-APPS";
	public static final String	DOMAINE_ENTREPRISE						= "Entreprise de mise en relation locale et internationale pour les appels d'offres et prestations";
	public static final String	TEMPLATE_NEWSLETTER						= "newsletter-ansut.3";
	public static final String	SUBJECT_NEWSLETTER						= "NEWSLETTER VOLONTARIAT";
	public static final String	SUBJECT_OFFRE_REFUSER					= "OFFRE REFUSEE";
	public static final String	SUBJECT_CREATION_COMPTE					= "Bienvenue chez CLOSE";
	public static final String	SUBJECT_DEMANDE_CREATION_COMPTE			= "Bienvenue chez CLOSE";
	public static final String	SUBJECT_DEMANDE_CREATION_COMPTE_REFUSER	= "Demande Invalider";
	public static final String	SUBJECT_DEMANDE_CREATION_COMPTE_EN_COURS = "Demande en cours";
	public static final String	SUBJECT_DEMANDE_CREATION_COMPTE_VALIDER	= "Demande Validée";
	public static final String	SUBJECT_CONTRAT							= "Contrat #";
	public static final String	SUBJECT_CREATION_COMPTE_VALIDER			= "Bienvenue chez CLOSE";
	public static final String	SUBJECT_NOUS_CONTACTER_DEFAULT_RETOUR   = "ACCUSE DE RECEPTION";
	//public static final String	SUBJECT_NOUS_CONTACTER				    = "ACCUSE DE RECEPTION";
	public static final String	BODY_CREATION_COMPTE					= "Votre compte a";
	public static final String	SUBJECT_FINALISATION_COMPTE				= "FINALISATION DE CREATION DE COMPTE";
	public static final String	SUBJECT_OFFRE_REVISE					= "OFFRE REVISEE";
	public static final String	BODY_OFFRE_REFUSER						= "Votre offre a été réjetée.";
	public static final String	BODY_OFFRE_REVISE						= "Votre offre a été revisée.";
	public static final String	SUBJECT_CANDIDAT_PRESELECTIONNE			= "CANDIDAT PRESELECTIONNE";
	public static final String	SUBJECT_CANDIDAT_SELECTIONNE			= "CANDIDAT SELECTIONNE";
	public static final String	BODY_CANDIDAT_PRESELECTIONNE			= "Vous avez a été préselectionné(e) à une offre.";
	public static final String	SUBJECT_INSC_SA_REFUSEE					= "INCRIPTION STRUCTURE D'ACCUEIL REFUSEE";
	public static final String	BODY_INSC_SA_REFUSEE					= "Votre inscription a été refusée.";
	public static final String	SUBJECT_INSC_VOLONTAIRE_VALIDEE			= "INCRIPTION VOLONTAIRE VALIDEE";
	public static final String	SUBJECT_INSC_SA_VALIDEE					= "INCRIPTION STRUCTURE D'ACCUEIL VALIDEE";
	public static final String	SUBJECT_INSC_VOLONTAIRE_REVISE			= "REVISION INCRIPTION VOLONTAIRE";
	public static final String	BODY_INSC_VOLONTAIRE_VALIDEE			= "Votre inscription a été validée.";
	public static final String	BODY_INSC_VOLONTAIRE_REVISE			    = "Votre  doit etre revisé.";
	public static final String	SUBJECT_PASSWORD_RESET					= "CHANGEMENT DU MOT DE PASSE";
	public static final String	BODY_PASSWORD_RESET						= "Votre password a été changé avec succès";
	public static final String	SUBJECT_RESET_PASSWORD_OK				= "MOT DE PASSE CHANGE";
	public static final String	SUBJECT_FORGET_PASSWORD					= "MOT DE PASSE OUBLIE";
	public static final String	SUBJECT_PASSWORD_RESET_EN				= "RENITIALISER MOT DE PASSE";
	public static final String	SUBJECT_RESET_PASSWORD_OK_EN			= "RESET PASSWORD USER OK";
	public static final String	SUBJECT_FORGET_PASSWORD_EN			    = "FORGET PASSWORD USER";
	public static final String	SUBJECT_NOUS_CONTACTER					= "NOUS CONTACTER";
	public static final String	SUBJECT_DEMANDE_ENTRANT_VOLONTAIRE		= "INSCRIPTION AU VOLONTARIAT";
	public static final String	SUBJECT_DEMANDE_BROUILLON_VOLONTAIRE	= "PRE-INSCRIPTION AU VOLONTARIAT";
	public static final String	SUBJECT_UPDATE_PROFIL_SA				= "MODIFICATION DE PROFIL STRUCTURE D'ACCUEIL";
	public static final String	SUBJECT_UPDATE_AUDIT_SA					= "VALIDATION AUDIT SA";
	public static final String	BODY_UPDATE_AUDIT_SA					= "Votre audit a été validé.";
	public static final String	BODY_UPDATE_PROFIL_SA					= "Une structure d'accueil vient de modifier son profil.";
	public static final String	SUBJECT_DEMANDE_ENTRANT_SA				= "INSCRIPTION STRUCTURE D'ACCUEIL";
	public static final String	BODY_DEMANDE_ENTRANT_SA					= "Une structure d'accueil vient de s'inscrire. Merci de valider son inscription";
	public static final String	BODY_DEMANDE_ENTRANT_VOLONTAIRE			= "Un volontaire vient de s'inscrire. Merci de valider son inscription";
	public static final String	body									= "body";
	public static final String	subject									= "subject";
	
	public static final String	Objet									= "Objet: ";
	public static final String	Telephone								= "Telephone: ";
	public static final String	Email									= "Email: ";
	public static final String	Nom_prenoms								= "Nom & prenom(s): ";
	public static final String	Message									= "message: ";
	public static final String	nom										= "nom: ";
	public static final String	token										= "token";
	
	
	public static final String	localeJava								= "localeJava";
	public static final String	listEmail								= "listEmail";
	public static final int 	MIN_CARACTERE_PASSWORD				    = 8;
	public static final String	SUBJECT_OFFRE_VALIDER					= "OFFRE VALIDEE";
	public static final String	SUBJECT_TEMOIGNAGE_VALIDER				= "TEMOIGNAGE VALIDE";
	public static final String	SUBJECT_TEMOIGNAGE_REFUSE			    = "TEMOIGNAGE REFUSE";
	public static final String	SUBJECT_TEMOIGNAGE_ENTRANT			    = "SOUMISSION TEMOIGNAGE";
	public static final String	SUBJECT_OFFRE_ENTRANT			        = "SOUMISSION OFFRE";
	public static final String	SUBJECT_OFFRE_ATTRIBUE_SA			    = "OFFRE ATTRIBUEE";
	public static final String	Actualités			    				= "Actualités";
	public static final String	MATRICULE_STAR_MOTORS			    	= "SM";
	public static final String	FICHE_STAR_MOTORS			    		= "FICHE";
	public static final String	Temoignages			    				= "Ils partagent leurs expériences...";
	
	public static final String	contenuMailEnCours						= "Votre demande de création est en cours de traitement par notre équipe. Merci de patienter nous allons vous revenir.";
	public static final String	contenuMailAccepter						= "Votre demande de création a été validée par notre équipe. Vous allez recevoir vos accès dans un autre mail. Merci de vous connecter pour profiter de nos services.";
	//public static final String	contenuMailAccepter						= "Votre demande de cr&eacute;ation a &eacute;t&eacute; valid&eacute; par notre &eacute;quipe. <br /><br /> Merci de finaliser votre inscription.";
	public static final String	contenuMailRejeter						= "Votre demande de création a été réjété; par notre équipe. Nous allons vous contacter pour vous donner plus de détails.";
	public static final String	contenuMailAccepterUserMobile						= "Profitez dès maintenant avec CLOSE de vos programmes de fidelité sur votre smartphone et sur votre tablette !!!";
	public static final String	contenuMailAccepterUserEnseigne						= "Commencez dès maintenant à créer vos programmes de fidélité et augmentez votre chiffre d'affaires !!!";
	//public static final String	contenuMailAccepter						= "Votre demande de création a été validée par notre équipe. Vous pouvez finaliser votre inscription. ";
	
				
	
	//public static final String	contenuMailEnCours						= "Votre demande de cr&eacute;ation est en cours de traitement par notre &eacute;quipe. <br /><br /> Merci de patienter nous allons vous revenir.";
	//public static final String	contenuMailAccepter						= "Votre demande de cr&eacute;ation a &eacute;t&eacute; valid&eacute; par notre &eacute;quipe. <br /><br /> Vous allez recevoir vos accès dans un autre mail. Merci de vous connecter pour profiter de nos services.";
	//public static final String	contenuMailAccepter						= "Votre demande de cr&eacute;ation a &eacute;t&eacute; valid&eacute; par notre &eacute;quipe. <br /><br /> Merci de finaliser votre inscription.";
	//public static final String	contenuMailRejeter						= "Votre demande de cr&eacute;ation a &eacute;t&eacute; r&eacute;j&eacute;t&eacute; par notre &eacute;quipe. <br /><br /> Nous allons vous contacter pour vous donner plus de d&eacute;tails.";
	//public static final String	contenuMailAccepter						= "Votre demande de création a été validée par notre équipe. Vous pouvez finaliser votre inscription. ";

	public static final String	bienvenueEnCours						= "Bienvenue ";
	public static final String	bienvenueAccepter						= "Félicitation ";
	public static final String	bienvenueRejeter						= "Désolé ";

	//public static final String	bienvenueAccepter						= "F&eacute;licitation ";
	//public static final String	bienvenueRejeter						= "D&eacute;sol&eacute; ";

	
		
}
