package com.closeapps.helper.enums;

public class UniteTempsEnum {
	public static final String	PAR_HEURE				= "PAR HEURE";
	public static final String	PAR_JOUR			    = "PAR JOUR";
	public static final String	PAR_SEMAINE				= "PAR SEMAINE";
	public static final String	PAR_MOIS				= "PAR MOIS";
	
	public static final String	MENSUEL				= "mensuel";
	public static final String	TRIMESTRIEL				= "trimestriel";
	public static final String	ANNUEL				= "annuel";
	public static final String	CINQ_CENT_ET_PLUS				= "501-+";
	
	

}

