package com.closeapps.helper.enums;

public class ModePaiementEnum {
	public static final String	MONEY				= "MONEY";
	public static final String	BANCAIRE				    = "BANCAIRE";
	public static final String	CINETPAY					= "CINETPAY";
	public static final String	PAYPAL				= "PAYPAL";
	// public static final String	ACCEPTER				= "ACCEPTER";
}

