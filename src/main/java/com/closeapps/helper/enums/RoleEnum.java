package com.closeapps.helper.enums;

public class RoleEnum {
	public static final String	ADMINISTRATEUR					= "ADMINISTRATEUR";
	public static final String	UTILISATEUR_MOBILE					= "UTILISATEUR MOBILE";
	public static final String	UTILISATEUR_ENSEIGNE					= "UTILISATEUR ENSEIGNE";
	public static final String	TRESORIER				= "TRESORIER";
	public static final String	GERANT					= "GERANT";
	public static final String	COMPTABLE				= "COMPTABLE";
	public static final String	FINANCIER				= "FINANCIER";
	
	public static final String	EN_COURS				= "EN_COURS";
	public static final String	ACCEPTER				= "ACCEPTER";
}

