


/*
 * Java transformer for entity table regle_conversion
 * Created on 2020-08-17 ( Time 14:14:50 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "regle_conversion"
 *
 * @author Back-End developper
 *
 */
@Component
public class RegleConversionBusiness implements IBasicBusiness<Request<RegleConversionDto>, Response<RegleConversionDto>> {

	private Response<RegleConversionDto> response;
	@Autowired
	private RegleConversionRepository regleConversionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public RegleConversionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create RegleConversion by using RegleConversionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RegleConversionDto> create(Request<RegleConversionDto> request, Locale locale)  {
		slf4jLogger.info("----begin create RegleConversion-----");

		response = new Response<RegleConversionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<RegleConversion> items = new ArrayList<RegleConversion>();

			for (RegleConversionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("valeurAcheter", dto.getValeurAcheter());
				fieldsToVerify.put("pointGagner", dto.getPointGagner());
				fieldsToVerify.put("sommeCorrespondant", dto.getSommeCorrespondant());
				fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				fieldsToVerify.put("produitId", dto.getProduitId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if regleConversion to insert do not exist
				RegleConversion existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("regleConversion -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if produit exist
				Produit existingProduit = produitRepository.findById(dto.getProduitId(), false);
				if (existingProduit == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getProduitId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if programmeFideliteCarte exist
				ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
				if (existingProgrammeFideliteCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				RegleConversion entityToSave = null;
				entityToSave = RegleConversionTransformer.INSTANCE.toEntity(dto, existingProduit, existingProgrammeFideliteCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<RegleConversion> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = regleConversionRepository.saveAll((Iterable<RegleConversion>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("regleConversion", locale));
					response.setHasError(true);
					return response;
				}
				List<RegleConversionDto> itemsDto = new ArrayList<RegleConversionDto>();
				for (RegleConversion entity : itemsSaved) {
					RegleConversionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create RegleConversion-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update RegleConversion by using RegleConversionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RegleConversionDto> update(Request<RegleConversionDto> request, Locale locale)  {
		slf4jLogger.info("----begin update RegleConversion-----");

		response = new Response<RegleConversionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<RegleConversion> items = new ArrayList<RegleConversion>();

			for (RegleConversionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la regleConversion existe
				RegleConversion entityToSave = null;
				entityToSave = regleConversionRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("regleConversion -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if produit exist
				if (dto.getProduitId() != null && dto.getProduitId() > 0){
					Produit existingProduit = produitRepository.findById(dto.getProduitId(), false);
					if (existingProduit == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getProduitId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProduit(existingProduit);
				}
				// Verify if programmeFideliteCarte exist
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
					ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				}
				if (Utilities.notBlank(dto.getValeurAcheter())) {
					entityToSave.setValeurAcheter(dto.getValeurAcheter());
				}
				if (Utilities.notBlank(dto.getPointGagner())) {
					entityToSave.setPointGagner(dto.getPointGagner());
				}
				if (Utilities.notBlank(dto.getSommeCorrespondant())) {
					entityToSave.setSommeCorrespondant(dto.getSommeCorrespondant());
				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<RegleConversion> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = regleConversionRepository.saveAll((Iterable<RegleConversion>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("regleConversion", locale));
					response.setHasError(true);
					return response;
				}
				List<RegleConversionDto> itemsDto = new ArrayList<RegleConversionDto>();
				for (RegleConversion entity : itemsSaved) {
					RegleConversionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update RegleConversion-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete RegleConversion by using RegleConversionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<RegleConversionDto> delete(Request<RegleConversionDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete RegleConversion-----");

		response = new Response<RegleConversionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<RegleConversion> items = new ArrayList<RegleConversion>();

			for (RegleConversionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la regleConversion existe
				RegleConversion existingEntity = null;
				existingEntity = regleConversionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("regleConversion -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				regleConversionRepository.saveAll((Iterable<RegleConversion>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete RegleConversion-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete RegleConversion by using RegleConversionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<RegleConversionDto> forceDelete(Request<RegleConversionDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete RegleConversion-----");

		response = new Response<RegleConversionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<RegleConversion> items = new ArrayList<RegleConversion>();

			for (RegleConversionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la regleConversion existe
				RegleConversion existingEntity = null;
				existingEntity = regleConversionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("regleConversion -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				regleConversionRepository.saveAll((Iterable<RegleConversion>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete RegleConversion-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get RegleConversion by using RegleConversionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<RegleConversionDto> getByCriteria(Request<RegleConversionDto> request, Locale locale) {
		slf4jLogger.info("----begin get RegleConversion-----");

		response = new Response<RegleConversionDto>();

		try {
			List<RegleConversion> items = null;
			items = regleConversionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<RegleConversionDto> itemsDto = new ArrayList<RegleConversionDto>();
				for (RegleConversion entity : items) {
					RegleConversionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(regleConversionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("regleConversion", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get RegleConversion-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full RegleConversionDto by using RegleConversion as object.
	 *
	 * @param entity, locale
	 * @return RegleConversionDto
	 *
	 */
	private RegleConversionDto getFullInfos(RegleConversion entity, Integer size, Locale locale) throws Exception {
		RegleConversionDto dto = RegleConversionTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
