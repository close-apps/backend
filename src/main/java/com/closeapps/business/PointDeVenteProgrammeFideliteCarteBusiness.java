


                                                                          /*
 * Java transformer for entity table point_de_vente_programme_fidelite_carte
 * Created on 2020-08-17 ( Time 14:14:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "point_de_vente_programme_fidelite_carte"
 *
* @author Back-End developper
   *
 */
@Component
public class PointDeVenteProgrammeFideliteCarteBusiness implements IBasicBusiness<Request<PointDeVenteProgrammeFideliteCarteDto>, Response<PointDeVenteProgrammeFideliteCarteDto>> {

  private Response<PointDeVenteProgrammeFideliteCarteDto> response;
  @Autowired
  private PointDeVenteProgrammeFideliteCarteRepository pointDeVenteProgrammeFideliteCarteRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PointDeVenteRepository pointDeVenteRepository;
  @Autowired
  private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
  
  @Autowired
  private ProgrammeFideliteCarteBusiness programmeFideliteCarteBusiness;
  
    
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public PointDeVenteProgrammeFideliteCarteBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteCarteDto> create(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin create PointDeVenteProgrammeFideliteCarte-----");

    response = new Response<PointDeVenteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteCarte> items = new ArrayList<PointDeVenteProgrammeFideliteCarte>();

      for (PointDeVenteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
        fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if pointDeVenteProgrammeFideliteCarte to insert do not exist
        PointDeVenteProgrammeFideliteCarte existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("pointDeVenteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if pointDeVente exist
                PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
                if (existingPointDeVente == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if programmeFideliteCarte exist
                ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
                if (existingProgrammeFideliteCarte == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
          response.setHasError(true);
          return response;
        }
        PointDeVenteProgrammeFideliteCarte entityToSave = null;
        entityToSave = PointDeVenteProgrammeFideliteCarteTransformer.INSTANCE.toEntity(dto, existingPointDeVente, existingProgrammeFideliteCarte);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteCarte> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<PointDeVenteProgrammeFideliteCarteDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteCarteDto>();
        for (PointDeVenteProgrammeFideliteCarte entity : itemsSaved) {
          PointDeVenteProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteCarteDto> update(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin update PointDeVenteProgrammeFideliteCarte-----");

    response = new Response<PointDeVenteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteCarte> items = new ArrayList<PointDeVenteProgrammeFideliteCarte>();

      for (PointDeVenteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pointDeVenteProgrammeFideliteCarte existe
        PointDeVenteProgrammeFideliteCarte entityToSave = null;
        entityToSave = pointDeVenteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if pointDeVente exist
        if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0){
                    PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
          if (existingPointDeVente == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPointDeVente(existingPointDeVente);
        }
        // Verify if programmeFideliteCarte exist
        if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
                    ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
          if (existingProgrammeFideliteCarte == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteCarte> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<PointDeVenteProgrammeFideliteCarteDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteCarteDto>();
        for (PointDeVenteProgrammeFideliteCarte entity : itemsSaved) {
          PointDeVenteProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteCarteDto> delete(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete PointDeVenteProgrammeFideliteCarte-----");

    response = new Response<PointDeVenteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteCarte> items = new ArrayList<PointDeVenteProgrammeFideliteCarte>();

      for (PointDeVenteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pointDeVenteProgrammeFideliteCarte existe
        PointDeVenteProgrammeFideliteCarte existingEntity = null;
        existingEntity = pointDeVenteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<PointDeVenteProgrammeFideliteCarteDto> forceDelete(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete PointDeVenteProgrammeFideliteCarte-----");

    response = new Response<PointDeVenteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteCarte> items = new ArrayList<PointDeVenteProgrammeFideliteCarte>();

      for (PointDeVenteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la pointDeVenteProgrammeFideliteCarte existe
        PointDeVenteProgrammeFideliteCarte existingEntity = null;
          existingEntity = pointDeVenteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<PointDeVenteProgrammeFideliteCarteDto> getByCriteria(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale) {
    slf4jLogger.info("----begin get PointDeVenteProgrammeFideliteCarte-----");

    response = new Response<PointDeVenteProgrammeFideliteCarteDto>();

    try {
      List<PointDeVenteProgrammeFideliteCarte> items = null;
      items = pointDeVenteProgrammeFideliteCarteRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteCarteDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteCarteDto>();
        for (PointDeVenteProgrammeFideliteCarte entity : items) {
          PointDeVenteProgrammeFideliteCarteDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(pointDeVenteProgrammeFideliteCarteRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("pointDeVenteProgrammeFideliteCarte", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  
  /**
   * get PointDeVenteProgrammeFideliteCarte by using PointDeVenteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  public Response<ProgrammeFideliteCarteDto> getByCriteriaCustom(Request<PointDeVenteProgrammeFideliteCarteDto> request, Locale locale) {
    slf4jLogger.info("----begin getByCriteriaCustom PointDeVenteProgrammeFideliteCarte-----");

    Response<ProgrammeFideliteCarteDto> responseLocal = new Response<>();

    try {
      List<ProgrammeFideliteCarte> items = null;
      items = pointDeVenteProgrammeFideliteCarteRepository.getByCriteriaCustom(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<ProgrammeFideliteCarteDto> itemsDto = new ArrayList<ProgrammeFideliteCarteDto>();
        for (ProgrammeFideliteCarte entity : items) {
        	ProgrammeFideliteCarteDto dto = programmeFideliteCarteBusiness.getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        responseLocal.setItems(itemsDto);
        responseLocal.setCount(pointDeVenteProgrammeFideliteCarteRepository.countCustom(request, em, locale));
        responseLocal.setHasError(false);
      } else {
    	  responseLocal.setStatus(functionalError.DATA_EMPTY("pointDeVenteProgrammeFideliteCarte", locale));
    	  responseLocal.setHasError(false);
        return responseLocal;
      }

      slf4jLogger.info("----end get PointDeVenteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(responseLocal, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(responseLocal, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(responseLocal, locale, e);
    } finally {
      if (responseLocal.isHasError() && responseLocal.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", responseLocal.getStatus().getCode(), responseLocal.getStatus().getMessage());
        throw new RuntimeException(responseLocal.getStatus().getCode() + ";" + responseLocal.getStatus().getMessage());
      }
    }
    return responseLocal;
  }

  /**
   * get full PointDeVenteProgrammeFideliteCarteDto by using PointDeVenteProgrammeFideliteCarte as object.
   *
   * @param entity, locale
   * @return PointDeVenteProgrammeFideliteCarteDto
   *
   */
  private PointDeVenteProgrammeFideliteCarteDto getFullInfos(PointDeVenteProgrammeFideliteCarte entity, Integer size, Locale locale) throws Exception {
    PointDeVenteProgrammeFideliteCarteDto dto = PointDeVenteProgrammeFideliteCarteTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
