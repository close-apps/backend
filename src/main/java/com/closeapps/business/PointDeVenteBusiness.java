


/*
 * Java transformer for entity table point_de_vente
 * Created on 2020-09-13 ( Time 11:28:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.EtatEnum;
import com.closeapps.helper.enums.GlobalEnum;
import com.closeapps.helper.enums.RoleEnum;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "point_de_vente"
 *
 * @author Back-End developper
 *
 */
@Component
public class PointDeVenteBusiness implements IBasicBusiness<Request<PointDeVenteDto>, Response<PointDeVenteDto>> {

	private Response<PointDeVenteDto> response;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private DelitProgrammeFideliteRepository delitProgrammeFideliteRepository;
	@Autowired
	private PointDeVenteProgrammeFideliteTamponRepository pointDeVenteProgrammeFideliteTamponRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository;
	@Autowired
	private PointDeVenteProgrammeFideliteCarteRepository pointDeVenteProgrammeFideliteCarteRepository;
	@Autowired
	private UserEnseignePointDeVenteRepository userEnseignePointDeVenteRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
	@Autowired
	private GpsPointDeVenteRepository gpsPointDeVenteRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;



	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;

	@Autowired
	private PointDeVenteProgrammeFideliteTamponBusiness pointDeVenteProgrammeFideliteTamponBusiness;


	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponBusiness historiqueCarteProgrammeFideliteTamponBusiness;


	@Autowired
	private PointDeVenteProgrammeFideliteCarteBusiness pointDeVenteProgrammeFideliteCarteBusiness;


	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteBusiness historiqueCarteProgrammeFideliteCarteBusiness;


	@Autowired
	private DelitProgrammeFideliteBusiness delitProgrammeFideliteBusiness;

	@Autowired
	private GpsPointDeVenteBusiness gpsPointDeVenteBusiness;

	@Autowired
	private UserBusiness userBusiness;

	@Autowired
	private UserEnseignePointDeVenteBusiness userEnseignePointDeVenteBusiness;





	public PointDeVenteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PointDeVenteDto> create(Request<PointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				&& !utilisateur.getEnseigne().getIsActive()) {
					response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!" , locale));
					response.setHasError(true);
					return response;
				}
			// TODO a revoir pourquoi le role a changer pour l'admin
//			if (!utilisateur.getRole().getCode().equals(RoleEnum.ADMINISTRATEUR)) {
//				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<PointDeVente> items = new ArrayList<PointDeVente>();
			List<UserEnseignePointDeVente> itemsUserEnseignePointDeVente = new ArrayList<UserEnseignePointDeVente>();
			List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

			List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteToUpdate = new ArrayList<ProgrammeFideliteCarte>();
			List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponToUpdate = new ArrayList<ProgrammeFideliteTampon>();


			for (PointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				fieldsToVerify.put("datasUserEnseigne", dto.getDatasUserEnseigne());
				
				


				//fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("code", dto.getCode());
				//fieldsToVerify.put("gpsPointDeVenteId", dto.getGpsPointDeVenteId());
				//fieldsToVerify.put("userEnseigneId", dto.getUserEnseigneId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
				
				// rendre le user obligatoire lors de la creation d'un PDV
				if (!Utilities.isNotEmpty(dto.getDatasUserEnseigne())) {
					response.setStatus(functionalError.FIELD_EMPTY("user PDV", locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if pointDeVente to insert do not exist
				PointDeVente existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if enseigne exist
				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}
				if (existingEnseigne.getIsAccepted() != null && !existingEnseigne.getIsAccepted()) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("l'enseigne -> " + existingEnseigne.getNom() + " n'est pas validée", locale));
					response.setHasError(true);
					return response;
				}
				if (existingEnseigne.getIsLocked() != null && existingEnseigne.getIsLocked()) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("l'enseigne -> " + existingEnseigne.getNom() + " est verrouillée", locale));
					response.setHasError(true);
					return response;
				}

				// unicite du nom
				//existingEntity = pointDeVenteRepository.findPointDeVenteByNom(dto.getNom(), false);
				existingEntity = pointDeVenteRepository.findPointDeVenteByEnseigneIdAndNom(dto.getEnseigneId(), dto.getNom(), false);

				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("pointDeVente -> " + dto.getNom(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les pointDeVentes", locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getCode())) {
					existingEntity = pointDeVenteRepository.findPointDeVenteByEnseigneIdAndCode(dto.getEnseigneId(), dto.getCode(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("pointDeVente -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les pointDeVentes", locale));
						response.setHasError(true);
						return response;
					}
				}


				// Verify if gpsPointDeVente exist
				GpsPointDeVente existingGpsPointDeVente = null ;
				if (dto.getDataGpsPointDeVente() != null) {

					Request<GpsPointDeVenteDto> rep = new Request<GpsPointDeVenteDto>(); 
					Response<GpsPointDeVenteDto> res = new Response<GpsPointDeVenteDto>(); 
					rep.setDatas(Arrays.asList(dto.getDataGpsPointDeVente()));
					rep.setUser(request.getUser());
					res = gpsPointDeVenteBusiness.create(rep, locale); 

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					if (Utilities.isNotEmpty(res.getItems())) {
						existingGpsPointDeVente = gpsPointDeVenteRepository.findById(res.getItems().get(0).getId(), false) ;
					}
				}
				
				Commune existingCommune = null ;
				if (dto.getCommuneId() != null) {
					existingCommune = communeRepository.findById(dto.getCommuneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("commune -> " + dto.getCommuneId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				if (dto.getIsAllCarteDipso() == null ) {
					dto.setIsAllCarteDipso(false);
				}
				if (dto.getIsAllTamponDipso() == null ) {
					dto.setIsAllTamponDipso(false);
				}




				


				// TODO : pas trop au point cette option 
				/*
				// celui qui créé un PDV est le premier user de ce PDV
				UserEnseignePointDeVente firstUserEnseignePointDeVente = new UserEnseignePointDeVente() ;
				firstUserEnseignePointDeVente.setUser(utilisateur);
				//firstUserEnseignePointDeVente.setUserEnseigne(utilisateur);
				firstUserEnseignePointDeVente.setPointDeVente(entityToSave);
				firstUserEnseignePointDeVente.setCreatedBy(request.getUser());
				firstUserEnseignePointDeVente.setCreatedAt(Utilities.getCurrentDate());
				firstUserEnseignePointDeVente.setIsDeleted(false);

				itemsUserEnseignePointDeVente.add(firstUserEnseignePointDeVente) ;
				//*/
				
				
				//UserEnseigne existingUserEnseigne = null ;
				//if (Utilities.isNotEmpty(dto.getDatasUserEnseigne())) {
					List<UserDto> itemsUserEnseigneDto = new ArrayList<>() ;
					Request<UserDto> rep = new Request<>(); 
					Response<UserDto> res = new Response<>(); 

					for (UserDto data : dto.getDatasUserEnseigne()) {
						// generation du login/password
						String passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userRepository, GlobalEnum.longueurDefaultPassword) ;
						//String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne, data.getRoleId());
						String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne);
						data.setLogin(loginGenerer);
						data.setPassword(passwordGenerer);
						data.setEnseigneId(dto.getEnseigneId());
						// data.setIsAdminEnseigne(false); pas necessaire

						while (itemsUserEnseigneDto.stream().anyMatch(a->a.getLogin().equals(data.getLogin()))) {
							//loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne, data.getRoleId());
							loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne);
							data.setLogin(loginGenerer);
						}
						while (itemsUserEnseigneDto.stream().anyMatch(a->a.getPassword().equals(data.getPassword()))) {
							passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userRepository, GlobalEnum.longueurDefaultPassword) ;
							data.setPassword(passwordGenerer);
						}
						
						data.setIsUserPDV(true);
						itemsUserEnseigneDto.add(data) ;						
					}

					rep.setDatas(itemsUserEnseigneDto);
					rep.setUser(request.getUser());
					res = userBusiness.create(rep, locale); 

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					
					User existingUser = userRepository.findById(res.getItems().get(0).getId(), false);

					PointDeVente entityToSave = null;
					entityToSave = PointDeVenteTransformer.INSTANCE.toEntity(dto, existingGpsPointDeVente, existingCommune, existingUser, existingEnseigne);
					entityToSave.setCreatedBy(request.getUser());
					entityToSave.setCreatedAt(Utilities.getCurrentDate());
					entityToSave.setIsDeleted(false);
					items.add(entityToSave);

					// preparation des itemsUserEnseignePointDeVente
					if (Utilities.isNotEmpty(res.getItems())) {
						for (UserDto userEnseigneDto : res.getItems()) {							
							UserEnseignePointDeVente userEnseignePointDeVente = new UserEnseignePointDeVente() ;
							//UserEnseigne existingUserEnseigne = userEnseigneRepository.findById(userEnseigneDto.getId(), false);
							User existingUserEnseigne = userRepository.findById(userEnseigneDto.getId(), false);

							//userEnseignePointDeVente.setUserEnseigne(existingUserEnseigne);
							userEnseignePointDeVente.setUser(existingUserEnseigne);
							userEnseignePointDeVente.setPointDeVente(entityToSave);
							userEnseignePointDeVente.setCreatedBy(request.getUser());
							userEnseignePointDeVente.setCreatedAt(Utilities.getCurrentDate());
							userEnseignePointDeVente.setIsDeleted(false);

							itemsUserEnseignePointDeVente.add(userEnseignePointDeVente) ;
						}
					}						

				//}

				/**
				if (dto.getDataUserEnseigne() != null) {

					Request<UserEnseigneDto> rep = new Request<UserEnseigneDto>(); 
					Response<UserEnseigneDto> res = new Response<UserEnseigneDto>(); 

					// generation du login/password
					String passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userEnseigneRepository, GlobalEnum.longueurDefaultPassword) ;
					String loginGenerer = Utilities.generateLoginUserEnseigne(userEnseigneRepository, GlobalEnum.loginUserEnseigne);
					dto.getDataUserEnseigne().setLogin(loginGenerer);
					dto.getDataUserEnseigne().setPassword(passwordGenerer);

					rep.setDatas(Arrays.asList(dto.getDataUserEnseigne()));
					rep.setUser(request.getUser());
					res = userEnseigneBusiness.create(rep, locale); 

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					// preparation des itemsUserEnseignePointDeVente
					if (Utilities.isNotEmpty(res.getItems())) {
						UserEnseignePointDeVente userEnseignePointDeVente = new UserEnseignePointDeVente() ;

						existingUserEnseigne = userEnseigneRepository.findById(res.getItems().get(0).getId(), false) ;

						userEnseignePointDeVente.setUserEnseigne(existingUserEnseigne);
						userEnseignePointDeVente.setPointDeVente(entityToSave);
						userEnseignePointDeVente.setCreatedBy(request.getUser());
						userEnseignePointDeVente.setCreatedAt(Utilities.getCurrentDate());
						userEnseignePointDeVente.setIsDeleted(false);

						itemsUserEnseignePointDeVente.add(userEnseignePointDeVente) ;
					}
				}
				 **/

				// liste des PDFC
				if (dto.getIsAllCarteDipso() != null && dto.getIsAllCarteDipso()) {
					List<ProgrammeFideliteCarte> programmeFideliteCartes = programmeFideliteCarteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(programmeFideliteCartes)) {
						for (ProgrammeFideliteCarte programmeFideliteCarte : programmeFideliteCartes) {
							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(programmeFideliteCarte);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;
						}
					}

				} else {
					if (Utilities.isNotEmpty(dto.getDatasProgrammeFideliteCarte())) {
						List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteForAllPointDeVente = programmeFideliteCarteRepository.findByEnseigneIdAndIsDispoInAllPdv(dto.getEnseigneId(), true, false) ;

						for (ProgrammeFideliteCarteDto data : dto.getDatasProgrammeFideliteCarte()) {

							ProgrammeFideliteCarte programmeFideliteCarte = programmeFideliteCarteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (programmeFideliteCarte == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + data.getLibelle(), locale));
								response.setHasError(true);
								return response;
							}
							if (Utilities.isNotEmpty(itemsProgrammeFideliteCarteForAllPointDeVente) && itemsProgrammeFideliteCarteForAllPointDeVente.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsProgrammeFideliteCarteForAllPointDeVente.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(programmeFideliteCarte);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;
						}
						if (Utilities.isNotEmpty(itemsProgrammeFideliteCarteForAllPointDeVente)) {
							itemsProgrammeFideliteCarteToUpdate.addAll(itemsProgrammeFideliteCarteForAllPointDeVente) ;
						}

					}
				}


				// liste des PDFT
				if (dto.getIsAllTamponDipso() != null && dto.getIsAllTamponDipso()) {

					List<ProgrammeFideliteTampon> programmeFideliteTampons = programmeFideliteTamponRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(programmeFideliteTampons)) {
						for (ProgrammeFideliteTampon programmeFideliteTampon : programmeFideliteTampons) {
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(programmeFideliteTampon);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasProgrammeFideliteTampon())) {
						List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponForAllPointDeVente = programmeFideliteTamponRepository.findByEnseigneIdAndIsDispoInAllPdv(dto.getEnseigneId(), true, false) ;

						for (ProgrammeFideliteTamponDto data : dto.getDatasProgrammeFideliteTampon()) {

							ProgrammeFideliteTampon programmeFideliteTampon = programmeFideliteTamponRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (programmeFideliteTampon == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + data.getLibelle(), locale));
								response.setHasError(true);
								return response;
							}
							if (Utilities.isNotEmpty(itemsProgrammeFideliteTamponForAllPointDeVente) && itemsProgrammeFideliteTamponForAllPointDeVente.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsProgrammeFideliteTamponForAllPointDeVente.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(programmeFideliteTampon);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;

						}

						if (Utilities.isNotEmpty(itemsProgrammeFideliteTamponForAllPointDeVente)) {
							itemsProgrammeFideliteTamponToUpdate.addAll(itemsProgrammeFideliteTamponForAllPointDeVente) ;
						}
					}
				}

			}

			if (!items.isEmpty()) {
				List<PointDeVente> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
					response.setHasError(true);
					return response;
				}

				// save itemsUserEnseignePointDeVente
				if (!itemsUserEnseignePointDeVente.isEmpty()) {
					List<UserEnseignePointDeVente> itemsUserEnseignePointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsUserEnseignePointDeVenteSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) itemsUserEnseignePointDeVente);
					if (itemsUserEnseignePointDeVenteSaved == null || itemsUserEnseignePointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("userEnseignePointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}

				// save pointDeVenteProgrammeFideliteTampon 
				if (!itemsPointDeVenteProgrammeFideliteTampon.isEmpty()) {
					List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsPointDeVenteProgrammeFideliteTampon);
					if (itemsPointDeVenteProgrammeFideliteTamponSaved == null || itemsPointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}				

				// save pointDeVenteProgrammeFideliteCarte
				if (!itemsPointDeVenteProgrammeFideliteCarte.isEmpty()) {
					List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsPointDeVenteProgrammeFideliteCarte);
					if (itemsPointDeVenteProgrammeFideliteCarteSaved == null || itemsPointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj programmeFideliteCarte
				if (!itemsProgrammeFideliteCarteToUpdate.isEmpty()) {
					for (ProgrammeFideliteCarte programmeFideliteCarte : itemsProgrammeFideliteCarteToUpdate) {
						programmeFideliteCarte.setIsDispoInAllPdv(false);
						programmeFideliteCarte.setUpdatedBy(request.getUser());
						programmeFideliteCarte.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsProgrammeFideliteCarteSaved = programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) itemsProgrammeFideliteCarteToUpdate);
					if (itemsProgrammeFideliteCarteSaved == null || itemsProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("programmeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj programmeFideliteTampon
				if (!itemsProgrammeFideliteTamponToUpdate.isEmpty()) {
					for (ProgrammeFideliteTampon programmeFideliteTampon : itemsProgrammeFideliteTamponToUpdate) {
						programmeFideliteTampon.setIsDispoInAllPdv(false);
						programmeFideliteTampon.setUpdatedBy(request.getUser());
						programmeFideliteTampon.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsProgrammeFideliteTamponSaved = programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) itemsProgrammeFideliteTamponToUpdate);
					if (itemsProgrammeFideliteTamponSaved == null || itemsProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("programmeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<PointDeVenteDto> itemsDto = new ArrayList<PointDeVenteDto>();
				for (PointDeVente entity : itemsSaved) {
					PointDeVenteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PointDeVenteDto> update(Request<PointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			// TODO a revoir pourquoi le role a changer pour l'admin
//			if (!utilisateur.getRole().getCode().equals(RoleEnum.ADMINISTRATEUR)) {
//				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<PointDeVente> items = new ArrayList<PointDeVente>();
			List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();
			List<PointDeVenteProgrammeFideliteCarte> itemsDeletePointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<PointDeVenteProgrammeFideliteTampon> itemsDeletePointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

			List<UserEnseignePointDeVente> itemsUserEnseignePointDeVente = new ArrayList<UserEnseignePointDeVente>();
			List<UserEnseignePointDeVente> itemsDeleteUserEnseignePointDeVente = new ArrayList<UserEnseignePointDeVente>();


			List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteToUpdate = new ArrayList<ProgrammeFideliteCarte>();
			List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponToUpdate = new ArrayList<ProgrammeFideliteTampon>();



			for (PointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pointDeVente existe
				PointDeVente entityToSave = null;
				entityToSave = pointDeVenteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}

					// verifier si l'enseigne est verrouillée
					if (existingEnseigne.getIsLocked() != null && existingEnseigne.getIsLocked()) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("enseigne -> " + existingEnseigne.getNom() + " est verrouillé. Veuillez finaliser l'inscription de l'enseigne !!!", locale));
						response.setHasError(true);
						return response;
					}

					// verifier l'etat de l'enseigne
					if (existingEnseigne.getIsAccepted() != null && !existingEnseigne.getIsAccepted()) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("l'enseigne -> " + existingEnseigne.getNom() + " n'est pas validée", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}
				
				// Verify if commune exist
				if (dto.getCommuneId() != null && dto.getCommuneId() > 0){
					Commune existingCommune = communeRepository.findById(dto.getCommuneId(), false);
					if (existingCommune == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("commune -> " + dto.getCommuneId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCommune(existingCommune);
				}
				
				// Verify if gpsPointDeVente exist
				if (dto.getDataGpsPointDeVente() != null) {

					GpsPointDeVente existingGpsPointDeVente = null ;

					Request<GpsPointDeVenteDto> rep = new Request<GpsPointDeVenteDto>(); 
					Response<GpsPointDeVenteDto> res = new Response<GpsPointDeVenteDto>(); 
					rep.setDatas(Arrays.asList(dto.getDataGpsPointDeVente()));
					rep.setUser(request.getUser());
					if (dto.getDataGpsPointDeVente().getId()!= null && dto.getDataGpsPointDeVente().getId() > 0){
						res = gpsPointDeVenteBusiness.update(rep, locale); 
					} else {
						res = gpsPointDeVenteBusiness.create(rep, locale); 
					}

					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					if (Utilities.isNotEmpty(res.getItems())) {
						existingGpsPointDeVente = gpsPointDeVenteRepository.findById(res.getItems().get(0).getId(), false) ;
					}
					entityToSave.setGpsPointDeVente(existingGpsPointDeVente);
				}

				// Verify if userEnseigne exist
				//UserEnseigne existingUserEnseigne = null ;

				if (Utilities.isNotEmpty(dto.getDatasUserEnseigne())) {
					List<UserDto> itemsUserEnseigneDtoToUpdate = new ArrayList<>() ;
					List<UserDto> itemsUserEnseigneDtoToCreate = new ArrayList<>() ;
					List<UserDto> allItemsUserEnseigneDto = new ArrayList<>() ;
					Request<UserDto> repToCreate = new Request<>(); 
					Response<UserDto> resToCreate = new Response<>(); 
					Request<UserDto> repToUpdate = new Request<>(); 
					Response<UserDto> resToUpdate = new Response<>(); 

					// recuperer les datas à supprimer
					List<UserEnseignePointDeVente> userEnseignePointDeVenteToDelete = userEnseignePointDeVenteRepository.findByPointDeVenteId(entityToSaveId, false) ;
					if (Utilities.isNotEmpty(userEnseignePointDeVenteToDelete)) {

						for (UserEnseignePointDeVente  userEnseignePointDeVente : userEnseignePointDeVenteToDelete) {
							userEnseignePointDeVente.setDeletedBy(request.getUser());
							userEnseignePointDeVente.setDeletedAt(Utilities.getCurrentDate());
							userEnseignePointDeVente.setIsDeleted(true);	
						}
						itemsDeleteUserEnseignePointDeVente.addAll(userEnseignePointDeVenteToDelete);
					}


					for (UserDto data : dto.getDatasUserEnseigne()) {

						if (data.getId() != null && data.getId() > 0) {

							if (itemsUserEnseigneDtoToUpdate.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du id '" + data.getId()+"' pour les userEnseignes", locale));
								response.setHasError(true);
								return response;
							}
							itemsUserEnseigneDtoToUpdate.add(data);
						} else {
							// generation du login/password
							String passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userRepository, GlobalEnum.longueurDefaultPassword) ;
							//String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne, data.getRoleId());
							String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne);							
							data.setLogin(loginGenerer);
							data.setPassword(passwordGenerer);
							data.setEnseigneId(dto.getEnseigneId());

							while (itemsUserEnseigneDtoToCreate.stream().anyMatch(a->a.getLogin().equals(data.getLogin()))) {
								//loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne, data.getRoleId());
								loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne);
								data.setLogin(loginGenerer);
							}
							while (itemsUserEnseigneDtoToCreate.stream().anyMatch(a->a.getPassword().equals(data.getPassword()))) {
								passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userRepository, GlobalEnum.longueurDefaultPassword) ;
								data.setPassword(passwordGenerer);
							}
							data.setIsUserPDV(true);
							itemsUserEnseigneDtoToCreate.add(data) ;	
						}

					}

					if (Utilities.isNotEmpty(itemsUserEnseigneDtoToCreate)) {
						repToCreate.setDatas(itemsUserEnseigneDtoToCreate);
						repToCreate.setUser(request.getUser());
						resToCreate = userBusiness.create(repToCreate, locale); 

						if (resToCreate != null && resToCreate.isHasError()) {
							response.setStatus(resToCreate.getStatus());
							response.setHasError(true);
							return response;
						}
						allItemsUserEnseigneDto.addAll(resToCreate.getItems()) ;
					}
					if (Utilities.isNotEmpty(itemsUserEnseigneDtoToUpdate)) {
						repToUpdate.setDatas(itemsUserEnseigneDtoToUpdate);
						repToUpdate.setUser(request.getUser());
						resToUpdate = userBusiness.update(repToUpdate, locale); 

						if (resToUpdate != null && resToUpdate.isHasError()) {
							response.setStatus(resToUpdate.getStatus());
							response.setHasError(true);
							return response;
						}
						allItemsUserEnseigneDto.addAll(resToUpdate.getItems()) ;
					}

					if (Utilities.isNotEmpty(allItemsUserEnseigneDto)) {
						for (UserDto userEnseigneDto : allItemsUserEnseigneDto) {							
							UserEnseignePointDeVente userEnseignePointDeVente = new UserEnseignePointDeVente() ;
							//UserEnseigne existingUserEnseigne = userEnseigneRepository.findById(userEnseigneDto.getId(), false);
							User existingUserEnseigne = userRepository.findById(userEnseigneDto.getId(), false);

							//userEnseignePointDeVente.setUserEnseigne(existingUserEnseigne);
							userEnseignePointDeVente.setUser(existingUserEnseigne);
							userEnseignePointDeVente.setPointDeVente(entityToSave);
							userEnseignePointDeVente.setCreatedBy(request.getUser());
							userEnseignePointDeVente.setCreatedAt(Utilities.getCurrentDate());
							userEnseignePointDeVente.setIsDeleted(false);

							itemsUserEnseignePointDeVente.add(userEnseignePointDeVente) ;
						}
						entityToSave.setUser(itemsUserEnseignePointDeVente.get(0).getUser());
					}	
				}

				/*
				if (dto.getDataUserEnseigne() != null) {

					Request<UserEnseigneDto> rep = new Request<UserEnseigneDto>(); 
					Response<UserEnseigneDto> res = new Response<UserEnseigneDto>(); 
					rep.setUser(request.getUser());

					if (dto.getDataUserEnseigne().getId()!= null && dto.getDataUserEnseigne().getId() > 0){
						rep.setDatas(Arrays.asList(dto.getDataUserEnseigne()));
						res = userEnseigneBusiness.update(rep, locale); 

					} else {
						// generation du login/password
						String passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userEnseigneRepository, GlobalEnum.longueurDefaultPassword) ;
						String loginGenerer = Utilities.generateLoginUserEnseigne(userEnseigneRepository, GlobalEnum.loginUserEnseigne);
						dto.getDataUserEnseigne().setLogin(loginGenerer);
						dto.getDataUserEnseigne().setPassword(passwordGenerer);
						rep.setDatas(Arrays.asList(dto.getDataUserEnseigne()));
						res = userEnseigneBusiness.create(rep, locale); 
					}


					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
					if (Utilities.isNotEmpty(res.getItems())) {
						existingUserEnseigne = userEnseigneRepository.findById(res.getItems().get(0).getId(), false) ;
					}
					entityToSave.setUserEnseigne(existingUserEnseigne);
				}
				 */


				if (dto.getIsAllTamponDipso() != null) {
					entityToSave.setIsAllTamponDipso(dto.getIsAllTamponDipso());
				}
				if (dto.getIsAllCarteDipso() != null) {
					entityToSave.setIsAllCarteDipso(dto.getIsAllCarteDipso());
				}
				if (Utilities.notBlank(dto.getLibelleCommune())) {
					entityToSave.setLibelleCommune(dto.getLibelleCommune());
				}
				if (Utilities.notBlank(dto.getVille())) {
					entityToSave.setVille(dto.getVille());
				}

				if (Utilities.notBlank(dto.getNom())) {
					PointDeVente existingEntity = pointDeVenteRepository.findPointDeVenteByEnseigneIdAndNom(entityToSave.getEnseigne().getId(), dto.getNom(), false);
					//PointDeVente existingEntity = pointDeVenteRepository.findPointDeVenteByNom(dto.getNom(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("pointDeVente -> " + dto.getNom(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les pointDeVentes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (Utilities.notBlank(dto.getCode())) {
					PointDeVente existingEntity = pointDeVenteRepository.findPointDeVenteByEnseigneIdAndCode(entityToSave.getEnseigne().getId(), dto.getCode(), false);

					//PointDeVente existingEntity = pointDeVenteRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("pointDeVente -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les pointDeVentes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}



				// liste des PDFC
				if (dto.getIsAllCarteDipso() != null && dto.getIsAllCarteDipso()) {

					// recuperer les datas à supprimer
					List<PointDeVenteProgrammeFideliteCarte> pointDeVenteProgrammeFideliteCartesToDelete = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(entityToSaveId, false) ;
					if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteCartesToDelete)) {

						for (PointDeVenteProgrammeFideliteCarte  pointDeVenteProgrammeFideliteCarte : pointDeVenteProgrammeFideliteCartesToDelete) {
							pointDeVenteProgrammeFideliteCarte.setDeletedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setDeletedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(true);	
						}
						itemsDeletePointDeVenteProgrammeFideliteCarte.addAll(pointDeVenteProgrammeFideliteCartesToDelete) ;
					}

					// datas à creer
					List<ProgrammeFideliteCarte> programmeFideliteCartes = programmeFideliteCarteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(programmeFideliteCartes)) {
						for (ProgrammeFideliteCarte programmeFideliteCarte : programmeFideliteCartes) {
							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(programmeFideliteCarte);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;
						}
					}

				} else {
					if (Utilities.isNotEmpty(dto.getDatasProgrammeFideliteCarte())) {

						// recuperer les datas à supprimer
						List<PointDeVenteProgrammeFideliteCarte> pointDeVenteProgrammeFideliteCartesToDelete = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(entityToSaveId, false) ;
						if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteCartesToDelete)) {

							for (PointDeVenteProgrammeFideliteCarte  pointDeVenteProgrammeFideliteCarte : pointDeVenteProgrammeFideliteCartesToDelete) {
								pointDeVenteProgrammeFideliteCarte.setDeletedBy(request.getUser());
								pointDeVenteProgrammeFideliteCarte.setDeletedAt(Utilities.getCurrentDate());
								pointDeVenteProgrammeFideliteCarte.setIsDeleted(true);	
							}
							itemsDeletePointDeVenteProgrammeFideliteCarte.addAll(pointDeVenteProgrammeFideliteCartesToDelete) ;
						}

						List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteForAllPointDeVente = programmeFideliteCarteRepository.findByEnseigneIdAndIsDispoInAllPdv(dto.getEnseigneId(), true, false) ;

						// datas à creer
						for (ProgrammeFideliteCarteDto data : dto.getDatasProgrammeFideliteCarte()) {

							ProgrammeFideliteCarte programmeFideliteCarte = programmeFideliteCarteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (programmeFideliteCarte == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + data.getLibelle(), locale));
								response.setHasError(true);
								return response;
							}

							if (Utilities.isNotEmpty(itemsProgrammeFideliteCarteForAllPointDeVente) && itemsProgrammeFideliteCarteForAllPointDeVente.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsProgrammeFideliteCarteForAllPointDeVente.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(programmeFideliteCarte);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;

						}
						if (Utilities.isNotEmpty(itemsProgrammeFideliteCarteForAllPointDeVente)) {
							itemsProgrammeFideliteCarteToUpdate.addAll(itemsProgrammeFideliteCarteForAllPointDeVente) ;
						}
					}
				}


				// liste des PDFT
				if (dto.getIsAllTamponDipso() != null && dto.getIsAllTamponDipso()) {

					// recuperer les datas à supprimer
					List<PointDeVenteProgrammeFideliteTampon> pointDeVenteProgrammeFideliteTamponsToDelete = pointDeVenteProgrammeFideliteTamponRepository.findByPointDeVenteId(entityToSaveId, false) ;
					if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteTamponsToDelete)) {

						for (PointDeVenteProgrammeFideliteTampon  pointDeVenteProgrammeFideliteTampon : pointDeVenteProgrammeFideliteTamponsToDelete) {
							pointDeVenteProgrammeFideliteTampon.setDeletedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setDeletedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(true);	
						}
						itemsDeletePointDeVenteProgrammeFideliteTampon.addAll(pointDeVenteProgrammeFideliteTamponsToDelete) ;
					}

					// datas à creer
					List<ProgrammeFideliteTampon> programmeFideliteTampons = programmeFideliteTamponRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(programmeFideliteTampons)) {
						for (ProgrammeFideliteTampon programmeFideliteTampon : programmeFideliteTampons) {
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(programmeFideliteTampon);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasProgrammeFideliteTampon())) {

						// recuperer les datas à supprimer
						List<PointDeVenteProgrammeFideliteTampon> pointDeVenteProgrammeFideliteTamponsToDelete = pointDeVenteProgrammeFideliteTamponRepository.findByPointDeVenteId(entityToSaveId, false) ;
						if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteTamponsToDelete)) {

							for (PointDeVenteProgrammeFideliteTampon  pointDeVenteProgrammeFideliteTampon : pointDeVenteProgrammeFideliteTamponsToDelete) {
								pointDeVenteProgrammeFideliteTampon.setDeletedBy(request.getUser());
								pointDeVenteProgrammeFideliteTampon.setDeletedAt(Utilities.getCurrentDate());
								pointDeVenteProgrammeFideliteTampon.setIsDeleted(true);	
							}
							itemsDeletePointDeVenteProgrammeFideliteTampon.addAll(pointDeVenteProgrammeFideliteTamponsToDelete) ;
						}

						List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponForAllPointDeVente = programmeFideliteTamponRepository.findByEnseigneIdAndIsDispoInAllPdv(dto.getEnseigneId(), true, false) ;

						// datas à creer
						for (ProgrammeFideliteTamponDto data : dto.getDatasProgrammeFideliteTampon()) {

							ProgrammeFideliteTampon programmeFideliteTampon = programmeFideliteTamponRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (programmeFideliteTampon == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + data.getLibelle(), locale));
								response.setHasError(true);
								return response;
							}
							if (Utilities.isNotEmpty(itemsProgrammeFideliteTamponForAllPointDeVente) && itemsProgrammeFideliteTamponForAllPointDeVente.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsProgrammeFideliteTamponForAllPointDeVente.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(programmeFideliteTampon);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;

						}
						if (Utilities.isNotEmpty(itemsProgrammeFideliteTamponForAllPointDeVente)) {
							itemsProgrammeFideliteTamponToUpdate.addAll(itemsProgrammeFideliteTamponForAllPointDeVente) ;
						}
					}
				}


				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PointDeVente> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
					response.setHasError(true);
					return response;
				}

				// save userEnseignePointDeVente 
				if (!itemsDeleteUserEnseignePointDeVente.isEmpty()) { // supppression
					List<UserEnseignePointDeVente> itemsDeleteUserEnseignePointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsDeleteUserEnseignePointDeVenteSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) itemsDeleteUserEnseignePointDeVente);
					if (itemsDeleteUserEnseignePointDeVenteSaved == null || itemsDeleteUserEnseignePointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("userEnseignePointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsUserEnseignePointDeVente.isEmpty()) { //creation
					List<UserEnseignePointDeVente> itemsUserEnseignePointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsUserEnseignePointDeVenteSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) itemsUserEnseignePointDeVente);
					if (itemsUserEnseignePointDeVenteSaved == null || itemsUserEnseignePointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("userEnseignePointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}


				// save pointDeVenteProgrammeFideliteTampon 
				if (!itemsDeletePointDeVenteProgrammeFideliteTampon.isEmpty()) { // supppression
					List<PointDeVenteProgrammeFideliteTampon> itemsDeletePointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsDeletePointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsDeletePointDeVenteProgrammeFideliteTampon);
					if (itemsDeletePointDeVenteProgrammeFideliteTamponSaved == null || itemsDeletePointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsPointDeVenteProgrammeFideliteTampon.isEmpty()) { //creation
					List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsPointDeVenteProgrammeFideliteTampon);
					if (itemsPointDeVenteProgrammeFideliteTamponSaved == null || itemsPointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				// save pointDeVenteProgrammeFideliteCarte
				if (!itemsDeletePointDeVenteProgrammeFideliteCarte.isEmpty()) { // supppression
					List<PointDeVenteProgrammeFideliteCarte> itemsDelelePointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsDelelePointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsDeletePointDeVenteProgrammeFideliteCarte);
					if (itemsDelelePointDeVenteProgrammeFideliteCarteSaved == null || itemsDelelePointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsPointDeVenteProgrammeFideliteCarte.isEmpty()) { //creation
					List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsPointDeVenteProgrammeFideliteCarte);
					if (itemsPointDeVenteProgrammeFideliteCarteSaved == null || itemsPointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj programmeFideliteCarte
				if (!itemsProgrammeFideliteCarteToUpdate.isEmpty()) {
					for (ProgrammeFideliteCarte programmeFideliteCarte : itemsProgrammeFideliteCarteToUpdate) {
						programmeFideliteCarte.setIsDispoInAllPdv(false);
						programmeFideliteCarte.setUpdatedBy(request.getUser());
						programmeFideliteCarte.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<ProgrammeFideliteCarte> itemsProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsProgrammeFideliteCarteSaved = programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) itemsProgrammeFideliteCarteToUpdate);
					if (itemsProgrammeFideliteCarteSaved == null || itemsProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("programmeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj programmeFideliteTampon
				if (!itemsProgrammeFideliteTamponToUpdate.isEmpty()) {
					for (ProgrammeFideliteTampon programmeFideliteTampon : itemsProgrammeFideliteTamponToUpdate) {
						programmeFideliteTampon.setIsDispoInAllPdv(false);
						programmeFideliteTampon.setUpdatedBy(request.getUser());
						programmeFideliteTampon.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<ProgrammeFideliteTampon> itemsProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsProgrammeFideliteTamponSaved = programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) itemsProgrammeFideliteTamponToUpdate);
					if (itemsProgrammeFideliteTamponSaved == null || itemsProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("programmeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<PointDeVenteDto> itemsDto = new ArrayList<PointDeVenteDto>();
				for (PointDeVente entity : itemsSaved) {
					PointDeVenteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * changerUserPDV PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<PointDeVenteDto> changerUserPDV(Request<PointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerUserPDV PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			// TODO a revoir pourquoi le role a changer pour l'admin
//			if (!utilisateur.getRole().getCode().equals(RoleEnum.ADMINISTRATEUR)) {
//				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<PointDeVente> items = new ArrayList<PointDeVente>();
			//List<PointDeVente> itemsUpdatePointDeVente = new ArrayList<PointDeVente>();
			List<UserEnseignePointDeVente> itemsUpdateUserEnseignePointDeVente = new ArrayList<UserEnseignePointDeVente>();
			List<UserEnseignePointDeVente> itemsCreateUserEnseignePointDeVente = new ArrayList<UserEnseignePointDeVente>();




			for (PointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("id", dto.getId());
				fieldsToVerify.put("userEnseigneId", dto.getUserEnseigneId());
				fieldsToVerify.put("newPointDeVenteId", dto.getNewPointDeVenteId());
				fieldsToVerify.put("oldPointDeVenteId", dto.getOldPointDeVenteId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pointDeVente existe
				//PointDeVente entityToSave = null;
				PointDeVente oldEntityToSave = null;

				PointDeVente newEntityToSave = null;

				newEntityToSave = pointDeVenteRepository.findById(dto.getNewPointDeVenteId(), false);
				if (newEntityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getNewPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}
				oldEntityToSave = pointDeVenteRepository.findById(dto.getOldPointDeVenteId(), false);
				if (oldEntityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getOldPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer oldEntityToSaveId = oldEntityToSave.getId();
				Integer newEntityToSaveId = newEntityToSave.getId();


				// Verify if userEnseigne exist
				/*
				if (dto.getUserEnseigneId() != null && dto.getUserEnseigneId() > 0){
					UserEnseigne existingUserEnseigne = userEnseigneRepository.findById(dto.getUserEnseigneId(), false);
					if (existingUserEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("userEnseigne -> " + dto.getUserEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}

					// verifier si userEnseigne a deja un PDV
					//PointDeVente existingPointDeVenteByUserEnseigne  = pointDeVenteRepository.findPointDeVenteByUserEnseigneId(dto.getUserEnseigneId(), false) ;
					List<UserEnseignePointDeVente> existingOldUserEnseignePointDeVente  = userEnseignePointDeVenteRepository.findByUserEnseigneIdAndPointDeVenteId(dto.getUserEnseigneId(), dto.getOldPointDeVenteId(), false) ;


					// List<UserEnseignePointDeVente> existingNewUserEnseignePointDeVente  = pointDeVenteRepository.findByUserEnseigneId(dto.getUserEnseigneId(), false) ;

					if (Utilities.isNotEmpty(existingPointDeVenteByUserEnseigne)) {
						for (PointDeVente pointDeVente : existingPointDeVenteByUserEnseigne) {
							pointDeVente.setUserEnseigne(null);
							pointDeVente.setUpdatedBy(request.getUser());
							pointDeVente.setUpdatedAt(Utilities.getCurrentDate());							
						}	
						itemsUpdatePointDeVente.addAll(existingPointDeVenteByUserEnseigne); 
					}
					entityToSave.setUserEnseigne(existingUserEnseigne);
				}
				 */			

				if (dto.getUserEnseigneId() != null && dto.getUserEnseigneId() > 0){
					//UserEnseigne existingUserEnseigne = userEnseigneRepository.findById(dto.getUserEnseigneId(), false);
					User existingUserEnseigne = userRepository.findById(dto.getUserEnseigneId(), false);
					if (existingUserEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("userEnseigne -> " + dto.getUserEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}

					// verifier si userEnseigne a deja un PDV
					//PointDeVente existingPointDeVenteByUserEnseigne  = pointDeVenteRepository.findPointDeVenteByUserEnseigneId(dto.getUserEnseigneId(), false) ;
					List<UserEnseignePointDeVente> existingOldUserEnseignePointDeVente  = userEnseignePointDeVenteRepository.findByUserEnseigneIdAndPointDeVenteId(dto.getUserEnseigneId(), oldEntityToSaveId, false) ;


					// List<UserEnseignePointDeVente> existingNewUserEnseignePointDeVente  = pointDeVenteRepository.findByUserEnseigneId(dto.getUserEnseigneId(), false) ;

					if (Utilities.isNotEmpty(existingOldUserEnseignePointDeVente)) {
						for (UserEnseignePointDeVente userEnseignePointDeVente : existingOldUserEnseignePointDeVente) {
							//userEnseignePointDeVente.setUserEnseigne(null);
							userEnseignePointDeVente.setUser(null);
							userEnseignePointDeVente.setUpdatedBy(request.getUser());
							userEnseignePointDeVente.setUpdatedAt(Utilities.getCurrentDate());							
						}	
						itemsUpdateUserEnseignePointDeVente.addAll(existingOldUserEnseignePointDeVente); 
					}

					UserEnseignePointDeVente newUserEnseignePointDeVente = new UserEnseignePointDeVente() ;

					//newUserEnseignePointDeVente.setUserEnseigne(existingUserEnseigne);
					newUserEnseignePointDeVente.setUser(existingUserEnseigne);
					newUserEnseignePointDeVente.setPointDeVente(newEntityToSave);
					newUserEnseignePointDeVente.setCreatedBy(request.getUser());
					newUserEnseignePointDeVente.setCreatedAt(Utilities.getCurrentDate());
					newUserEnseignePointDeVente.setIsDeleted(false);

					itemsCreateUserEnseignePointDeVente.add(newUserEnseignePointDeVente) ;

				}								
			}

			if (!itemsUpdateUserEnseignePointDeVente.isEmpty() || !itemsCreateUserEnseignePointDeVente.isEmpty()) {
				/*
				List<PointDeVente> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
					response.setHasError(true);
					return response;
				}
				 */

				// save userEnseignePointDeVente
				if (!itemsUpdateUserEnseignePointDeVente.isEmpty()) {
					List<UserEnseignePointDeVente> itemsUpdateUserEnseignePointDeVenteSaved = null;

					// inserer les donnees en base de donnees
					itemsUpdateUserEnseignePointDeVenteSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) itemsUpdateUserEnseignePointDeVente);
					if (itemsUpdateUserEnseignePointDeVenteSaved == null || itemsUpdateUserEnseignePointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<UserEnseignePointDeVente> itemsCreateUserEnseignePointDeVenteSaved = null;

				// save userEnseignePointDeVente
				if (!itemsCreateUserEnseignePointDeVente.isEmpty()) {

					// inserer les donnees en base de donnees
					itemsCreateUserEnseignePointDeVenteSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) itemsCreateUserEnseignePointDeVente);
					if (itemsCreateUserEnseignePointDeVenteSaved == null || itemsCreateUserEnseignePointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<PointDeVenteDto> itemsDto = new ArrayList<PointDeVenteDto>();
				for (UserEnseignePointDeVente userEnseignePointDeVente : itemsCreateUserEnseignePointDeVenteSaved) {
					PointDeVenteDto dto = getFullInfos(userEnseignePointDeVente.getPointDeVente(), itemsCreateUserEnseignePointDeVenteSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end changerUserPDV PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PointDeVenteDto> delete(Request<PointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			// TODO a revoir pourquoi le role a changer pour l'admin
//			if (!utilisateur.getRole().getCode().equals(RoleEnum.ADMINISTRATEUR)) {
//				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<PointDeVente> items = new ArrayList<PointDeVente>();

			for (PointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pointDeVente existe
				PointDeVente existingEntity = null;
				existingEntity = pointDeVenteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDelitProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pointDeVenteProgrammeFideliteTampon
				List<PointDeVenteProgrammeFideliteTampon> listOfPointDeVenteProgrammeFideliteTampon = pointDeVenteProgrammeFideliteTamponRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteTampon != null && !listOfPointDeVenteProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPointDeVenteProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pointDeVenteProgrammeFideliteCarte
				List<PointDeVenteProgrammeFideliteCarte> listOfPointDeVenteProgrammeFideliteCarte = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteCarte != null && !listOfPointDeVenteProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPointDeVenteProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				
				// userEnseignePointDeVente
				List<UserEnseignePointDeVente> listOfUserEnseignePointDeVente = userEnseignePointDeVenteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfUserEnseignePointDeVente != null && !listOfUserEnseignePointDeVente.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserEnseignePointDeVente.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				pointDeVenteRepository.saveAll((Iterable<PointDeVente>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<PointDeVenteDto> forceDelete(Request<PointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}
			// TODO a revoir pourquoi le role a changer pour l'admin
//			if (!utilisateur.getRole().getCode().equals(RoleEnum.ADMINISTRATEUR)) {
//				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<PointDeVente> items = new ArrayList<PointDeVente>();

			for (PointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la pointDeVente existe
				PointDeVente existingEntity = null;
				existingEntity = pointDeVenteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					Request<DelitProgrammeFideliteDto> deleteRequest = new Request<DelitProgrammeFideliteDto>();
					deleteRequest.setDatas(DelitProgrammeFideliteTransformer.INSTANCE.toDtos(listOfDelitProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<DelitProgrammeFideliteDto> deleteResponse = delitProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// pointDeVenteProgrammeFideliteTampon
				List<PointDeVenteProgrammeFideliteTampon> listOfPointDeVenteProgrammeFideliteTampon = pointDeVenteProgrammeFideliteTamponRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteTampon != null && !listOfPointDeVenteProgrammeFideliteTampon.isEmpty()){
					Request<PointDeVenteProgrammeFideliteTamponDto> deleteRequest = new Request<PointDeVenteProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(PointDeVenteProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfPointDeVenteProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<PointDeVenteProgrammeFideliteTamponDto> deleteResponse = pointDeVenteProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteTamponDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteTamponDto> deleteResponse = historiqueCarteProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// pointDeVenteProgrammeFideliteCarte
				List<PointDeVenteProgrammeFideliteCarte> listOfPointDeVenteProgrammeFideliteCarte = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteCarte != null && !listOfPointDeVenteProgrammeFideliteCarte.isEmpty()){
					Request<PointDeVenteProgrammeFideliteCarteDto> deleteRequest = new Request<PointDeVenteProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(PointDeVenteProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfPointDeVenteProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<PointDeVenteProgrammeFideliteCarteDto> deleteResponse = pointDeVenteProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteCarteDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteCarteDto> deleteResponse = historiqueCarteProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// userEnseignePointDeVente
				List<UserEnseignePointDeVente> listOfUserEnseignePointDeVente = userEnseignePointDeVenteRepository.findByPointDeVenteId(existingEntity.getId(), false);
				if (listOfUserEnseignePointDeVente != null && !listOfUserEnseignePointDeVente.isEmpty()){
					Request<UserEnseignePointDeVenteDto> deleteRequest = new Request<UserEnseignePointDeVenteDto>();
					deleteRequest.setDatas(UserEnseignePointDeVenteTransformer.INSTANCE.toDtos(listOfUserEnseignePointDeVente));
					deleteRequest.setUser(request.getUser());
					Response<UserEnseignePointDeVenteDto> deleteResponse = userEnseignePointDeVenteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				pointDeVenteRepository.saveAll((Iterable<PointDeVente>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get PointDeVente by using PointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<PointDeVenteDto> getByCriteria(Request<PointDeVenteDto> request, Locale locale) {
		slf4jLogger.info("----begin get PointDeVente-----");

		response = new Response<PointDeVenteDto>();

		try {
			List<PointDeVente> items = null;
			items = pointDeVenteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<PointDeVenteDto> itemsDto = new ArrayList<PointDeVenteDto>();
				if (request.isStat()) {
					PointDeVenteDto dto = new PointDeVenteDto() ;
					dto.setNom(GlobalEnum.AUCUN);
					itemsDto.add(dto);
				}
				for (PointDeVente entity : items) {
					PointDeVenteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				
				response.setItems(itemsDto);
				response.setCount(pointDeVenteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				if (request.isStat()) {
					PointDeVenteDto dto = new PointDeVenteDto() ;
					dto.setNom(GlobalEnum.AUCUN);
					response.setItems(Arrays.asList(dto));
				}
				response.setStatus(functionalError.DATA_EMPTY("pointDeVente", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get PointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full PointDeVenteDto by using PointDeVente as object.
	 *
	 * @param entity, locale
	 * @return PointDeVenteDto
	 *
	 */
	private PointDeVenteDto getFullInfos(PointDeVente entity, Integer size, Locale locale) throws Exception {
		PointDeVenteDto dto = PointDeVenteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		// info GPS
		GpsPointDeVente gpsPointDeVente = gpsPointDeVenteRepository.findById(dto.getGpsPointDeVenteId(), false) ;
		if (gpsPointDeVente != null) {
			dto.setDataGpsPointDeVente(GpsPointDeVenteTransformer.INSTANCE.toDto(gpsPointDeVente));
		}
		List<ProgrammeFideliteCarte> datasProgrammeFideliteCarte = pointDeVenteProgrammeFideliteCarteRepository.findAllProgrammeFideliteCarteByPointDeVenteId(dto.getId(), false) ;
		dto.setNbreProgrammeFideliteCarte(Utilities.isNotEmpty(datasProgrammeFideliteCarte) ? datasProgrammeFideliteCarte.size() : 0);

		List<ProgrammeFideliteTampon> datasProgrammeFideliteTampon = pointDeVenteProgrammeFideliteTamponRepository.findAllProgrammeFideliteTamponByPointDeVenteId(dto.getId(), false) ;
		dto.setNbreProgrammeFideliteTampon(Utilities.isNotEmpty(datasProgrammeFideliteTampon) ? datasProgrammeFideliteTampon.size() : 0);

		List<User> datasUserEnseigne = userEnseignePointDeVenteRepository.findAllUserEnseigneByPointDeVenteId(dto.getId(), false) ;
		dto.setNbreUserEnseigne(Utilities.isNotEmpty(datasUserEnseigne) ? datasUserEnseigne.size() : 0);


		if (size > 1) {
			return dto;
		}

		// renvoi des listes de PDFCARTE
		if (Utilities.isNotEmpty(datasProgrammeFideliteCarte)) {
			dto.setDatasProgrammeFideliteCarte(ProgrammeFideliteCarteTransformer.INSTANCE.toDtos(datasProgrammeFideliteCarte)) ;
		}

		// renvoi des listes de PDFTAMPON
		if (Utilities.isNotEmpty(datasProgrammeFideliteTampon)) {
			dto.setDatasProgrammeFideliteTampon(ProgrammeFideliteTamponTransformer.INSTANCE.toDtos(datasProgrammeFideliteTampon)) ;
		}

		// info userEnseigne
		/*
		UserEnseigne userEnseigne = userEnseigneRepository.findById(dto.getUserEnseigneId(), false) ;
		if (userEnseigne != null) {
			dto.setDataUserEnseigne(UserEnseigneTransformer.INSTANCE.toDto(userEnseigne));
		}
		 */

		// renvoi des listes de UserEnseigne
		if (Utilities.isNotEmpty(datasUserEnseigne)) {
			dto.setDatasUserEnseigne(UserTransformer.INSTANCE.toDtos(datasUserEnseigne)) ;
		}

		// info enseigne
		Enseigne enseigne = enseigneRepository.findById(dto.getEnseigneId(), false) ;
		if (enseigne != null) {
			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(enseigne));
		}

		return dto;
	}
}
