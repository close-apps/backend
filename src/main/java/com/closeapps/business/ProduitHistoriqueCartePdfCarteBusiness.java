


                                                                          /*
 * Java transformer for entity table produit_historique_carte_pdf_carte
 * Created on 2020-08-17 ( Time 14:14:49 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "produit_historique_carte_pdf_carte"
 *
* @author Back-End developper
   *
 */
@Component
public class ProduitHistoriqueCartePdfCarteBusiness implements IBasicBusiness<Request<ProduitHistoriqueCartePdfCarteDto>, Response<ProduitHistoriqueCartePdfCarteDto>> {

  private Response<ProduitHistoriqueCartePdfCarteDto> response;
  @Autowired
  private ProduitHistoriqueCartePdfCarteRepository produitHistoriqueCartePdfCarteRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
    @Autowired
  private ProduitRepository produitRepository;
  
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public ProduitHistoriqueCartePdfCarteBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create ProduitHistoriqueCartePdfCarte by using ProduitHistoriqueCartePdfCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<ProduitHistoriqueCartePdfCarteDto> create(Request<ProduitHistoriqueCartePdfCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin create ProduitHistoriqueCartePdfCarte-----");

    response = new Response<ProduitHistoriqueCartePdfCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<ProduitHistoriqueCartePdfCarte> items = new ArrayList<ProduitHistoriqueCartePdfCarte>();

      for (ProduitHistoriqueCartePdfCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("produitId", dto.getProduitId());
        fieldsToVerify.put("historiqueCarteProgrammeFideliteCarteId", dto.getHistoriqueCarteProgrammeFideliteCarteId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if produitHistoriqueCartePdfCarte to insert do not exist
        ProduitHistoriqueCartePdfCarte existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("produitHistoriqueCartePdfCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if historiqueCarteProgrammeFideliteCarte exist
                HistoriqueCarteProgrammeFideliteCarte existingHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findById(dto.getHistoriqueCarteProgrammeFideliteCarteId(), false);
                if (existingHistoriqueCarteProgrammeFideliteCarte == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getHistoriqueCarteProgrammeFideliteCarteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if produit exist
                Produit existingProduit = produitRepository.findById(dto.getProduitId(), false);
                if (existingProduit == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getProduitId(), locale));
          response.setHasError(true);
          return response;
        }
        ProduitHistoriqueCartePdfCarte entityToSave = null;
        entityToSave = ProduitHistoriqueCartePdfCarteTransformer.INSTANCE.toEntity(dto, existingHistoriqueCarteProgrammeFideliteCarte, existingProduit);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<ProduitHistoriqueCartePdfCarte> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = produitHistoriqueCartePdfCarteRepository.saveAll((Iterable<ProduitHistoriqueCartePdfCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("produitHistoriqueCartePdfCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<ProduitHistoriqueCartePdfCarteDto> itemsDto = new ArrayList<ProduitHistoriqueCartePdfCarteDto>();
        for (ProduitHistoriqueCartePdfCarte entity : itemsSaved) {
          ProduitHistoriqueCartePdfCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create ProduitHistoriqueCartePdfCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update ProduitHistoriqueCartePdfCarte by using ProduitHistoriqueCartePdfCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<ProduitHistoriqueCartePdfCarteDto> update(Request<ProduitHistoriqueCartePdfCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin update ProduitHistoriqueCartePdfCarte-----");

    response = new Response<ProduitHistoriqueCartePdfCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<ProduitHistoriqueCartePdfCarte> items = new ArrayList<ProduitHistoriqueCartePdfCarte>();

      for (ProduitHistoriqueCartePdfCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la produitHistoriqueCartePdfCarte existe
        ProduitHistoriqueCartePdfCarte entityToSave = null;
        entityToSave = produitHistoriqueCartePdfCarteRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("produitHistoriqueCartePdfCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if historiqueCarteProgrammeFideliteCarte exist
        if (dto.getHistoriqueCarteProgrammeFideliteCarteId() != null && dto.getHistoriqueCarteProgrammeFideliteCarteId() > 0){
                    HistoriqueCarteProgrammeFideliteCarte existingHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findById(dto.getHistoriqueCarteProgrammeFideliteCarteId(), false);
          if (existingHistoriqueCarteProgrammeFideliteCarte == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getHistoriqueCarteProgrammeFideliteCarteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setHistoriqueCarteProgrammeFideliteCarte(existingHistoriqueCarteProgrammeFideliteCarte);
        }
        // Verify if produit exist
        if (dto.getProduitId() != null && dto.getProduitId() > 0){
                    Produit existingProduit = produitRepository.findById(dto.getProduitId(), false);
          if (existingProduit == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getProduitId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setProduit(existingProduit);
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<ProduitHistoriqueCartePdfCarte> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = produitHistoriqueCartePdfCarteRepository.saveAll((Iterable<ProduitHistoriqueCartePdfCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("produitHistoriqueCartePdfCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<ProduitHistoriqueCartePdfCarteDto> itemsDto = new ArrayList<ProduitHistoriqueCartePdfCarteDto>();
        for (ProduitHistoriqueCartePdfCarte entity : itemsSaved) {
          ProduitHistoriqueCartePdfCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update ProduitHistoriqueCartePdfCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete ProduitHistoriqueCartePdfCarte by using ProduitHistoriqueCartePdfCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<ProduitHistoriqueCartePdfCarteDto> delete(Request<ProduitHistoriqueCartePdfCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete ProduitHistoriqueCartePdfCarte-----");

    response = new Response<ProduitHistoriqueCartePdfCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<ProduitHistoriqueCartePdfCarte> items = new ArrayList<ProduitHistoriqueCartePdfCarte>();

      for (ProduitHistoriqueCartePdfCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la produitHistoriqueCartePdfCarte existe
        ProduitHistoriqueCartePdfCarte existingEntity = null;
        existingEntity = produitHistoriqueCartePdfCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("produitHistoriqueCartePdfCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        produitHistoriqueCartePdfCarteRepository.saveAll((Iterable<ProduitHistoriqueCartePdfCarte>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete ProduitHistoriqueCartePdfCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete ProduitHistoriqueCartePdfCarte by using ProduitHistoriqueCartePdfCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<ProduitHistoriqueCartePdfCarteDto> forceDelete(Request<ProduitHistoriqueCartePdfCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete ProduitHistoriqueCartePdfCarte-----");

    response = new Response<ProduitHistoriqueCartePdfCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<ProduitHistoriqueCartePdfCarte> items = new ArrayList<ProduitHistoriqueCartePdfCarte>();

      for (ProduitHistoriqueCartePdfCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la produitHistoriqueCartePdfCarte existe
        ProduitHistoriqueCartePdfCarte existingEntity = null;
          existingEntity = produitHistoriqueCartePdfCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("produitHistoriqueCartePdfCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          produitHistoriqueCartePdfCarteRepository.saveAll((Iterable<ProduitHistoriqueCartePdfCarte>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete ProduitHistoriqueCartePdfCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get ProduitHistoriqueCartePdfCarte by using ProduitHistoriqueCartePdfCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<ProduitHistoriqueCartePdfCarteDto> getByCriteria(Request<ProduitHistoriqueCartePdfCarteDto> request, Locale locale) {
    slf4jLogger.info("----begin get ProduitHistoriqueCartePdfCarte-----");

    response = new Response<ProduitHistoriqueCartePdfCarteDto>();

    try {
      List<ProduitHistoriqueCartePdfCarte> items = null;
      items = produitHistoriqueCartePdfCarteRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<ProduitHistoriqueCartePdfCarteDto> itemsDto = new ArrayList<ProduitHistoriqueCartePdfCarteDto>();
        for (ProduitHistoriqueCartePdfCarte entity : items) {
          ProduitHistoriqueCartePdfCarteDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(produitHistoriqueCartePdfCarteRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("produitHistoriqueCartePdfCarte", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get ProduitHistoriqueCartePdfCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full ProduitHistoriqueCartePdfCarteDto by using ProduitHistoriqueCartePdfCarte as object.
   *
   * @param entity, locale
   * @return ProduitHistoriqueCartePdfCarteDto
   *
   */
  private ProduitHistoriqueCartePdfCarteDto getFullInfos(ProduitHistoriqueCartePdfCarte entity, Integer size, Locale locale) throws Exception {
    ProduitHistoriqueCartePdfCarteDto dto = ProduitHistoriqueCartePdfCarteTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
