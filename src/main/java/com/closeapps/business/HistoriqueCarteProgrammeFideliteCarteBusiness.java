


                                                                                                                    /*
 * Java transformer for entity table historique_carte_programme_fidelite_carte
 * Created on 2020-08-17 ( Time 14:14:46 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "historique_carte_programme_fidelite_carte"
 *
* @author Back-End developper
   *
 */
@Component
public class HistoriqueCarteProgrammeFideliteCarteBusiness implements IBasicBusiness<Request<HistoriqueCarteProgrammeFideliteCarteDto>, Response<HistoriqueCarteProgrammeFideliteCarteDto>> {

  private Response<HistoriqueCarteProgrammeFideliteCarteDto> response;
  @Autowired
  private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
    @Autowired
  private TypeActionRepository typeActionRepository;
    @Autowired
  private CarteRepository carteRepository;
    @Autowired
  private PointDeVenteRepository pointDeVenteRepository;
    @Autowired
    private ProduitHistoriqueCartePdfCarteRepository produitHistoriqueCartePdfCarteRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                                      
  @Autowired
  private ProduitHistoriqueCartePdfCarteBusiness produitHistoriqueCartePdfCarteBusiness;

      

  public HistoriqueCarteProgrammeFideliteCarteBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create HistoriqueCarteProgrammeFideliteCarte by using HistoriqueCarteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteCarteDto> create(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin create HistoriqueCarteProgrammeFideliteCarte-----");

    response = new Response<HistoriqueCarteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteCarte> items = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

      for (HistoriqueCarteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("carteId", dto.getCarteId());
        fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
        fieldsToVerify.put("typeActionId", dto.getTypeActionId());
//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
//        fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
        fieldsToVerify.put("nbrePointOperation", dto.getNbrePointOperation());
        fieldsToVerify.put("nbrePointActuel", dto.getNbrePointActuel());
        fieldsToVerify.put("dateAction", dto.getDateAction());
        fieldsToVerify.put("heureAction", dto.getHeureAction());
        fieldsToVerify.put("nbrePointAvantAction", dto.getNbrePointAvantAction());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if historiqueCarteProgrammeFideliteCarte to insert do not exist
        HistoriqueCarteProgrammeFideliteCarte existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if programmeFideliteCarte exist
                ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
                if (existingProgrammeFideliteCarte == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if typeAction exist
                TypeAction existingTypeAction = typeActionRepository.findById(dto.getTypeActionId(), false);
                if (existingTypeAction == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if carte exist
                Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
                if (existingCarte == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if pointDeVente exist
                PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
                if (existingPointDeVente == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
          response.setHasError(true);
          return response;
        }
        HistoriqueCarteProgrammeFideliteCarte entityToSave = null;
        entityToSave = HistoriqueCarteProgrammeFideliteCarteTransformer.INSTANCE.toEntity(dto, existingProgrammeFideliteCarte, existingTypeAction, existingCarte, existingPointDeVente);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteCarte> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<HistoriqueCarteProgrammeFideliteCarteDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteCarteDto>();
        for (HistoriqueCarteProgrammeFideliteCarte entity : itemsSaved) {
          HistoriqueCarteProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create HistoriqueCarteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update HistoriqueCarteProgrammeFideliteCarte by using HistoriqueCarteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteCarteDto> update(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin update HistoriqueCarteProgrammeFideliteCarte-----");

    response = new Response<HistoriqueCarteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteCarte> items = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

      for (HistoriqueCarteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la historiqueCarteProgrammeFideliteCarte existe
        HistoriqueCarteProgrammeFideliteCarte entityToSave = null;
        entityToSave = historiqueCarteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if programmeFideliteCarte exist
        if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
                    ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
          if (existingProgrammeFideliteCarte == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
        }
        // Verify if typeAction exist
        if (dto.getTypeActionId() != null && dto.getTypeActionId() > 0){
                    TypeAction existingTypeAction = typeActionRepository.findById(dto.getTypeActionId(), false);
          if (existingTypeAction == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setTypeAction(existingTypeAction);
        }
        // Verify if carte exist
        if (dto.getCarteId() != null && dto.getCarteId() > 0){
                    Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
          if (existingCarte == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setCarte(existingCarte);
        }
        // Verify if pointDeVente exist
        if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0){
                    PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
          if (existingPointDeVente == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPointDeVente(existingPointDeVente);
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getSommeOperation() != null && dto.getSommeOperation() > 0) {
          entityToSave.setSommeOperation(dto.getSommeOperation());
        }
        if (dto.getNbrePointOperation() != null && dto.getNbrePointOperation() > 0) {
            entityToSave.setNbrePointOperation(dto.getNbrePointOperation());
          }
        if (dto.getChiffreDaffaire() != null && dto.getChiffreDaffaire() > 0) {
            entityToSave.setChiffreDaffaire(dto.getChiffreDaffaire());
          }
        if (dto.getNbrePointActuel() != null && dto.getNbrePointActuel() > 0) {
          entityToSave.setNbrePointActuel(dto.getNbrePointActuel());
        }
        if (Utilities.notBlank(dto.getDateAction())) {
          entityToSave.setDateAction(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateAction()));
        }
        if (Utilities.notBlank(dto.getHeureAction())) {
          entityToSave.setHeureAction(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getHeureAction()));
        }
        if (dto.getNbrePointAvantAction() != null && dto.getNbrePointAvantAction() > 0) {
          entityToSave.setNbrePointAvantAction(dto.getNbrePointAvantAction());
        }
        if (Utilities.notBlank(dto.getDescription())) {
			entityToSave.setDescription(dto.getDescription());
		}
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteCarte> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
          response.setHasError(true);
          return response;
        }
        List<HistoriqueCarteProgrammeFideliteCarteDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteCarteDto>();
        for (HistoriqueCarteProgrammeFideliteCarte entity : itemsSaved) {
          HistoriqueCarteProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update HistoriqueCarteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete HistoriqueCarteProgrammeFideliteCarte by using HistoriqueCarteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteCarteDto> delete(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete HistoriqueCarteProgrammeFideliteCarte-----");

    response = new Response<HistoriqueCarteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteCarte> items = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

      for (HistoriqueCarteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la historiqueCarteProgrammeFideliteCarte existe
        HistoriqueCarteProgrammeFideliteCarte existingEntity = null;
        existingEntity = historiqueCarteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // produitHistoriqueCartePdfCarte
        List<ProduitHistoriqueCartePdfCarte> listOfProduitHistoriqueCartePdfCarte = produitHistoriqueCartePdfCarteRepository.findByHistoriqueCarteProgrammeFideliteCarteId(existingEntity.getId(), false);
        if (listOfProduitHistoriqueCartePdfCarte != null && !listOfProduitHistoriqueCartePdfCarte.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProduitHistoriqueCartePdfCarte.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete HistoriqueCarteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete HistoriqueCarteProgrammeFideliteCarte by using HistoriqueCarteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<HistoriqueCarteProgrammeFideliteCarteDto> forceDelete(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete HistoriqueCarteProgrammeFideliteCarte-----");

    response = new Response<HistoriqueCarteProgrammeFideliteCarteDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteCarte> items = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

      for (HistoriqueCarteProgrammeFideliteCarteDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la historiqueCarteProgrammeFideliteCarte existe
        HistoriqueCarteProgrammeFideliteCarte existingEntity = null;
          existingEntity = historiqueCarteProgrammeFideliteCarteRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteCarte -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                            // produitHistoriqueCartePdfCarte
        List<ProduitHistoriqueCartePdfCarte> listOfProduitHistoriqueCartePdfCarte = produitHistoriqueCartePdfCarteRepository.findByHistoriqueCarteProgrammeFideliteCarteId(existingEntity.getId(), false);
        if (listOfProduitHistoriqueCartePdfCarte != null && !listOfProduitHistoriqueCartePdfCarte.isEmpty()){
          Request<ProduitHistoriqueCartePdfCarteDto> deleteRequest = new Request<ProduitHistoriqueCartePdfCarteDto>();
          deleteRequest.setDatas(ProduitHistoriqueCartePdfCarteTransformer.INSTANCE.toDtos(listOfProduitHistoriqueCartePdfCarte));
          deleteRequest.setUser(request.getUser());
          Response<ProduitHistoriqueCartePdfCarteDto> deleteResponse = produitHistoriqueCartePdfCarteBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
                                                      items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete HistoriqueCarteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get HistoriqueCarteProgrammeFideliteCarte by using HistoriqueCarteProgrammeFideliteCarteDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<HistoriqueCarteProgrammeFideliteCarteDto> getByCriteria(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, Locale locale) {
    slf4jLogger.info("----begin get HistoriqueCarteProgrammeFideliteCarte-----");

    response = new Response<HistoriqueCarteProgrammeFideliteCarteDto>();

    try {
      List<HistoriqueCarteProgrammeFideliteCarte> items = null;
      items = historiqueCarteProgrammeFideliteCarteRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteCarteDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteCarteDto>();
        for (HistoriqueCarteProgrammeFideliteCarte entity : items) {
          HistoriqueCarteProgrammeFideliteCarteDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(historiqueCarteProgrammeFideliteCarteRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("historiqueCarteProgrammeFideliteCarte", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get HistoriqueCarteProgrammeFideliteCarte-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full HistoriqueCarteProgrammeFideliteCarteDto by using HistoriqueCarteProgrammeFideliteCarte as object.
   *
   * @param entity, locale
   * @return HistoriqueCarteProgrammeFideliteCarteDto
   *
   */
  private HistoriqueCarteProgrammeFideliteCarteDto getFullInfos(HistoriqueCarteProgrammeFideliteCarte entity, Integer size, Locale locale) throws Exception {
    HistoriqueCarteProgrammeFideliteCarteDto dto = HistoriqueCarteProgrammeFideliteCarteTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }

    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
