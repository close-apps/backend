


/*
 * Java transformer for entity table user_enseigne_point_de_vente
 * Created on 2020-09-25 ( Time 10:54:16 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "user_enseigne_point_de_vente"
 *
 * @author Back-End developper
 *
 */
@Component
public class UserEnseignePointDeVenteBusiness implements IBasicBusiness<Request<UserEnseignePointDeVenteDto>, Response<UserEnseignePointDeVenteDto>> {

	private Response<UserEnseignePointDeVenteDto> response;
	@Autowired
	private UserEnseignePointDeVenteRepository userEnseignePointDeVenteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public UserEnseignePointDeVenteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create UserEnseignePointDeVente by using UserEnseignePointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserEnseignePointDeVenteDto> create(Request<UserEnseignePointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create UserEnseignePointDeVente-----");

		response = new Response<UserEnseignePointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<UserEnseignePointDeVente> items = new ArrayList<UserEnseignePointDeVente>();

			for (UserEnseignePointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("userId", dto.getUserId());
				fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if userEnseignePointDeVente to insert do not exist
				UserEnseignePointDeVente existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("userEnseignePointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if pointDeVente exist
				PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
				if (existingPointDeVente == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if userEnseigne exist
				User existingUser = userRepository.findById(dto.getUserId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
					response.setHasError(true);
					return response;
				}
				UserEnseignePointDeVente entityToSave = null;
				entityToSave = UserEnseignePointDeVenteTransformer.INSTANCE.toEntity(dto, existingPointDeVente, existingUser );
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<UserEnseignePointDeVente> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("userEnseignePointDeVente", locale));
					response.setHasError(true);
					return response;
				}
				List<UserEnseignePointDeVenteDto> itemsDto = new ArrayList<UserEnseignePointDeVenteDto>();
				for (UserEnseignePointDeVente entity : itemsSaved) {
					UserEnseignePointDeVenteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create UserEnseignePointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update UserEnseignePointDeVente by using UserEnseignePointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserEnseignePointDeVenteDto> update(Request<UserEnseignePointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update UserEnseignePointDeVente-----");

		response = new Response<UserEnseignePointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);

			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<UserEnseignePointDeVente> items = new ArrayList<UserEnseignePointDeVente>();

			for (UserEnseignePointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la userEnseignePointDeVente existe
				UserEnseignePointDeVente entityToSave = null;
				entityToSave = userEnseignePointDeVenteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("userEnseignePointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if pointDeVente exist
				if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0){
					PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
					if (existingPointDeVente == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setPointDeVente(existingPointDeVente);
				}
				// Verify if userEnseigne exist
				if (dto.getUserId() != null && dto.getUserId() > 0){
					User existingUser = userRepository.findById(dto.getUserId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<UserEnseignePointDeVente> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("userEnseignePointDeVente", locale));
					response.setHasError(true);
					return response;
				}
				List<UserEnseignePointDeVenteDto> itemsDto = new ArrayList<UserEnseignePointDeVenteDto>();
				for (UserEnseignePointDeVente entity : itemsSaved) {
					UserEnseignePointDeVenteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update UserEnseignePointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete UserEnseignePointDeVente by using UserEnseignePointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserEnseignePointDeVenteDto> delete(Request<UserEnseignePointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete UserEnseignePointDeVente-----");

		response = new Response<UserEnseignePointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);

			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<UserEnseignePointDeVente> items = new ArrayList<UserEnseignePointDeVente>();

			for (UserEnseignePointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la userEnseignePointDeVente existe
				UserEnseignePointDeVente existingEntity = null;
				existingEntity = userEnseignePointDeVenteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("userEnseignePointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete UserEnseignePointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete UserEnseignePointDeVente by using UserEnseignePointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<UserEnseignePointDeVenteDto> forceDelete(Request<UserEnseignePointDeVenteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete UserEnseignePointDeVente-----");

		response = new Response<UserEnseignePointDeVenteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);

			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<UserEnseignePointDeVente> items = new ArrayList<UserEnseignePointDeVente>();

			for (UserEnseignePointDeVenteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la userEnseignePointDeVente existe
				UserEnseignePointDeVente existingEntity = null;
				existingEntity = userEnseignePointDeVenteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("userEnseignePointDeVente -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userEnseignePointDeVenteRepository.saveAll((Iterable<UserEnseignePointDeVente>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete UserEnseignePointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get UserEnseignePointDeVente by using UserEnseignePointDeVenteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<UserEnseignePointDeVenteDto> getByCriteria(Request<UserEnseignePointDeVenteDto> request, Locale locale) {
		slf4jLogger.info("----begin get UserEnseignePointDeVente-----");

		response = new Response<UserEnseignePointDeVenteDto>();

		try {
			List<UserEnseignePointDeVente> items = null;
			items = userEnseignePointDeVenteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<UserEnseignePointDeVenteDto> itemsDto = new ArrayList<UserEnseignePointDeVenteDto>();
				for (UserEnseignePointDeVente entity : items) {
					UserEnseignePointDeVenteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(userEnseignePointDeVenteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("userEnseignePointDeVente", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get UserEnseignePointDeVente-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full UserEnseignePointDeVenteDto by using UserEnseignePointDeVente as object.
	 *
	 * @param entity, locale
	 * @return UserEnseignePointDeVenteDto
	 *
	 */
	private UserEnseignePointDeVenteDto getFullInfos(UserEnseignePointDeVente entity, Integer size, Locale locale) throws Exception {
		UserEnseignePointDeVenteDto dto = UserEnseignePointDeVenteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
