


/*
 * Java transformer for entity table souscription
 * Created on 2020-08-17 ( Time 14:14:51 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.dao.entity.DureeForfait;
import com.closeapps.dao.entity.Enseigne;
import com.closeapps.dao.entity.HistoriqueSouscriptionEnseigne;
import com.closeapps.dao.entity.PallierForfait;
import com.closeapps.dao.entity.Souscription;
import com.closeapps.dao.entity.User;
import com.closeapps.dao.repository.DureeForfaitRepository;
import com.closeapps.dao.repository.EnseigneRepository;
import com.closeapps.dao.repository.HistoriqueSouscriptionEnseigneRepository;
import com.closeapps.dao.repository.PallierForfaitRepository;
import com.closeapps.dao.repository.SouscriptionRepository;
import com.closeapps.dao.repository.UserRepository;
import com.closeapps.helper.ExceptionUtils;
import com.closeapps.helper.FunctionalError;
import com.closeapps.helper.TechnicalError;
import com.closeapps.helper.Utilities;
import com.closeapps.helper.Validate;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.HistoriqueSouscriptionEnseigneDto;
import com.closeapps.helper.dto.PointDeVenteDto;
import com.closeapps.helper.dto.SouscriptionDto;
import com.closeapps.helper.dto.transformer.EnseigneTransformer;
import com.closeapps.helper.dto.transformer.HistoriqueSouscriptionEnseigneTransformer;
import com.closeapps.helper.dto.transformer.SouscriptionTransformer;
import com.closeapps.helper.enums.GlobalEnum;

/**
BUSINESS for table "souscription"
 *
 * @author Back-End developper
 *
 */
@Component
public class SouscriptionBusiness implements IBasicBusiness<Request<SouscriptionDto>, Response<SouscriptionDto>> {

	private Response<SouscriptionDto> response;
	@Autowired
	private SouscriptionRepository souscriptionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private HistoriqueSouscriptionEnseigneRepository historiqueSouscriptionEnseigneRepository;
	@Autowired
	private PallierForfaitRepository pallierForfaitRepository;
	@Autowired
	private DureeForfaitRepository dureeForfaitRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private EnseigneBusiness enseigneBusiness;
@Autowired
	private HistoriqueSouscriptionEnseigneBusiness historiqueSouscriptionEnseigneBusiness;



	public SouscriptionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionDto> create(Request<SouscriptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Souscription> items = new ArrayList<Souscription>();

			for (SouscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
//				fieldsToVerify.put("pallierForfaitId", dto.getPallierForfaitId());
//				fieldsToVerify.put("dureeForfaitId", dto.getDureeForfaitId());
				fieldsToVerify.put("pallierForfaitLibelle", dto.getPallierForfaitLibelle());
				fieldsToVerify.put("dureeForfaitLibelle", dto.getDureeForfaitLibelle());
				fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("urlImage", dto.getUrlImage());
				fieldsToVerify.put("prix", dto.getPrix());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if souscription to insert do not exist
				Souscription existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscription -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if pallierForfait exist
				PallierForfait existingPallierForfait = pallierForfaitRepository.findByLibelle(dto.getPallierForfaitLibelle(), false);
				//PallierForfait existingPallierForfait = pallierForfaitRepository.findById(dto.getPallierForfaitId(), false);
				if (existingPallierForfait == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pallierForfait -> " + dto.getPallierForfaitLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if dureeForfait exist
				DureeForfait existingDureeForfait = dureeForfaitRepository.findByLibelle(dto.getDureeForfaitLibelle(), false);
				//DureeForfait existingDureeForfait = dureeForfaitRepository.findById(dto.getDureeForfaitId(), false);
				if (existingDureeForfait == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("dureeForfait -> " + dto.getDureeForfaitLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				Souscription entityToSave = null;
				entityToSave = SouscriptionTransformer.INSTANCE.toEntity(dto, existingPallierForfait, existingDureeForfait);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Souscription> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = souscriptionRepository.saveAll((Iterable<Souscription>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscription", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionDto> itemsDto = new ArrayList<SouscriptionDto>();
				for (Souscription entity : itemsSaved) {
					SouscriptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionDto> update(Request<SouscriptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Souscription> items = new ArrayList<Souscription>();

			for (SouscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscription existe
				Souscription entityToSave = null;
				entityToSave = souscriptionRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if pallierForfait exist
				//if (dto.getPallierForfaitId() != null && dto.getPallierForfaitId() > 0){
				if (Utilities.notBlank(dto.getPallierForfaitLibelle())) {
					PallierForfait existingPallierForfait = pallierForfaitRepository.findByLibelle(dto.getPallierForfaitLibelle(), false);
					//PallierForfait existingPallierForfait = pallierForfaitRepository.findById(dto.getPallierForfaitId(), false);
					if (existingPallierForfait == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("pallierForfait -> " + dto.getPallierForfaitLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setPallierForfait(existingPallierForfait);
				}
				// Verify if dureeForfait exist
				//if (dto.getDureeForfaitId() != null && dto.getDureeForfaitId() > 0){
				if (Utilities.notBlank(dto.getDureeForfaitLibelle())) {
					DureeForfait existingDureeForfait = dureeForfaitRepository.findByLibelle(dto.getDureeForfaitLibelle(), false);
					//DureeForfait existingDureeForfait = dureeForfaitRepository.findById(dto.getDureeForfaitId(), false);
					if (existingDureeForfait == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("dureeForfait -> " + dto.getDureeForfaitLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setDureeForfait(existingDureeForfait);
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (Utilities.notBlank(dto.getUrlImage())) {
					entityToSave.setUrlImage(dto.getUrlImage());
				}
//				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
//					entityToSave.setCreatedBy(dto.getCreatedBy());
//				}
//				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
//					entityToSave.setUpdatedBy(dto.getUpdatedBy());
//				}
//				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
//					entityToSave.setDeletedBy(dto.getDeletedBy());
//				}
//				if (Utilities.notBlank(dto.getDeletedAt())) {
//					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
//				}
				if (Utilities.notBlank(dto.getPrix())) {
					entityToSave.setPrix(dto.getPrix());
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Souscription> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionRepository.saveAll((Iterable<Souscription>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscription", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionDto> itemsDto = new ArrayList<SouscriptionDto>();
				for (Souscription entity : itemsSaved) {
					SouscriptionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionDto> delete(Request<SouscriptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Souscription> items = new ArrayList<Souscription>();

			for (SouscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscription existe
				Souscription existingEntity = null;
				existingEntity = souscriptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// enseigne
				List<Enseigne> listOfEnseigne = enseigneRepository.findBySouscriptionId(existingEntity.getId(), false);
				if (listOfEnseigne != null && !listOfEnseigne.isEmpty()) {
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueSouscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findBySouscriptionId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueSouscriptionEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionRepository.saveAll((Iterable<Souscription>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SouscriptionDto> forceDelete(Request<SouscriptionDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Souscription> items = new ArrayList<Souscription>();

			for (SouscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscription existe
				Souscription existingEntity = null;
				existingEntity = souscriptionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// historiqueSouscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findBySouscriptionId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()) {
					Request<HistoriqueSouscriptionEnseigneDto> deleteRequest = new Request<HistoriqueSouscriptionEnseigneDto>();
					deleteRequest.setDatas(HistoriqueSouscriptionEnseigneTransformer.INSTANCE.toDtos(listOfHistoriqueSouscriptionEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueSouscriptionEnseigneDto> deleteResponse = historiqueSouscriptionEnseigneBusiness
							.delete(deleteRequest, locale);
					if (deleteResponse.isHasError()) {
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// enseigne
				List<Enseigne> listOfEnseigne = enseigneRepository.findBySouscriptionId(existingEntity.getId(), false);
				if (listOfEnseigne != null && !listOfEnseigne.isEmpty()) {
					Request<EnseigneDto> deleteRequest = new Request<EnseigneDto>();
					deleteRequest.setDatas(EnseigneTransformer.INSTANCE.toDtos(listOfEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<EnseigneDto> deleteResponse = enseigneBusiness
							.delete(deleteRequest, locale);
					if (deleteResponse.isHasError()) {
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionRepository.saveAll((Iterable<Souscription>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SouscriptionDto> getByCriteria(Request<SouscriptionDto> request, Locale locale) {
		slf4jLogger.info("----begin get Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			List<Souscription> items = null;
			items = souscriptionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SouscriptionDto> itemsDto = new ArrayList<SouscriptionDto>();
				if (request.isStat()) {
					SouscriptionDto dto = new SouscriptionDto() ;
					dto.setPallierForfaitLibelle(GlobalEnum.AUCUN);
					itemsDto.add(dto);
				}
				for (Souscription entity : items) {
					SouscriptionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(souscriptionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				if (request.isStat()) {
					SouscriptionDto dto = new SouscriptionDto() ;
					dto.setPallierForfaitLibelle(GlobalEnum.AUCUN);
					response.setItems(Arrays.asList(dto));
				}
				response.setStatus(functionalError.DATA_EMPTY("souscription", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * getCustom Souscription by using SouscriptionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<SouscriptionDto> getByCriteriaCustom(Request<SouscriptionDto> request, Locale locale) {
		slf4jLogger.info("----begin getCustom Souscription-----");

		response = new Response<SouscriptionDto>();

		try {
			List<Souscription> items = null;
			items = souscriptionRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SouscriptionDto> itemsDto = new ArrayList<SouscriptionDto>();
				if (request.isStat()) {
					SouscriptionDto dto = new SouscriptionDto() ;
					dto.setPallierForfaitLibelle(GlobalEnum.AUCUN);
					itemsDto.add(dto);
				}
				for (Souscription entity : items) {
					SouscriptionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(souscriptionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				if (request.isStat()) {
					SouscriptionDto dto = new SouscriptionDto() ;
					dto.setPallierForfaitLibelle(GlobalEnum.AUCUN);
					response.setItems(Arrays.asList(dto));
				}
				response.setStatus(functionalError.DATA_EMPTY("souscription", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end getCustom Souscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SouscriptionDto by using Souscription as object.
	 *
	 * @param entity, locale
	 * @return SouscriptionDto
	 *
	 */
	private SouscriptionDto getFullInfos(Souscription entity, Integer size, Locale locale) throws Exception {
		SouscriptionDto dto = SouscriptionTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
