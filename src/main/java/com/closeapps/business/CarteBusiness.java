


/*
 * Java transformer for entity table carte
 * Created on 2020-08-17 ( Time 14:14:43 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.TypeActionEnum;
import com.closeapps.helper.enums.UniteTempsEnum;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "carte"
 *
 * @author Back-End developper
 *
 */
@Component
public class CarteBusiness implements IBasicBusiness<Request<CarteDto>, Response<CarteDto>> {

	private Response<CarteDto> response;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private UserRepository userRepository;



	@Autowired
	private NotesCloseRepository notesCloseRepository;
	@Autowired
	private DelitProgrammeFideliteRepository delitProgrammeFideliteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteRepository souscriptionProgrammeFideliteRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository  programmeFideliteCarteRepository;
	@Autowired
	private TypeActionRepository  typeActionRepository;
	@Autowired
	private UserEnseignePointDeVenteRepository  userEnseignePointDeVenteRepository;
	@Autowired
	private PointDeVenteProgrammeFideliteTamponRepository  pointDeVenteProgrammeFideliteTamponRepository;


	@Autowired
	private EnseigneRepository enseigneRepository ;
	@Autowired
	private PointDeVenteProgrammeFideliteCarteRepository  pointDeVenteProgrammeFideliteCarteRepository;

	@Autowired
	private InformationRegleSecuriteCarteRepository  informationRegleSecuriteCarteRepository;
	@Autowired
	private InformationRegleSecuriteTamponRepository  informationRegleSecuriteTamponRepository;



	@Autowired
	private ParamsUtils paramsUtils;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private NotesCloseBusiness notesCloseBusiness;

	@Autowired
	private DelitProgrammeFideliteBusiness delitProgrammeFideliteBusiness;


	@Autowired
	private SouscriptionProgrammeFideliteTamponBusiness souscriptionProgrammeFideliteTamponBusiness;


	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponBusiness historiqueCarteProgrammeFideliteTamponBusiness;


	@Autowired
	private SouscriptionProgrammeFideliteCarteBusiness souscriptionProgrammeFideliteCarteBusiness;
	@Autowired
	private SouscriptionProgrammeFideliteBusiness souscriptionProgrammeFideliteBusiness;


	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteBusiness historiqueCarteProgrammeFideliteCarteBusiness;



	public CarteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CarteDto> create(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Carte-----");

		response = new Response<CarteDto>();

		try {

			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {
				//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<Carte> items = new ArrayList<Carte>();

			for (CarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("codeBarre", dto.getCodeBarre());
				//fieldsToVerify.put("qrCode", dto.getQrCode());
				//fieldsToVerify.put("code", dto.getCode());
				//fieldsToVerify.put("urlImage", dto.getUrlImage());
				fieldsToVerify.put("userId", dto.getUserId());

				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if carte to insert do not exist
				Carte existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("carte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = carteRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("carte -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les cartes", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if enseigne exist
				//				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				//				if (existingEnseigne == null) {
				//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}


				// TODO: generation du code barre et qrcode
				dto.setCodeBarre(Utilities.generateCodeBarre(carteRepository, programmeFideliteTamponRepository));
				dto.setQrCode(Utilities.generateQRCode(carteRepository, programmeFideliteTamponRepository));

				// generation du code de la carte
				dto.setCode(Utilities.generateCodeCarte(carteRepository));

				if (!Utilities.notBlank(dto.getUrlImage())) {
					dto.setUrlImage(paramsUtils.getDefaultUrlImageCarte());
				}


				dto.setIsLocked(false);
				Carte entityToSave = null;
				//entityToSave = CarteTransformer.INSTANCE.toEntity(dto, existingUser, existingEnseigne);
				entityToSave = CarteTransformer.INSTANCE.toEntity(dto, existingUser);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Carte> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = carteRepository.saveAll((Iterable<Carte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("carte", locale));
					response.setHasError(true);
					return response;
				}
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : itemsSaved) {
					CarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CarteDto> update(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Carte-----");

		response = new Response<CarteDto>();

		try {

			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {
				//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<Carte> items = new ArrayList<Carte>();

			for (CarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la carte existe
				Carte entityToSave = null;
				entityToSave = carteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if user exist
				if (dto.getUserId() != null && dto.getUserId() > 0){
					User existingUser = userRepository.findById(dto.getUserId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				// Verify if enseigne exist
				//				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
				//					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				//					if (existingEnseigne == null) {
				//						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setEnseigne(existingEnseigne);
				//				}
				//				if (Utilities.notBlank(dto.getCodeBarre())) {
				//					entityToSave.setCodeBarre(dto.getCodeBarre());
				//				}
				//				if (Utilities.notBlank(dto.getQrCode())) {
				//					entityToSave.setQrCode(dto.getQrCode());
				//				}
				//				if (Utilities.notBlank(dto.getCode())) {
				//					Carte existingEntity = carteRepository.findByCode(dto.getCode(), false);
				//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
				//						response.setStatus(functionalError.DATA_EXIST("carte -> " + dto.getCode(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les cartes", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCode(dto.getCode());
				//				}
				if (Utilities.notBlank(dto.getUrlImage())) {
					entityToSave.setUrlImage(dto.getUrlImage());
				}

				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Carte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = carteRepository.saveAll((Iterable<Carte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("carte", locale));
					response.setHasError(true);
					return response;
				}
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : itemsSaved) {
					CarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/*
	 * flasherTampon Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<CarteDto> flasherTampon(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin flasherTampon Carte-----");

		response = new Response<CarteDto>();

		boolean isDelitError = false ;

		try {

			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			//if (request.getUser() != null && request.getUser() > 0 ) {
			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			
			if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				&& !utilisateur.getEnseigne().getIsActive()) {
					response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!" , locale));
					response.setHasError(true);
					return response;
				}
			//}

			List<Carte> items = new ArrayList<Carte>();
			List<SouscriptionProgrammeFideliteTampon> itemsSouscriptionProgrammeFideliteTampon = new ArrayList<>();
			List<HistoriqueCarteProgrammeFideliteTampon> itemsHistoriqueCarteProgrammeFideliteTampon = new ArrayList<>();

			for (CarteDto dto : request.getDatas()) {
				Date dateFin = Utilities.getCurrentDate() ;

				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("contenuScanner", dto.getContenuScanner());
				if (dto.getIsScanManuel() != null && dto.getIsScanManuel()) {
					fieldsToVerify.put("identifiant", dto.getUserLogin());
				}else {
					fieldsToVerify.put("id", dto.getId());
				}
				fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
				fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
				//fieldsToVerify.put("typeActionCode", dto.getTypeActionCode());
				//fieldsToVerify.put("somme", dto.getSomme());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */


				// recuperation des informations
				//				String[] listContenuScanner = dto.getContenuScanner().split(" ");
				//				if (listContenuScanner == null || listContenuScanner.length <= 0) {
				//					response.setStatus(functionalError.INVALID_DATA("Le code scanné n'a pas un contenu exploitable", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//
				//				Integer carteId = Integer.parseInt(listContenuScanner[0]);
				//				Integer programmeFideliteCarteId = Integer.parseInt(listContenuScanner[1]);

				// Verifier si la carte existe
				Carte entityToSave = null;
				if (dto.getIsScanManuel() != null && dto.getIsScanManuel()) {
					List<Carte> cartes = carteRepository.findByUserLogin(dto.getUserLogin(), false);
					if (!Utilities.isNotEmpty(cartes)) {
						cartes = carteRepository.findByUserEmail(dto.getUserLogin(), false);
					} 
					if (Utilities.isNotEmpty(cartes)) {
						entityToSave = cartes.get(0);
					}

					if (entityToSave == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("identifiant -> " + dto.getUserLogin(), locale));
						response.setHasError(true);
						return response;
					}
				}else {
					entityToSave = carteRepository.findById(dto.getId(), false);
					if (entityToSave == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				ProgrammeFideliteTampon existingProgrammeFideliteTampon = null;
				existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
				if (existingProgrammeFideliteTampon == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
					response.setHasError(true);
					return response;
				}
				PointDeVente existingPointDeVente = null;
				existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
				if (existingPointDeVente == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}

				// verifier que le PDF est bien valide pour ce PDV
				List<PointDeVenteProgrammeFideliteTampon> existingPointDeVenteProgrammeFideliteTampons = 
						pointDeVenteProgrammeFideliteTamponRepository.findByPdvIdAndPdfTamponId(dto.getPointDeVenteId(), dto.getProgrammeFideliteTamponId(), false ) ;
				if (!Utilities.isNotEmpty(existingPointDeVenteProgrammeFideliteTampons)) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("Ce PDF Tampon n'existe pas pour ce point de vente", locale));
					response.setHasError(true);
					return response;
				}
				// verification si le userEnseigne peux flasher le code 
				if(utilisateur.getIsAdminEnseigne() == null || !utilisateur.getIsAdminEnseigne()) {
					List<UserEnseignePointDeVente> userEnseignePointDeVentes = userEnseignePointDeVenteRepository.findByUserEnseigneIdAndPointDeVenteId(utilisateur.getId(), dto.getPointDeVenteId(), false) ;
					if (!Utilities.isNotEmpty(userEnseignePointDeVentes)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Cet utilisateur ne peut pas exercer dans ce point de vente", locale));
						response.setHasError(true);
						return response;
					}
				}

				TypeAction existingTypeAction = null;
				//existingTypeAction = typeActionRepository.findByCode(dto.getTypeActionCode(), false);
				existingTypeAction = typeActionRepository.findByCode(TypeActionEnum.CREDITER, false);
				if (existingTypeAction == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionCode(), locale));
					response.setHasError(true);
					return response;
				}

				// calcul nbre point obtenu
				// verifier la souscription du user au programme 
				List<SouscriptionProgrammeFideliteTampon> souscriptionProgrammeFideliteTampons = 
						souscriptionProgrammeFideliteTamponRepository.findByCarteIdAndProgrammeFideliteTamponId(entityToSave.getId(), dto.getProgrammeFideliteTamponId(), false) ;
				if (souscriptionProgrammeFideliteTampons == null || souscriptionProgrammeFideliteTampons.size() == 0) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Aucune souscription trouvée pour ce code ou client", locale));
					response.setHasError(true);
					return response;
				}



				// prise en compte des regles de securites
				if (existingTypeAction.getCode().equals(TypeActionEnum.CREDITER)) {
					List<InformationRegleSecuriteTampon> listInformationRegleSecuriteTampon = new ArrayList<>() ;

					listInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findByProgrammeFideliteTamponId(dto.getProgrammeFideliteTamponId(), false) ;

					if (Utilities.isNotEmpty(listInformationRegleSecuriteTampon)) {
						for (InformationRegleSecuriteTampon data : listInformationRegleSecuriteTampon) {

							Calendar calendar = new GregorianCalendar() ;
							calendar.setTime(dateFin);

							switch (data.getUniteTemps().getCode()) {
							case UniteTempsEnum.PAR_HEURE:
								calendar.add(Calendar.HOUR, -1);
								break;

							case UniteTempsEnum.PAR_JOUR:
								calendar.add(Calendar.DAY_OF_YEAR, -1);
								break;

							case UniteTempsEnum.PAR_SEMAINE:
								calendar.add(Calendar.WEEK_OF_YEAR, -1);

								break;

							case UniteTempsEnum.PAR_MOIS:
								calendar.add(Calendar.MONTH, -1);
								break;

							default:
								break;
							}

							Date dateDebut = calendar.getTime() ;

							List<HistoriqueCarteProgrammeFideliteTampon> listHistoriqueCarteProgrammeFideliteTampon = new ArrayList<>() ;

							listHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByCarteIdAndPdfTamponIdAndPdvIdAndTypeActionIdAndDateActionBetween(entityToSave.getId(), dto.getProgrammeFideliteTamponId(), dto.getPointDeVenteId(), existingTypeAction.getId(), dateDebut, dateFin, false);

							DelitProgrammeFidelite  delitProgrammeFideliteTampon = new DelitProgrammeFidelite() ;

							delitProgrammeFideliteTampon.setDateDelit(dateDebut);
							delitProgrammeFideliteTampon.setHeureDelit(dateDebut);
							delitProgrammeFideliteTampon.setIsNotifed(false);
							delitProgrammeFideliteTampon.setIsApplicateSanction(false);
							delitProgrammeFideliteTampon.setCarte(entityToSave);
							delitProgrammeFideliteTampon.setInformationRegleSecuriteTampon(data);
							delitProgrammeFideliteTampon.setPointDeVente(existingPointDeVente);

							delitProgrammeFideliteTampon.setCreatedBy(request.getUser());
							delitProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							delitProgrammeFideliteTampon.setIsDeleted(false);

							if (Utilities.isNotEmpty(listHistoriqueCarteProgrammeFideliteTampon)) {

								Integer nbreDeFois = listHistoriqueCarteProgrammeFideliteTampon.size() ;

								if (data.getNbreMaxiParPdv() != null && data.getNbreMaxiParPdv() > 0 && nbreDeFois >=  data.getNbreMaxiParPdv()) {
									// creer le delit
									delitProgrammeFideliteRepository.save(delitProgrammeFideliteTampon) ;
									isDelitError = true ;
									// error
									response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le nombre de flash pour ce PDV est dépassé", locale));
									response.setHasError(true);
									return response;
								}				
							} 

							if (data.getNbreMaxiAllPdv() != null && data.getNbreMaxiAllPdv() > 0) {
								// on fait un new get
								List<HistoriqueCarteProgrammeFideliteTampon> listAllHistoriqueCarteProgrammeFideliteTampon = new ArrayList<>() ;

								listAllHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByCarteIdAndPdfTamponIdAndTypeActionIdAndDateActionBetween(entityToSave.getId(), dto.getProgrammeFideliteCarteId(), existingTypeAction.getId(), dateDebut, dateFin, false);

								if (Utilities.isNotEmpty(listAllHistoriqueCarteProgrammeFideliteTampon) && listAllHistoriqueCarteProgrammeFideliteTampon.size() >=  data.getNbreMaxiAllPdv()) {
									// creer le delit
									delitProgrammeFideliteRepository.save(delitProgrammeFideliteTampon) ;
									isDelitError = true ;
									// error
									response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le nombre de flash pour tous les PDV est dépassé", locale));
									response.setHasError(true);
									return response;
								}																
							}
						}
					}
				}



				SouscriptionProgrammeFideliteTampon souscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTampons.get(0) ;

				//ajouter le nbre ou retrancher selon le typeCodeAction

				int nbreTamponActuelle = souscriptionProgrammeFideliteTampon.getNbreTamponActuelle() != null ? souscriptionProgrammeFideliteTampon.getNbreTamponActuelle() : 0 ;


				if (nbreTamponActuelle >= existingProgrammeFideliteTampon.getNbreTampon()) {
					souscriptionProgrammeFideliteTampon.setNbreTamponActuelle(0);
					souscriptionProgrammeFideliteTampon.setNbreTamponPrecedent(nbreTamponActuelle);
				} else {
					souscriptionProgrammeFideliteTampon.setNbreTamponActuelle(nbreTamponActuelle + 1);
					souscriptionProgrammeFideliteTampon.setNbreTamponPrecedent(nbreTamponActuelle);					
				}


				souscriptionProgrammeFideliteTampon.setUpdatedBy(request.getUser());
				souscriptionProgrammeFideliteTampon.setUpdatedAt(Utilities.getCurrentDate());
				itemsSouscriptionProgrammeFideliteTampon.add(souscriptionProgrammeFideliteTampon) ;



				HistoriqueCarteProgrammeFideliteTampon historiqueCarteProgrammeFideliteTampon = new HistoriqueCarteProgrammeFideliteTampon();
				//TypeAction typeAction = typeActionRepository.findByCode(TypeActionEnum.SOUSCRIPTION, false) ;


				historiqueCarteProgrammeFideliteTampon.setCarte(entityToSave);
				historiqueCarteProgrammeFideliteTampon.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				historiqueCarteProgrammeFideliteTampon.setTypeAction(existingTypeAction);
				historiqueCarteProgrammeFideliteTampon.setPointDeVente(existingPointDeVente);

				historiqueCarteProgrammeFideliteTampon.setNbreTamponObtenu(1);
				historiqueCarteProgrammeFideliteTampon.setNbreTamponActuel(souscriptionProgrammeFideliteTampon.getNbreTamponActuelle());
				historiqueCarteProgrammeFideliteTampon.setNbreTamponAvantAction(souscriptionProgrammeFideliteTampon.getNbreTamponPrecedent());

				historiqueCarteProgrammeFideliteTampon.setCreatedBy(request.getUser());
				historiqueCarteProgrammeFideliteTampon.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCarteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
				historiqueCarteProgrammeFideliteTampon.setIsDeleted(false);
				historiqueCarteProgrammeFideliteTampon.setDescription(dto.getDescription()); // ajout du commentaire lors du scan


				// TODO : revoir l'heureAction, gerer dans dateAction
				historiqueCarteProgrammeFideliteTampon.setHeureAction(Utilities.getCurrentDate()); // heureAction



				historiqueCarteProgrammeFideliteTampon.setSouscriptionProgrammeFideliteTampon(souscriptionProgrammeFideliteTampon);
				itemsHistoriqueCarteProgrammeFideliteTampon.add(historiqueCarteProgrammeFideliteTampon) ;



				// maj enseigne
				Enseigne enseignePdf = existingProgrammeFideliteTampon.getEnseigne() ;

				//enseignePdfCarte.setChiffreDaffaire(enseignePdfCarte.getChiffreDaffaire() != null ? enseignePdfCarte.getChiffreDaffaire() + dto.getChiffreDaffaire() : dto.getChiffreDaffaire());
				enseignePdf.setNbreTransaction(enseignePdf.getNbreTransaction() != null ? enseignePdf.getNbreTransaction() + 1 : 1);

				if (nbreTamponActuelle >= existingProgrammeFideliteTampon.getNbreTampon()) {
					// recompense remboursée
					enseignePdf.setRecompenseRembouserTampon(enseignePdf.getRecompenseRembouserTampon() != null ? enseignePdf.getRecompenseRembouserTampon() + 1 : 1);	
				}
				enseignePdf.setUpdatedAt(Utilities.getCurrentDate());

				enseigneRepository.save(enseignePdf) ;



				// Verify if enseigne exist
				//				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
				//					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				//					if (existingEnseigne == null) {
				//						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setEnseigne(existingEnseigne);
				//				}
				//				if (Utilities.notBlank(dto.getCodeBarre())) {
				//					entityToSave.setCodeBarre(dto.getCodeBarre());
				//				}
				//				if (Utilities.notBlank(dto.getQrCode())) {
				//					entityToSave.setQrCode(dto.getQrCode());
				//				}
				//				if (Utilities.notBlank(dto.getCode())) {
				//					Carte existingEntity = carteRepository.findByCode(dto.getCode(), false);
				//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
				//						response.setStatus(functionalError.DATA_EXIST("carte -> " + dto.getCode(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les cartes", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCode(dto.getCode());
				//				}

				//entityToSave.setUpdatedBy(request.getUser());
				//entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!itemsSouscriptionProgrammeFideliteTampon.isEmpty()) {
				List<SouscriptionProgrammeFideliteTampon> itemsSouscriptionProgrammeFideliteTamponSaved = null;
				// maj les donnees en base
				itemsSouscriptionProgrammeFideliteTamponSaved = souscriptionProgrammeFideliteTamponRepository.saveAll((Iterable<SouscriptionProgrammeFideliteTampon>) itemsSouscriptionProgrammeFideliteTampon);
				if (itemsSouscriptionProgrammeFideliteTamponSaved == null || itemsSouscriptionProgrammeFideliteTamponSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}

				List<HistoriqueCarteProgrammeFideliteTampon> itemsSavedHistoriqueCarteProgrammeFideliteTampon = null;
				// maj les donnees en base
				itemsSavedHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) itemsHistoriqueCarteProgrammeFideliteTampon);
				if (itemsSavedHistoriqueCarteProgrammeFideliteTampon == null || itemsSavedHistoriqueCarteProgrammeFideliteTampon.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : items) {
					CarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end flasherTampon Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null && !isDelitError) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}





	/**
	 * flasherCarte Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<CarteDto> flasherCarte(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin flasherCarte Carte-----");

		response = new Response<CarteDto>();
		boolean isDelitError = false ;

		try {

			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			//if (request.getUser() != null && request.getUser() > 0 ) {
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				&& !utilisateur.getEnseigne().getIsActive()) {
					response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!" , locale));
					response.setHasError(true);
					return response;
				}
			//}

			List<Carte> items = new ArrayList<Carte>();
			List<SouscriptionProgrammeFideliteCarte> itemsSouscriptionProgrammeFideliteCarte = new ArrayList<>();
			List<HistoriqueCarteProgrammeFideliteCarte> itemsHistoriqueCarteProgrammeFideliteCarte = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

			for (CarteDto dto : request.getDatas()) {
				Date dateFin = Utilities.getCurrentDate() ;


				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("contenuScanner", dto.getContenuScanner());
				if (dto.getIsScanManuel() != null && dto.getIsScanManuel()) {
					fieldsToVerify.put("identifiant", dto.getUserLogin());

				}else {
					fieldsToVerify.put("id", dto.getId());
				}
				fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
				fieldsToVerify.put("typeActionCode", dto.getTypeActionCode());
				fieldsToVerify.put("somme", dto.getSomme());
				fieldsToVerify.put("chiffreDaffaire", dto.getChiffreDaffaire());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */


				// recuperation des informations
				//				String[] listContenuScanner = dto.getContenuScanner().split(" ");
				//				if (listContenuScanner == null || listContenuScanner.length <= 0) {
				//					response.setStatus(functionalError.INVALID_DATA("Le code scanné n'a pas un contenu exploitable", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//
				//				Integer carteId = Integer.parseInt(listContenuScanner[0]);
				//				Integer programmeFideliteCarteId = Integer.parseInt(listContenuScanner[1]);

				// Verifier si la carte existe
				Carte entityToSave = null;
				if (dto.getIsScanManuel() != null && dto.getIsScanManuel()) {
					List<Carte> cartes = carteRepository.findByUserLogin(dto.getUserLogin(), false);
					if (!Utilities.isNotEmpty(cartes)) {
						cartes = carteRepository.findByUserEmail(dto.getUserLogin(), false);
					} 
					if (Utilities.isNotEmpty(cartes)) {
						entityToSave = cartes.get(0);
					}

					if (entityToSave == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("identifiant ou email -> " + dto.getUserLogin(), locale));
						response.setHasError(true);
						return response;
					}
				}else {
					entityToSave = carteRepository.findById(dto.getId(), false);
					if (entityToSave == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				ProgrammeFideliteCarte existingProgrammeFideliteCarte = null;
				existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
				if (existingProgrammeFideliteCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
					response.setHasError(true);
					return response;
				}

				TypeAction existingTypeAction = null;
				existingTypeAction = typeActionRepository.findByCode(dto.getTypeActionCode(), false);
				if (existingTypeAction == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionCode(), locale));
					response.setHasError(true);
					return response;
				}

				PointDeVente existingPointDeVente = null;
				existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
				if (existingPointDeVente == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}

				// verifier que le PDF est bien valide pour ce PDV
				List<PointDeVenteProgrammeFideliteCarte>  existingPointDeVenteProgrammeFideliteCartes = 
						pointDeVenteProgrammeFideliteCarteRepository.findByPdvIdAndPdfCarteId(dto.getPointDeVenteId(), dto.getProgrammeFideliteCarteId(), false ) ;
				if (!Utilities.isNotEmpty(existingPointDeVenteProgrammeFideliteCartes)) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("Ce PDF Tampon n'existe pas pour ce point de vente", locale));
					response.setHasError(true);
					return response;
				}
				// verification si le userEnseigne peux flasher le code 
				if(utilisateur.getIsAdminEnseigne() == null || !utilisateur.getIsAdminEnseigne()) {
					List<UserEnseignePointDeVente> userEnseignePointDeVentes = userEnseignePointDeVenteRepository.findByUserEnseigneIdAndPointDeVenteId(utilisateur.getId(), dto.getPointDeVenteId(), false) ;
					if (!Utilities.isNotEmpty(userEnseignePointDeVentes)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Cet utilisateur ne peut pas exercer dans ce point de vente", locale));
						response.setHasError(true);
						return response;
					}
				}				

				// calcul nbre point obtenu
				double pourXAcheter = existingProgrammeFideliteCarte.getPourXAcheter() ;
				DecimalFormat df = new DecimalFormat("#");

				Integer nbreDePoint = 0 ;

				if (dto.getTypeActionCode().equals(TypeActionEnum.CREDITER)) {
					nbreDePoint  = Integer.parseInt(
							df.format((double)(existingProgrammeFideliteCarte.getNbrePointObtenu() * dto.getSomme()) / pourXAcheter)) ;
				}else {
					if (dto.getTypeActionCode().equals(TypeActionEnum.DEBITER)) {
						nbreDePoint  = Integer.parseInt(
								df.format((double)(existingProgrammeFideliteCarte.getNbrePointObtenu() * dto.getSomme()) / existingProgrammeFideliteCarte.getCorrespondanceNbrePoint())) ;
					}						
				}


				//				Integer nbrePointObtenu =Integer.parseInt(
				//						df.format((existingProgrammeFideliteCarte.getCorrespondanceNbrePoint() * dto.getSomme()) / pourXAcheter)) ;

				List<SouscriptionProgrammeFideliteCarte> souscriptionProgrammeFideliteCartes = 
						souscriptionProgrammeFideliteCarteRepository.findByCarteIdAndProgrammeFideliteCarteId(entityToSave.getId(), dto.getProgrammeFideliteCarteId(), false) ;
				if (souscriptionProgrammeFideliteCartes == null || souscriptionProgrammeFideliteCartes.size() == 0) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Aucune souscription trouvée pour ce code ou client", locale));
					response.setHasError(true);
					return response;
				}

				// prise en compte des regles de securites
				if (existingTypeAction.getCode().equals(TypeActionEnum.CREDITER)) {
					List<InformationRegleSecuriteCarte> listInformationRegleSecuriteCarte = new ArrayList<InformationRegleSecuriteCarte>() ;

					listInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findByProgrammeFideliteCarteId(dto.getProgrammeFideliteCarteId(), false) ;

					if (Utilities.isNotEmpty(listInformationRegleSecuriteCarte)) {
						for (InformationRegleSecuriteCarte data : listInformationRegleSecuriteCarte) {

							Calendar calendar = new GregorianCalendar() ;
							calendar.setTime(dateFin);

							switch (data.getUniteTemps().getCode()) {
							case UniteTempsEnum.PAR_HEURE:
								calendar.add(Calendar.HOUR, -1);
								break;

							case UniteTempsEnum.PAR_JOUR:
								calendar.add(Calendar.DAY_OF_YEAR, -1);
								break;

							case UniteTempsEnum.PAR_SEMAINE:
								calendar.add(Calendar.WEEK_OF_YEAR, -1);

								break;

							case UniteTempsEnum.PAR_MOIS:
								calendar.add(Calendar.MONTH, -1);
								break;

							default:
								break;
							}

							Date dateDebut = calendar.getTime() ;

							List<HistoriqueCarteProgrammeFideliteCarte> listHistoriqueCarteProgrammeFideliteCarte = new ArrayList<>() ;

							listHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByCarteIdAndPdfCarteIdAndPdvIdAndTypeActionIdAndDateActionBetween(entityToSave.getId(), dto.getProgrammeFideliteCarteId(), dto.getPointDeVenteId(), existingTypeAction.getId(), dateDebut, dateFin, false);

							DelitProgrammeFidelite  delitProgrammeFideliteCarte = new DelitProgrammeFidelite() ;

							delitProgrammeFideliteCarte.setDateDelit(dateDebut);
							delitProgrammeFideliteCarte.setHeureDelit(dateDebut);
							delitProgrammeFideliteCarte.setIsNotifed(false);
							delitProgrammeFideliteCarte.setIsApplicateSanction(false);
							delitProgrammeFideliteCarte.setCarte(entityToSave);
							delitProgrammeFideliteCarte.setInformationRegleSecuriteCarte(data);
							delitProgrammeFideliteCarte.setPointDeVente(existingPointDeVente);

							delitProgrammeFideliteCarte.setCreatedBy(request.getUser());
							delitProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							delitProgrammeFideliteCarte.setIsDeleted(false);


							Integer nbreDeFois = Utilities.isNotEmpty(listHistoriqueCarteProgrammeFideliteCarte) ? listHistoriqueCarteProgrammeFideliteCarte.size() : 0;

							if (data.getNbreFoisCrediterParPdv() != null && data.getNbreFoisCrediterParPdv() > 0 && nbreDeFois + 1 >  data.getNbreFoisCrediterParPdv()) {

								// creer le delit
								delitProgrammeFideliteRepository.save(delitProgrammeFideliteCarte) ;
								// error
								isDelitError = true ;
								response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le nombre de flash pour ce PDV est dépassé", locale));
								response.setHasError(true);
								return response;

							}

							if (data.getPlafondSommeACrediter() != null && data.getPlafondSommeACrediter() > 0 ) {
								int plafond = 0 ;

								if (Utilities.isNotEmpty(listHistoriqueCarteProgrammeFideliteCarte)) {
									for (HistoriqueCarteProgrammeFideliteCarte elt : listHistoriqueCarteProgrammeFideliteCarte) {
										plafond += (elt.getSommeOperation() != null ? elt.getSommeOperation()  : 0 ) ;
									}
								}

								if (plafond + dto.getSomme() > data.getPlafondSommeACrediter()) {
									// creer le delit
									delitProgrammeFideliteRepository.save(delitProgrammeFideliteCarte) ;
									// error
									isDelitError = true ;
									// response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le plafond de point est dépassé", locale));
									response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le plafond de la somme à créditer est dépassé", locale));
									response.setHasError(true);
									return response;
								}
							}


							if (data.getNbreFoisCrediterAllPdv() != null && data.getNbreFoisCrediterAllPdv() > 0) {
								// on fait un new get
								List<HistoriqueCarteProgrammeFideliteCarte> listAllHistoriqueCarteProgrammeFideliteCarte = new ArrayList<>() ;

								listAllHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByCarteIdAndPdfCarteIdAndTypeActionIdAndDateActionBetween(entityToSave.getId(), dto.getProgrammeFideliteCarteId(), existingTypeAction.getId(), dateDebut, dateFin, false);

								if (Utilities.isNotEmpty(listAllHistoriqueCarteProgrammeFideliteCarte)  && listAllHistoriqueCarteProgrammeFideliteCarte.size() + 1 >  data.getNbreFoisCrediterParPdv()) {
									// creer le delit
									delitProgrammeFideliteRepository.save(delitProgrammeFideliteCarte) ;
									// error
									isDelitError = true ;
									response.setStatus(functionalError.DISALLOWED_OPERATION("Fraude : le nombre de flash pour tous les PDV est dépassé", locale));
									response.setHasError(true);
									return response;

								}																
							}
						}
					}
				}




				SouscriptionProgrammeFideliteCarte souscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCartes.get(0) ;

				Integer nbreActuelleAvantAction = souscriptionProgrammeFideliteCarte.getNbrePointActuelle() != null ? souscriptionProgrammeFideliteCarte.getNbrePointActuelle() : 0 ;
				//ajouter le nbre ou retrancher selon le typeCodeAction
				if (dto.getTypeActionCode().equals(TypeActionEnum.CREDITER)) {
					souscriptionProgrammeFideliteCarte.setNbrePointActuelle(nbreActuelleAvantAction + nbreDePoint);
				} else {
					if (dto.getTypeActionCode().equals(TypeActionEnum.DEBITER)) {
						if (nbreActuelleAvantAction - nbreDePoint < 0) {
							response.setStatus(functionalError.DISALLOWED_OPERATION("Vos points sont insuffisant pour cette action", locale));
							response.setHasError(true);
							return response;
						}
						souscriptionProgrammeFideliteCarte.setNbrePointActuelle(nbreActuelleAvantAction - nbreDePoint);
					}
				}

				souscriptionProgrammeFideliteCarte.setNbrePointPrecedent(nbreActuelleAvantAction);
				souscriptionProgrammeFideliteCarte.setUpdatedBy(request.getUser());
				souscriptionProgrammeFideliteCarte.setUpdatedAt(Utilities.getCurrentDate());
				itemsSouscriptionProgrammeFideliteCarte.add(souscriptionProgrammeFideliteCarte) ;



				HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarte = new HistoriqueCarteProgrammeFideliteCarte();
				//TypeAction typeAction = typeActionRepository.findByCode(TypeActionEnum.SOUSCRIPTION, false) ;


				historiqueCarteProgrammeFideliteCarte.setCarte(entityToSave);
				historiqueCarteProgrammeFideliteCarte.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				historiqueCarteProgrammeFideliteCarte.setTypeAction(existingTypeAction);
				historiqueCarteProgrammeFideliteCarte.setPointDeVente(existingPointDeVente);


				historiqueCarteProgrammeFideliteCarte.setNbrePointOperation(nbreDePoint);
				historiqueCarteProgrammeFideliteCarte.setSommeOperation(dto.getSomme());
				historiqueCarteProgrammeFideliteCarte.setChiffreDaffaire(dto.getChiffreDaffaire().intValue());
				historiqueCarteProgrammeFideliteCarte.setNbrePointActuel(souscriptionProgrammeFideliteCarte.getNbrePointActuelle());
				historiqueCarteProgrammeFideliteCarte.setNbrePointAvantAction(souscriptionProgrammeFideliteCarte.getNbrePointPrecedent());

				historiqueCarteProgrammeFideliteCarte.setCreatedBy(request.getUser());
				historiqueCarteProgrammeFideliteCarte.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCarteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
				historiqueCarteProgrammeFideliteCarte.setIsDeleted(false);
				historiqueCarteProgrammeFideliteCarte.setDescription(dto.getDescription()); // ajout du commentaire lors du scan

				// TODO : revoir l'heureAction, gerer dans dateAction
				// historiqueCarteProgrammeFideliteCarte.setHeureAction(Utilities.getCurrentDate()); // heureAction



				itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarte) ;


				// maj enseigne
				Enseigne enseignePdf = existingProgrammeFideliteCarte.getEnseigne() ;

				enseignePdf.setChiffreDaffaire(enseignePdf.getChiffreDaffaire() != null ? enseignePdf.getChiffreDaffaire() + dto.getChiffreDaffaire() : dto.getChiffreDaffaire());
				enseignePdf.setNbreTransaction(enseignePdf.getNbreTransaction() != null ? enseignePdf.getNbreTransaction() + 1 : 1);

				if (dto.getTypeActionCode().equals(TypeActionEnum.CREDITER)) {
					int currentRecompenseEngagerCarte = (nbreDePoint * existingProgrammeFideliteCarte.getCorrespondanceNbrePoint()) / existingProgrammeFideliteCarte.getNbrePointObtenu() ;
					enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + currentRecompenseEngagerCarte : currentRecompenseEngagerCarte);
				}else {
					if (dto.getTypeActionCode().equals(TypeActionEnum.DEBITER)) {
						enseignePdf.setRecompenseRembouserCarte(enseignePdf.getRecompenseRembouserCarte() != null ? enseignePdf.getRecompenseRembouserCarte() + dto.getSomme() : dto.getSomme());	
					}
				}
				enseignePdf.setUpdatedAt(Utilities.getCurrentDate());


				enseigneRepository.save(enseignePdf) ;


				// Verify if enseigne exist
				//				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
				//					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				//					if (existingEnseigne == null) {
				//						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setEnseigne(existingEnseigne);
				//				}
				//				if (Utilities.notBlank(dto.getCodeBarre())) {
				//					entityToSave.setCodeBarre(dto.getCodeBarre());
				//				}
				//				if (Utilities.notBlank(dto.getQrCode())) {
				//					entityToSave.setQrCode(dto.getQrCode());
				//				}
				//				if (Utilities.notBlank(dto.getCode())) {
				//					Carte existingEntity = carteRepository.findByCode(dto.getCode(), false);
				//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
				//						response.setStatus(functionalError.DATA_EXIST("carte -> " + dto.getCode(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les cartes", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCode(dto.getCode());
				//				}

				//entityToSave.setUpdatedBy(request.getUser());
				//entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!itemsSouscriptionProgrammeFideliteCarte.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarte> itemsSouscriptionProgrammeFideliteCarteSaved = null;
				// maj les donnees en base
				itemsSouscriptionProgrammeFideliteCarteSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) itemsSouscriptionProgrammeFideliteCarte);
				if (itemsSouscriptionProgrammeFideliteCarteSaved == null || itemsSouscriptionProgrammeFideliteCarteSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}

				List<HistoriqueCarteProgrammeFideliteCarte> itemsSavedHistoriqueCarteProgrammeFideliteCarte = null;
				// maj les donnees en base
				itemsSavedHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) itemsHistoriqueCarteProgrammeFideliteCarte);
				if (itemsSavedHistoriqueCarteProgrammeFideliteCarte == null || itemsSavedHistoriqueCarteProgrammeFideliteCarte.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : items) {
					CarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end flasherCarte Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null && !isDelitError) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}




	/**
	 * delete Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<CarteDto> delete(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Carte-----");

		response = new Response<CarteDto>();

		try {

			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {
				//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}


			List<Carte> items = new ArrayList<Carte>();

			for (CarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la carte existe
				Carte existingEntity = null;
				existingEntity = carteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notesClose
				List<NotesClose> listOfNotesClose = notesCloseRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfNotesClose != null && !listOfNotesClose.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotesClose.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// delitProgrammeFideliteCarte
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDelitProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFideliteTampon
				List<SouscriptionProgrammeFideliteTampon> listOfSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteTampon != null && !listOfSouscriptionProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFideliteCarte
				List<SouscriptionProgrammeFideliteCarte> listOfSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteCarte != null && !listOfSouscriptionProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				carteRepository.saveAll((Iterable<Carte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<CarteDto> forceDelete(Request<CarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Carte-----");

		response = new Response<CarteDto>();

		try {

			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {
				//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}


			List<Carte> items = new ArrayList<Carte>();

			for (CarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la carte existe
				Carte existingEntity = null;
				existingEntity = carteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notesClose
				List<NotesClose> listOfNotesClose = notesCloseRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfNotesClose != null && !listOfNotesClose.isEmpty()){
					Request<NotesCloseDto> deleteRequest = new Request<NotesCloseDto>();
					deleteRequest.setDatas(NotesCloseTransformer.INSTANCE.toDtos(listOfNotesClose));
					deleteRequest.setUser(request.getUser());
					Response<NotesCloseDto> deleteResponse = notesCloseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					Request<DelitProgrammeFideliteDto> deleteRequest = new Request<DelitProgrammeFideliteDto>();
					deleteRequest.setDatas(DelitProgrammeFideliteTransformer.INSTANCE.toDtos(listOfDelitProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<DelitProgrammeFideliteDto> deleteResponse = delitProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					Request<SouscriptionProgrammeFideliteDto> deleteRequest = new Request<SouscriptionProgrammeFideliteDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteDto> deleteResponse = souscriptionProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFideliteTampon
				List<SouscriptionProgrammeFideliteTampon> listOfSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteTampon != null && !listOfSouscriptionProgrammeFideliteTampon.isEmpty()){
					Request<SouscriptionProgrammeFideliteTamponDto> deleteRequest = new Request<SouscriptionProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteTamponDto> deleteResponse = souscriptionProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteTamponDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteTamponDto> deleteResponse = historiqueCarteProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFideliteCarte
				List<SouscriptionProgrammeFideliteCarte> listOfSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteCarte != null && !listOfSouscriptionProgrammeFideliteCarte.isEmpty()){
					Request<SouscriptionProgrammeFideliteCarteDto> deleteRequest = new Request<SouscriptionProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteCarteDto> deleteResponse = souscriptionProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteCarteDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteCarteDto> deleteResponse = historiqueCarteProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				carteRepository.saveAll((Iterable<Carte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get Carte by using CarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<CarteDto> getByCriteria(Request<CarteDto> request, Locale locale) {
		slf4jLogger.info("----begin get Carte-----");

		response = new Response<CarteDto>();

		try {
			List<Carte> items = null;
			items = carteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : items) {
					CarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(carteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("carte", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Carte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full CarteDto by using Carte as object.
	 *
	 * @param entity, locale
	 * @return CarteDto
	 *
	 */
	private CarteDto getFullInfos(Carte entity, Integer size, Locale locale) throws Exception {
		CarteDto dto = CarteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		// get datas souscriptions

		List<SouscriptionProgrammeFideliteCarte> datasSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByCarteId(dto.getId(), false) ;
		List<SouscriptionProgrammeFideliteTampon> datasSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByCarteId(dto.getId(), false) ;

		dto.setDatasSouscriptionProgrammeFideliteCarte(SouscriptionProgrammeFideliteCarteTransformer.INSTANCE.toDtos(datasSouscriptionProgrammeFideliteCarte));
		dto.setDatasSouscriptionProgrammeFideliteTampon(SouscriptionProgrammeFideliteTamponTransformer.INSTANCE.toDtos(datasSouscriptionProgrammeFideliteTampon));


		return dto;
	}
}
