


/*
 * Java transformer for entity table souscription_programme_fidelite_carte
 * Created on 2020-08-31 ( Time 10:15:45 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.TypeActionEnum;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "souscription_programme_fidelite_carte"
 *
 * @author Back-End developper
 *
 */
@Component
public class SouscriptionProgrammeFideliteCarteBusiness implements IBasicBusiness<Request<SouscriptionProgrammeFideliteCarteDto>, Response<SouscriptionProgrammeFideliteCarteDto>> {

	private Response<SouscriptionProgrammeFideliteCarteDto> response;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private TypeActionRepository typeActionRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository ;

	
	@Autowired
	private EnseigneRepository enseigneRepository ;
	@Autowired
	private SouscriptionProgrammeFideliteBusiness souscriptionProgrammeFideliteBusiness ;

	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private NotePdfBusiness notePdfBusiness;



	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@Autowired
	private ParamsUtils paramsUtils;


	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	public SouscriptionProgrammeFideliteCarteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteCarteDto> create(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteDto> datasSouscriptionProgrammeFidelite = new ArrayList<>();
			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();
			List<SouscriptionProgrammeFideliteCarte> itemsParrain = new ArrayList<SouscriptionProgrammeFideliteCarte>();
			List<HistoriqueCarteProgrammeFideliteCarte> itemsHistoriqueCarteProgrammeFideliteCarte = new ArrayList<>();

			for (SouscriptionProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("carteId", dto.getCarteId());
				fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				//fieldsToVerify.put("libelle", dto.getLibelle());
				//fieldsToVerify.put("code", dto.getCode());



				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("rasoinLocked", dto.getRasoinLocked());
				//fieldsToVerify.put("unsubscribe", dto.getUnsubscribe());
				//fieldsToVerify.put("raisonUnsubscribe", dto.getRaisonUnsubscribe());
				//fieldsToVerify.put("nbrePointActuelle", dto.getNbrePointActuelle());
				//fieldsToVerify.put("nbrePointPrecedent", dto.getNbrePointPrecedent());
				//fieldsToVerify.put("dateSouscription", dto.getDateSouscription());


				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if souscriptionProgrammeFideliteCarte to insert do not exist
				SouscriptionProgrammeFideliteCarte existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				//				existingEntity = souscriptionProgrammeFideliteCarteRepository.findByCode(dto.getCode(), false);
				//				if (existingEntity != null) {
				//					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getCode(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
				//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les souscriptionProgrammeFideliteCartes", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				// Verify if carte exist
				Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
				if (existingCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if programmeFideliteCarte exist
				ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
				if (existingProgrammeFideliteCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
					response.setHasError(true);
					return response;
				}

				// verifier l'unicite de la souscription au PDF
				List<SouscriptionProgrammeFideliteCarte> listExistingEntity = souscriptionProgrammeFideliteCarteRepository.findByCarteIdAndProgrammeFideliteCarteId(dto.getCarteId(), dto.getProgrammeFideliteCarteId(), false);
				if (Utilities.isNotEmpty(listExistingEntity)) {
					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getLibelle())) {
					existingEntity = souscriptionProgrammeFideliteCarteRepository.findByCarteIdAndLibelle(dto.getCarteId(), dto.getLibelle(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les souscriptionProgrammeFideliteCartes", locale));
						response.setHasError(true);
						return response;
					}
				}


				Integer pointObtenuParParrain = existingProgrammeFideliteCarte.getPointObtenuParParrain() ;
				Integer pointObtenuParFilleul = existingProgrammeFideliteCarte.getPointObtenuParFilleul() ;
				Integer pointObtenuApresSouscription = existingProgrammeFideliteCarte.getPointObtenuApresSouscription() != null ? existingProgrammeFideliteCarte.getPointObtenuApresSouscription() : 0 ;
				Integer correspondanceNbrePointPDF = existingProgrammeFideliteCarte.getCorrespondanceNbrePoint() ;
				Integer nbrePointObtenuPDF = existingProgrammeFideliteCarte.getCorrespondanceNbrePoint() ;
				Boolean boolSommeOperation = correspondanceNbrePointPDF != null && nbrePointObtenuPDF != null ;

				Integer sommeOperation = 0 ;
				DecimalFormat df = new DecimalFormat("#");


				if (!Utilities.notBlank(dto.getLibelle())) {
					dto.setLibelle(existingProgrammeFideliteCarte.getLibelle()) ;
				}


				HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarte = new HistoriqueCarteProgrammeFideliteCarte();
				TypeAction typeAction = typeActionRepository.findByCode(TypeActionEnum.SOUSCRIPTION, false) ;


				historiqueCarteProgrammeFideliteCarte.setCarte(existingCarte);
				historiqueCarteProgrammeFideliteCarte.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				if (typeAction != null) {
					historiqueCarteProgrammeFideliteCarte.setTypeAction(typeAction);
				}
				historiqueCarteProgrammeFideliteCarte.setNbrePointOperation(pointObtenuApresSouscription);
				if (boolSommeOperation) {
					sommeOperation = Integer.parseInt(
							df.format((double)(correspondanceNbrePointPDF * pointObtenuApresSouscription / nbrePointObtenuPDF)));
				}
				historiqueCarteProgrammeFideliteCarte.setSommeOperation(sommeOperation);
				historiqueCarteProgrammeFideliteCarte.setNbrePointActuel(pointObtenuApresSouscription);
				historiqueCarteProgrammeFideliteCarte.setNbrePointAvantAction(0);

				historiqueCarteProgrammeFideliteCarte.setCreatedBy(request.getUser());
				historiqueCarteProgrammeFideliteCarte.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCarteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
				historiqueCarteProgrammeFideliteCarte.setIsDeleted(false);

				// TODO : revoir l'heureAction


				itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarte) ;

				// TODO : des verifications sur la validite du PDF (la souscription de l'enseigne)


				// maj enseigne
				Enseigne enseignePdf = existingProgrammeFideliteCarte.getEnseigne() ;

				//enseignePdf.setChiffreDaffaire(enseignePdf.getChiffreDaffaire() != null ? enseignePdf.getChiffreDaffaire() + dto.getChiffreDaffaire() : dto.getChiffreDaffaire());
				//enseignePdf.setNbreTransaction(enseignePdf.getNbreTransaction() != null ? enseignePdf.getNbreTransaction() + 1 : 1);
				enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + sommeOperation : sommeOperation);	


				dto.setIsLocked(false);
				dto.setUnsubscribe(false);


				// TODO : nbre de point initial dependra du PDF
				dto.setNbrePointActuelle(pointObtenuApresSouscription);
				dto.setNbrePointPrecedent(0);


				// TODO : parrainage

				if (Utilities.notBlank(dto.getEmailParrain()) || Utilities.notBlank(dto.getTelephoneParrain()) ) {

					SouscriptionProgrammeFideliteCarte existingSPFCarteParrain = 
							souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByUserTelephoneAndByProgrammeFideliteCarteId(dto.getTelephoneParrain(), dto.getProgrammeFideliteCarteId(), false) ;

					if (existingSPFCarteParrain == null ) {
						existingSPFCarteParrain = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByUserEmailAndByProgrammeFideliteCarteId(dto.getEmailParrain(), dto.getProgrammeFideliteCarteId(), false) ;
						if (existingSPFCarteParrain == null ) {
							response.setStatus(functionalError.DATA_NOT_EXIST("Désolé, votre parrain n'est pas inscrit à ce programme. Veuillez modifier ou laisser les champs vides", locale));
							response.setHasError(true);
							return response;
						}
					}


					// maj souscription parrain 

					TypeAction typeActionParrain = typeActionRepository.findByCode(TypeActionEnum.PARRAINER, false) ;

					existingSPFCarteParrain.setNbrePointPrecedent(existingSPFCarteParrain.getNbrePointActuelle() != null ? existingSPFCarteParrain.getNbrePointActuelle() : 0);
					existingSPFCarteParrain.setNbrePointActuelle(existingSPFCarteParrain.getNbrePointActuelle() != null ? existingSPFCarteParrain.getNbrePointActuelle() + pointObtenuParParrain : pointObtenuParParrain);
					existingSPFCarteParrain.setUpdatedBy(existingCarte.getUser().getId());
					existingSPFCarteParrain.setUpdatedAt(Utilities.getCurrentDate());
					itemsParrain.add(existingSPFCarteParrain) ;


					HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarteParrain = new HistoriqueCarteProgrammeFideliteCarte() ;

					historiqueCarteProgrammeFideliteCarteParrain.setCarte(existingSPFCarteParrain.getCarte());
					historiqueCarteProgrammeFideliteCarteParrain.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
					historiqueCarteProgrammeFideliteCarteParrain.setTypeAction(typeActionParrain);

					sommeOperation = 0 ;
					historiqueCarteProgrammeFideliteCarteParrain.setNbrePointOperation(pointObtenuParParrain);
					if (boolSommeOperation) {
						sommeOperation = Integer.parseInt(
								df.format((double)(correspondanceNbrePointPDF * pointObtenuParParrain / nbrePointObtenuPDF)));
					}
					historiqueCarteProgrammeFideliteCarteParrain.setSommeOperation(sommeOperation);
					historiqueCarteProgrammeFideliteCarteParrain.setNbrePointActuel(existingSPFCarteParrain.getNbrePointActuelle());
					historiqueCarteProgrammeFideliteCarteParrain.setNbrePointAvantAction(existingSPFCarteParrain.getNbrePointPrecedent());

					historiqueCarteProgrammeFideliteCarteParrain.setCreatedBy(request.getUser());
					historiqueCarteProgrammeFideliteCarteParrain.setDateAction(Utilities.getCurrentDate()); // dateSouscription
					historiqueCarteProgrammeFideliteCarteParrain.setCreatedAt(Utilities.getCurrentDate());
					historiqueCarteProgrammeFideliteCarteParrain.setIsDeleted(false);

					itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarteParrain) ;


					enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + sommeOperation : sommeOperation);	

					// ajout a la soussicrption du filleul
					dto.setNbrePointPrecedent(dto.getNbrePointActuelle());
					dto.setNbrePointActuelle(pointObtenuApresSouscription + pointObtenuParFilleul);


					HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarteFilleul = new HistoriqueCarteProgrammeFideliteCarte() ;
					TypeAction typeActionFilleul = typeActionRepository.findByCode(TypeActionEnum.ETRE_PARRAINER, false) ;

					historiqueCarteProgrammeFideliteCarteFilleul.setCarte(existingCarte);
					historiqueCarteProgrammeFideliteCarteFilleul.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
					historiqueCarteProgrammeFideliteCarteFilleul.setTypeAction(typeActionFilleul);
					sommeOperation = 0 ;
					historiqueCarteProgrammeFideliteCarteFilleul.setNbrePointOperation(pointObtenuParFilleul);
					if (boolSommeOperation) {
						sommeOperation = Integer.parseInt(
								df.format((double)(correspondanceNbrePointPDF * pointObtenuParFilleul / nbrePointObtenuPDF)));
					}
					historiqueCarteProgrammeFideliteCarteFilleul.setSommeOperation(sommeOperation);
					historiqueCarteProgrammeFideliteCarteFilleul.setNbrePointActuel(dto.getNbrePointActuelle());
					historiqueCarteProgrammeFideliteCarteFilleul.setNbrePointAvantAction(dto.getNbrePointPrecedent());

					historiqueCarteProgrammeFideliteCarteFilleul.setCreatedBy(request.getUser());
					historiqueCarteProgrammeFideliteCarteFilleul.setDateAction(Utilities.getCurrentDate()); // dateSouscription
					historiqueCarteProgrammeFideliteCarteFilleul.setCreatedAt(Utilities.getCurrentDate());
					historiqueCarteProgrammeFideliteCarteFilleul.setIsDeleted(false);


					itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarteFilleul) ;

					enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + sommeOperation : sommeOperation);	

				}

				enseignePdf.setUpdatedAt(Utilities.getCurrentDate());
				enseigneRepository.save(enseignePdf) ;



				// generation du code de la souscription
				Utilities.generateCodeSouscription() ;




				SouscriptionProgrammeFideliteCarte entityToSave = null;
				entityToSave = SouscriptionProgrammeFideliteCarteTransformer.INSTANCE.toEntity(dto, existingCarte, existingProgrammeFideliteCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setDateSouscription(Utilities.getCurrentDate()); // dateSouscription
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);


				SouscriptionProgrammeFideliteDto dataSouscriptionProgrammeFideliteDto = new SouscriptionProgrammeFideliteDto() ;
				dataSouscriptionProgrammeFideliteDto.setCarteId(dto.getCarteId());
				dataSouscriptionProgrammeFideliteDto.setEnseigneId(existingProgrammeFideliteCarte.getEnseigne().getId());
				dataSouscriptionProgrammeFideliteDto.setProgrammeFideliteCarteId(dto.getProgrammeFideliteCarteId());
				datasSouscriptionProgrammeFidelite.add(dataSouscriptionProgrammeFideliteDto) ;
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarte> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				if (!itemsParrain.isEmpty()) {
					List<SouscriptionProgrammeFideliteCarte> itemsParrainSaved = null;
					// inserer les donnees en base de donnees
					itemsParrainSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) itemsParrain);
					if (itemsParrainSaved == null || itemsParrainSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}


				// pour faciliter certains get 
				Request<SouscriptionProgrammeFideliteDto> req = new Request<>() ;
				req.setDatas(datasSouscriptionProgrammeFidelite);
				req.setUser(request.getUser());
				Response<SouscriptionProgrammeFideliteDto> res = souscriptionProgrammeFideliteBusiness.create(req, locale) ;
				if (res != null && res.isHasError()) {
					response.setStatus(res.getStatus());
					response.setHasError(true);
					return response;
				}


				List<HistoriqueCarteProgrammeFideliteCarte> itemsSavedHistoriqueCarteProgrammeFideliteCarte = null;
				if (!itemsHistoriqueCarteProgrammeFideliteCarte.isEmpty()) {
					// maj les donnees en base
					itemsSavedHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) itemsHistoriqueCarteProgrammeFideliteCarte);
					if (itemsSavedHistoriqueCarteProgrammeFideliteCarte == null || itemsSavedHistoriqueCarteProgrammeFideliteCarte.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}


				List<SouscriptionProgrammeFideliteCarteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteCarteDto>();
				for (SouscriptionProgrammeFideliteCarte entity : itemsSaved) {
					SouscriptionProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteCarteDto> update(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();

			for (SouscriptionProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteCarte existe
				SouscriptionProgrammeFideliteCarte entityToSave = null;
				entityToSave = souscriptionProgrammeFideliteCarteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if carte exist
				if (dto.getCarteId() != null && dto.getCarteId() > 0){
					Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
					if (existingCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCarte(existingCarte);
				}
				// Verify if programmeFideliteCarte exist
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
					ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				}
				//				if (Utilities.notBlank(dto.getCode())) {
				//					SouscriptionProgrammeFideliteCarte existingEntity = souscriptionProgrammeFideliteCarteRepository.findByCode(dto.getCode(), false);
				//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
				//						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getCode(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les souscriptionProgrammeFideliteCartes", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCode(dto.getCode());
				//				}
				if (Utilities.notBlank(dto.getLibelle())) {
					SouscriptionProgrammeFideliteCarte existingEntity = souscriptionProgrammeFideliteCarteRepository.findByCarteIdAndLibelle(entityToSave.getCarte().getId(), dto.getLibelle(), false);

					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les souscriptionProgrammeFideliteCartes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (Utilities.notBlank(dto.getRaisonLocked())) {
					entityToSave.setRaisonLocked(dto.getRaisonLocked());
				}
				if (dto.getIsNotifed() != null) {
					entityToSave.setIsNotifed(dto.getIsNotifed());
				}
				if (Utilities.notBlank(dto.getMessageNotifed())) {
					entityToSave.setMessageNotifed(dto.getMessageNotifed());
				}
				if (Utilities.notBlank(dto.getEmailParrain())) {
					entityToSave.setEmailParrain(dto.getEmailParrain());
				}
				if (Utilities.notBlank(dto.getTelephoneParrain())) {
					entityToSave.setTelephoneParrain(dto.getTelephoneParrain());
				}
				if (dto.getUnsubscribe() != null) {
					entityToSave.setUnsubscribe(dto.getUnsubscribe());
				}
				if (dto.getIsShowLastNotification() != null) {
					entityToSave.setIsShowLastNotification(dto.getIsShowLastNotification());
				}
				if (Utilities.notBlank(dto.getRaisonUnsubscribe())) {
					entityToSave.setRaisonUnsubscribe(dto.getRaisonUnsubscribe());
				}
				if (dto.getNbrePointActuelle() != null && dto.getNbrePointActuelle() > 0) {
					entityToSave.setNbrePointActuelle(dto.getNbrePointActuelle());
				}
				if (dto.getNbrePointPrecedent() != null && dto.getNbrePointPrecedent() > 0) {
					entityToSave.setNbrePointPrecedent(dto.getNbrePointPrecedent());
				}


				//				if (Utilities.notBlank(dto.getDateSouscription())) {
				//					entityToSave.setDateSouscription(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSouscription()));
				//				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteCarteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteCarteDto>();
				for (SouscriptionProgrammeFideliteCarte entity : itemsSaved) {
					SouscriptionProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteCarteDto> delete(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();

			for (SouscriptionProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteCarte existe
				SouscriptionProgrammeFideliteCarte existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findBySouscriptionProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findBySouscriptionProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * parrainer SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<SouscriptionProgrammeFideliteCarteDto> parrainer(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin parrainer SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();
			List<HistoriqueCarteProgrammeFideliteCarte> itemsHistoriqueCarteProgrammeFideliteCarte = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();



			SouscriptionProgrammeFideliteCarteDto dto = request.getData() ;
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("carteId", dto.getCarteId());
			fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());

			fieldsToVerify.put("datasUser", dto.getDatasUser());
			//fieldsToVerify.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			if(!Utilities.isNotEmpty(dto.getDatasUser())) {
				response.setStatus(functionalError.FIELD_EMPTY("datasUser", locale));
				response.setHasError(true);
				return response;
			}

			/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
			 */

			// Verifier si la souscriptionProgrammeFideliteCarte existe
			SouscriptionProgrammeFideliteCarte entityToSave = null;
			entityToSave = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(dto.getCarteId(), dto.getProgrammeFideliteCarteId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			ProgrammeFideliteCarte programmeFideliteCarte = entityToSave.getProgrammeFideliteCarte() ;
			Carte carteParrain = entityToSave.getCarte() ;








			//TypeAction typeAction = typeActionRepository.findByCode(TypeActionEnum.PARRAINER, false) ;

			TypeAction typeActionParrain = typeActionRepository.findByCode(TypeActionEnum.PARRAINER, false) ;
			TypeAction typeActionFilleul = typeActionRepository.findByCode(TypeActionEnum.ETRE_PARRAINER, false) ;

			Integer entityToSaveId = entityToSave.getId();
			Integer pointObtenuParParrain = programmeFideliteCarte.getPointObtenuParParrain() != null ? programmeFideliteCarte.getPointObtenuParParrain() : 0 ;
			Integer pointObtenuParFilleul = programmeFideliteCarte.getPointObtenuParFilleul()!= null ? programmeFideliteCarte.getPointObtenuParFilleul() : 0 ;
			Integer correspondanceNbrePointPDF = programmeFideliteCarte.getCorrespondanceNbrePoint() ;
			Integer nbrePointObtenuPDF = programmeFideliteCarte.getCorrespondanceNbrePoint() ;
			Boolean boolSommeOperation = correspondanceNbrePointPDF != null && nbrePointObtenuPDF != null ;

			int sommeOperation = 0 ;
			DecimalFormat df = new DecimalFormat("#");

			entityToSave.setNbrePointPrecedent(entityToSave.getNbrePointActuelle());
			entityToSave.setNbrePointActuelle(entityToSave.getNbrePointActuelle() + pointObtenuParParrain);

			
			// maj enseigne
			Enseigne enseignePdf = programmeFideliteCarte.getEnseigne() ;
			
			//enseignePdfCarte.setChiffreDaffaire(enseignePdfCarte.getChiffreDaffaire() != null ? enseignePdfCarte.getChiffreDaffaire() + dto.getChiffreDaffaire() : dto.getChiffreDaffaire());
			//enseignePdf.setNbreTransaction(enseignePdf.getNbreTransaction() != null ? enseignePdf.getNbreTransaction() + 1 : 1);

			
			
			// verifier que les filleuls appartiennent au meme PDF

			for (UserDto filleul : dto.getDatasUser()) {
				if (Utilities.notBlank(filleul.getEmail()) || Utilities.notBlank(filleul.getTelephone()) ) {
					sommeOperation = 0 ;
					// existence compte
					Carte carteFilleul = carteRepository.findCarteByTelephone(filleul.getTelephone(), false) ;
					if (carteFilleul == null) {
						carteFilleul = carteRepository.findCarteByEmail(filleul.getEmail(), false) ;
						if (carteFilleul == null) {
							response.setStatus(functionalError.DATA_NOT_EXIST("filleul -> " + filleul.getTelephone(), locale));
							response.setHasError(true);
							return response;
						}
					}

					if (carteFilleul.getId().equals(carteParrain.getId())) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de se parrainer soi meme", locale));
						response.setHasError(true);
						return response;
					}

					// existence souscription au PDF
					SouscriptionProgrammeFideliteCarte souscriptionPDFFilleul = null;
					souscriptionPDFFilleul = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(carteFilleul.getId(), dto.getProgrammeFideliteCarteId(), false);
					if (souscriptionPDFFilleul == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + filleul.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}				
					souscriptionPDFFilleul.setNbrePointPrecedent(souscriptionPDFFilleul.getNbrePointActuelle() != null ? souscriptionPDFFilleul.getNbrePointActuelle() :0);
					souscriptionPDFFilleul.setNbrePointActuelle(souscriptionPDFFilleul.getNbrePointActuelle() != null ? souscriptionPDFFilleul.getNbrePointActuelle() + pointObtenuParFilleul : pointObtenuParFilleul);
					souscriptionPDFFilleul.setUpdatedAt(Utilities.getCurrentDate());
					souscriptionPDFFilleul.setUpdatedBy(request.getUser());


					// verifier que le parrain est plus ancien que le filleul
					if (carteFilleul.getUser() != null && carteFilleul.getUser().getCreatedAt().after(utilisateur.getCreatedAt()) ) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de parrainer celui est plus ancien que nous", locale));
						response.setHasError(true);
						return response;
					}


					// unicite du parrainage
					HistoriqueCarteProgrammeFideliteCarte uniciteParrainage =  historiqueCarteProgrammeFideliteCarteRepository.uniciteParrainage(dto.getProgrammeFideliteCarteId(), carteFilleul.getId(), typeActionFilleul.getCode(), request.getUser(), false);
					if (uniciteParrainage != null) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de parrainer plus d'une fois", locale));
						response.setHasError(true);
						return response;
					}	

					uniciteParrainage =  historiqueCarteProgrammeFideliteCarteRepository.uniciteParrainage(dto.getProgrammeFideliteCarteId(), carteParrain.getId(), typeActionParrain.getCode(), carteFilleul.getUser().getId(), false);
					if (uniciteParrainage != null) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Impossible de parrainer celui qui nous a déjà parrainé", locale));
						response.setHasError(true);
						return response;
					}	
					// *********************


					// formation de la liste des HistoriqueCarteProgrammeFideliteCarte
					HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarte = new HistoriqueCarteProgrammeFideliteCarte() ;

					historiqueCarteProgrammeFideliteCarte.setCarte(carteFilleul);
					historiqueCarteProgrammeFideliteCarte.setProgrammeFideliteCarte(souscriptionPDFFilleul.getProgrammeFideliteCarte());
					if (typeActionFilleul  != null) {
						historiqueCarteProgrammeFideliteCarte.setTypeAction(typeActionFilleul);
					}				

					historiqueCarteProgrammeFideliteCarte.setNbrePointOperation(pointObtenuParFilleul);
					if (boolSommeOperation) {
						sommeOperation = Integer.parseInt(
								df.format((double)(correspondanceNbrePointPDF * pointObtenuParFilleul / nbrePointObtenuPDF)));
					}
					historiqueCarteProgrammeFideliteCarte.setSommeOperation(sommeOperation);

					historiqueCarteProgrammeFideliteCarte.setNbrePointActuel(souscriptionPDFFilleul.getNbrePointActuelle());
					historiqueCarteProgrammeFideliteCarte.setNbrePointAvantAction(souscriptionPDFFilleul.getNbrePointPrecedent());

					historiqueCarteProgrammeFideliteCarte.setCreatedBy(request.getUser());
					historiqueCarteProgrammeFideliteCarte.setDateAction(Utilities.getCurrentDate()); // dateSouscription
					historiqueCarteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
					historiqueCarteProgrammeFideliteCarte.setIsDeleted(false);

					// TODO : revoir l'heureAction

					
					enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + sommeOperation : sommeOperation);	


					itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarte) ;
					items.add(souscriptionPDFFilleul) ;
				}else {
					response.setStatus(functionalError.FIELD_EMPTY("email et telephone", locale));
					response.setHasError(true);
					return response;
				}


			}

			HistoriqueCarteProgrammeFideliteCarte historiqueCarteProgrammeFideliteCarteParrain = new HistoriqueCarteProgrammeFideliteCarte() ;

			historiqueCarteProgrammeFideliteCarteParrain.setCarte(carteParrain);
			historiqueCarteProgrammeFideliteCarteParrain.setProgrammeFideliteCarte(programmeFideliteCarte);
			if (typeActionParrain != null) {
				historiqueCarteProgrammeFideliteCarteParrain.setTypeAction(typeActionParrain);
			}			
			sommeOperation = 0 ;
			historiqueCarteProgrammeFideliteCarteParrain.setNbrePointOperation(pointObtenuParParrain);
			if (boolSommeOperation) {
				sommeOperation = Integer.parseInt(
						df.format((double)(correspondanceNbrePointPDF * pointObtenuParParrain / nbrePointObtenuPDF)));
			}
			historiqueCarteProgrammeFideliteCarteParrain.setSommeOperation(sommeOperation);

			historiqueCarteProgrammeFideliteCarteParrain.setNbrePointActuel(entityToSave.getNbrePointActuelle());
			historiqueCarteProgrammeFideliteCarteParrain.setNbrePointAvantAction(entityToSave.getNbrePointPrecedent());

			historiqueCarteProgrammeFideliteCarteParrain.setCreatedBy(request.getUser());
			historiqueCarteProgrammeFideliteCarteParrain.setDateAction(Utilities.getCurrentDate()); // dateSouscription
			historiqueCarteProgrammeFideliteCarteParrain.setCreatedAt(Utilities.getCurrentDate());
			historiqueCarteProgrammeFideliteCarteParrain.setIsDeleted(false);

			itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCarteProgrammeFideliteCarteParrain) ;

			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			items.add(entityToSave);
			
			
			enseignePdf.setRecompenseEngagerCarte(enseignePdf.getRecompenseEngagerCarte() != null ? enseignePdf.getRecompenseEngagerCarte() + sommeOperation : sommeOperation);	
			
			enseignePdf.setUpdatedAt(Utilities.getCurrentDate());
			
			enseigneRepository.save(enseignePdf) ;


			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<HistoriqueCarteProgrammeFideliteCarte> itemsSavedHistoriqueCarteProgrammeFideliteCarte = null;
				// maj les donnees en base
				itemsSavedHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) itemsHistoriqueCarteProgrammeFideliteCarte);
				if (itemsSavedHistoriqueCarteProgrammeFideliteCarte == null || itemsSavedHistoriqueCarteProgrammeFideliteCarte.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteCarteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteCarteDto>();
				SouscriptionProgrammeFideliteCarteDto souscriptionProgrammeFideliteCarteDto = getFullInfos(entityToSave, itemsSaved.size(), locale);
				if (souscriptionProgrammeFideliteCarteDto != null) {
					itemsDto.add(souscriptionProgrammeFideliteCarteDto);
				}

				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end parrainer SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * partagerPoint SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<SouscriptionProgrammeFideliteCarteDto> partagerPoint(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin partagerPoint SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();
			List<HistoriqueCarteProgrammeFideliteCarte> itemsHistoriqueCarteProgrammeFideliteCarte = new ArrayList<HistoriqueCarteProgrammeFideliteCarte>();

			for (SouscriptionProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("carteId", dto.getCarteId());
				fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				fieldsToVerify.put("telephoneReceveur", dto.getTelephoneReceveur());
				fieldsToVerify.put("pointPartager", dto.getPointPartager());

				//fieldsToVerify.put("emailReceveur", dto.getEmailReceveur());

				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}


				/*
	        // Verify if utilisateur exist
	        User utilisateur = userRepository.findById(request.getUser(), false);
	        if (utilisateur == null) {
	          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
	          response.setHasError(true);
	          return response;
	        }
				 */

				// Verifier si la souscriptionProgrammeFideliteCarte existe
				SouscriptionProgrammeFideliteCarte entityToSave = null;
				entityToSave = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(dto.getCarteId(), dto.getProgrammeFideliteCarteId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getPointPartager() >  entityToSave.getNbrePointActuelle()) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("nbre de point à partager supérieur -> " + dto.getPointPartager() , locale));
					response.setHasError(true);
					return response;
				}	
				ProgrammeFideliteCarte programmeFideliteCarte = entityToSave.getProgrammeFideliteCarte() ;
				Carte carteDonneur = entityToSave.getCarte() ;

				TypeAction typeActionReceveur = typeActionRepository.findByCode(TypeActionEnum.RECU_PAR_PARTAGER, false) ;
				TypeAction typeActionDonneur = typeActionRepository.findByCode(TypeActionEnum.PARTAGER, false) ;
				Integer correspondanceNbrePointPDF = programmeFideliteCarte.getCorrespondanceNbrePoint() ;
				Integer nbrePointObtenuPDF = programmeFideliteCarte.getCorrespondanceNbrePoint() ;
				Boolean boolSommeOperation = correspondanceNbrePointPDF != null && nbrePointObtenuPDF != null ;

				int sommeOperation = 0 ;
				DecimalFormat df = new DecimalFormat("#");

				Integer entityToSaveId = entityToSave.getId();
				//Integer nbrePointActuelle = programmeFideliteCarte.getPointObtenuParParrain();
				//Integer pointObtenuParFilleul = programmeFideliteCarte.getPointObtenuParFilleul();


				entityToSave.setNbrePointPrecedent(entityToSave.getNbrePointActuelle());
				entityToSave.setNbrePointActuelle(entityToSave.getNbrePointActuelle() - dto.getPointPartager());

				// verifier que les filleuls appartiennent au meme PDF

				// existence compte
				Carte carteReceveur = carteRepository.findCarteByTelephone(dto.getTelephoneReceveur(), false) ;

				if (carteReceveur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getTelephoneReceveur(), locale));
					response.setHasError(true);
					return response;
				}

				// existence souscription au PDF
				SouscriptionProgrammeFideliteCarte souscriptionPDFReceveur = null;
				souscriptionPDFReceveur = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(carteReceveur.getId(), dto.getProgrammeFideliteCarteId(), false);
				if (souscriptionPDFReceveur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getTelephoneReceveur(), locale));
					response.setHasError(true);
					return response;
				}				
				souscriptionPDFReceveur.setNbrePointPrecedent(souscriptionPDFReceveur.getNbrePointActuelle());
				souscriptionPDFReceveur.setNbrePointActuelle(souscriptionPDFReceveur.getNbrePointActuelle() + dto.getPointPartager());
				souscriptionPDFReceveur.setUpdatedAt(Utilities.getCurrentDate());
				souscriptionPDFReceveur.setUpdatedBy(request.getUser());



				// formation de la liste des HistoriqueCarteProgrammeFideliteCarte
				HistoriqueCarteProgrammeFideliteCarte historiqueCartePDFReceveur = new HistoriqueCarteProgrammeFideliteCarte() ;

				historiqueCartePDFReceveur.setCarte(carteReceveur);
				historiqueCartePDFReceveur.setProgrammeFideliteCarte(souscriptionPDFReceveur.getProgrammeFideliteCarte());
				if (typeActionReceveur != null) {
					historiqueCartePDFReceveur.setTypeAction(typeActionReceveur);
				}

				sommeOperation = 0 ;
				historiqueCartePDFReceveur.setNbrePointOperation(dto.getPointPartager());
				if (boolSommeOperation) {
					sommeOperation = Integer.parseInt(
							df.format((double)(correspondanceNbrePointPDF * dto.getPointPartager() / nbrePointObtenuPDF)));
				}
				historiqueCartePDFReceveur.setSommeOperation(sommeOperation);
				historiqueCartePDFReceveur.setNbrePointActuel(souscriptionPDFReceveur.getNbrePointActuelle());
				historiqueCartePDFReceveur.setNbrePointAvantAction(souscriptionPDFReceveur.getNbrePointPrecedent());
				historiqueCartePDFReceveur.setDescription(dto.getDescription());

				historiqueCartePDFReceveur.setCreatedBy(request.getUser());
				historiqueCartePDFReceveur.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCartePDFReceveur.setCreatedAt(Utilities.getCurrentDate());
				historiqueCartePDFReceveur.setIsDeleted(false);

				// TODO : revoir l'heureAction

				itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCartePDFReceveur) ;
				items.add(souscriptionPDFReceveur) ;


				HistoriqueCarteProgrammeFideliteCarte historiqueCartePDFDonneur = new HistoriqueCarteProgrammeFideliteCarte() ;

				historiqueCartePDFDonneur.setCarte(carteDonneur);
				historiqueCartePDFDonneur.setProgrammeFideliteCarte(programmeFideliteCarte);
				if (typeActionDonneur != null) {
					historiqueCartePDFDonneur.setTypeAction(typeActionDonneur);
				}
				sommeOperation = 0 ;
				historiqueCartePDFDonneur.setNbrePointOperation(dto.getPointPartager());
				if (boolSommeOperation) {
					sommeOperation = Integer.parseInt(
							df.format((double)(correspondanceNbrePointPDF * dto.getPointPartager() / nbrePointObtenuPDF)));
				}
				historiqueCartePDFDonneur.setSommeOperation(sommeOperation);

				historiqueCartePDFDonneur.setNbrePointActuel(entityToSave.getNbrePointActuelle());
				historiqueCartePDFDonneur.setNbrePointAvantAction(entityToSave.getNbrePointPrecedent());
				historiqueCartePDFDonneur.setCreatedBy(request.getUser());
				historiqueCartePDFDonneur.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCartePDFDonneur.setCreatedAt(Utilities.getCurrentDate());
				historiqueCartePDFDonneur.setIsDeleted(false);

				itemsHistoriqueCarteProgrammeFideliteCarte.add(historiqueCartePDFDonneur) ;

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);

			}



			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<HistoriqueCarteProgrammeFideliteCarte> itemsSavedHistoriqueCarteProgrammeFideliteCarte = null;
				// maj les donnees en base
				itemsSavedHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteCarte>) itemsHistoriqueCarteProgrammeFideliteCarte);
				if (itemsSavedHistoriqueCarteProgrammeFideliteCarte == null || itemsSavedHistoriqueCarteProgrammeFideliteCarte.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteCarteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteCarteDto>();				
				for (SouscriptionProgrammeFideliteCarte entity : itemsSaved) {
					SouscriptionProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}

				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end partagerPoint SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}




	/**
	 * forceDelete SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SouscriptionProgrammeFideliteCarteDto> forceDelete(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteCarte> items = new ArrayList<SouscriptionProgrammeFideliteCarte>();

			for (SouscriptionProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteCarte existe
				SouscriptionProgrammeFideliteCarte existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findBySouscriptionProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findBySouscriptionProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteCarteRepository.saveAll((Iterable<SouscriptionProgrammeFideliteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get SouscriptionProgrammeFideliteCarte by using SouscriptionProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SouscriptionProgrammeFideliteCarteDto> getByCriteria(Request<SouscriptionProgrammeFideliteCarteDto> request, Locale locale) {
		slf4jLogger.info("----begin get SouscriptionProgrammeFideliteCarte-----");

		response = new Response<SouscriptionProgrammeFideliteCarteDto>();

		try {
			List<SouscriptionProgrammeFideliteCarte> items = null;
			items = souscriptionProgrammeFideliteCarteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SouscriptionProgrammeFideliteCarteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteCarteDto>();
				for (SouscriptionProgrammeFideliteCarte entity : items) {
					SouscriptionProgrammeFideliteCarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(souscriptionProgrammeFideliteCarteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("souscriptionProgrammeFideliteCarte", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get SouscriptionProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SouscriptionProgrammeFideliteCarteDto by using SouscriptionProgrammeFideliteCarte as object.
	 *
	 * @param entity, locale
	 * @return SouscriptionProgrammeFideliteCarteDto
	 *
	 */
	private SouscriptionProgrammeFideliteCarteDto getFullInfos(SouscriptionProgrammeFideliteCarte entity, Integer size, Locale locale) throws Exception {
		SouscriptionProgrammeFideliteCarteDto dto = SouscriptionProgrammeFideliteCarteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (entity.getProgrammeFideliteCarte() != null && entity.getProgrammeFideliteCarte().getUrlCarte() != null) {
			dto.setUrlLogo(paramsUtils.getBaseUrlImageApp() + entity.getProgrammeFideliteCarte().getUrlCarte());
		}
		if (size > 1) {
			return dto;
		}

		// info carte
		Carte carte = carteRepository.findById(dto.getCarteId(), false) ;
		if (carte != null) {
			dto.setDataCarte(CarteTransformer.INSTANCE.toDto(carte));
		}
		// info programmeFideliteCarte
		ProgrammeFideliteCarte programmeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false) ;
		if (programmeFideliteCarte != null) {
			dto.setDataProgrammeFideliteCarte(ProgrammeFideliteCarteTransformer.INSTANCE.toDto(programmeFideliteCarte));
		}

		return dto;
	}
}
