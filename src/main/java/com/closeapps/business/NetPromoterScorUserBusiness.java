


/*
 * Java transformer for entity table net_promoter_scor_user
 * Created on 2021-08-26 ( Time 17:44:07 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "net_promoter_scor_user"
 *
 * @author Back-End developper
 *
 */
@Component
public class NetPromoterScorUserBusiness implements IBasicBusiness<Request<NetPromoterScorUserDto>, Response<NetPromoterScorUserDto>> {

	private Response<NetPromoterScorUserDto> response;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TypeNoteNpsRepository typeNoteNpsRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public NetPromoterScorUserBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create NetPromoterScorUser by using NetPromoterScorUserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NetPromoterScorUserDto> create(Request<NetPromoterScorUserDto> request, Locale locale)  {
		slf4jLogger.info("----begin create NetPromoterScorUser-----");

		response = new Response<NetPromoterScorUserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NetPromoterScorUser> items = new ArrayList<NetPromoterScorUser>();

			for (NetPromoterScorUserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("commentaire", dto.getCommentaire());
				//fieldsToVerify.put("retourEnseigne", dto.getRetourEnseigne());
				fieldsToVerify.put("note", dto.getNote());
				//fieldsToVerify.put("dateAction", dto.getDateAction());
				fieldsToVerify.put("userId", dto.getUserId());
				fieldsToVerify.put("typeNoteNpsCode", dto.getTypeNoteNpsCode());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				//fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				//fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
				//fieldsToVerify.put("souscriptionProgrammeFideliteCarteId", dto.getSouscriptionProgrammeFideliteCarteId());
				//fieldsToVerify.put("souscriptionProgrammeFideliteTamponId", dto.getSouscriptionProgrammeFideliteTamponId());
				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */


				if (dto.getProgrammeFideliteCarteId() == null && dto.getProgrammeFideliteTamponId() == null ) {
					response.setStatus(functionalError.FIELD_EMPTY("ProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}
				if (dto.getSouscriptionProgrammeFideliteCarteId() == null && dto.getSouscriptionProgrammeFideliteTamponId() == null ) {
					response.setStatus(functionalError.FIELD_EMPTY("SouscriptionProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if netPromoterScorUser to insert do not exist
				NetPromoterScorUser existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("netPromoterScorUser -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if typeNoteNps exist
				TypeNoteNps existingTypeNoteNps = typeNoteNpsRepository.findByCode(dto.getTypeNoteNpsCode(), false);
				if (existingTypeNoteNps == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeNoteNps -> " + dto.getTypeNoteNpsCode(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if programmeFideliteTampon exist
				ProgrammeFideliteTampon existingProgrammeFideliteTampon =  null ;
				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() >0  ) {
					existingProgrammeFideliteTampon  = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if enseigne exist
				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if souscriptionProgrammeFideliteTampon exist
				SouscriptionProgrammeFideliteTampon existingSouscriptionProgrammeFideliteTampon = null;

				if (dto.getSouscriptionProgrammeFideliteTamponId() != null && dto.getSouscriptionProgrammeFideliteTamponId() >0  ) {
					existingSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findById(dto.getSouscriptionProgrammeFideliteTamponId(), false);
					if (existingSouscriptionProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getSouscriptionProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if programmeFideliteCarte exist
				ProgrammeFideliteCarte existingProgrammeFideliteCarte = null;
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() >0  ) {
					existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if user exist
				User existingUser = userRepository.findById(dto.getUserId(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if souscriptionProgrammeFideliteCarte exist
				SouscriptionProgrammeFideliteCarte existingSouscriptionProgrammeFideliteCarte = null;
				if (dto.getSouscriptionProgrammeFideliteCarteId() != null && dto.getSouscriptionProgrammeFideliteCarteId() >0  ) {
					existingSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findById(dto.getSouscriptionProgrammeFideliteCarteId(), false);
					if (existingSouscriptionProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getSouscriptionProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				NetPromoterScorUser entityToSave = null;
				entityToSave = NetPromoterScorUserTransformer.INSTANCE.toEntity(dto, existingTypeNoteNps, existingProgrammeFideliteTampon, existingEnseigne, existingSouscriptionProgrammeFideliteTampon, existingProgrammeFideliteCarte, existingUser, existingSouscriptionProgrammeFideliteCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setDateAction(Utilities.getCurrentDate());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<NetPromoterScorUser> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = netPromoterScorUserRepository.saveAll((Iterable<NetPromoterScorUser>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("netPromoterScorUser", locale));
					response.setHasError(true);
					return response;
				}
				List<NetPromoterScorUserDto> itemsDto = new ArrayList<NetPromoterScorUserDto>();
				for (NetPromoterScorUser entity : itemsSaved) {
					NetPromoterScorUserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create NetPromoterScorUser-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update NetPromoterScorUser by using NetPromoterScorUserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NetPromoterScorUserDto> update(Request<NetPromoterScorUserDto> request, Locale locale)  {
		slf4jLogger.info("----begin update NetPromoterScorUser-----");

		response = new Response<NetPromoterScorUserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NetPromoterScorUser> items = new ArrayList<NetPromoterScorUser>();

			for (NetPromoterScorUserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la netPromoterScorUser existe
				NetPromoterScorUser entityToSave = null;
				entityToSave = netPromoterScorUserRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("netPromoterScorUser -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Integer entityToSaveId = entityToSave.getId();

				// Verify if typeNoteNps exist
				if (Utilities.notBlank(dto.getTypeNoteNpsCode())){
					TypeNoteNps existingTypeNoteNps = typeNoteNpsRepository.findByCode(dto.getTypeNoteNpsCode(), false);
					if (existingTypeNoteNps == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeNoteNps -> " + dto.getTypeNoteNpsCode(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeNoteNps(existingTypeNoteNps);
				}
				// Verify if programmeFideliteTampon exist
				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
					ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				}
				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}
				// Verify if souscriptionProgrammeFideliteTampon exist
				if (dto.getSouscriptionProgrammeFideliteTamponId() != null && dto.getSouscriptionProgrammeFideliteTamponId() > 0){
					SouscriptionProgrammeFideliteTampon existingSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findById(dto.getSouscriptionProgrammeFideliteTamponId(), false);
					if (existingSouscriptionProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getSouscriptionProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSouscriptionProgrammeFideliteTampon(existingSouscriptionProgrammeFideliteTampon);
				}
				// Verify if programmeFideliteCarte exist
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
					ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				}
				// Verify if user exist
				if (dto.getUserId() != null && dto.getUserId() > 0){
					User existingUser = userRepository.findById(dto.getUserId(), false);
					if (existingUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getUserId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUser(existingUser);
				}
				// Verify if souscriptionProgrammeFideliteCarte exist
				if (dto.getSouscriptionProgrammeFideliteCarteId() != null && dto.getSouscriptionProgrammeFideliteCarteId() > 0){
					SouscriptionProgrammeFideliteCarte existingSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findById(dto.getSouscriptionProgrammeFideliteCarteId(), false);
					if (existingSouscriptionProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteCarte -> " + dto.getSouscriptionProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSouscriptionProgrammeFideliteCarte(existingSouscriptionProgrammeFideliteCarte);
				}
				if (dto.getNote() != null && dto.getNote() > 0) {
					entityToSave.setNote(dto.getNote());
				}
				if (Utilities.notBlank(dto.getCommentaire())) {
					entityToSave.setCommentaire(dto.getCommentaire());
				}
				if (Utilities.notBlank(dto.getRetourEnseigne())) {
					entityToSave.setRetourEnseigne(dto.getRetourEnseigne());
				}
//				if (Utilities.notBlank(dto.getDateAction())) {
//					entityToSave.setDateAction(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateAction()));
//				}
				
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<NetPromoterScorUser> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = netPromoterScorUserRepository.saveAll((Iterable<NetPromoterScorUser>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("netPromoterScorUser", locale));
					response.setHasError(true);
					return response;
				}
				List<NetPromoterScorUserDto> itemsDto = new ArrayList<NetPromoterScorUserDto>();
				for (NetPromoterScorUser entity : itemsSaved) {
					NetPromoterScorUserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update NetPromoterScorUser-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete NetPromoterScorUser by using NetPromoterScorUserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NetPromoterScorUserDto> delete(Request<NetPromoterScorUserDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete NetPromoterScorUser-----");

		response = new Response<NetPromoterScorUserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NetPromoterScorUser> items = new ArrayList<NetPromoterScorUser>();

			for (NetPromoterScorUserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la netPromoterScorUser existe
				NetPromoterScorUser existingEntity = null;
				existingEntity = netPromoterScorUserRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("netPromoterScorUser -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				netPromoterScorUserRepository.saveAll((Iterable<NetPromoterScorUser>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete NetPromoterScorUser-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete NetPromoterScorUser by using NetPromoterScorUserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<NetPromoterScorUserDto> forceDelete(Request<NetPromoterScorUserDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete NetPromoterScorUser-----");

		response = new Response<NetPromoterScorUserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<NetPromoterScorUser> items = new ArrayList<NetPromoterScorUser>();

			for (NetPromoterScorUserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la netPromoterScorUser existe
				NetPromoterScorUser existingEntity = null;
				existingEntity = netPromoterScorUserRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("netPromoterScorUser -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				netPromoterScorUserRepository.saveAll((Iterable<NetPromoterScorUser>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete NetPromoterScorUser-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get NetPromoterScorUser by using NetPromoterScorUserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<NetPromoterScorUserDto> getByCriteria(Request<NetPromoterScorUserDto> request, Locale locale) {
		slf4jLogger.info("----begin get NetPromoterScorUser-----");

		response = new Response<NetPromoterScorUserDto>();

		try {
			List<NetPromoterScorUser> items = null;
			items = netPromoterScorUserRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<NetPromoterScorUserDto> itemsDto = new ArrayList<NetPromoterScorUserDto>();
				for (NetPromoterScorUser entity : items) {
					NetPromoterScorUserDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(netPromoterScorUserRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("netPromoterScorUser", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get NetPromoterScorUser-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full NetPromoterScorUserDto by using NetPromoterScorUser as object.
	 *
	 * @param entity, locale
	 * @return NetPromoterScorUserDto
	 *
	 */
	private NetPromoterScorUserDto getFullInfos(NetPromoterScorUser entity, Integer size, Locale locale) throws Exception {
		NetPromoterScorUserDto dto = NetPromoterScorUserTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
