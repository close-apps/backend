


/*
 * Java transformer for entity table information_regle_securite_tampon
 * Created on 2020-08-17 ( Time 14:14:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "information_regle_securite_tampon"
 *
 * @author Back-End developper
 *
 */
@Component
public class InformationRegleSecuriteTamponBusiness implements IBasicBusiness<Request<InformationRegleSecuriteTamponDto>, Response<InformationRegleSecuriteTamponDto>> {

	private Response<InformationRegleSecuriteTamponDto> response;
	@Autowired
	private InformationRegleSecuriteTamponRepository informationRegleSecuriteTamponRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private UniteTempsRepository uniteTempsRepository;
	@Autowired
	private DelitProgrammeFideliteRepository delitProgrammeFideliteRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private DelitProgrammeFideliteBusiness delitProgrammeFideliteBusiness;



	public InformationRegleSecuriteTamponBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create InformationRegleSecuriteTampon by using InformationRegleSecuriteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteTamponDto> create(Request<InformationRegleSecuriteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin create InformationRegleSecuriteTampon-----");

		response = new Response<InformationRegleSecuriteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
						if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteTampon> items = new ArrayList<InformationRegleSecuriteTampon>();

			for (InformationRegleSecuriteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nbreMaxiParPdv", dto.getNbreMaxiParPdv());
				//fieldsToVerify.put("nbreMaxiPdv", dto.getNbreMaxiPdv());
				fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
				fieldsToVerify.put("uniteTempsId", dto.getUniteTempsId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if informationRegleSecuriteTampon to insert do not exist
				InformationRegleSecuriteTampon existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("informationRegleSecuriteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if programmeFideliteTampon exist
				ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
				if (existingProgrammeFideliteTampon == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if uniteTemps exist
				UniteTemps existingUniteTemps = uniteTempsRepository.findById(dto.getUniteTempsId(), false);
				if (existingUniteTemps == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("uniteTemps -> " + dto.getUniteTempsId(), locale));
					response.setHasError(true);
					return response;
				}
				
				InformationRegleSecuriteTampon entityToSave = null;
				entityToSave = InformationRegleSecuriteTamponTransformer.INSTANCE.toEntity(dto, existingProgrammeFideliteTampon, existingUniteTemps);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<InformationRegleSecuriteTampon> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = informationRegleSecuriteTamponRepository.saveAll((Iterable<InformationRegleSecuriteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteTampon", locale));
					response.setHasError(true);
					return response;
				}
				List<InformationRegleSecuriteTamponDto> itemsDto = new ArrayList<InformationRegleSecuriteTamponDto>();
				for (InformationRegleSecuriteTampon entity : itemsSaved) {
					InformationRegleSecuriteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create InformationRegleSecuriteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update InformationRegleSecuriteTampon by using InformationRegleSecuriteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteTamponDto> update(Request<InformationRegleSecuriteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin update InformationRegleSecuriteTampon-----");

		response = new Response<InformationRegleSecuriteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
						if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteTampon> items = new ArrayList<InformationRegleSecuriteTampon>();

			for (InformationRegleSecuriteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteTampon existe
				InformationRegleSecuriteTampon entityToSave = null;
				entityToSave = informationRegleSecuriteTamponRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if programmeFideliteTampon exist
				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
					ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				}
				// Verify if uniteTemps exist
				if (dto.getUniteTempsId() != null && dto.getUniteTempsId() > 0){
					UniteTemps existingUniteTemps = uniteTempsRepository.findById(dto.getUniteTempsId(), false);
					if (existingUniteTemps == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("uniteTemps -> " + dto.getUniteTempsId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUniteTemps(existingUniteTemps);
				}
				
				if (dto.getNbreMaxiParPdv() != null && dto.getNbreMaxiParPdv() > 0) {
					entityToSave.setNbreMaxiParPdv(dto.getNbreMaxiParPdv());
				}
				if (dto.getNbreMaxiParPdv() != null && dto.getNbreMaxiParPdv() > 0) {
					entityToSave.setNbreMaxiParPdv(dto.getNbreMaxiParPdv());
				}
				

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<InformationRegleSecuriteTampon> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = informationRegleSecuriteTamponRepository.saveAll((Iterable<InformationRegleSecuriteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteTampon", locale));
					response.setHasError(true);
					return response;
				}
				List<InformationRegleSecuriteTamponDto> itemsDto = new ArrayList<InformationRegleSecuriteTamponDto>();
				for (InformationRegleSecuriteTampon entity : itemsSaved) {
					InformationRegleSecuriteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update InformationRegleSecuriteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete InformationRegleSecuriteTampon by using InformationRegleSecuriteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteTamponDto> delete(Request<InformationRegleSecuriteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete InformationRegleSecuriteTampon-----");

		response = new Response<InformationRegleSecuriteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
						if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteTampon> items = new ArrayList<InformationRegleSecuriteTampon>();

			for (InformationRegleSecuriteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteTampon existe
				InformationRegleSecuriteTampon existingEntity = null;
				existingEntity = informationRegleSecuriteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFideliteTampon
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByInformationRegleSecuriteTamponId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDelitProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				informationRegleSecuriteTamponRepository.saveAll((Iterable<InformationRegleSecuriteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete InformationRegleSecuriteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete InformationRegleSecuriteTampon by using InformationRegleSecuriteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<InformationRegleSecuriteTamponDto> forceDelete(Request<InformationRegleSecuriteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete InformationRegleSecuriteTampon-----");

		response = new Response<InformationRegleSecuriteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
						if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteTampon> items = new ArrayList<InformationRegleSecuriteTampon>();

			for (InformationRegleSecuriteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteTampon existe
				InformationRegleSecuriteTampon existingEntity = null;
				existingEntity = informationRegleSecuriteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByInformationRegleSecuriteTamponId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					Request<DelitProgrammeFideliteDto> deleteRequest = new Request<DelitProgrammeFideliteDto>();
					deleteRequest.setDatas(DelitProgrammeFideliteTransformer.INSTANCE.toDtos(listOfDelitProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<DelitProgrammeFideliteDto> deleteResponse = delitProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				informationRegleSecuriteTamponRepository.saveAll((Iterable<InformationRegleSecuriteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete InformationRegleSecuriteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get InformationRegleSecuriteTampon by using InformationRegleSecuriteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<InformationRegleSecuriteTamponDto> getByCriteria(Request<InformationRegleSecuriteTamponDto> request, Locale locale) {
		slf4jLogger.info("----begin get InformationRegleSecuriteTampon-----");

		response = new Response<InformationRegleSecuriteTamponDto>();

		try {
			List<InformationRegleSecuriteTampon> items = null;
			items = informationRegleSecuriteTamponRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<InformationRegleSecuriteTamponDto> itemsDto = new ArrayList<InformationRegleSecuriteTamponDto>();
				for (InformationRegleSecuriteTampon entity : items) {
					InformationRegleSecuriteTamponDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(informationRegleSecuriteTamponRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("informationRegleSecuriteTampon", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get InformationRegleSecuriteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full InformationRegleSecuriteTamponDto by using InformationRegleSecuriteTampon as object.
	 *
	 * @param entity, locale
	 * @return InformationRegleSecuriteTamponDto
	 *
	 */
	private InformationRegleSecuriteTamponDto getFullInfos(InformationRegleSecuriteTampon entity, Integer size, Locale locale) throws Exception {
		InformationRegleSecuriteTamponDto dto = InformationRegleSecuriteTamponTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
