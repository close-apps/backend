


/*
 * Java transformer for entity table historique_souscription_enseigne
 * Created on 2021-12-16 ( Time 11:03:01 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.EtatEnum;
import com.closeapps.helper.enums.ModePaiementEnum;
import com.closeapps.helper.enums.UniteTempsEnum;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "historique_souscription_enseigne"
 *
 * @author Back-End developper
 *
 */
@Component
@EnableScheduling
public class HistoriqueSouscriptionEnseigneBusiness implements IBasicBusiness<Request<HistoriqueSouscriptionEnseigneDto>, Response<HistoriqueSouscriptionEnseigneDto>> {

	private Response<HistoriqueSouscriptionEnseigneDto> response;
	@Autowired
	private HistoriqueSouscriptionEnseigneRepository historiqueSouscriptionEnseigneRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SouscriptionRepository souscriptionRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private ModePaiementRepository modePaiementRepository;
	@Autowired
	private EtatSouscriptionRepository etatSouscriptionRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public HistoriqueSouscriptionEnseigneBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<HistoriqueSouscriptionEnseigneDto> create(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin create HistoriqueSouscriptionEnseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<HistoriqueSouscriptionEnseigne> items = new ArrayList<HistoriqueSouscriptionEnseigne>();

			for (HistoriqueSouscriptionEnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("souscriptionId", dto.getSouscriptionId());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				fieldsToVerify.put("nbreSouscription", dto.getNbreSouscription());
				fieldsToVerify.put("nbreUtilisateurMobile", dto.getNbreUtilisateurMobile());
				fieldsToVerify.put("commentaire", dto.getCommentaire());
				fieldsToVerify.put("prixSpecifique", dto.getPrixSpecifique());
				fieldsToVerify.put("modePaiementId", dto.getModePaiementId());
				fieldsToVerify.put("etatSouscriptionId", dto.getEtatSouscriptionId());
				fieldsToVerify.put("createdBy", dto.getCreatedBy());
				fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				fieldsToVerify.put("isActive", dto.getIsActive());
				fieldsToVerify.put("dateExpiration", dto.getDateExpiration());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if historiqueSouscriptionEnseigne to insert do not exist
				HistoriqueSouscriptionEnseigne existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("historiqueSouscriptionEnseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if souscription exist
				Souscription existingSouscription = souscriptionRepository.findById(dto.getSouscriptionId(), false);
				if (existingSouscription == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getSouscriptionId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if enseigne exist
				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if modePaiement exist
				ModePaiement existingModePaiement = modePaiementRepository.findById(dto.getModePaiementId(), false);
				if (existingModePaiement == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("modePaiement -> " + dto.getModePaiementId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if etatSouscription exist
				EtatSouscription existingEtatSouscription = etatSouscriptionRepository.findById(dto.getEtatSouscriptionId(), false);
				if (existingEtatSouscription == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etatSouscription -> " + dto.getEtatSouscriptionId(), locale));
					response.setHasError(true);
					return response;
				}
				HistoriqueSouscriptionEnseigne entityToSave = null;
				entityToSave = HistoriqueSouscriptionEnseigneTransformer.INSTANCE.toEntity(dto, existingSouscription, existingEnseigne, existingModePaiement, existingEtatSouscription);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<HistoriqueSouscriptionEnseigne> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = historiqueSouscriptionEnseigneRepository.saveAll((Iterable<HistoriqueSouscriptionEnseigne>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueSouscriptionEnseigne", locale));
					response.setHasError(true);
					return response;
				}
				List<HistoriqueSouscriptionEnseigneDto> itemsDto = new ArrayList<HistoriqueSouscriptionEnseigneDto>();
				for (HistoriqueSouscriptionEnseigne entity : itemsSaved) {
					HistoriqueSouscriptionEnseigneDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create HistoriqueSouscriptionEnseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<HistoriqueSouscriptionEnseigneDto> update(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin update HistoriqueSouscriptionEnseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<HistoriqueSouscriptionEnseigne> items = new ArrayList<HistoriqueSouscriptionEnseigne>();

			for (HistoriqueSouscriptionEnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la historiqueSouscriptionEnseigne existe
				HistoriqueSouscriptionEnseigne entityToSave = null;
				entityToSave = historiqueSouscriptionEnseigneRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(
							functionalError.DATA_NOT_EXIST("historiqueSouscriptionEnseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Enseigne entityEnseigneToSave = entityToSave.getEnseigne();

				Integer entityToSaveId = entityToSave.getId();

				// Verify if souscription exist
				if (dto.getSouscriptionId() != null && dto.getSouscriptionId() > 0){
					Souscription existingSouscription = souscriptionRepository.findById(dto.getSouscriptionId(), false);
					if (existingSouscription == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getSouscriptionId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSouscription(existingSouscription);
					entityEnseigneToSave.setSouscription(existingSouscription);
				}
				// Verify if enseigne exist
				//        if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
				//                    Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				//          if (existingEnseigne == null) {
				//                      response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
				//            response.setHasError(true);
				//            return response;
				//          }
				//          entityToSave.setEnseigne(existingEnseigne);
				//        }
				// Verify if modePaiement exist
				if (dto.getModePaiementId() != null && dto.getModePaiementId() > 0){
					ModePaiement existingModePaiement = modePaiementRepository.findById(dto.getModePaiementId(), false);
					if (existingModePaiement == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("modePaiement -> " + dto.getModePaiementId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setModePaiement(existingModePaiement);
				}
				// Verify if etatSouscription exist
				if (Utilities.notBlank(dto.getEtatSouscriptionCode())) {
					EtatSouscription existingEtatSouscription = etatSouscriptionRepository.findByCode(dto.getEtatSouscriptionCode(), false);
					if (existingEtatSouscription == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("etatSouscription -> " + dto.getEtatSouscriptionCode(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEtatSouscription(existingEtatSouscription);
					entityEnseigneToSave.setEtatSouscription(existingEtatSouscription);
				}
				if (dto.getNbreSouscription() != null && dto.getNbreSouscription() > 0) {
					entityToSave.setNbreSouscription(dto.getNbreSouscription());
				}
				if (dto.getNbreUtilisateurMobile() != null && dto.getNbreUtilisateurMobile() > 0) {
					entityToSave.setNbreUtilisateurMobile(dto.getNbreUtilisateurMobile());
				}
				if (Utilities.notBlank(dto.getCommentaire())) {
					entityToSave.setCommentaire(dto.getCommentaire());
				}
				if (dto.getPrixSpecifique() != null && dto.getPrixSpecifique() > 0) {
					entityToSave.setPrixSpecifique(dto.getPrixSpecifique());
					entityEnseigneToSave.setPrixSpecifique(dto.getPrixSpecifique());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				if (dto.getIsActive() != null) {
					entityToSave.setIsActive(dto.getIsActive());
				}
				if (Utilities.notBlank(dto.getDateExpiration())) {
					entityToSave.setDateExpiration(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateExpiration()));
				}

				enseigneRepository.save(entityEnseigneToSave);

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<HistoriqueSouscriptionEnseigne> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = historiqueSouscriptionEnseigneRepository.saveAll((Iterable<HistoriqueSouscriptionEnseigne>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("historiqueSouscriptionEnseigne", locale));
					response.setHasError(true);
					return response;
				}
				List<HistoriqueSouscriptionEnseigneDto> itemsDto = new ArrayList<HistoriqueSouscriptionEnseigneDto>();
				for (HistoriqueSouscriptionEnseigne entity : itemsSaved) {
					HistoriqueSouscriptionEnseigneDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update HistoriqueSouscriptionEnseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * editerSouscriptionEnseigne HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<HistoriqueSouscriptionEnseigneDto> editerSouscriptionEnseigne(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin editerSouscriptionEnseigne Enseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<HistoriqueSouscriptionEnseigne> items = new ArrayList<>();


			HistoriqueSouscriptionEnseigneDto dto =  request.getData() ;


			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("id", dto.getId());
			fieldsToVerify.put("etatSouscriptionCode", dto.getEtatSouscriptionCode());
			//fieldsToVerify.put("modePaiementId", dto.getModePaiementId());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if historiqueSouscriptionEnseigne to insert do not exist
			HistoriqueSouscriptionEnseigne entityToSave = null;

			// unicite du nom
			entityToSave = historiqueSouscriptionEnseigneRepository.findById(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("historiqueSouscriptionEnseigne -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityId = entityToSave.getId();

			Souscription existingSouscription = entityToSave.getSouscription();
			Enseigne existingEnseigne = entityToSave.getEnseigne();

			EtatSouscription existingEtatSouscription = etatSouscriptionRepository.findByCode(dto.getEtatSouscriptionCode(), false);
			if (existingEtatSouscription == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("etatSouscription -> " + dto.getEtatSouscriptionCode(), locale));
				response.setHasError(true);
				return response;
			}

			if (existingEnseigne.getSouscription() != null && existingEnseigne.getNbreUtilisateurMobile() != null 
					&& existingEnseigne.getNbreUtilisateurMobile() > existingSouscription.getNbreMaxiUser()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("Cette enseigne a "+ existingEnseigne.getNbreUtilisateurMobile() +" abonnées actuellement. Cette offre ne correspond pas à son profil !!!", locale));
				response.setHasError(true);
				return response;	
			}

			if(entityToSave.getEtatSouscription().getCode().equals(EtatEnum.ENTRANT) 
					&& dto.getEtatSouscriptionCode().equals(EtatEnum.ACCEPTER)) {

				EtatSouscription etatSouscriptionTerminer = etatSouscriptionRepository.findByCode(EtatEnum.TERMINER, false);


				int currentPrix = 0 ;

				if (!existingSouscription.getPallierForfait().getCode().equals(UniteTempsEnum.CINQ_CENT_ET_PLUS)) {
					currentPrix = Integer.parseInt(existingSouscription.getPrix());
				}

				List<HistoriqueSouscriptionEnseigne> listHistoriqueSouscriptionEnseigne = null;

				Calendar calendar = new GregorianCalendar() ;

				HistoriqueSouscriptionEnseigne historiqueSouscriptionEnseigneToUpdate = null;

				listHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneIdAndIsActiveAndDateExpirationBefore(entityId, calendar.getTime(), true, false);
				if (Utilities.isNotEmpty(listHistoriqueSouscriptionEnseigne)) {
					// maj souscriptionEnseigne 
					historiqueSouscriptionEnseigneToUpdate = listHistoriqueSouscriptionEnseigne.get(0) ;


					calendar.setTime(historiqueSouscriptionEnseigneToUpdate.getDateExpiration());

					// faire le controle si la souscription courante a deja depasser 20 jours
					calendar.add(Calendar.DAY_OF_MONTH, -11) ;
					if (calendar.getTime().after(Utilities.getCurrentDate())) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Votre souscription n'a pas depassée 20 jours. Merci de contacter le service assistance !!!", locale));
						response.setHasError(true);
						return response;					
					}

					calendar.setTime(historiqueSouscriptionEnseigneToUpdate.getDateExpiration());
					switch (existingSouscription.getDureeForfait().getCode()) {
					case UniteTempsEnum.MENSUEL:
						calendar.add(Calendar.MONTH, 1) ;

						break;
					case UniteTempsEnum.TRIMESTRIEL:
						calendar.add(Calendar.MONTH, 3) ;

						break;
					case UniteTempsEnum.ANNUEL:
						calendar.add(Calendar.YEAR, 1) ;
						break;
					default:
						break;
					}

					historiqueSouscriptionEnseigneToUpdate.setUpdatedBy(request.getUser());
					historiqueSouscriptionEnseigneToUpdate.setUpdatedAt(Utilities.getCurrentDate());
					historiqueSouscriptionEnseigneToUpdate.setIsActive(false);
					historiqueSouscriptionEnseigneToUpdate.setEtatSouscription(etatSouscriptionTerminer);
				}else {
					// creation souscriptionEnseigne 

					listHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneIdAndIsActive(entityId, true, false);
					if (Utilities.isNotEmpty(listHistoriqueSouscriptionEnseigne)) {
						for (HistoriqueSouscriptionEnseigne data : listHistoriqueSouscriptionEnseigne) {
							data.setIsActive(false);
							data.setDeletedBy(request.getUser());
							data.setDeletedAt(Utilities.getCurrentDate());
							data.setEtatSouscription(etatSouscriptionTerminer);
						}
						historiqueSouscriptionEnseigneRepository.saveAll(listHistoriqueSouscriptionEnseigne) ;
					}


					switch (existingSouscription.getDureeForfait().getCode()) {
					case UniteTempsEnum.MENSUEL:
						calendar.add(Calendar.MONTH, 1) ;

						break;
					case UniteTempsEnum.TRIMESTRIEL:
						calendar.add(Calendar.MONTH, 3) ;

						break;
					case UniteTempsEnum.ANNUEL:
						calendar.add(Calendar.YEAR, 1) ;
						break;
					default:
						break;
					}

				}

				entityToSave.setDateExpiration(calendar.getTime());
				entityToSave.setIsActive(true);

				if (historiqueSouscriptionEnseigneToUpdate != null) {
					items.add(historiqueSouscriptionEnseigneToUpdate) ;
				}

				// maj enseigne			
				existingEnseigne.setModePaiement(entityToSave.getModePaiement());
				existingEnseigne.setSouscription(existingSouscription);
				existingEnseigne.setEtatSouscription(existingEtatSouscription);

				existingEnseigne.setIsActive(true);
				existingEnseigne.setNbreMaxiUserSouscription(existingSouscription.getNbreMaxiUser());
				existingEnseigne.setNbreSouscription(entityToSave.getNbreSouscription());
				existingEnseigne.setPrixSpecifique(currentPrix);
				existingEnseigne.setDateExpirationSouscription(calendar.getTime());

				existingEnseigne.setUpdatedAt(Utilities.getCurrentDate());

				enseigneRepository.save(existingEnseigne) ;
			}


			entityToSave.setEtatSouscription(existingEtatSouscription);
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());

			items.add(entityToSave) ;

			List<HistoriqueSouscriptionEnseigne> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = historiqueSouscriptionEnseigneRepository.saveAll((Iterable<HistoriqueSouscriptionEnseigne>) items);
			if (itemsSaved == null || itemsSaved.isEmpty()) {
				response.setStatus(functionalError.SAVE_FAIL("historiqueSouscriptionEnseigne", locale));
				response.setHasError(true);
				return response;
			}
			HistoriqueSouscriptionEnseigneDto itemDto = getFullInfos(entityToSave, itemsSaved.size(), locale);

			response.setItems(Arrays.asList(itemDto));

			response.setHasError(false);

			slf4jLogger.info("----end editerSouscriptionEnseigne Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * delete HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<HistoriqueSouscriptionEnseigneDto> delete(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete HistoriqueSouscriptionEnseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<HistoriqueSouscriptionEnseigne> items = new ArrayList<HistoriqueSouscriptionEnseigne>();

			for (HistoriqueSouscriptionEnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la historiqueSouscriptionEnseigne existe
				HistoriqueSouscriptionEnseigne existingEntity = null;
				existingEntity = historiqueSouscriptionEnseigneRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("historiqueSouscriptionEnseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				historiqueSouscriptionEnseigneRepository.saveAll((Iterable<HistoriqueSouscriptionEnseigne>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete HistoriqueSouscriptionEnseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<HistoriqueSouscriptionEnseigneDto> forceDelete(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete HistoriqueSouscriptionEnseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<HistoriqueSouscriptionEnseigne> items = new ArrayList<HistoriqueSouscriptionEnseigne>();

			for (HistoriqueSouscriptionEnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la historiqueSouscriptionEnseigne existe
				HistoriqueSouscriptionEnseigne existingEntity = null;
				existingEntity = historiqueSouscriptionEnseigneRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("historiqueSouscriptionEnseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				historiqueSouscriptionEnseigneRepository.saveAll((Iterable<HistoriqueSouscriptionEnseigne>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete HistoriqueSouscriptionEnseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get HistoriqueSouscriptionEnseigne by using HistoriqueSouscriptionEnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<HistoriqueSouscriptionEnseigneDto> getByCriteria(Request<HistoriqueSouscriptionEnseigneDto> request, Locale locale) {
		slf4jLogger.info("----begin get HistoriqueSouscriptionEnseigne-----");

		response = new Response<HistoriqueSouscriptionEnseigneDto>();

		try {
			List<HistoriqueSouscriptionEnseigne> items = null;
			items = historiqueSouscriptionEnseigneRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<HistoriqueSouscriptionEnseigneDto> itemsDto = new ArrayList<HistoriqueSouscriptionEnseigneDto>();
				for (HistoriqueSouscriptionEnseigne entity : items) {
					HistoriqueSouscriptionEnseigneDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(historiqueSouscriptionEnseigneRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("historiqueSouscriptionEnseigne", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get HistoriqueSouscriptionEnseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	@Scheduled(cron="0 0 18 * * *")	
	public void majEtatSouscriptionEnseigne()  {
		slf4jLogger.info("----begin majEtatSouscriptionEnseigne User-----");
		
		Calendar calendar = new GregorianCalendar() ;
		
		calendar.setTime(Utilities.getCurrentDate());
		
		
		EtatSouscription etatSouscription =  etatSouscriptionRepository.findByCode(EtatEnum.ACCEPTER, false) ;
		EtatSouscription etatSouscriptionTerminer =  etatSouscriptionRepository.findByCode(EtatEnum.TERMINER, false) ;
		
		List<HistoriqueSouscriptionEnseigne> itemsHistoriqueSouscriptionEnseigne = 
				historiqueSouscriptionEnseigneRepository.findByEtatSouscriptionIdAndIsActiveAndDateExpirationBefore(etatSouscription.getId(), true, calendar.getTime(),false) ;
		
		if (Utilities.isNotEmpty(itemsHistoriqueSouscriptionEnseigne)) {
			
			List<Enseigne> itemsEnseigne = new ArrayList<>() ;
			
			for (HistoriqueSouscriptionEnseigne historiqueSouscriptionEnseigne : itemsHistoriqueSouscriptionEnseigne) {
				historiqueSouscriptionEnseigne.setIsActive(false);
				historiqueSouscriptionEnseigne.setUpdatedAt(Utilities.getCurrentDate());
				historiqueSouscriptionEnseigne.setEtatSouscription(etatSouscriptionTerminer);
				
				Enseigne enseigne = historiqueSouscriptionEnseigne.getEnseigne() ;
				
				enseigne.setIsActive(false);
				enseigne.setEtatSouscription(etatSouscriptionTerminer);
				enseigne.setUpdatedAt(Utilities.getCurrentDate());
				
				itemsEnseigne.add(enseigne) ;
			}
			enseigneRepository.saveAll(itemsEnseigne) ;
			historiqueSouscriptionEnseigneRepository.saveAll(itemsHistoriqueSouscriptionEnseigne) ;
		
		}
		
		slf4jLogger.info("----end majEtatSouscriptionEnseigne User-----");
		

	}

	/**
	 * get full HistoriqueSouscriptionEnseigneDto by using HistoriqueSouscriptionEnseigne as object.
	 *
	 * @param entity, locale
	 * @return HistoriqueSouscriptionEnseigneDto
	 *
	 */
	private HistoriqueSouscriptionEnseigneDto getFullInfos(HistoriqueSouscriptionEnseigne entity, Integer size, Locale locale) throws Exception {
		HistoriqueSouscriptionEnseigneDto dto = HistoriqueSouscriptionEnseigneTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
