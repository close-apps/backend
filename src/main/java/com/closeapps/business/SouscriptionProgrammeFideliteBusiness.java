


/*
 * Java transformer for entity table souscription_programme_fidelite
 * Created on 2021-06-18 ( Time 09:17:11 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "souscription_programme_fidelite"
 *
 * @author Back-End developper
 *
 */
@Component
public class SouscriptionProgrammeFideliteBusiness implements IBasicBusiness<Request<SouscriptionProgrammeFideliteDto>, Response<SouscriptionProgrammeFideliteDto>> {

	private Response<SouscriptionProgrammeFideliteDto> response;
	@Autowired
	private SouscriptionProgrammeFideliteRepository souscriptionProgrammeFideliteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public SouscriptionProgrammeFideliteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteDto> create(Request<SouscriptionProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create SouscriptionProgrammeFidelite-----");

		response = new Response<SouscriptionProgrammeFideliteDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			//
			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
			//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			List<SouscriptionProgrammeFidelite> items = new ArrayList<SouscriptionProgrammeFidelite>();

			for (SouscriptionProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("carteId", dto.getCarteId());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				//        fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				//        fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
				//        fieldsToVerify.put("dateSouscription", dto.getDateSouscription());
				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if souscriptionProgrammeFidelite to insert do not exist
				SouscriptionProgrammeFidelite existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if carte exist
				Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
				if (existingCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if programmeFideliteTampon exist
				ProgrammeFideliteTampon existingProgrammeFideliteTampon = null ;

				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0) {
					existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
				}


				// Verify if programmeFideliteCarte exist
				ProgrammeFideliteCarte existingProgrammeFideliteCarte = null ;
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0) {
					existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				
				// Verify if enseigne exist
				Enseigne existingEnseigne = null ;
				existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}

				// decompte du nombre d'utilisateur d'une enseigne
				List<SouscriptionProgrammeFidelite> existingThisUserForEnseigne = souscriptionProgrammeFideliteRepository.findByEnseigneIdAndCarteId(dto.getEnseigneId(), dto.getCarteId(), false);
				if (existingThisUserForEnseigne == null || existingThisUserForEnseigne.size() == 0) {
					if (existingEnseigne.getNbreUtilisateurMobile() == existingEnseigne.getNbreMaxiUserSouscription()) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Nombre d'abonnés atteint. Contactez l'enseigne pour plus de details !!!", locale));
						response.setHasError(true);
						return response;
					}
					existingEnseigne.setNbreUtilisateurMobile(existingEnseigne.getNbreUtilisateurMobile() + 1);
				}

				SouscriptionProgrammeFidelite entityToSave = null;
				entityToSave = SouscriptionProgrammeFideliteTransformer.INSTANCE.toEntity(dto, existingCarte, existingProgrammeFideliteTampon, existingEnseigne, existingProgrammeFideliteCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				if (Utilities.notBlank(dto.getDateSouscription())) {
					entityToSave.setDateSouscription(Utilities.getCurrentDate());
				}
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFidelite> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = souscriptionProgrammeFideliteRepository.saveAll((Iterable<SouscriptionProgrammeFidelite>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteDto>();
				for (SouscriptionProgrammeFidelite entity : itemsSaved) {
					SouscriptionProgrammeFideliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteDto> update(Request<SouscriptionProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update SouscriptionProgrammeFidelite-----");

		response = new Response<SouscriptionProgrammeFideliteDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}
			//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
			//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			List<SouscriptionProgrammeFidelite> items = new ArrayList<SouscriptionProgrammeFidelite>();

			for (SouscriptionProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFidelite existe
				SouscriptionProgrammeFidelite entityToSave = null;
				entityToSave = souscriptionProgrammeFideliteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if carte exist
				if (dto.getCarteId() != null && dto.getCarteId() > 0){
					Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
					if (existingCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCarte(existingCarte);
				}
				// Verify if programmeFideliteTampon exist
				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
					ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				}
				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}
				// Verify if programmeFideliteCarte exist
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
					ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				}
				if (Utilities.notBlank(dto.getDateSouscription())) {
					entityToSave.setDateSouscription(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSouscription()));
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
					entityToSave.setDeletedBy(dto.getDeletedBy());
				}
				if (Utilities.notBlank(dto.getDeletedAt())) {
					entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFidelite> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionProgrammeFideliteRepository.saveAll((Iterable<SouscriptionProgrammeFidelite>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteDto>();
				for (SouscriptionProgrammeFidelite entity : itemsSaved) {
					SouscriptionProgrammeFideliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteDto> delete(Request<SouscriptionProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete SouscriptionProgrammeFidelite-----");

		response = new Response<SouscriptionProgrammeFideliteDto>();

		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<SouscriptionProgrammeFidelite> items = new ArrayList<SouscriptionProgrammeFidelite>();

			for (SouscriptionProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFidelite existe
				SouscriptionProgrammeFidelite existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteRepository.saveAll((Iterable<SouscriptionProgrammeFidelite>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SouscriptionProgrammeFideliteDto> forceDelete(Request<SouscriptionProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete SouscriptionProgrammeFidelite-----");

		response = new Response<SouscriptionProgrammeFideliteDto>();

		try {
//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			User utilisateur = userRepository.findById(request.getUser(), false);
//			if (utilisateur == null) {
//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
//				response.setHasError(true);
//				return response;
//			}
//			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
//				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
//				response.setHasError(true);
//				return response;
//			}

			List<SouscriptionProgrammeFidelite> items = new ArrayList<SouscriptionProgrammeFidelite>();

			for (SouscriptionProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFidelite existe
				SouscriptionProgrammeFidelite existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteRepository.saveAll((Iterable<SouscriptionProgrammeFidelite>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SouscriptionProgrammeFideliteDto> getByCriteria(Request<SouscriptionProgrammeFideliteDto> request, Locale locale) {
		slf4jLogger.info("----begin get SouscriptionProgrammeFidelite-----");

		response = new Response<SouscriptionProgrammeFideliteDto>();

		try {
			List<SouscriptionProgrammeFidelite> items = null;
			items = souscriptionProgrammeFideliteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SouscriptionProgrammeFideliteDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteDto>();
				for (SouscriptionProgrammeFidelite entity : items) {
					SouscriptionProgrammeFideliteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(souscriptionProgrammeFideliteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("souscriptionProgrammeFidelite", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * get SouscriptionProgrammeFidelite by using SouscriptionProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<CarteDto> getByCriteriaCustom(Request<SouscriptionProgrammeFideliteDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom SouscriptionProgrammeFidelite-----");

		Response<CarteDto>  responseLocal = new Response<CarteDto>();

		try {
			List<Carte> items = null;
			items = souscriptionProgrammeFideliteRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<CarteDto> itemsDto = new ArrayList<CarteDto>();
				for (Carte entity : items) {
					//CarteDto dto = getFullInfos(entity, items.size(), locale);
					CarteDto dto = CarteTransformer.INSTANCE.toDto(entity);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				responseLocal.setItems(itemsDto);
				responseLocal.setCount(souscriptionProgrammeFideliteRepository.countCustom(request, em, locale));
				responseLocal.setHasError(false);
			} else {
				responseLocal.setStatus(functionalError.DATA_EMPTY("souscriptionProgrammeFidelite", locale));
				responseLocal.setHasError(false);
				return responseLocal;
			}

			slf4jLogger.info("----end get SouscriptionProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(responseLocal, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(responseLocal, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(responseLocal, locale, e);
		} finally {
			if (responseLocal.isHasError() && responseLocal.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", responseLocal.getStatus().getCode(), responseLocal.getStatus().getMessage());
				throw new RuntimeException(responseLocal.getStatus().getCode() + ";" + responseLocal.getStatus().getMessage());
			}
		}
		return responseLocal;
	}

	/**
	 * get full SouscriptionProgrammeFideliteDto by using SouscriptionProgrammeFidelite as object.
	 *
	 * @param entity, locale
	 * @return SouscriptionProgrammeFideliteDto
	 *
	 */
	private SouscriptionProgrammeFideliteDto getFullInfos(SouscriptionProgrammeFidelite entity, Integer size, Locale locale) throws Exception {
		SouscriptionProgrammeFideliteDto dto = SouscriptionProgrammeFideliteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
