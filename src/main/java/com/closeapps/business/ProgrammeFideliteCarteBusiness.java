


/*
 * Java transformer for entity table programme_fidelite_carte
 * Created on 2020-08-17 ( Time 14:50:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.customize._Image;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.GlobalEnum;


import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "programme_fidelite_carte"
 *
 * @author Back-End developper
 *
 */
@Component
public class ProgrammeFideliteCarteBusiness implements IBasicBusiness<Request<ProgrammeFideliteCarteDto>, Response<ProgrammeFideliteCarteDto>> {

	private Response<ProgrammeFideliteCarteDto> response;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TypeProgrammeFideliteCarteRepository typeProgrammeFideliteCarteRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private InformationRegleSecuriteCarteRepository informationRegleSecuriteCarteRepository;
	@Autowired
	private RegleConversionRepository regleConversionRepository;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteRepository souscriptionProgrammeFideliteRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
	@Autowired
	private PointDeVenteProgrammeFideliteCarteRepository pointDeVenteProgrammeFideliteCarteRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private NotePdfBusiness notePdfBusiness;



	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private InformationRegleSecuriteCarteBusiness informationRegleSecuriteCarteBusiness;


	@Autowired
	private RegleConversionBusiness regleConversionBusiness;


	@Autowired
	private SouscriptionProgrammeFideliteCarteBusiness souscriptionProgrammeFideliteCarteBusiness;

	@Autowired
	private SouscriptionProgrammeFideliteBusiness souscriptionProgrammeFideliteBusiness;


	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteBusiness historiqueCarteProgrammeFideliteCarteBusiness;


	@Autowired
	private PointDeVenteProgrammeFideliteCarteBusiness pointDeVenteProgrammeFideliteCarteBusiness;

	@Autowired
	private ParamsUtils paramsUtils;


	public ProgrammeFideliteCarteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ProgrammeFideliteCarte by using ProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteCarteDto> create(Request<ProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ProgrammeFideliteCarte-----");

		response = new Response<ProgrammeFideliteCarteDto>();
		List<String> listOfFilesCreate = new 	ArrayList<>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				&& !utilisateur.getEnseigne().getIsActive()) {
					response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!" , locale));
					response.setHasError(true);
					return response;
				}

			List<ProgrammeFideliteCarte> items = new ArrayList<ProgrammeFideliteCarte>();
			List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<InformationRegleSecuriteCarteDto> itemsInformationRegleSecuriteCarteDto = new ArrayList<InformationRegleSecuriteCarteDto>();

			List<PointDeVente> itemsPointDeVenteToUpdate = new ArrayList<PointDeVente>();


			for (ProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				//fieldsToVerify.put("typeProgrammeFideliteCarteId", dto.getTypeProgrammeFideliteCarteId());


				fieldsToVerify.put("pourXAcheter", dto.getPourXAcheter());
				fieldsToVerify.put("nbrePointObtenu", dto.getNbrePointObtenu());
				fieldsToVerify.put("correspondanceNbrePoint", dto.getCorrespondanceNbrePoint());
				fieldsToVerify.put("pointObtenuParParrain", dto.getPointObtenuParParrain());
				fieldsToVerify.put("pointObtenuParFilleul", dto.getPointObtenuParFilleul());

				//fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("isDispoAllPdv", dto.getIsDispoAllPdv());
				//fieldsToVerify.put("nbreDeSouscrit", dto.getNbreDeSouscrit());
				//fieldsToVerify.put("urlCarte", dto.getUrlCarte());


				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if programmeFideliteCarte to insert do not exist
				ProgrammeFideliteCarte existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("programmeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if enseigne exist
				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}

				existingEntity = programmeFideliteCarteRepository.findByLibelleAndEnseigneId(dto.getLibelle(), dto.getEnseigneId(), false);
				//existingEntity = programmeFideliteCarteRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("programmeFideliteCarte -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les programmeFideliteCartes", locale));
					response.setHasError(true);
					return response;
				}


				//TODO: gerer les cas de regles secondaires
				// Verify if typeProgrammeFideliteCarte exist
				TypeProgrammeFideliteCarte existingTypeProgrammeFideliteCarte = typeProgrammeFideliteCarteRepository.findByCode(GlobalEnum.PDFCarteAReglePrincipale, false);

				//				TypeProgrammeFideliteCarte existingTypeProgrammeFideliteCarte = typeProgrammeFideliteCarteRepository.findById(dto.getTypeProgrammeFideliteCarteId(), false);
				//				if (existingTypeProgrammeFideliteCarte == null) {
				//					response.setStatus(functionalError.DATA_NOT_EXIST("typeProgrammeFideliteCarte -> " + dto.getTypeProgrammeFideliteCarteId(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}

				if (dto.getIsDispoInAllPdv() == null ) {
					dto.setIsDispoInAllPdv(false);
				}
				//				if (dto.getPointObtenuApresSouscription() == null ) {
				//					dto.setPointObtenuApresSouscription(0);
				//				}
				dto.setIsDesactived(false);
				dto.setIsLocked(false);


				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();

					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);
					if(image2 != null){

						cheminFile =  Utilities.saveFile(image2, paramsUtils);
						//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
						if(cheminFile==null){
							response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
							response.setHasError(true);
							return response;
						}
						//dto.setUrlCarte(cheminFile.split("."+image2.getExtension())[0]);
						dto.setUrlCarte(cheminFile);
						listOfFilesCreate.add(cheminFile);
					}
					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}

				ProgrammeFideliteCarte entityToSave = null;
				entityToSave = ProgrammeFideliteCarteTransformer.INSTANCE.toEntity(dto, existingTypeProgrammeFideliteCarte, existingEnseigne);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);

				// liste des regles de securites plus les informations
				if (Utilities.isNotEmpty(dto.getDatasInformationRegleSecuriteCarte())) {
					for (InformationRegleSecuriteCarteDto data : dto.getDatasInformationRegleSecuriteCarte()) {
						data.setEntityProgrammeFideliteCarte(entityToSave);
					}
					itemsInformationRegleSecuriteCarteDto.addAll(dto.getDatasInformationRegleSecuriteCarte()) ;
				}

				// liste des PDV
				if (dto.getIsDispoInAllPdv() != null && dto.getIsDispoInAllPdv()) {

					List<PointDeVente> pointDeVentes = pointDeVenteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(pointDeVentes)) {
						for (PointDeVente pointDeVente : pointDeVentes) {
							PointDeVenteProgrammeFideliteCarte data = new PointDeVenteProgrammeFideliteCarte() ;
							data.setProgrammeFideliteCarte(entityToSave);
							data.setPointDeVente(pointDeVente);
							data.setCreatedBy(request.getUser());
							data.setCreatedAt(Utilities.getCurrentDate());
							data.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(data) ;
						}
					}else {
						response.setStatus(functionalError.DATA_NOT_EXIST("Aucun point de vente pour cette enseigne. Veuillez en créer d'abord !!!", locale));
						response.setHasError(true);
						return response;
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasPointDeVente())) {

						List<PointDeVente> itemsPointDeVenteForAllProgrammeFideliteCarte = pointDeVenteRepository.findByEnseigneIdAndIsAllCarteDipso(dto.getEnseigneId(), true, false) ;


						for (PointDeVenteDto data : dto.getDatasPointDeVente()) {

							PointDeVente pointDeVente = pointDeVenteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (pointDeVente == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + data.getNom(), locale));
								response.setHasError(true);
								return response;
							}

							if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteCarte) && itemsPointDeVenteForAllProgrammeFideliteCarte.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsPointDeVenteForAllProgrammeFideliteCarte.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;
						}
						if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteCarte)) {
							itemsPointDeVenteToUpdate.addAll(itemsPointDeVenteForAllProgrammeFideliteCarte) ;
						}
					}
				}
			}

			if (!items.isEmpty()) {
				List<ProgrammeFideliteCarte> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("programmeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}

				// save informationRegleSecuriteCarte 
				if (!itemsInformationRegleSecuriteCarteDto.isEmpty()) {

					for (InformationRegleSecuriteCarteDto informationRegleSecuriteCarteDto : itemsInformationRegleSecuriteCarteDto) {
						informationRegleSecuriteCarteDto.setProgrammeFideliteCarteId(informationRegleSecuriteCarteDto.getEntityProgrammeFideliteCarte().getId());
					}
					Request<InformationRegleSecuriteCarteDto> req = new Request<>() ;
					req.setDatas(itemsInformationRegleSecuriteCarteDto);
					req.setUser(request.getUser());
					Response<InformationRegleSecuriteCarteDto> res = informationRegleSecuriteCarteBusiness.create(req, locale) ;
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// save pointDeVenteProgrammeFideliteCarte 
				if (!itemsPointDeVenteProgrammeFideliteCarte.isEmpty()) {
					List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsPointDeVenteProgrammeFideliteCarte);
					if (itemsPointDeVenteProgrammeFideliteCarteSaved == null || itemsPointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj pointDeVente
				if (!itemsPointDeVenteToUpdate.isEmpty()) {
					for (PointDeVente pointDeVente : itemsPointDeVenteToUpdate) {
						pointDeVente.setIsAllCarteDipso(false);
						pointDeVente.setUpdatedBy(request.getUser());
						pointDeVente.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<PointDeVente> itemsPointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) itemsPointDeVenteToUpdate);
					if (itemsPointDeVenteSaved == null || itemsPointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<ProgrammeFideliteCarteDto> itemsDto = new ArrayList<ProgrammeFideliteCarteDto>();
				for (ProgrammeFideliteCarte entity : itemsSaved) {
					ProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ProgrammeFideliteCarte by using ProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteCarteDto> update(Request<ProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ProgrammeFideliteCarte-----");

		response = new Response<ProgrammeFideliteCarteDto>();
		List<String> listOfFilesCreate = new 	ArrayList<>();
		List<String> listOfOldFiles = new 	ArrayList<>();


		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteCarte> items = new ArrayList<ProgrammeFideliteCarte>();
			List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<InformationRegleSecuriteCarteDto> itemsInformationRegleSecuriteCarteDto = new ArrayList<InformationRegleSecuriteCarteDto>();

			List<PointDeVenteProgrammeFideliteCarte> itemsDeletePointDeVenteProgrammeFideliteCarte = new ArrayList<PointDeVenteProgrammeFideliteCarte>();
			List<InformationRegleSecuriteCarte> itemsDeleteInformationRegleSecuriteCarte = new ArrayList<InformationRegleSecuriteCarte>();

			List<PointDeVente> itemsPointDeVenteToUpdate = new ArrayList<PointDeVente>();

			for (ProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteCarte existe
				ProgrammeFideliteCarte entityToSave = null;
				entityToSave = programmeFideliteCarteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if typeProgrammeFideliteCarte exist
				if (dto.getTypeProgrammeFideliteCarteId() != null && dto.getTypeProgrammeFideliteCarteId() > 0){
					TypeProgrammeFideliteCarte existingTypeProgrammeFideliteCarte = typeProgrammeFideliteCarteRepository.findById(dto.getTypeProgrammeFideliteCarteId(), false);
					if (existingTypeProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeProgrammeFideliteCarte -> " + dto.getTypeProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTypeProgrammeFideliteCarte(existingTypeProgrammeFideliteCarte);
				}
				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					ProgrammeFideliteCarte existingEntity = programmeFideliteCarteRepository.findByLibelleAndEnseigneId(dto.getLibelle(), entityToSave.getEnseigne().getId(), false);

					//ProgrammeFideliteCarte existingEntity = programmeFideliteCarteRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("programmeFideliteCarte -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les programmeFideliteCartes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (Utilities.notBlank(dto.getConditionsDutilisation())) {
					entityToSave.setConditionsDutilisation(dto.getConditionsDutilisation());
				}
				if (dto.getPourXAcheter() != null && dto.getPourXAcheter() > 0) {
					entityToSave.setPourXAcheter(dto.getPourXAcheter());
				}
				if (dto.getNbrePointObtenu() != null && dto.getNbrePointObtenu() > 0) {
					entityToSave.setNbrePointObtenu(dto.getNbrePointObtenu());
				}
				if (dto.getCorrespondanceNbrePoint() != null && dto.getCorrespondanceNbrePoint() > 0) {
					entityToSave.setCorrespondanceNbrePoint(dto.getCorrespondanceNbrePoint());
				}
				if (dto.getIsDispoInAllPdv() != null) {
					entityToSave.setIsDispoInAllPdv(dto.getIsDispoInAllPdv());
				}
				if (dto.getPointObtenuParParrain() != null && dto.getPointObtenuParParrain() > 0) {
					entityToSave.setPointObtenuParParrain(dto.getPointObtenuParParrain());
				}
				if (dto.getPointObtenuParFilleul() != null && dto.getPointObtenuParFilleul() > 0) {
					entityToSave.setPointObtenuParFilleul(dto.getPointObtenuParFilleul());
				}
				if (dto.getNbreDeSouscrit() != null && dto.getNbreDeSouscrit() > 0) {
					entityToSave.setNbreDeSouscrit(dto.getNbreDeSouscrit());
				}
				if (Utilities.notBlank(dto.getUrlCarte())) {
					entityToSave.setUrlCarte(dto.getUrlCarte());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsDesactived() != null) {
					entityToSave.setIsDesactived(dto.getIsDesactived());
				}
				if (dto.getPointObtenuApresSouscription() != null && dto.getPointObtenuApresSouscription() > 0) {
					entityToSave.setPointObtenuApresSouscription(dto.getPointObtenuApresSouscription());
				}


				// liste des regles de securites plus les informations
				if (Utilities.isNotEmpty(dto.getDatasInformationRegleSecuriteCarte())) {
					// recuperer les datas à supprimer
					List<InformationRegleSecuriteCarte> informationRegleSecuriteCartesToDelete = informationRegleSecuriteCarteRepository.findByProgrammeFideliteCarteId(entityToSaveId, false)  ;

					if (Utilities.isNotEmpty(informationRegleSecuriteCartesToDelete)) {
						for (InformationRegleSecuriteCarte  data : informationRegleSecuriteCartesToDelete) {
							data.setDeletedBy(request.getUser());
							data.setDeletedAt(Utilities.getCurrentDate());
							data.setIsDeleted(true);	
						}
						itemsDeleteInformationRegleSecuriteCarte.addAll(informationRegleSecuriteCartesToDelete) ;
					}
					// datas à creer
					for (InformationRegleSecuriteCarteDto data : dto.getDatasInformationRegleSecuriteCarte()) {
						data.setProgrammeFideliteCarteId(entityToSaveId);
					}
					itemsInformationRegleSecuriteCarteDto.addAll(dto.getDatasInformationRegleSecuriteCarte()) ;
				}

				// liste des PDV
				if (dto.getIsDispoInAllPdv() != null && dto.getIsDispoInAllPdv()) {
					// recuperer les datas à supprimer
					List<PointDeVenteProgrammeFideliteCarte> pointDeVenteProgrammeFideliteCartesToDelete = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(entityToSaveId, false) ;
					if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteCartesToDelete)) {

						for (PointDeVenteProgrammeFideliteCarte  pointDeVenteProgrammeFideliteCarte : pointDeVenteProgrammeFideliteCartesToDelete) {
							pointDeVenteProgrammeFideliteCarte.setDeletedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setDeletedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(true);	
						}
						itemsDeletePointDeVenteProgrammeFideliteCarte.addAll(pointDeVenteProgrammeFideliteCartesToDelete) ;
					}

					// datas à creer
					List<PointDeVente> pointDeVentes = pointDeVenteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(pointDeVentes)) {
						for (PointDeVente pointDeVente : pointDeVentes) {
							PointDeVenteProgrammeFideliteCarte data = new PointDeVenteProgrammeFideliteCarte() ;
							data.setProgrammeFideliteCarte(entityToSave);
							data.setPointDeVente(pointDeVente);
							data.setCreatedBy(request.getUser());
							data.setCreatedAt(Utilities.getCurrentDate());
							data.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(data) ;
						}
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasPointDeVente())) {

						// recuperer les datas à supprimer
						List<PointDeVenteProgrammeFideliteCarte> pointDeVenteProgrammeFideliteCartesToDelete = pointDeVenteProgrammeFideliteCarteRepository.findByPointDeVenteId(entityToSaveId, false) ;
						if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteCartesToDelete)) {

							for (PointDeVenteProgrammeFideliteCarte  pointDeVenteProgrammeFideliteCarte : pointDeVenteProgrammeFideliteCartesToDelete) {
								pointDeVenteProgrammeFideliteCarte.setDeletedBy(request.getUser());
								pointDeVenteProgrammeFideliteCarte.setDeletedAt(Utilities.getCurrentDate());
								pointDeVenteProgrammeFideliteCarte.setIsDeleted(true);	
							}
							itemsDeletePointDeVenteProgrammeFideliteCarte.addAll(pointDeVenteProgrammeFideliteCartesToDelete) ;
						}


						List<PointDeVente> itemsPointDeVenteForAllProgrammeFideliteCarte = pointDeVenteRepository.findByEnseigneIdAndIsAllCarteDipso(dto.getEnseigneId(), true, false) ;

						// datas à creer
						for (PointDeVenteDto data : dto.getDatasPointDeVente()) {

							PointDeVente pointDeVente = pointDeVenteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (pointDeVente == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + data.getNom(), locale));
								response.setHasError(true);
								return response;
							}

							if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteCarte) && itemsPointDeVenteForAllProgrammeFideliteCarte.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsPointDeVenteForAllProgrammeFideliteCarte.removeIf(a->a.getId().equals(data.getId())) ;
							}

							PointDeVenteProgrammeFideliteCarte pointDeVenteProgrammeFideliteCarte = new PointDeVenteProgrammeFideliteCarte() ;
							pointDeVenteProgrammeFideliteCarte.setProgrammeFideliteCarte(entityToSave);
							pointDeVenteProgrammeFideliteCarte.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteCarte.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteCarte.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteCarte.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteCarte.add(pointDeVenteProgrammeFideliteCarte) ;
						}
						if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteCarte)) {
							itemsPointDeVenteToUpdate.addAll(itemsPointDeVenteForAllProgrammeFideliteCarte) ;
						}
					}
				}				


				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// TODO : reste la suppression du fichier sur le disque dur
					//listOfOldFiles.add(entityToSave.getUrlLogo());

					listOfOldFiles.add(entityToSave.getUrlCarte() );

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();

					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);

					cheminFile =  Utilities.saveFile(image2, paramsUtils);
					//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
					if(cheminFile==null){
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUrlCarte(cheminFile);
					//entityToSave.setUrlLogo(cheminFile.split("."+dto.getExtensionLogo())[0]);

					//entityToSave.setExtensionLogo(dto.getExtensionLogo());
					listOfFilesCreate.add(cheminFile);

					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ProgrammeFideliteCarte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("programmeFideliteCarte", locale));
					response.setHasError(true);
					return response;
				}

				// save informationRegleSecuriteCarte 

				if (!itemsDeleteInformationRegleSecuriteCarte.isEmpty()) { // suppression
					List<InformationRegleSecuriteCarte> itemsDeleteInformationRegleSecuriteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsDeleteInformationRegleSecuriteCarteSaved = informationRegleSecuriteCarteRepository.saveAll((Iterable<InformationRegleSecuriteCarte>) itemsDeleteInformationRegleSecuriteCarte);
					if (itemsDeleteInformationRegleSecuriteCarteSaved == null || itemsDeleteInformationRegleSecuriteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				if (!itemsInformationRegleSecuriteCarteDto.isEmpty()) { //creation

					Request<InformationRegleSecuriteCarteDto> req = new Request<>() ;
					req.setDatas(itemsInformationRegleSecuriteCarteDto);
					req.setUser(request.getUser());
					Response<InformationRegleSecuriteCarteDto> res = informationRegleSecuriteCarteBusiness.create(req, locale) ;
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// save pointDeVenteProgrammeFideliteCarte 
				if (!itemsDeletePointDeVenteProgrammeFideliteCarte.isEmpty()) { // supppression
					List<PointDeVenteProgrammeFideliteCarte> itemsDelelePointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsDelelePointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsDeletePointDeVenteProgrammeFideliteCarte);
					if (itemsDelelePointDeVenteProgrammeFideliteCarteSaved == null || itemsDelelePointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsPointDeVenteProgrammeFideliteCarte.isEmpty()) { //creation
					List<PointDeVenteProgrammeFideliteCarte> itemsPointDeVenteProgrammeFideliteCarteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteCarteSaved = pointDeVenteProgrammeFideliteCarteRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteCarte>) itemsPointDeVenteProgrammeFideliteCarte);
					if (itemsPointDeVenteProgrammeFideliteCarteSaved == null || itemsPointDeVenteProgrammeFideliteCarteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteCarte", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj pointDeVente
				if (!itemsPointDeVenteToUpdate.isEmpty()) {
					for (PointDeVente pointDeVente : itemsPointDeVenteToUpdate) {
						pointDeVente.setIsAllCarteDipso(false);
						pointDeVente.setUpdatedBy(request.getUser());
						pointDeVente.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<PointDeVente> itemsPointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) itemsPointDeVenteToUpdate);
					if (itemsPointDeVenteSaved == null || itemsPointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}
				List<ProgrammeFideliteCarteDto> itemsDto = new ArrayList<ProgrammeFideliteCarteDto>();
				for (ProgrammeFideliteCarte entity : itemsSaved) {
					ProgrammeFideliteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}


	/**
	 * delete ProgrammeFideliteCarte by using ProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteCarteDto> delete(Request<ProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ProgrammeFideliteCarte-----");

		response = new Response<ProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteCarte> items = new ArrayList<ProgrammeFideliteCarte>();

			for (ProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteCarte existe
				ProgrammeFideliteCarte existingEntity = null;
				existingEntity = programmeFideliteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// informationRegleSecuriteCarte
				List<InformationRegleSecuriteCarte> listOfInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfInformationRegleSecuriteCarte != null && !listOfInformationRegleSecuriteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfInformationRegleSecuriteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// regleConversion
				List<RegleConversion> listOfRegleConversion = regleConversionRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfRegleConversion != null && !listOfRegleConversion.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfRegleConversion.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFideliteCarte
				List<SouscriptionProgrammeFideliteCarte> listOfSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteCarte != null && !listOfSouscriptionProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pointDeVenteProgrammeFideliteCarte
				List<PointDeVenteProgrammeFideliteCarte> listOfPointDeVenteProgrammeFideliteCarte = pointDeVenteProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteCarte != null && !listOfPointDeVenteProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPointDeVenteProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ProgrammeFideliteCarte by using ProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ProgrammeFideliteCarteDto> forceDelete(Request<ProgrammeFideliteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ProgrammeFideliteCarte-----");

		response = new Response<ProgrammeFideliteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteCarte> items = new ArrayList<ProgrammeFideliteCarte>();

			for (ProgrammeFideliteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteCarte existe
				ProgrammeFideliteCarte existingEntity = null;
				existingEntity = programmeFideliteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// informationRegleSecuriteCarte
				List<InformationRegleSecuriteCarte> listOfInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfInformationRegleSecuriteCarte != null && !listOfInformationRegleSecuriteCarte.isEmpty()){
					Request<InformationRegleSecuriteCarteDto> deleteRequest = new Request<InformationRegleSecuriteCarteDto>();
					deleteRequest.setDatas(InformationRegleSecuriteCarteTransformer.INSTANCE.toDtos(listOfInformationRegleSecuriteCarte));
					deleteRequest.setUser(request.getUser());
					Response<InformationRegleSecuriteCarteDto> deleteResponse = informationRegleSecuriteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// regleConversion
				List<RegleConversion> listOfRegleConversion = regleConversionRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfRegleConversion != null && !listOfRegleConversion.isEmpty()){
					Request<RegleConversionDto> deleteRequest = new Request<RegleConversionDto>();
					deleteRequest.setDatas(RegleConversionTransformer.INSTANCE.toDtos(listOfRegleConversion));
					deleteRequest.setUser(request.getUser());
					Response<RegleConversionDto> deleteResponse = regleConversionBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					Request<SouscriptionProgrammeFideliteDto> deleteRequest = new Request<SouscriptionProgrammeFideliteDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteDto> deleteResponse = souscriptionProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFideliteCarte
				List<SouscriptionProgrammeFideliteCarte> listOfSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteCarte != null && !listOfSouscriptionProgrammeFideliteCarte.isEmpty()){
					Request<SouscriptionProgrammeFideliteCarteDto> deleteRequest = new Request<SouscriptionProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteCarteDto> deleteResponse = souscriptionProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historiqueCarteProgrammeFideliteCarte
				List<HistoriqueCarteProgrammeFideliteCarte> listOfHistoriqueCarteProgrammeFideliteCarte = historiqueCarteProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteCarte != null && !listOfHistoriqueCarteProgrammeFideliteCarte.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteCarteDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteCarteDto> deleteResponse = historiqueCarteProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// pointDeVenteProgrammeFideliteCarte
				List<PointDeVenteProgrammeFideliteCarte> listOfPointDeVenteProgrammeFideliteCarte = pointDeVenteProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteCarte != null && !listOfPointDeVenteProgrammeFideliteCarte.isEmpty()){
					Request<PointDeVenteProgrammeFideliteCarteDto> deleteRequest = new Request<PointDeVenteProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(PointDeVenteProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfPointDeVenteProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<PointDeVenteProgrammeFideliteCarteDto> deleteResponse = pointDeVenteProgrammeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				programmeFideliteCarteRepository.saveAll((Iterable<ProgrammeFideliteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ProgrammeFideliteCarte by using ProgrammeFideliteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ProgrammeFideliteCarteDto> getByCriteria(Request<ProgrammeFideliteCarteDto> request, Locale locale) {
		slf4jLogger.info("----begin get ProgrammeFideliteCarte-----");

		response = new Response<ProgrammeFideliteCarteDto>();

		try {
			List<ProgrammeFideliteCarte> items = null;
			items = programmeFideliteCarteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ProgrammeFideliteCarteDto> itemsDto = new ArrayList<ProgrammeFideliteCarteDto>();
				if (request.isStat()) {
					ProgrammeFideliteCarteDto dto = new ProgrammeFideliteCarteDto() ;
					dto.setLibelle(GlobalEnum.AUCUN);
					itemsDto.add(dto);
				}
				for (ProgrammeFideliteCarte entity : items) {
					ProgrammeFideliteCarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(programmeFideliteCarteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				if (request.isStat()) {
					ProgrammeFideliteCarteDto dto = new ProgrammeFideliteCarteDto() ;
					dto.setLibelle(GlobalEnum.AUCUN);
					response.setItems(Arrays.asList(dto));
				}
				response.setStatus(functionalError.DATA_EMPTY("programmeFideliteCarte", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ProgrammeFideliteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ProgrammeFideliteCarteDto by using ProgrammeFideliteCarte as object.
	 *
	 * @param entity, locale
	 * @return ProgrammeFideliteCarteDto
	 *
	 */
	public ProgrammeFideliteCarteDto getFullInfos(ProgrammeFideliteCarte entity, Integer size, Locale locale) throws Exception {
		ProgrammeFideliteCarteDto dto = ProgrammeFideliteCarteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		// renvoi nbre de PDV
		List<PointDeVente> datasPointDeVente = pointDeVenteProgrammeFideliteCarteRepository.findAllPointDeVenteByProgrammeFideliteCarteId(dto.getId(), false) ;
		dto.setNbrePDV(Utilities.isNotEmpty(datasPointDeVente) ? datasPointDeVente.size() : 0);		

		// renvoi nbre de user du PDF
		List<SouscriptionProgrammeFideliteCarte> datasSouscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndIsLocked(dto.getId(), false, false) ;
		dto.setNbreUtilisateur(Utilities.isNotEmpty(datasSouscriptionProgrammeFideliteCarte) ? datasSouscriptionProgrammeFideliteCarte.size() : 0);		

		// info enseigne
		Enseigne enseigne = enseigneRepository.findById(dto.getEnseigneId(), false) ;
		if (enseigne != null) {
			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(enseigne));
		}
		if (dto.getUrlCarte() != null) {
			dto.setUrlLogo(paramsUtils.getBaseUrlImageApp() + dto.getUrlCarte());
		}
		if (size > 1) {
			return dto;
		}

		// renvoi des listes de PDV
		if (Utilities.isNotEmpty(datasPointDeVente)) {
			dto.setDatasPointDeVente(PointDeVenteTransformer.INSTANCE.toDtos(datasPointDeVente)) ;
		}

		// renvoi des listes informationRegleSecuriteCarte
		List<InformationRegleSecuriteCarte> datasInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findByProgrammeFideliteCarteId(dto.getId(), false)  ;
		if (Utilities.isNotEmpty(datasInformationRegleSecuriteCarte)) {
			dto.setDatasInformationRegleSecuriteCarte(InformationRegleSecuriteCarteTransformer.INSTANCE.toDtos(datasInformationRegleSecuriteCarte)) ;
		}

		// info enseigne
		//		Enseigne enseigne = enseigneRepository.findById(dto.getEnseigneId(), false) ;
		//		if (enseigne != null) {
		//			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(enseigne));
		//		}

		// TODO : renvoi de logo a revoir

		//		if (Utilities.notBlank(dto.getUrlImage())) {
		//			//String fileNameByExtension = Utilities.addExtensionInFileName( dto.getUrlImage() , dto.getExtensionLogo()) ;
		//
		//			dto.setCheminFichier(Utilities.getSuitableFileUrl(dto.getUrlImage(), paramsUtils));
		//			
		//			// formatter le name du fichier
		//			dto.setUrlLogo(Utilities.getBasicNameFile(dto.getUrlLogo()));
		//			
		//			//dto.setCheminFichier(Utilities.getSuitableFileUrl(dto.getUrlLogo(), paramsUtils));
		//			//dto.setCheminFichier(Utilities.getSuitableFileUrlCustom(dto.getUrlLogo(), GlobalEnum.profils, paramsUtils));
		//		}

		return dto;
	}
}
