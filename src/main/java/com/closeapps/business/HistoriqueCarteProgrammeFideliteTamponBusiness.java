


                                                                                                                                      /*
 * Java transformer for entity table historique_carte_programme_fidelite_tampon
 * Created on 2021-09-11 ( Time 11:16:10 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "historique_carte_programme_fidelite_tampon"
 *
* @author Back-End developper
   *
 */
@Component
public class HistoriqueCarteProgrammeFideliteTamponBusiness implements IBasicBusiness<Request<HistoriqueCarteProgrammeFideliteTamponDto>, Response<HistoriqueCarteProgrammeFideliteTamponDto>> {

  private Response<HistoriqueCarteProgrammeFideliteTamponDto> response;
  @Autowired
  private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private CarteRepository carteRepository;
    @Autowired
  private PointDeVenteRepository pointDeVenteRepository;
    @Autowired
  private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
    @Autowired
  private TypeActionRepository typeActionRepository;
    @Autowired
  private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
    @Autowired
    private ProduitHistoriqueCartePdfTamponRepository produitHistoriqueCartePdfTamponRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                                          
  @Autowired
  private ProduitHistoriqueCartePdfTamponBusiness produitHistoriqueCartePdfTamponBusiness;

      

  public HistoriqueCarteProgrammeFideliteTamponBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create HistoriqueCarteProgrammeFideliteTampon by using HistoriqueCarteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteTamponDto> create(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin create HistoriqueCarteProgrammeFideliteTampon-----");

    response = new Response<HistoriqueCarteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteTampon> items = new ArrayList<HistoriqueCarteProgrammeFideliteTampon>();

      for (HistoriqueCarteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("carteId", dto.getCarteId());
        fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
        fieldsToVerify.put("typeActionId", dto.getTypeActionId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
        fieldsToVerify.put("souscriptionProgrammeFideliteTamponId", dto.getSouscriptionProgrammeFideliteTamponId());
        fieldsToVerify.put("nbreTamponObtenu", dto.getNbreTamponObtenu());
        fieldsToVerify.put("nbreTamponActuel", dto.getNbreTamponActuel());
        fieldsToVerify.put("dateAction", dto.getDateAction());
        fieldsToVerify.put("heureAction", dto.getHeureAction());
        fieldsToVerify.put("nbreTamponAvantAction", dto.getNbreTamponAvantAction());
        fieldsToVerify.put("description", dto.getDescription());
        fieldsToVerify.put("chiffreDaffaire", dto.getChiffreDaffaire());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if historiqueCarteProgrammeFideliteTampon to insert do not exist
        HistoriqueCarteProgrammeFideliteTampon existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("historiqueCarteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if carte exist
                Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
                if (existingCarte == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if pointDeVente exist
                PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
                if (existingPointDeVente == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if programmeFideliteTampon exist
                ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
                if (existingProgrammeFideliteTampon == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if typeAction exist
                TypeAction existingTypeAction = typeActionRepository.findById(dto.getTypeActionId(), false);
                if (existingTypeAction == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if souscriptionProgrammeFideliteTampon exist
                SouscriptionProgrammeFideliteTampon existingSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findById(dto.getSouscriptionProgrammeFideliteTamponId(), false);
                if (existingSouscriptionProgrammeFideliteTampon == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getSouscriptionProgrammeFideliteTamponId(), locale));
          response.setHasError(true);
          return response;
        }
        HistoriqueCarteProgrammeFideliteTampon entityToSave = null;
        entityToSave = HistoriqueCarteProgrammeFideliteTamponTransformer.INSTANCE.toEntity(dto, existingCarte, existingPointDeVente, existingProgrammeFideliteTampon, existingTypeAction, existingSouscriptionProgrammeFideliteTampon);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteTampon> itemsSaved = null;
        // inserer les donnees en base de donnees
                itemsSaved = historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteTampon", locale));
          response.setHasError(true);
          return response;
        }
        List<HistoriqueCarteProgrammeFideliteTamponDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteTamponDto>();
        for (HistoriqueCarteProgrammeFideliteTampon entity : itemsSaved) {
          HistoriqueCarteProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create HistoriqueCarteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update HistoriqueCarteProgrammeFideliteTampon by using HistoriqueCarteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteTamponDto> update(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin update HistoriqueCarteProgrammeFideliteTampon-----");

    response = new Response<HistoriqueCarteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteTampon> items = new ArrayList<HistoriqueCarteProgrammeFideliteTampon>();

      for (HistoriqueCarteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la historiqueCarteProgrammeFideliteTampon existe
        HistoriqueCarteProgrammeFideliteTampon entityToSave = null;
        entityToSave = historiqueCarteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if carte exist
        if (dto.getCarteId() != null && dto.getCarteId() > 0){
                    Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
          if (existingCarte == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setCarte(existingCarte);
        }
        // Verify if pointDeVente exist
        if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0){
                    PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
          if (existingPointDeVente == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPointDeVente(existingPointDeVente);
        }
        // Verify if programmeFideliteTampon exist
        if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
                    ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
          if (existingProgrammeFideliteTampon == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
        }
        // Verify if typeAction exist
        if (dto.getTypeActionId() != null && dto.getTypeActionId() > 0){
                    TypeAction existingTypeAction = typeActionRepository.findById(dto.getTypeActionId(), false);
          if (existingTypeAction == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("typeAction -> " + dto.getTypeActionId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setTypeAction(existingTypeAction);
        }
        // Verify if souscriptionProgrammeFideliteTampon exist
        if (dto.getSouscriptionProgrammeFideliteTamponId() != null && dto.getSouscriptionProgrammeFideliteTamponId() > 0){
                    SouscriptionProgrammeFideliteTampon existingSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findById(dto.getSouscriptionProgrammeFideliteTamponId(), false);
          if (existingSouscriptionProgrammeFideliteTampon == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getSouscriptionProgrammeFideliteTamponId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setSouscriptionProgrammeFideliteTampon(existingSouscriptionProgrammeFideliteTampon);
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        if (dto.getNbreTamponObtenu() != null && dto.getNbreTamponObtenu() > 0) {
          entityToSave.setNbreTamponObtenu(dto.getNbreTamponObtenu());
        }
        if (dto.getNbreTamponActuel() != null && dto.getNbreTamponActuel() > 0) {
          entityToSave.setNbreTamponActuel(dto.getNbreTamponActuel());
        }
        if (Utilities.notBlank(dto.getDateAction())) {
          entityToSave.setDateAction(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateAction()));
        }
        if (Utilities.notBlank(dto.getHeureAction())) {
          entityToSave.setHeureAction(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getHeureAction()));
        }
        if (dto.getNbreTamponAvantAction() != null && dto.getNbreTamponAvantAction() > 0) {
          entityToSave.setNbreTamponAvantAction(dto.getNbreTamponAvantAction());
        }
        if (Utilities.notBlank(dto.getDescription())) {
                  entityToSave.setDescription(dto.getDescription());
        }
        if (dto.getChiffreDaffaire() != null && dto.getChiffreDaffaire() > 0) {
          entityToSave.setChiffreDaffaire(dto.getChiffreDaffaire());
        }
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteTampon> itemsSaved = null;
        // maj les donnees en base
                itemsSaved = historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteTampon", locale));
          response.setHasError(true);
          return response;
        }
        List<HistoriqueCarteProgrammeFideliteTamponDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteTamponDto>();
        for (HistoriqueCarteProgrammeFideliteTampon entity : itemsSaved) {
          HistoriqueCarteProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update HistoriqueCarteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete HistoriqueCarteProgrammeFideliteTampon by using HistoriqueCarteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<HistoriqueCarteProgrammeFideliteTamponDto> delete(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete HistoriqueCarteProgrammeFideliteTampon-----");

    response = new Response<HistoriqueCarteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteTampon> items = new ArrayList<HistoriqueCarteProgrammeFideliteTampon>();

      for (HistoriqueCarteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la historiqueCarteProgrammeFideliteTampon existe
        HistoriqueCarteProgrammeFideliteTampon existingEntity = null;
        existingEntity = historiqueCarteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // produitHistoriqueCartePdfTampon
        List<ProduitHistoriqueCartePdfTampon> listOfProduitHistoriqueCartePdfTampon = produitHistoriqueCartePdfTamponRepository.findByHistoriqueCarteProgrammeFideliteTamponId(existingEntity.getId(), false);
        if (listOfProduitHistoriqueCartePdfTampon != null && !listOfProduitHistoriqueCartePdfTampon.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProduitHistoriqueCartePdfTampon.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
                historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete HistoriqueCarteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete HistoriqueCarteProgrammeFideliteTampon by using HistoriqueCarteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<HistoriqueCarteProgrammeFideliteTamponDto> forceDelete(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete HistoriqueCarteProgrammeFideliteTampon-----");

    response = new Response<HistoriqueCarteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<HistoriqueCarteProgrammeFideliteTampon> items = new ArrayList<HistoriqueCarteProgrammeFideliteTampon>();

      for (HistoriqueCarteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la historiqueCarteProgrammeFideliteTampon existe
        HistoriqueCarteProgrammeFideliteTampon existingEntity = null;
          existingEntity = historiqueCarteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("historiqueCarteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                                // produitHistoriqueCartePdfTampon
        List<ProduitHistoriqueCartePdfTampon> listOfProduitHistoriqueCartePdfTampon = produitHistoriqueCartePdfTamponRepository.findByHistoriqueCarteProgrammeFideliteTamponId(existingEntity.getId(), false);
        if (listOfProduitHistoriqueCartePdfTampon != null && !listOfProduitHistoriqueCartePdfTampon.isEmpty()){
          Request<ProduitHistoriqueCartePdfTamponDto> deleteRequest = new Request<ProduitHistoriqueCartePdfTamponDto>();
          deleteRequest.setDatas(ProduitHistoriqueCartePdfTamponTransformer.INSTANCE.toDtos(listOfProduitHistoriqueCartePdfTampon));
          deleteRequest.setUser(request.getUser());
          Response<ProduitHistoriqueCartePdfTamponDto> deleteResponse = produitHistoriqueCartePdfTamponBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
                                                                      items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
                  historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete HistoriqueCarteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get HistoriqueCarteProgrammeFideliteTampon by using HistoriqueCarteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<HistoriqueCarteProgrammeFideliteTamponDto> getByCriteria(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, Locale locale) {
    slf4jLogger.info("----begin get HistoriqueCarteProgrammeFideliteTampon-----");

    response = new Response<HistoriqueCarteProgrammeFideliteTamponDto>();

    try {
      List<HistoriqueCarteProgrammeFideliteTampon> items = null;
      items = historiqueCarteProgrammeFideliteTamponRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<HistoriqueCarteProgrammeFideliteTamponDto> itemsDto = new ArrayList<HistoriqueCarteProgrammeFideliteTamponDto>();
        for (HistoriqueCarteProgrammeFideliteTampon entity : items) {
          HistoriqueCarteProgrammeFideliteTamponDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(historiqueCarteProgrammeFideliteTamponRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("historiqueCarteProgrammeFideliteTampon", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get HistoriqueCarteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full HistoriqueCarteProgrammeFideliteTamponDto by using HistoriqueCarteProgrammeFideliteTampon as object.
   *
   * @param entity, locale
   * @return HistoriqueCarteProgrammeFideliteTamponDto
   *
   */
  private HistoriqueCarteProgrammeFideliteTamponDto getFullInfos(HistoriqueCarteProgrammeFideliteTampon entity, Integer size, Locale locale) throws Exception {
    HistoriqueCarteProgrammeFideliteTamponDto dto = HistoriqueCarteProgrammeFideliteTamponTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
