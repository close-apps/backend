


/*
 * Java transformer for entity table mode_paiement
 * Created on 2020-08-17 ( Time 14:14:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.dao.entity.Enseigne;
import com.closeapps.dao.entity.HistoriqueSouscriptionEnseigne;
import com.closeapps.dao.entity.ModePaiement;
import com.closeapps.dao.entity.User;
import com.closeapps.dao.repository.EnseigneRepository;
import com.closeapps.dao.repository.HistoriqueSouscriptionEnseigneRepository;
import com.closeapps.dao.repository.ModePaiementRepository;
import com.closeapps.dao.repository.UserRepository;
import com.closeapps.helper.ExceptionUtils;
import com.closeapps.helper.FunctionalError;
import com.closeapps.helper.TechnicalError;
import com.closeapps.helper.Utilities;
import com.closeapps.helper.Validate;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.HistoriqueSouscriptionEnseigneDto;
import com.closeapps.helper.dto.ModePaiementDto;
import com.closeapps.helper.dto.transformer.EnseigneTransformer;
import com.closeapps.helper.dto.transformer.HistoriqueSouscriptionEnseigneTransformer;
import com.closeapps.helper.dto.transformer.ModePaiementTransformer;

/**
BUSINESS for table "mode_paiement"
 *
 * @author Back-End developper
 *
 */
@Component
public class ModePaiementBusiness implements IBasicBusiness<Request<ModePaiementDto>, Response<ModePaiementDto>> {

	private Response<ModePaiementDto> response;
	@Autowired
	private ModePaiementRepository modePaiementRepository;
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private HistoriqueSouscriptionEnseigneRepository historiqueSouscriptionEnseigneRepository;
@Autowired
	private EnseigneRepository enseigneRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private HistoriqueSouscriptionEnseigneBusiness historiqueSouscriptionEnseigneBusiness;

@Autowired
	private EnseigneBusiness enseigneBusiness;



	public ModePaiementBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ModePaiement by using ModePaiementDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ModePaiementDto> create(Request<ModePaiementDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ModePaiement-----");

		response = new Response<ModePaiementDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ModePaiement> items = new ArrayList<ModePaiement>();

			for (ModePaiementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("code", dto.getCode());
				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if modePaiement to insert do not exist
				ModePaiement existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("modePaiement -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = modePaiementRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("modePaiement -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les modePaiements", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = modePaiementRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("modePaiement -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les modePaiements", locale));
					response.setHasError(true);
					return response;
				}

				ModePaiement entityToSave = null;
				entityToSave = ModePaiementTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ModePaiement> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = modePaiementRepository.saveAll((Iterable<ModePaiement>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("modePaiement", locale));
					response.setHasError(true);
					return response;
				}
				List<ModePaiementDto> itemsDto = new ArrayList<ModePaiementDto>();
				for (ModePaiement entity : itemsSaved) {
					ModePaiementDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ModePaiement-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ModePaiement by using ModePaiementDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ModePaiementDto> update(Request<ModePaiementDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ModePaiement-----");

		response = new Response<ModePaiementDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ModePaiement> items = new ArrayList<ModePaiement>();

			for (ModePaiementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la modePaiement existe
				ModePaiement entityToSave = null;
				entityToSave = modePaiementRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("modePaiement -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getLibelle())) {
					ModePaiement existingEntity = modePaiementRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("modePaiement -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les modePaiements", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getCode())) {
					ModePaiement existingEntity = modePaiementRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("modePaiement -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les modePaiements", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				//        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				//          entityToSave.setCreatedBy(dto.getCreatedBy());
				//        }
				//        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				//          entityToSave.setUpdatedBy(dto.getUpdatedBy());
				//        }
				//        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				//          entityToSave.setDeletedBy(dto.getDeletedBy());
				//        }
				//        if (Utilities.notBlank(dto.getDeletedAt())) {
				//          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				//        }
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ModePaiement> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = modePaiementRepository.saveAll((Iterable<ModePaiement>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("modePaiement", locale));
					response.setHasError(true);
					return response;
				}
				List<ModePaiementDto> itemsDto = new ArrayList<ModePaiementDto>();
				for (ModePaiement entity : itemsSaved) {
					ModePaiementDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ModePaiement-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete ModePaiement by using ModePaiementDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ModePaiementDto> delete(Request<ModePaiementDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ModePaiement-----");

		response = new Response<ModePaiementDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ModePaiement> items = new ArrayList<ModePaiement>();

			for (ModePaiementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la modePaiement existe
				ModePaiement existingEntity = null;
				existingEntity = modePaiementRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("modePaiement -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				
				// enseigne
				List<Enseigne> listOfEnseigne = enseigneRepository.findByModePaiementId(existingEntity.getId(), false);
				if (listOfEnseigne != null && !listOfEnseigne.isEmpty()) {
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// historiqueSouscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByModePaiementId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueSouscriptionEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				modePaiementRepository.saveAll((Iterable<ModePaiement>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ModePaiement-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ModePaiement by using ModePaiementDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ModePaiementDto> forceDelete(Request<ModePaiementDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ModePaiement-----");

		response = new Response<ModePaiementDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ModePaiement> items = new ArrayList<ModePaiement>();

			for (ModePaiementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la modePaiement existe
				ModePaiement existingEntity = null;
				existingEntity = modePaiementRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("modePaiement -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				
				// historiqueSouscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByModePaiementId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()) {
					Request<HistoriqueSouscriptionEnseigneDto> deleteRequest = new Request<HistoriqueSouscriptionEnseigneDto>();
					deleteRequest.setDatas(HistoriqueSouscriptionEnseigneTransformer.INSTANCE.toDtos(listOfHistoriqueSouscriptionEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueSouscriptionEnseigneDto> deleteResponse = historiqueSouscriptionEnseigneBusiness
							.delete(deleteRequest, locale);
					if (deleteResponse.isHasError()) {
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// enseigne
				List<Enseigne> listOfEnseigne = enseigneRepository.findByModePaiementId(existingEntity.getId(), false);
				if (listOfEnseigne != null && !listOfEnseigne.isEmpty()) {
					Request<EnseigneDto> deleteRequest = new Request<EnseigneDto>();
					deleteRequest.setDatas(EnseigneTransformer.INSTANCE.toDtos(listOfEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<EnseigneDto> deleteResponse = enseigneBusiness
							.delete(deleteRequest, locale);
					if (deleteResponse.isHasError()) {
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				modePaiementRepository.saveAll((Iterable<ModePaiement>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ModePaiement-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ModePaiement by using ModePaiementDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ModePaiementDto> getByCriteria(Request<ModePaiementDto> request, Locale locale) {
		slf4jLogger.info("----begin get ModePaiement-----");

		response = new Response<ModePaiementDto>();

		try {
			List<ModePaiement> items = null;
			items = modePaiementRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ModePaiementDto> itemsDto = new ArrayList<ModePaiementDto>();
				for (ModePaiement entity : items) {
					ModePaiementDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(modePaiementRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("modePaiement", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ModePaiement-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ModePaiementDto by using ModePaiement as object.
	 *
	 * @param entity, locale
	 * @return ModePaiementDto
	 *
	 */
	private ModePaiementDto getFullInfos(ModePaiement entity, Integer size, Locale locale) throws Exception {
		ModePaiementDto dto = ModePaiementTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
