


/*
 * Java transformer for entity table programme_fidelite_tampon
 * Created on 2020-08-17 ( Time 14:14:49 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.customize._Image;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.GlobalEnum;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "programme_fidelite_tampon"
 *
 * @author Back-End developper
 *
 */
@Component
public class ProgrammeFideliteTamponBusiness implements IBasicBusiness<Request<ProgrammeFideliteTamponDto>, Response<ProgrammeFideliteTamponDto>> {

	private Response<ProgrammeFideliteTamponDto> response;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private InformationRegleSecuriteTamponRepository informationRegleSecuriteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteRepository souscriptionProgrammeFideliteRepository;
	@Autowired
	private PointDeVenteProgrammeFideliteTamponRepository pointDeVenteProgrammeFideliteTamponRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private NotePdfBusiness notePdfBusiness;



	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponBusiness historiqueCarteProgrammeFideliteTamponBusiness;


	@Autowired
	private InformationRegleSecuriteTamponBusiness informationRegleSecuriteTamponBusiness;


	@Autowired
	private SouscriptionProgrammeFideliteTamponBusiness souscriptionProgrammeFideliteTamponBusiness;

	@Autowired
	private SouscriptionProgrammeFideliteBusiness souscriptionProgrammeFideliteBusiness;


	@Autowired
	private PointDeVenteProgrammeFideliteTamponBusiness pointDeVenteProgrammeFideliteTamponBusiness;

	@Autowired
	private ParamsUtils paramsUtils;

	public ProgrammeFideliteTamponBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create ProgrammeFideliteTampon by using ProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteTamponDto> create(Request<ProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin create ProgrammeFideliteTampon-----");

		List<String> listOfFilesCreate = new 	ArrayList<>();
		response = new Response<ProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}

			if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				&& !utilisateur.getEnseigne().getIsActive()) {
					response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!" , locale));
					response.setHasError(true);
					return response;
				}

			List<ProgrammeFideliteTampon> items = new ArrayList<ProgrammeFideliteTampon>();
			List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();
			List<InformationRegleSecuriteTamponDto> itemsInformationRegleSecuriteTamponDto = new ArrayList<InformationRegleSecuriteTamponDto>();


			List<PointDeVente> itemsPointDeVenteToUpdate = new ArrayList<PointDeVente>();

			for (ProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				fieldsToVerify.put("nbreTampon", dto.getNbreTampon());
				fieldsToVerify.put("nbrePallier", dto.getNbrePallier());
				fieldsToVerify.put("gain", dto.getGain());
				//fieldsToVerify.put("valeurMaxGain", dto.getValeurMaxGain());
				//fieldsToVerify.put("isValeurMaxGain", dto.getIsValeurMaxGain());


				//fieldsToVerify.put("codeBarre", dto.getCodeBarre());
				//fieldsToVerify.put("qrCode", dto.getQrCode());

				//fieldsToVerify.put("description", dto.getDescription());
				//fieldsToVerify.put("urlImage", dto.getUrlImage());
				//fieldsToVerify.put("isDispoAllPdv", dto.getIsDispoAllPdv());

				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if programmeFideliteTampon to insert do not exist
				ProgrammeFideliteTampon existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("programmeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if enseigne exist
				Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
				if (existingEnseigne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
					response.setHasError(true);
					return response;
				}

				//existingEntity = programmeFideliteTamponRepository.findByLibelle(dto.getLibelle(), false);
				existingEntity = programmeFideliteTamponRepository.findByLibelleAndEnseigneId(dto.getLibelle(), dto.getEnseigneId(), false);

				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("programmeFideliteTampon -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les programmeFideliteTampons", locale));
					response.setHasError(true);
					return response;
				}


				if (dto.getValeurMaxGain() != null && dto.getValeurMaxGain() > 0 ) {
					dto.setIsValeurMaxGain(true);
				}else {
					dto.setIsValeurMaxGain(false);
				}


				// TODO: generation du code barre et qrcode
				dto.setCodeBarre(Utilities.generateCodeBarre(carteRepository, programmeFideliteTamponRepository));
				dto.setQrCode(Utilities.generateQRCode(carteRepository, programmeFideliteTamponRepository));


				if (dto.getIsDispoInAllPdv() == null ) {
					dto.setIsDispoInAllPdv(false);
				}

				/// defaultvalue
				if (dto.getTamponObtenuApresSouscription() == null ) {
					dto.setTamponObtenuApresSouscription(0);
				}
				dto.setIsDesactived(false);
				dto.setIsLocked(false);

				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();
					
					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);
					if(image2 != null){

						cheminFile =  Utilities.saveFile(image2, paramsUtils);
						//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
						if(cheminFile==null){
							response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
							response.setHasError(true);
							return response;
						}
						//dto.setUrlCarte(cheminFile.split("."+image2.getExtension())[0]);
						dto.setUrlImage(cheminFile);
						listOfFilesCreate.add(cheminFile);
					}
					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}


				ProgrammeFideliteTampon entityToSave = null;
				entityToSave = ProgrammeFideliteTamponTransformer.INSTANCE.toEntity(dto, existingEnseigne);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);


				// liste des regles de securites plus les informations
				if (Utilities.isNotEmpty(dto.getDatasInformationRegleSecuriteTampon())) {
					for (InformationRegleSecuriteTamponDto data : dto.getDatasInformationRegleSecuriteTampon()) {
						data.setEntityProgrammeFideliteTampon(entityToSave);
					}
					itemsInformationRegleSecuriteTamponDto.addAll(dto.getDatasInformationRegleSecuriteTampon()) ;
				}

				// liste des PDV
				if (dto.getIsDispoInAllPdv() != null && dto.getIsDispoInAllPdv()) {

					List<PointDeVente> pointDeVentes = pointDeVenteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(pointDeVentes)) {
						for (PointDeVente pointDeVente : pointDeVentes) {
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}
					}else {
						response.setStatus(functionalError.DATA_NOT_EXIST("Aucun point de vente pour cette enseigne. Veuillez en créer d'abord !!!", locale));
						response.setHasError(true);
						return response;
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasPointDeVente())) {

						List<PointDeVente> itemsPointDeVenteForAllProgrammeFideliteTampon = pointDeVenteRepository.findByEnseigneIdAndIsAllTamponDipso(dto.getEnseigneId(), true, false) ;


						for (PointDeVenteDto data : dto.getDatasPointDeVente()) {

							PointDeVente pointDeVente = pointDeVenteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (pointDeVente == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + data.getNom(), locale));
								response.setHasError(true);
								return response;
							}
							if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteTampon) && itemsPointDeVenteForAllProgrammeFideliteTampon.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsPointDeVenteForAllProgrammeFideliteTampon.removeIf(a->a.getId().equals(data.getId())) ;
							}

							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}
						if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteTampon)) {
							itemsPointDeVenteToUpdate.addAll(itemsPointDeVenteForAllProgrammeFideliteTampon) ;
						}

					}
				}
			}

			if (!items.isEmpty()) {
				List<ProgrammeFideliteTampon> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("programmeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}

				// save informationRegleSecuriteTampon 
				if (!itemsInformationRegleSecuriteTamponDto.isEmpty()) {

					for (InformationRegleSecuriteTamponDto informationRegleSecuriteTamponDto : itemsInformationRegleSecuriteTamponDto) {
						informationRegleSecuriteTamponDto.setProgrammeFideliteTamponId(informationRegleSecuriteTamponDto.getEntityProgrammeFideliteTampon().getId());
					}
					Request<InformationRegleSecuriteTamponDto> req = new Request<>() ;
					req.setDatas(itemsInformationRegleSecuriteTamponDto);
					req.setUser(request.getUser());
					Response<InformationRegleSecuriteTamponDto> res = informationRegleSecuriteTamponBusiness.create(req, locale) ;
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// save pointDeVenteProgrammeFideliteTampon 
				if (!itemsPointDeVenteProgrammeFideliteTampon.isEmpty()) {
					List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsPointDeVenteProgrammeFideliteTampon);
					if (itemsPointDeVenteProgrammeFideliteTamponSaved == null || itemsPointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj pointDeVente
				if (!itemsPointDeVenteToUpdate.isEmpty()) {
					for (PointDeVente pointDeVente : itemsPointDeVenteToUpdate) {
						pointDeVente.setIsAllTamponDipso(false);
						pointDeVente.setUpdatedBy(request.getUser());
						pointDeVente.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<PointDeVente> itemsPointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) itemsPointDeVenteToUpdate);
					if (itemsPointDeVenteSaved == null || itemsPointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<ProgrammeFideliteTamponDto> itemsDto = new ArrayList<ProgrammeFideliteTamponDto>();
				for (ProgrammeFideliteTampon entity : itemsSaved) {
					ProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create ProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ProgrammeFideliteTampon by using ProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteTamponDto> update(Request<ProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin update ProgrammeFideliteTampon-----");

		List<String> listOfFilesCreate = new 	ArrayList<>();
		List<String> listOfOldFiles = new 	ArrayList<>();
		response = new Response<ProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsAdminEnseigne() != null && !utilisateur.getIsAdminEnseigne()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur "+utilisateur.getLogin()+" "+" n'est pas un administrateur" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteTampon> items = new ArrayList<ProgrammeFideliteTampon>();
			List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();
			List<InformationRegleSecuriteTamponDto> itemsInformationRegleSecuriteTamponDto = new ArrayList<InformationRegleSecuriteTamponDto>();
			List<PointDeVenteProgrammeFideliteTampon> itemsDeletePointDeVenteProgrammeFideliteTampon = new ArrayList<PointDeVenteProgrammeFideliteTampon>();
			List<InformationRegleSecuriteTampon> itemsDeleteInformationRegleSecuriteTampon = new ArrayList<InformationRegleSecuriteTampon>();


			List<PointDeVente> itemsPointDeVenteToUpdate = new ArrayList<PointDeVente>();

			for (ProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteTampon existe
				ProgrammeFideliteTampon entityToSave = null;
				entityToSave = programmeFideliteTamponRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					ProgrammeFideliteTampon existingEntity = programmeFideliteTamponRepository.findByLibelleAndEnseigneId(dto.getLibelle(), entityToSave.getEnseigne().getId(), false);

					//ProgrammeFideliteTampon existingEntity = programmeFideliteTamponRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("programmeFideliteTampon -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les programmeFideliteTampons", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (Utilities.notBlank(dto.getConditionsDutilisation())) {
					entityToSave.setConditionsDutilisation(dto.getConditionsDutilisation());
				}
				if (dto.getNbreTampon() != null && dto.getNbreTampon() > 0) {
					entityToSave.setNbreTampon(dto.getNbreTampon());
				}
				if (dto.getNbrePallier() != null && dto.getNbrePallier() > 0) {
					entityToSave.setNbrePallier(dto.getNbrePallier());
				}
				if (Utilities.notBlank(dto.getUrlImage())) {
					entityToSave.setUrlImage(dto.getUrlImage());
				}
				if (Utilities.notBlank(dto.getGain())) {
					entityToSave.setGain(dto.getGain());
				}
				if (dto.getValeurMaxGain() != null && dto.getValeurMaxGain() > 0) {
					entityToSave.setValeurMaxGain(dto.getValeurMaxGain());
				}
				if (dto.getIsValeurMaxGain() != null) {
					entityToSave.setIsValeurMaxGain(dto.getIsValeurMaxGain());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsDesactived() != null) {
					entityToSave.setIsDesactived(dto.getIsDesactived());
				}
				if (Utilities.notBlank(dto.getCodeBarre())) {
					entityToSave.setCodeBarre(dto.getCodeBarre());
				}
				if (Utilities.notBlank(dto.getQrCode())) {
					entityToSave.setQrCode(dto.getQrCode());
				}
				if (dto.getTamponObtenuApresSouscription() != null && dto.getTamponObtenuApresSouscription() > 0) {
					entityToSave.setTamponObtenuApresSouscription(dto.getTamponObtenuApresSouscription());
				}

				// liste des regles de securites plus les informations
				if (Utilities.isNotEmpty(dto.getDatasInformationRegleSecuriteTampon())) {

					// recuperer les datas à supprimer
					List<InformationRegleSecuriteTampon> informationRegleSecuriteTamponsToDelete = informationRegleSecuriteTamponRepository.findByProgrammeFideliteTamponId(entityToSaveId, false)  ;

					if (Utilities.isNotEmpty(informationRegleSecuriteTamponsToDelete)) {
						for (InformationRegleSecuriteTampon  data : informationRegleSecuriteTamponsToDelete) {
							data.setDeletedBy(request.getUser());
							data.setDeletedAt(Utilities.getCurrentDate());
							data.setIsDeleted(true);	
						}
						itemsDeleteInformationRegleSecuriteTampon.addAll(informationRegleSecuriteTamponsToDelete) ;
					}

					// datas à creer
					for (InformationRegleSecuriteTamponDto data : dto.getDatasInformationRegleSecuriteTampon()) {
						data.setProgrammeFideliteTamponId(entityToSaveId);
					}
					itemsInformationRegleSecuriteTamponDto.addAll(dto.getDatasInformationRegleSecuriteTampon()) ;
				}

				// liste des PDV
				if (dto.getIsDispoInAllPdv() != null && dto.getIsDispoInAllPdv()) {

					// recuperer les datas à supprimer
					List<PointDeVenteProgrammeFideliteTampon> pointDeVenteProgrammeFideliteTamponsToDelete = pointDeVenteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(entityToSaveId, false) ;

					if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteTamponsToDelete)) {

						for (PointDeVenteProgrammeFideliteTampon  data : pointDeVenteProgrammeFideliteTamponsToDelete) {
							data.setDeletedBy(request.getUser());
							data.setDeletedAt(Utilities.getCurrentDate());
							data.setIsDeleted(true);	
						}
						itemsDeletePointDeVenteProgrammeFideliteTampon.addAll(pointDeVenteProgrammeFideliteTamponsToDelete) ;
					}					

					// datas à creer
					List<PointDeVente> pointDeVentes = pointDeVenteRepository.findByEnseigneId(dto.getEnseigneId(), false);
					if (Utilities.isNotEmpty(pointDeVentes)) {
						for (PointDeVente pointDeVente : pointDeVentes) {
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}
					}
				} else {
					if (Utilities.isNotEmpty(dto.getDatasPointDeVente())) {

						// recuperer les datas à supprimer
						List<PointDeVenteProgrammeFideliteTampon> pointDeVenteProgrammeFideliteTamponsToDelete = pointDeVenteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(entityToSaveId, false) ;

						if (Utilities.isNotEmpty(pointDeVenteProgrammeFideliteTamponsToDelete)) {

							for (PointDeVenteProgrammeFideliteTampon  data : pointDeVenteProgrammeFideliteTamponsToDelete) {
								data.setDeletedBy(request.getUser());
								data.setDeletedAt(Utilities.getCurrentDate());
								data.setIsDeleted(true);	
							}
							itemsDeletePointDeVenteProgrammeFideliteTampon.addAll(pointDeVenteProgrammeFideliteTamponsToDelete) ;
						}

						List<PointDeVente> itemsPointDeVenteForAllProgrammeFideliteTampon = pointDeVenteRepository.findByEnseigneIdAndIsAllTamponDipso(dto.getEnseigneId(), true, false) ;						

						for (PointDeVenteDto data : dto.getDatasPointDeVente()) {

							PointDeVente pointDeVente = pointDeVenteRepository.findById(data.getId(), false);
							//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
							if (pointDeVente == null) {
								response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + data.getNom(), locale));
								response.setHasError(true);
								return response;
							}
							if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteTampon) && itemsPointDeVenteForAllProgrammeFideliteTampon.stream().anyMatch(a->a.getId().equals(data.getId()))) {
								itemsPointDeVenteForAllProgrammeFideliteTampon.removeIf(a->a.getId().equals(data.getId())) ;
							}
							PointDeVenteProgrammeFideliteTampon pointDeVenteProgrammeFideliteTampon = new PointDeVenteProgrammeFideliteTampon() ;
							pointDeVenteProgrammeFideliteTampon.setProgrammeFideliteTampon(entityToSave);
							pointDeVenteProgrammeFideliteTampon.setPointDeVente(pointDeVente);
							pointDeVenteProgrammeFideliteTampon.setCreatedBy(request.getUser());
							pointDeVenteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
							pointDeVenteProgrammeFideliteTampon.setIsDeleted(false);

							itemsPointDeVenteProgrammeFideliteTampon.add(pointDeVenteProgrammeFideliteTampon) ;
						}

						if (Utilities.isNotEmpty(itemsPointDeVenteForAllProgrammeFideliteTampon)) {
							itemsPointDeVenteToUpdate.addAll(itemsPointDeVenteForAllProgrammeFideliteTampon) ;
						}
					}
				}	

				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// TODO : reste la suppression du fichier sur le disque dur
					//listOfOldFiles.add(entityToSave.getUrlLogo());

					listOfOldFiles.add(entityToSave.getUrlImage() );

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();

					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);

					cheminFile =  Utilities.saveFile(image2, paramsUtils);
					//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
					if(cheminFile==null){
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUrlImage(cheminFile);
					//entityToSave.setUrlLogo(cheminFile.split("."+dto.getExtensionLogo())[0]);

					//entityToSave.setExtensionLogo(dto.getExtensionLogo());
					listOfFilesCreate.add(cheminFile);

					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ProgrammeFideliteTampon> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("programmeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}


				// save informationRegleSecuriteTampon 
				if (!itemsDeleteInformationRegleSecuriteTampon.isEmpty()) { // suppression
					List<InformationRegleSecuriteTampon> itemsDeleteInformationRegleSecuriteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsDeleteInformationRegleSecuriteTamponSaved = informationRegleSecuriteTamponRepository.saveAll((Iterable<InformationRegleSecuriteTampon>) itemsDeleteInformationRegleSecuriteTampon);
					if (itemsDeleteInformationRegleSecuriteTamponSaved == null || itemsDeleteInformationRegleSecuriteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsInformationRegleSecuriteTamponDto.isEmpty()) { // creation

					Request<InformationRegleSecuriteTamponDto> req = new Request<>() ;
					req.setDatas(itemsInformationRegleSecuriteTamponDto);
					req.setUser(request.getUser());
					Response<InformationRegleSecuriteTamponDto> res = informationRegleSecuriteTamponBusiness.create(req, locale) ;
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				// save pointDeVenteProgrammeFideliteTampon 
				if (!itemsDeletePointDeVenteProgrammeFideliteTampon.isEmpty()) { // suppression
					List<PointDeVenteProgrammeFideliteTampon> itemsDeletePointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsDeletePointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsDeletePointDeVenteProgrammeFideliteTampon);
					if (itemsDeletePointDeVenteProgrammeFideliteTamponSaved == null || itemsDeletePointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsPointDeVenteProgrammeFideliteTampon.isEmpty()) { // creation 
					List<PointDeVenteProgrammeFideliteTampon> itemsPointDeVenteProgrammeFideliteTamponSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteProgrammeFideliteTamponSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) itemsPointDeVenteProgrammeFideliteTampon);
					if (itemsPointDeVenteProgrammeFideliteTamponSaved == null || itemsPointDeVenteProgrammeFideliteTamponSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				// maj pointDeVente
				if (!itemsPointDeVenteToUpdate.isEmpty()) {
					for (PointDeVente pointDeVente : itemsPointDeVenteToUpdate) {
						pointDeVente.setIsAllTamponDipso(false);
						pointDeVente.setUpdatedBy(request.getUser());
						pointDeVente.setUpdatedAt(Utilities.getCurrentDate());
					}
					List<PointDeVente> itemsPointDeVenteSaved = null;
					// inserer les donnees en base de donnees
					itemsPointDeVenteSaved = pointDeVenteRepository.saveAll((Iterable<PointDeVente>) itemsPointDeVenteToUpdate);
					if (itemsPointDeVenteSaved == null || itemsPointDeVenteSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("pointDeVente", locale));
						response.setHasError(true);
						return response;
					}
				}
				List<ProgrammeFideliteTamponDto> itemsDto = new ArrayList<ProgrammeFideliteTamponDto>();
				for (ProgrammeFideliteTampon entity : itemsSaved) {
					ProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update ProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}


	/**
	 * delete ProgrammeFideliteTampon by using ProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ProgrammeFideliteTamponDto> delete(Request<ProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete ProgrammeFideliteTampon-----");

		response = new Response<ProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteTampon> items = new ArrayList<ProgrammeFideliteTampon>();

			for (ProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteTampon existe
				ProgrammeFideliteTampon existingEntity = null;
				existingEntity = programmeFideliteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueCarteProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// informationRegleSecuriteTampon
				List<InformationRegleSecuriteTampon> listOfInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfInformationRegleSecuriteTampon != null && !listOfInformationRegleSecuriteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfInformationRegleSecuriteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFidelite.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionProgrammeFideliteTampon
				List<SouscriptionProgrammeFideliteTampon> listOfSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteTampon != null && !listOfSouscriptionProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSouscriptionProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pointDeVenteProgrammeFideliteTampon
				List<PointDeVenteProgrammeFideliteTampon> listOfPointDeVenteProgrammeFideliteTampon = pointDeVenteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteTampon != null && !listOfPointDeVenteProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPointDeVenteProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}

				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete ProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete ProgrammeFideliteTampon by using ProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<ProgrammeFideliteTamponDto> forceDelete(Request<ProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete ProgrammeFideliteTampon-----");

		response = new Response<ProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<ProgrammeFideliteTampon> items = new ArrayList<ProgrammeFideliteTampon>();

			for (ProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la programmeFideliteTampon existe
				ProgrammeFideliteTampon existingEntity = null;
				existingEntity = programmeFideliteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// historiqueCarteProgrammeFideliteTampon
				List<HistoriqueCarteProgrammeFideliteTampon> listOfHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfHistoriqueCarteProgrammeFideliteTampon != null && !listOfHistoriqueCarteProgrammeFideliteTampon.isEmpty()){
					Request<HistoriqueCarteProgrammeFideliteTamponDto> deleteRequest = new Request<HistoriqueCarteProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(HistoriqueCarteProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfHistoriqueCarteProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueCarteProgrammeFideliteTamponDto> deleteResponse = historiqueCarteProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// informationRegleSecuriteTampon
				List<InformationRegleSecuriteTampon> listOfInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfInformationRegleSecuriteTampon != null && !listOfInformationRegleSecuriteTampon.isEmpty()){
					Request<InformationRegleSecuriteTamponDto> deleteRequest = new Request<InformationRegleSecuriteTamponDto>();
					deleteRequest.setDatas(InformationRegleSecuriteTamponTransformer.INSTANCE.toDtos(listOfInformationRegleSecuriteTampon));
					deleteRequest.setUser(request.getUser());
					Response<InformationRegleSecuriteTamponDto> deleteResponse = informationRegleSecuriteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				
				// souscriptionProgrammeFidelite
				List<SouscriptionProgrammeFidelite> listOfSouscriptionProgrammeFidelite = souscriptionProgrammeFideliteRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFidelite != null && !listOfSouscriptionProgrammeFidelite.isEmpty()){
					Request<SouscriptionProgrammeFideliteDto> deleteRequest = new Request<SouscriptionProgrammeFideliteDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteDto> deleteResponse = souscriptionProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// souscriptionProgrammeFideliteTampon
				List<SouscriptionProgrammeFideliteTampon> listOfSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfSouscriptionProgrammeFideliteTampon != null && !listOfSouscriptionProgrammeFideliteTampon.isEmpty()){
					Request<SouscriptionProgrammeFideliteTamponDto> deleteRequest = new Request<SouscriptionProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(SouscriptionProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfSouscriptionProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<SouscriptionProgrammeFideliteTamponDto> deleteResponse = souscriptionProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// pointDeVenteProgrammeFideliteTampon
				List<PointDeVenteProgrammeFideliteTampon> listOfPointDeVenteProgrammeFideliteTampon = pointDeVenteProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfPointDeVenteProgrammeFideliteTampon != null && !listOfPointDeVenteProgrammeFideliteTampon.isEmpty()){
					Request<PointDeVenteProgrammeFideliteTamponDto> deleteRequest = new Request<PointDeVenteProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(PointDeVenteProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfPointDeVenteProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<PointDeVenteProgrammeFideliteTamponDto> deleteResponse = pointDeVenteProgrammeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				programmeFideliteTamponRepository.saveAll((Iterable<ProgrammeFideliteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete ProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get ProgrammeFideliteTampon by using ProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ProgrammeFideliteTamponDto> getByCriteria(Request<ProgrammeFideliteTamponDto> request, Locale locale) {
		slf4jLogger.info("----begin get ProgrammeFideliteTampon-----");

		response = new Response<ProgrammeFideliteTamponDto>();

		try {
			List<ProgrammeFideliteTampon> items = null;
			items = programmeFideliteTamponRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<ProgrammeFideliteTamponDto> itemsDto = new ArrayList<ProgrammeFideliteTamponDto>();
				if (request.isStat()) {
					ProgrammeFideliteTamponDto dto = new ProgrammeFideliteTamponDto() ;
					dto.setLibelle(GlobalEnum.AUCUN);
					itemsDto.add(dto);
				}
				for (ProgrammeFideliteTampon entity : items) {
					ProgrammeFideliteTamponDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(programmeFideliteTamponRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				if (request.isStat()) {
					ProgrammeFideliteTamponDto dto = new ProgrammeFideliteTamponDto() ;
					dto.setLibelle(GlobalEnum.AUCUN);
					response.setItems(Arrays.asList(dto));
				}
				response.setStatus(functionalError.DATA_EMPTY("programmeFideliteTampon", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get ProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ProgrammeFideliteTamponDto by using ProgrammeFideliteTampon as object.
	 *
	 * @param entity, locale
	 * @return ProgrammeFideliteTamponDto
	 *
	 */
	public ProgrammeFideliteTamponDto getFullInfos(ProgrammeFideliteTampon entity, Integer size, Locale locale) throws Exception {
		ProgrammeFideliteTamponDto dto = ProgrammeFideliteTamponTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		// renvoi nbre de PDV
		List<PointDeVente> datasPointDeVente = pointDeVenteProgrammeFideliteTamponRepository.findAllPointDeVenteByProgrammeFideliteTamponId(dto.getId(), false) ;
		dto.setNbrePDV(Utilities.isNotEmpty(datasPointDeVente) ? datasPointDeVente.size() : 0);		

		// renvoi nbre de user du PDF
		List<SouscriptionProgrammeFideliteTampon> datasSouscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndIsLocked(dto.getId(), false, false) ;
		dto.setNbreUtilisateur(Utilities.isNotEmpty(datasSouscriptionProgrammeFideliteTampon) ? datasSouscriptionProgrammeFideliteTampon.size() : 0);		

		// info enseigne
		Enseigne enseigne = enseigneRepository.findById(dto.getEnseigneId(), false) ;
		if (enseigne != null) {
			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(enseigne));
		}
		if (dto.getUrlImage() != null) {
			dto.setUrlLogo(paramsUtils.getBaseUrlImageApp() + dto.getUrlImage());
		}		
		if (size > 1) {
			return dto;
		}


		// renvoi des listes de PDV
		if (Utilities.isNotEmpty(datasPointDeVente)) {
			dto.setDatasPointDeVente(PointDeVenteTransformer.INSTANCE.toDtos(datasPointDeVente)) ;
		}

		// renvoi des listes informationRegleSecuriteTampon
		List<InformationRegleSecuriteTampon> datasInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findByProgrammeFideliteTamponId(dto.getId(), false)  ;
		if (Utilities.isNotEmpty(datasInformationRegleSecuriteTampon)) {
			dto.setDatasInformationRegleSecuriteTampon(InformationRegleSecuriteTamponTransformer.INSTANCE.toDtos(datasInformationRegleSecuriteTampon)) ;
		}

		// info enseigne
		//		Enseigne enseigne = enseigneRepository.findById(dto.getEnseigneId(), false) ;
		//		if (enseigne != null) {
		//			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(enseigne));
		//		}

		// TODO : renvoi de logo a revoir
		
//		if (Utilities.notBlank(dto.getUrlImage())) {
//			//String fileNameByExtension = Utilities.addExtensionInFileName( dto.getUrlImage() , dto.getExtensionLogo()) ;
//
//			dto.setCheminFichier(Utilities.getSuitableFileUrl(dto.getUrlImage(), paramsUtils));
//			
//			// formatter le name du fichier
//			dto.setUrlLogo(Utilities.getBasicNameFile(dto.getUrlLogo()));
//			
//			//dto.setCheminFichier(Utilities.getSuitableFileUrl(dto.getUrlLogo(), paramsUtils));
//			//dto.setCheminFichier(Utilities.getSuitableFileUrlCustom(dto.getUrlLogo(), GlobalEnum.profils, paramsUtils));
//		}
		return dto;
	}
}


