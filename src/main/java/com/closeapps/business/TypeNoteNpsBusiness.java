


                                                                          /*
 * Java transformer for entity table type_note_nps
 * Created on 2021-08-26 ( Time 17:44:08 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "type_note_nps"
 *
* @author Back-End developper
   *
 */
@Component
public class TypeNoteNpsBusiness implements IBasicBusiness<Request<TypeNoteNpsDto>, Response<TypeNoteNpsDto>> {

  private Response<TypeNoteNpsDto> response;
  @Autowired
  private TypeNoteNpsRepository typeNoteNpsRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
    private NetPromoterScorUserRepository netPromoterScorUserRepository;

  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

                      
  @Autowired
  private NetPromoterScorUserBusiness netPromoterScorUserBusiness;

      

  public TypeNoteNpsBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create TypeNoteNps by using TypeNoteNpsDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TypeNoteNpsDto> create(Request<TypeNoteNpsDto> request, Locale locale)  {
    slf4jLogger.info("----begin create TypeNoteNps-----");

    response = new Response<TypeNoteNpsDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TypeNoteNps> items = new ArrayList<TypeNoteNps>();

      for (TypeNoteNpsDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("libelle", dto.getLibelle());
        fieldsToVerify.put("code", dto.getCode());
//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if typeNoteNps to insert do not exist
        TypeNoteNps existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("typeNoteNps -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = typeNoteNpsRepository.findByLibelle(dto.getLibelle(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("typeNoteNps -> " + dto.getLibelle(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les typeNoteNpss", locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = typeNoteNpsRepository.findByCode(dto.getCode(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("typeNoteNps -> " + dto.getCode(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les typeNoteNpss", locale));
          response.setHasError(true);
          return response;
        }

        TypeNoteNps entityToSave = null;
        entityToSave = TypeNoteNpsTransformer.INSTANCE.toEntity(dto);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<TypeNoteNps> itemsSaved = null;
        // inserer les donnees en base de donnees
                itemsSaved = typeNoteNpsRepository.saveAll((Iterable<TypeNoteNps>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("typeNoteNps", locale));
          response.setHasError(true);
          return response;
        }
        List<TypeNoteNpsDto> itemsDto = new ArrayList<TypeNoteNpsDto>();
        for (TypeNoteNps entity : itemsSaved) {
          TypeNoteNpsDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create TypeNoteNps-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update TypeNoteNps by using TypeNoteNpsDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TypeNoteNpsDto> update(Request<TypeNoteNpsDto> request, Locale locale)  {
    slf4jLogger.info("----begin update TypeNoteNps-----");

    response = new Response<TypeNoteNpsDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TypeNoteNps> items = new ArrayList<TypeNoteNps>();

      for (TypeNoteNpsDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la typeNoteNps existe
        TypeNoteNps entityToSave = null;
        entityToSave = typeNoteNpsRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeNoteNps -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        if (Utilities.notBlank(dto.getLibelle())) {
                        TypeNoteNps existingEntity = typeNoteNpsRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("typeNoteNps -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les typeNoteNpss", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setLibelle(dto.getLibelle());
        }
        if (Utilities.notBlank(dto.getCode())) {
                        TypeNoteNps existingEntity = typeNoteNpsRepository.findByCode(dto.getCode(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("typeNoteNps -> " + dto.getCode(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les typeNoteNpss", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setCode(dto.getCode());
        }
        
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<TypeNoteNps> itemsSaved = null;
        // maj les donnees en base
                itemsSaved = typeNoteNpsRepository.saveAll((Iterable<TypeNoteNps>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("typeNoteNps", locale));
          response.setHasError(true);
          return response;
        }
        List<TypeNoteNpsDto> itemsDto = new ArrayList<TypeNoteNpsDto>();
        for (TypeNoteNps entity : itemsSaved) {
          TypeNoteNpsDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update TypeNoteNps-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete TypeNoteNps by using TypeNoteNpsDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<TypeNoteNpsDto> delete(Request<TypeNoteNpsDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete TypeNoteNps-----");

    response = new Response<TypeNoteNpsDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TypeNoteNps> items = new ArrayList<TypeNoteNps>();

      for (TypeNoteNpsDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la typeNoteNps existe
        TypeNoteNps existingEntity = null;
        existingEntity = typeNoteNpsRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeNoteNps -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // netPromoterScorUser
        List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByTypeNoteNpsId(existingEntity.getId(), false);
        if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
                typeNoteNpsRepository.saveAll((Iterable<TypeNoteNps>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete TypeNoteNps-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete TypeNoteNps by using TypeNoteNpsDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<TypeNoteNpsDto> forceDelete(Request<TypeNoteNpsDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete TypeNoteNps-----");

    response = new Response<TypeNoteNpsDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<TypeNoteNps> items = new ArrayList<TypeNoteNps>();

      for (TypeNoteNpsDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la typeNoteNps existe
        TypeNoteNps existingEntity = null;
          existingEntity = typeNoteNpsRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("typeNoteNps -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                                          // netPromoterScorUser
        List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByTypeNoteNpsId(existingEntity.getId(), false);
        if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
          Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
          deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
          deleteRequest.setUser(request.getUser());
          Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
                  typeNoteNpsRepository.saveAll((Iterable<TypeNoteNps>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete TypeNoteNps-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get TypeNoteNps by using TypeNoteNpsDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<TypeNoteNpsDto> getByCriteria(Request<TypeNoteNpsDto> request, Locale locale) {
    slf4jLogger.info("----begin get TypeNoteNps-----");

    response = new Response<TypeNoteNpsDto>();

    try {
      List<TypeNoteNps> items = null;
      items = typeNoteNpsRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<TypeNoteNpsDto> itemsDto = new ArrayList<TypeNoteNpsDto>();
        for (TypeNoteNps entity : items) {
          TypeNoteNpsDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(typeNoteNpsRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("typeNoteNps", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get TypeNoteNps-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full TypeNoteNpsDto by using TypeNoteNps as object.
   *
   * @param entity, locale
   * @return TypeNoteNpsDto
   *
   */
  private TypeNoteNpsDto getFullInfos(TypeNoteNps entity, Integer size, Locale locale) throws Exception {
    TypeNoteNpsDto dto = TypeNoteNpsTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
