


                                                                          /*
 * Java transformer for entity table point_de_vente_programme_fidelite_tampon
 * Created on 2020-08-17 ( Time 14:14:48 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "point_de_vente_programme_fidelite_tampon"
 *
* @author Back-End developper
   *
 */
@Component
public class PointDeVenteProgrammeFideliteTamponBusiness implements IBasicBusiness<Request<PointDeVenteProgrammeFideliteTamponDto>, Response<PointDeVenteProgrammeFideliteTamponDto>> {

  private Response<PointDeVenteProgrammeFideliteTamponDto> response;
  @Autowired
  private PointDeVenteProgrammeFideliteTamponRepository pointDeVenteProgrammeFideliteTamponRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
    @Autowired
  private PointDeVenteRepository pointDeVenteRepository;
    @Autowired
    private ProgrammeFideliteTamponBusiness programmeFideliteTamponBusiness;
    
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateFormat;

          

  public PointDeVenteProgrammeFideliteTamponBusiness() {
    dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteTamponDto> create(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin create PointDeVenteProgrammeFideliteTampon-----");

    response = new Response<PointDeVenteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteTampon> items = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

      for (PointDeVenteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
        fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
        fieldsToVerify.put("createdBy", dto.getCreatedBy());
        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */

        // Verify if pointDeVenteProgrammeFideliteTampon to insert do not exist
        PointDeVenteProgrammeFideliteTampon existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("pointDeVenteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // Verify if programmeFideliteTampon exist
                ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
                if (existingProgrammeFideliteTampon == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
          response.setHasError(true);
          return response;
        }
        // Verify if pointDeVente exist
                PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
                if (existingPointDeVente == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
          response.setHasError(true);
          return response;
        }
        PointDeVenteProgrammeFideliteTampon entityToSave = null;
        entityToSave = PointDeVenteProgrammeFideliteTamponTransformer.INSTANCE.toEntity(dto, existingProgrammeFideliteTampon, existingPointDeVente);
        entityToSave.setCreatedBy(request.getUser());
    entityToSave.setCreatedAt(Utilities.getCurrentDate());
        entityToSave.setIsDeleted(false);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteTampon> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
          response.setHasError(true);
          return response;
        }
        List<PointDeVenteProgrammeFideliteTamponDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteTamponDto>();
        for (PointDeVenteProgrammeFideliteTampon entity : itemsSaved) {
          PointDeVenteProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteTamponDto> update(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin update PointDeVenteProgrammeFideliteTampon-----");

    response = new Response<PointDeVenteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteTampon> items = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

      for (PointDeVenteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pointDeVenteProgrammeFideliteTampon existe
        PointDeVenteProgrammeFideliteTampon entityToSave = null;
        entityToSave = pointDeVenteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }


        Integer entityToSaveId = entityToSave.getId();

        // Verify if programmeFideliteTampon exist
        if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
                    ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
          if (existingProgrammeFideliteTampon == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
        }
        // Verify if pointDeVente exist
        if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0){
                    PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
          if (existingPointDeVente == null) {
                      response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
            response.setHasError(true);
            return response;
          }
          entityToSave.setPointDeVente(existingPointDeVente);
        }
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
        entityToSave.setUpdatedBy(request.getUser());
        entityToSave.setUpdatedAt(Utilities.getCurrentDate());
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteTampon> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("pointDeVenteProgrammeFideliteTampon", locale));
          response.setHasError(true);
          return response;
        }
        List<PointDeVenteProgrammeFideliteTamponDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteTamponDto>();
        for (PointDeVenteProgrammeFideliteTampon entity : itemsSaved) {
          PointDeVenteProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<PointDeVenteProgrammeFideliteTamponDto> delete(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete PointDeVenteProgrammeFideliteTampon-----");

    response = new Response<PointDeVenteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteTampon> items = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

      for (PointDeVenteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        //fieldsToVerify.put("user", request.getUser());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
*/

        // Verifier si la pointDeVenteProgrammeFideliteTampon existe
        PointDeVenteProgrammeFideliteTampon existingEntity = null;
        existingEntity = pointDeVenteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------



        existingEntity.setDeletedBy(request.getUser());
        existingEntity.setDeletedAt(Utilities.getCurrentDate());
        existingEntity.setIsDeleted(true);
        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<PointDeVenteProgrammeFideliteTamponDto> forceDelete(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete PointDeVenteProgrammeFideliteTampon-----");

    response = new Response<PointDeVenteProgrammeFideliteTamponDto>();

    try {
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    User utilisateur = userRepository.findById(request.getUser(), false);
    if (utilisateur == null) {
      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
      response.setHasError(true);
      return response;
    }
    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
      response.setHasError(true);
      return response;
    }

      List<PointDeVenteProgrammeFideliteTampon> items = new ArrayList<PointDeVenteProgrammeFideliteTampon>();

      for (PointDeVenteProgrammeFideliteTamponDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            //fieldsToVerify.put("user", request.getUser());
          if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

    /*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
  */
  
        // Verifier si la pointDeVenteProgrammeFideliteTampon existe
        PointDeVenteProgrammeFideliteTampon existingEntity = null;
          existingEntity = pointDeVenteProgrammeFideliteTamponRepository.findById(dto.getId(), false);
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVenteProgrammeFideliteTampon -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

          

                                              existingEntity.setDeletedBy(request.getUser());
            existingEntity.setDeletedAt(Utilities.getCurrentDate());
                  existingEntity.setIsDeleted(true);
              items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          pointDeVenteProgrammeFideliteTamponRepository.saveAll((Iterable<PointDeVenteProgrammeFideliteTampon>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<PointDeVenteProgrammeFideliteTamponDto> getByCriteria(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale) {
    slf4jLogger.info("----begin get PointDeVenteProgrammeFideliteTampon-----");

    response = new Response<PointDeVenteProgrammeFideliteTamponDto>();

    try {
      List<PointDeVenteProgrammeFideliteTampon> items = null;
      items = pointDeVenteProgrammeFideliteTamponRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<PointDeVenteProgrammeFideliteTamponDto> itemsDto = new ArrayList<PointDeVenteProgrammeFideliteTamponDto>();
        for (PointDeVenteProgrammeFideliteTampon entity : items) {
          PointDeVenteProgrammeFideliteTamponDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(pointDeVenteProgrammeFideliteTamponRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("pointDeVenteProgrammeFideliteTampon", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }
  
  /**
   * getByCriteriaCustom PointDeVenteProgrammeFideliteTampon by using PointDeVenteProgrammeFideliteTamponDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  public Response<ProgrammeFideliteTamponDto> getByCriteriaCustom(Request<PointDeVenteProgrammeFideliteTamponDto> request, Locale locale) {
    slf4jLogger.info("----begin getByCriteriaCustom PointDeVenteProgrammeFideliteTampon-----");

    Response<ProgrammeFideliteTamponDto> responseLocal = new Response<>();

    try {
      List<ProgrammeFideliteTampon> items = null;
      items = pointDeVenteProgrammeFideliteTamponRepository.getByCriteriaCustom(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<ProgrammeFideliteTamponDto> itemsDto = new ArrayList<>();
        for (ProgrammeFideliteTampon entity : items) {
        	ProgrammeFideliteTamponDto dto = programmeFideliteTamponBusiness.getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        responseLocal.setItems(itemsDto);
        responseLocal.setCount(pointDeVenteProgrammeFideliteTamponRepository.countCustom(request, em, locale));
        responseLocal.setHasError(false);
      } else {
    	  responseLocal.setStatus(functionalError.DATA_EMPTY("pointDeVenteProgrammeFideliteTampon", locale));
        responseLocal.setHasError(false);
        return responseLocal;
      }

      slf4jLogger.info("----end get PointDeVenteProgrammeFideliteTampon-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(responseLocal, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(responseLocal, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(responseLocal, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(responseLocal, locale, e);
    } finally {
      if (responseLocal.isHasError() && responseLocal.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", responseLocal.getStatus().getCode(), responseLocal.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + responseLocal.getStatus().getMessage());
      }
    }
    return responseLocal;
  }

  /**
   * get full PointDeVenteProgrammeFideliteTamponDto by using PointDeVenteProgrammeFideliteTampon as object.
   *
   * @param entity, locale
   * @return PointDeVenteProgrammeFideliteTamponDto
   *
   */
  private PointDeVenteProgrammeFideliteTamponDto getFullInfos(PointDeVenteProgrammeFideliteTampon entity, Integer size, Locale locale) throws Exception {
    PointDeVenteProgrammeFideliteTamponDto dto = PointDeVenteProgrammeFideliteTamponTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }
}
