


/*
 * Java transformer for entity table user
 * Created on 2020-08-17 ( Time 14:14:53 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import com.closeapps.dao.entity.Carte;
import com.closeapps.dao.entity.Enseigne;
import com.closeapps.dao.entity.EtatInscripitionEnseigne;
import com.closeapps.dao.entity.EtatSouscription;
import com.closeapps.dao.entity.HistoriqueCarteProgrammeFideliteCarte;
import com.closeapps.dao.entity.HistoriqueCarteProgrammeFideliteTampon;
import com.closeapps.dao.entity.HistoriqueSouscriptionEnseigne;
import com.closeapps.dao.entity.NetPromoterScorUser;
import com.closeapps.dao.entity.NotePdf;
import com.closeapps.dao.entity.NotesClose;
import com.closeapps.dao.entity.PointDeVente;
import com.closeapps.dao.entity.ProgrammeFideliteCarte;
import com.closeapps.dao.entity.ProgrammeFideliteTampon;
import com.closeapps.dao.entity.Role;
import com.closeapps.dao.entity.SecteurActiviteEnseigne;
import com.closeapps.dao.entity.SouscriptionProgrammeFideliteCarte;
import com.closeapps.dao.entity.SouscriptionProgrammeFideliteTampon;
import com.closeapps.dao.entity.User;
import com.closeapps.dao.entity.UserEnseignePointDeVente;
import com.closeapps.dao.repository.CarteRepository;
import com.closeapps.dao.repository.EnseigneRepository;
import com.closeapps.dao.repository.EtatInscripitionEnseigneRepository;
import com.closeapps.dao.repository.EtatSouscriptionRepository;
import com.closeapps.dao.repository.HistoriqueSouscriptionEnseigneRepository;
import com.closeapps.dao.repository.NetPromoterScorUserRepository;
import com.closeapps.dao.repository.NotePdfRepository;
import com.closeapps.dao.repository.NotesCloseRepository;
import com.closeapps.dao.repository.RoleRepository;
import com.closeapps.dao.repository.UserEnseignePointDeVenteRepository;
import com.closeapps.dao.repository.UserRepository;
import com.closeapps.helper.ExceptionUtils;
import com.closeapps.helper.FunctionalError;
import com.closeapps.helper.HostingUtils;
import com.closeapps.helper.ParamsUtils;
import com.closeapps.helper.TechnicalError;
import com.closeapps.helper.Utilities;
import com.closeapps.helper.Validate;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.dto.CarteDto;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.NetPromoterScorUserDto;
import com.closeapps.helper.dto.NotePdfDto;
import com.closeapps.helper.dto.NotesCloseDto;
import com.closeapps.helper.dto.UserDto;
import com.closeapps.helper.dto.UserEnseignePointDeVenteDto;
import com.closeapps.helper.dto.customize._GraphCirculaireDto;
import com.closeapps.helper.dto.customize._RetourGroupByDto;
import com.closeapps.helper.dto.transformer.CarteTransformer;
import com.closeapps.helper.dto.transformer.EnseigneTransformer;
import com.closeapps.helper.dto.transformer.NetPromoterScorUserTransformer;
import com.closeapps.helper.dto.transformer.NotePdfTransformer;
import com.closeapps.helper.dto.transformer.NotesCloseTransformer;
import com.closeapps.helper.dto.transformer.PointDeVenteTransformer;
import com.closeapps.helper.dto.transformer.UserEnseignePointDeVenteTransformer;
import com.closeapps.helper.dto.transformer.UserTransformer;
import com.closeapps.helper.enums.EmailEnum;
import com.closeapps.helper.enums.EtatEnum;
import com.closeapps.helper.enums.GlobalEnum;
import com.closeapps.helper.enums.RoleEnum;
import com.closeapps.helper.enums.TypeActionEnum;
import com.closeapps.helper.enums.TypeNoteNpsEnum;


/**
BUSINESS for table "user"
 *
 * @author Back-End developper
 *
 */
@Component
@EnableAsync // pour activer le mode asynchrone
public class UserBusiness implements IBasicBusiness<Request<UserDto>, Response<UserDto>> {

	private Response<UserDto> response;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CarteRepository carteRepository;

	@Autowired
	private UserEnseignePointDeVenteRepository userEnseignePointDeVenteRepository;
	@Autowired
	private NotesCloseRepository notesCloseRepository;

	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private HistoriqueSouscriptionEnseigneRepository historiqueSouscriptionEnseigneRepository;
	@Autowired
	private EtatSouscriptionRepository etatSouscriptionRepository;	
	@Autowired
	private EtatInscripitionEnseigneRepository etatInscripitionEnseigneRepository;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private NotePdfBusiness notePdfBusiness;






	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormatNew;


	@Autowired
	private CarteBusiness carteBusiness;
	@Autowired
	private UserEnseignePointDeVenteBusiness userEnseignePointDeVenteBusiness ;

	@Autowired
	private NotesCloseBusiness notesCloseBusiness;

	@Autowired
	private ParamsUtils paramsUtils;

	private Context context;

	@Autowired
	private HostingUtils hostingUtils;




	public UserBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormatNew = new SimpleDateFormat("dd/MM/yyyy");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> create(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin create User-----");

		response = new Response<UserDto>();

		try {
			//    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//    fieldsToVerifyUser.put("user", request.getUser());
			//    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//      response.setHasError(true);
			//      return response;
			//    }
			//
			//    User utilisateur = userRepository.findById(request.getUser(), false);
			//    if (utilisateur == null) {
			//      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
			//      response.setHasError(true);
			//      return response;
			//    }
			//    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
			//      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			//      response.setHasError(true);
			//      return response;
			//    }

			List<User> items = new ArrayList<User>();
			List<CarteDto> datasCarte = new ArrayList<CarteDto>();

			for (UserDto dto : request.getDatas()) {

				System.out.println("********* getByFirebaseUser: " + dto.getByFirebaseUser());
				System.out.println("********* getIsUserMobile: " + dto.getIsUserMobile());

				if (dto.getByFirebaseUser() != null && dto.getByFirebaseUser()) {
					// generation login/password
					String passwordGenerer = Utilities.generatePassowrdForUserAndroid(userRepository, GlobalEnum.longueurDefaultPassword) ;
					//String loginGenerer = Utilities.generateLoginUserAndroid(userRepository, GlobalEnum.loginUser);	

					String loginUserMobile = "" ;
					if (Utilities.notBlank(dto.getEmailGoogle())) {
						loginUserMobile = dto.getEmailGoogle();
					}
					if ( loginUserMobile.isEmpty() && Utilities.notBlank(dto.getEmailFacebook())) {
						loginUserMobile = dto.getEmailGoogle();
					}
					if (loginUserMobile.isEmpty() && Utilities.notBlank(dto.getTelephoneFacebook())) {
						loginUserMobile = dto.getEmailGoogle();
					}
					if (loginUserMobile.isEmpty() && Utilities.notBlank(dto.getTelephoneGoogle())) {
						loginUserMobile = dto.getEmailGoogle();
					}

					dto.setLogin(loginUserMobile);
					dto.setPassword(passwordGenerer);

					System.out.println("********* loginUserMobile: " + dto.getLogin());
					System.out.println("********* passwordGenerer: " + dto.getPassword());
				}
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("prenoms", dto.getPrenoms());
				//fieldsToVerify.put("telephone", dto.getTelephone());

				//fieldsToVerify.put("matricule", dto.getMatricule());
				//if (dto.getIsUserMobile() != null || dto.getIsUserMobile()) {

				if (dto.getIsUserMobile() != null && dto.getIsUserMobile()) {
					Role roleUserMobile = roleRepository.findByCode(RoleEnum.UTILISATEUR_MOBILE, false);
					if (roleUserMobile != null) {
						dto.setRoleId(roleUserMobile.getId());
					}
				} else {
					fieldsToVerify.put("enseigneId", dto.getEnseigneId());
				}
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());

				if (dto.getIsUserPDV() == null || !dto.getIsUserPDV()) {
					fieldsToVerify.put("roleId", dto.getRoleId());
				}



				//        fieldsToVerify.put("isLocked", dto.getIsLocked());
				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if user to insert do not exist
				User existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}



				if (dto.getIsUserMobile() != null && dto.getIsUserMobile()) {

					existingEntity = userRepository.findByEmailAndRoleId(dto.getEmail(), dto.getRoleId(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					
					if (Utilities.notBlank(dto.getTelephone())) {
						existingEntity = userRepository.findByTelephoneAndRoleId(dto.getTelephone(), dto.getRoleId(), false);
						if (existingEntity != null) {
							response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
							response.setHasError(true);
							return response;
						}
					}
				}else{
					existingEntity = userRepository.findByEmailAndEnseigneIsNotNull(dto.getEmail(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					
					if (Utilities.notBlank(dto.getTelephone())) {
						existingEntity = userRepository.findByTelephoneAndEnseigneIsNotNull(dto.getTelephone(), false);
						if (existingEntity != null) {
							response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
							response.setHasError(true);
							return response;
						}
					}
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					if (Utilities.notBlank(dto.getTelephone())) {
						
						if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
							response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
							response.setHasError(true);
							return response;
						}
					}


				

				existingEntity = userRepository.findByLogin(dto.getLogin(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
					response.setHasError(true);
					return response;
				}



				// Verify if enseigne exist
				Enseigne existingEnseigne = null ;
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0) {
					existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}
					// verifier si l'enseigne est verrouillée
					if (existingEnseigne.getIsLocked()) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("enseigne -> " + existingEnseigne.getNom() + " est verrouillé. Veuillez finaliser l'inscription de l'enseigne !!!", locale));
						response.setHasError(true);
						return response;
					}

					// verifier l'etat de l'enseigne
					EtatInscripitionEnseigne etatInscripitionEnseigne = etatInscripitionEnseigneRepository.findEtatInscripitionEnseigneByEnseigneIdAndIsValid(existingEnseigne.getId(), true, false) ;

					if (etatInscripitionEnseigne == null || !etatInscripitionEnseigne.getEtatInscripition().getCode().equals(EtatEnum.ACCEPTER)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Veuillez finaliser l'inscription de l'enseigne !!!", locale));
						response.setHasError(true);
						return response;
					}

				}
				// Verify if role exist
				Role existingRole = null ;
				if (dto.getIsUserPDV() == null || !dto.getIsUserPDV()) {
					existingRole = roleRepository.findById(dto.getRoleId(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// generation du matricule puis verification de son unicite

				Date firstDate = Utilities.firstDateOfYear() ;

				Date lastDate = Utilities.lastDateOfYear() ;


				//List<User> userSysteme = userRepository.findAllUserByYearAndRoleId(dto.getRoleId(), firstDate, lastDate);
				List<User> userSysteme = existingRole != null && existingRole.getCode().equals(RoleEnum.UTILISATEUR_MOBILE) ? 
						userRepository.findAllUserByYearAndRoleCode(RoleEnum.UTILISATEUR_MOBILE, firstDate, lastDate) 
						: userRepository.findAllUserByYearAndEnseigneNotNull(firstDate, lastDate) ;



						Integer numeroUser = (Utilities.isNotEmpty(userSysteme) ? userSysteme.size() : 0);

						String code = existingRole != null && existingRole.getCode().equals(RoleEnum.UTILISATEUR_MOBILE) ? GlobalEnum.codeUser : GlobalEnum.codeUserEnseigne ;


						String matricule = Utilities.getCodeByCritreria(code, numeroUser) ;
						//String matricule = Utilities.getCodeByCritreria(GlobalEnum.codeUser, numeroUser) ;
						dto.setMatricule(matricule);

						/*

        existingEntity = userRepository.findByMatricule(dto.getMatricule(), false);
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getMatricule(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getMatricule().equalsIgnoreCase(dto.getMatricule()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du matricule '" + dto.getMatricule()+"' pour les users", locale));
          response.setHasError(true);
          return response;
        }
						 */






						dto.setIsLocked(false);
						dto.setIsFirstlyConnexion(true);


						// HASHAGE du password avant de le mettre dans la BD
						String passwordHasher = Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()) ;



						if (dto.getIsAdminEnseigne() == null) {
							dto.setIsAdminEnseigne(false) ;
						}
						User entityToSave = null;
						entityToSave = UserTransformer.INSTANCE.toEntity(dto, existingEnseigne, existingRole);
						entityToSave.setPassword(passwordHasher);
						entityToSave.setPasswordGenerate(passwordHasher);
						entityToSave.setCreatedBy(request.getUser());
						entityToSave.setCreatedAt(Utilities.getCurrentDate());
						entityToSave.setIsDeleted(false);
						items.add(entityToSave);


						if (existingRole != null && existingRole.getCode().equals(RoleEnum.UTILISATEUR_MOBILE)) {
							// ajout de la carte à l'utilisateur				
							CarteDto carteDto = new CarteDto() ;
							carteDto.setEntityUser(entityToSave);
							datasCarte.add(carteDto) ;
						}				

			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = userRepository.saveAll((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				// TODO: creation de(s) (la) carte(s)
				if (!datasCarte.isEmpty()) {
					for (CarteDto carteDto : datasCarte) {
						carteDto.setUserId(carteDto.getEntityUser().getId());
					}
					Request<CarteDto> req = new Request<>() ;
					req.setDatas(datasCarte);
					req.setUser(request.getUser());
					Response<CarteDto> res = carteBusiness.create(req, locale) ;
					if (res != null && res.isHasError()) {
						response.setStatus(res.getStatus());
						response.setHasError(true);
						return response;
					}

				}

				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);

				// envoi de mail pourl la creation du compte

				// envoi de mail 
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					//for (User user : itemsSaved) {
					for (UserDto user : request.getDatas()) {

						if (user.getByFirebaseUser() == null || !user.getByFirebaseUser()) {

							//recipients
							List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
							//		        	for (User user : itemsSaved) {
							Map<String, String> recipient = new HashMap<String, String>();
							recipient = new HashMap<String, String>();
							recipient.put("email", user.getEmail());
							recipient.put("user", user.getLogin());
							//		        	recipient.put("user", user.getNom());

							toRecipients.add(recipient); 
							//					}

							//subject
							// ajout du champ objet a la newsletter
							//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
							String subject = EmailEnum.SUBJECT_CREATION_COMPTE;
							//String body = EmailEnum.BODY_PASSWORD_RESET;
							String body = "";
							context = new Context();
							//String template = paramsUtils.getTemplateCreationCompteParUser();
							String template = paramsUtils.getTemplateCreationCompteParUser();

							//context.setVariable("email", user.getEmail());
							//context.setVariable("login", user.getLogin());
							//context.setVariable("token", user.getToken());
							//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));


							context.setVariable(EmailEnum.appLink, appLink); // @{|${appLink}/#/reset/${email}/${code}|}

							//context.setVariable(EmailEnum.nomEntreprise, user.getEntreprise().getNom());
							context.setVariable(EmailEnum.nomUser, (Utilities.notBlank(user.getPrenoms()) ? user.getPrenoms() : user.getLogin()));
							context.setVariable(EmailEnum.userName, user.getLogin());
							context.setVariable(EmailEnum.password, user.getPassword());
							context.setVariable(EmailEnum.contenuMail, datasCarte.isEmpty() ? EmailEnum.contenuMailAccepterUserEnseigne : EmailEnum.contenuMailAccepterUserMobile );


							//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
							context.setVariable(EmailEnum.date, dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
							Response<UserDto> responseEnvoiEmail = new Response<>();
							responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
							// si jamais une exception c'est produite pour un mail

						}
					}
				}
			}

			slf4jLogger.info("----end create User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> update(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin update User-----");

		response = new Response<UserDto>();

		try {
			//    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//    fieldsToVerifyUser.put("user", request.getUser());
			//    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//      response.setHasError(true);
			//      return response;
			//    }

			if (request.getUser() != null && request.getUser() > 0 ) {


				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}

			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User entityToSave = null;
				entityToSave = userRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if enseigne exist
				if (dto.getEnseigneId() != null && dto.getEnseigneId() > 0){
					Enseigne existingEnseigne = enseigneRepository.findById(dto.getEnseigneId(), false);
					if (existingEnseigne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getEnseigneId(), locale));
						response.setHasError(true);
						return response;
					}

					// verifier si l'enseigne est verrouillée
					if (existingEnseigne.getIsLocked() != null && existingEnseigne.getIsLocked()) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("enseigne -> " + existingEnseigne.getNom() + " est verrouillé. Veuillez finaliser l'inscription de l'enseigne !!!", locale));
						response.setHasError(true);
						return response;
					}

					// verifier l'etat de l'enseigne
					EtatInscripitionEnseigne etatInscripitionEnseigne = etatInscripitionEnseigneRepository.findEtatInscripitionEnseigneByEnseigneIdAndIsValid(existingEnseigne.getId(), true, false) ;

					if (etatInscripitionEnseigne == null || !etatInscripitionEnseigne.getEtatInscripition().getCode().equals(EtatEnum.ACCEPTER)) {
						response.setStatus(functionalError.DISALLOWED_OPERATION("Veuillez finaliser l'inscription de l'enseigne !!!", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEnseigne(existingEnseigne);
				}

				if (dto.getRoleId() != null && dto.getRoleId() > 0){
					Role existingRole = roleRepository.findById(dto.getRoleId(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setRole(existingRole);
				}

				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenoms())) {
					entityToSave.setPrenoms(dto.getPrenoms());
				}
				if (Utilities.notBlank(dto.getTelephoneFacebook())) {
					entityToSave.setTelephoneFacebook(dto.getTelephoneFacebook());
				}
				if (Utilities.notBlank(dto.getTelephoneGoogle())) {
					entityToSave.setTelephoneGoogle(dto.getTelephoneGoogle());
				}
				if (Utilities.notBlank(dto.getEmailFacebook())) {
					entityToSave.setEmailFacebook(dto.getEmailFacebook());
				}
				if (Utilities.notBlank(dto.getEmailGoogle())) {
					entityToSave.setEmailGoogle(dto.getEmailGoogle());
				}
				if (dto.getByFirebaseUser() != null) {
					entityToSave.setByFirebaseUser(dto.getByFirebaseUser());
				}

				Role roleUserMobile = roleRepository.findByCode(RoleEnum.UTILISATEUR_MOBILE, false);

				
				if (Utilities.notBlank(dto.getEmail())) {
					User existingEntity = null;
					
					if(roleUserMobile != null && entityToSave.getRole() != null &&
						roleUserMobile.getId().equals(entityToSave.getRole().getId())){
						existingEntity = userRepository.findByEmailAndRoleId(dto.getEmail(), roleUserMobile.getId(), false);
					}else{
						existingEntity = userRepository.findByEmailAndEnseigneIsNotNull(dto.getEmail(), false);
					}
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}
				if (Utilities.notBlank(dto.getTelephone())) {
					User existingEntity = null;
					
					if(roleUserMobile != null && entityToSave.getRole() != null &&
						roleUserMobile.getId().equals(entityToSave.getRole().getId())){
						existingEntity = userRepository.findByTelephoneAndRoleId(dto.getTelephone(), roleUserMobile.getId(), false);
					}else{
						existingEntity = userRepository.findByTelephoneAndEnseigneIsNotNull(dto.getTelephone(), false);
					}
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (Utilities.notBlank(dto.getLogin())) {
					User existingEntity = userRepository.findByLogin(dto.getLogin(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les users", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLogin(dto.getLogin());
				}
				/*

        if (Utilities.notBlank(dto.getMatricule())) {
                        User existingEntity = userRepository.findByMatricule(dto.getMatricule(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getMatricule(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getMatricule().equalsIgnoreCase(dto.getMatricule()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du matricule '" + dto.getMatricule()+"' pour les users", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setMatricule(dto.getMatricule());
        }*/
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}	
				if (dto.getIsAdminEnseigne() != null) {
					entityToSave.setIsAdminEnseigne(dto.getIsAdminEnseigne());
				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.saveAll((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<UserDto> delete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notesClose
				List<NotesClose> listOfNotesClose = notesCloseRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNotesClose != null && !listOfNotesClose.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotesClose.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// userEnseignePointDeVente
				List<UserEnseignePointDeVente> listOfUserEnseignePointDeVente = userEnseignePointDeVenteRepository.findByUserId(existingEntity.getId(), false);
				if (listOfUserEnseignePointDeVente != null && !listOfUserEnseignePointDeVente.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserEnseignePointDeVente.size() + ")", locale));
					response.setHasError(true);
					return response;
				}

				// carte
				List<Carte> listOfCarte = carteRepository.findByUserId(existingEntity.getId(), false);
				if (listOfCarte != null && !listOfCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}

				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.saveAll((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<UserDto> forceDelete(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete User-----");

		response = new Response<UserDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<User> items = new ArrayList<User>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la user existe
				User existingEntity = null;
				existingEntity = userRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notesClose
				List<NotesClose> listOfNotesClose = notesCloseRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNotesClose != null && !listOfNotesClose.isEmpty()){
					Request<NotesCloseDto> deleteRequest = new Request<NotesCloseDto>();
					deleteRequest.setDatas(NotesCloseTransformer.INSTANCE.toDtos(listOfNotesClose));
					deleteRequest.setUser(request.getUser());
					Response<NotesCloseDto> deleteResponse = notesCloseBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// carte
				List<Carte> listOfCarte = carteRepository.findByUserId(existingEntity.getId(), false);
				if (listOfCarte != null && !listOfCarte.isEmpty()){
					Request<CarteDto> deleteRequest = new Request<CarteDto>();
					deleteRequest.setDatas(CarteTransformer.INSTANCE.toDtos(listOfCarte));
					deleteRequest.setUser(request.getUser());
					Response<CarteDto> deleteResponse = carteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// userEnseignePointDeVente
				List<UserEnseignePointDeVente> listOfUserEnseignePointDeVente = userEnseignePointDeVenteRepository.findByUserId(existingEntity.getId(), false);
				if (listOfUserEnseignePointDeVente != null && !listOfUserEnseignePointDeVente.isEmpty()){
					Request<UserEnseignePointDeVenteDto> deleteRequest = new Request<UserEnseignePointDeVenteDto>();
					deleteRequest.setDatas(UserEnseignePointDeVenteTransformer.INSTANCE.toDtos(listOfUserEnseignePointDeVente));
					deleteRequest.setUser(request.getUser());
					Response<UserEnseignePointDeVenteDto> deleteResponse = userEnseignePointDeVenteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByUserId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				userRepository.saveAll((Iterable<User>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<UserDto> getByCriteria(Request<UserDto> request, Locale locale) {
		slf4jLogger.info("----begin get User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = null;
			items = userRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : items) {
					UserDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(userRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("user", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}




	/**
	 * connexion User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> connexion(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin connexion User-----");

		response = new Response<UserDto>();

		try {

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();


			// Verify if user to insert do not exist
			User existingEntity = null;

			if (dto.getByFirebaseUser() != null && dto.getByFirebaseUser()){

				if ( !Utilities.notBlank(dto.getTelephoneFacebook()) && !Utilities.notBlank(dto.getTelephoneGoogle())
						&& !Utilities.notBlank(dto.getEmailFacebook())
						&& !Utilities.notBlank(dto.getEmailGoogle())) {
					response.setStatus(functionalError.FIELD_EMPTY("", locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getEmailGoogle())) {
					existingEntity = userRepository.findUserByEmailGoogle(dto.getEmailGoogle(), false);
				}
				if (existingEntity == null && Utilities.notBlank(dto.getEmailFacebook())) {
					existingEntity = userRepository.findUserByEmailFacebook(dto.getEmailFacebook(), false);
				}
				if (existingEntity == null && Utilities.notBlank(dto.getTelephoneFacebook())) {
					existingEntity = userRepository.findUserByTelephoneFacebook(dto.getTelephoneFacebook(), false);
				}
				if (existingEntity == null && Utilities.notBlank(dto.getTelephoneGoogle())) {
					existingEntity = userRepository.findUserByTelephoneGoogle(dto.getTelephoneGoogle(), false);
				}

				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Cet user avec ces acces n'existe pas." , locale));
					response.setHasError(true);
					return response;
				}

			}else {

				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				String oldPassword = dto.getPassword() ;

				// cryptage du password avant de rechercher dans la BD
				dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));

				existingEntity = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user avec le login-> " + dto.getLogin() + " et le password -> " + oldPassword + "n'existe pas." , locale));
					response.setHasError(true);
					return response;
				}
				//			if (existingEntity.getIsLocked() != null && existingEntity.getIsLocked()) {
				//				response.setStatus(functionalError.DATA_NOT_EXIST("Le compte -> " + dto.getLogin() + " est verrouillé. Merci de le déverrouiller !!!" , locale));
				//				response.setHasError(true);
				//				return response;
				//			}
				//			
				//			if (existingEntity.getEntreprise()!= null && existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {
				//				response.setStatus(functionalError.DATA_NOT_EXIST("L'entreprise -> " + existingEntity.getEntreprise().getNom() + " est verrouillée. Merci de la déverrouiller !!!" , locale));
				//				response.setHasError(true);
				//				return response;
				//			}

			}
			if (existingEntity.getIsLocked() != null && existingEntity.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("Cet utilisateur est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}



			/*
			if (existingEntity.getEntreprise()!= null && existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {

			//if (existingEntity.getEntreprise().getIsLocked() != null && existingEntity.getEntreprise().getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+existingEntity.getLogin()+" "+" est verouille(e) car son entreprise " + existingEntity.getEntreprise().getNom()+ " est verouillée " , locale));
				response.setHasError(true);
				return response;
			}
			 */
			UserDto userDto = getFullInfos(existingEntity, 1, locale);

			response.setItems(Arrays.asList(userDto));
			response.setHasError(false);

			slf4jLogger.info("----end connexion User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * changerPassword User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPassword(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPassword User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			fieldsToVerify.put("password", dto.getPassword());
			//fieldsToVerify.put("userId", request.getUser());

			//			 fieldsToVerify.put("newPassword", dto.getNewPassword());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if user to insert do not exist
			User entityToSave = null;

			// cryptage du password avant de rechercher dans la BD
			String passwordSaisie = dto.getPassword() ;
			dto.setPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getPassword()));
			entityToSave = userRepository.findByLoginAndPassword(dto.getLogin(), dto.getPassword(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("le login-> " + dto.getLogin() + " avec ce password "+ passwordSaisie +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			if ((entityToSave.getIsLocked() != null && entityToSave.getIsLocked())) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur est verrouillé", locale));
				response.setHasError(true);
				return response;
			}


			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();

			if (request.getUser() != null  && request.getUser() > 0) {
				// Verify if utilisateur exist
				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
					response.setHasError(true);
					return response;
				}

				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
				// verifier que le user connecter correspond à celui qui veut changer le password
				//if (!entityToSave.getId().equals(utilisateur.getId()) && !entityToSave.getIsSuperUser()) {
				if (!entityToSave.getId().equals(utilisateur.getId())) {
					response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
					response.setHasError(true);
					return response;
				}
			}



			//			User entityToSaveOld = null;
			//			// pour historisation
			//			entityToSaveOld = (User) entityToSave.clone();
			//			itemsAvant.add(entityToSaveOld);

			Integer entityToSaveId = entityToSave.getId();

			//			 // renseigner les valeurs a MAJ
			//			 if (Utilities.notBlank(dto.getNewPassword())) {
			//				 entityToSave.setPassword(dto.getNewPassword());
			//			 }
			//			 if (Utilities.notBlank(dto.getNewLogin())) {
			//				 User existingEntity = userRepository.findByLogin(dto.getNewLogin(), false);
			//				 if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
			//					 response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getNewLogin() +" existe deja.", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getNewLogin()) && !a.getId().equals(entityToSaveId))) {
			//					 response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getNewLogin()+"' pour les users", locale));
			//					 response.setHasError(true);
			//					 return response;
			//				 }
			//				 entityToSave.setLogin(dto.getNewLogin());
			//			 }


			// imposer que le newPassword >= 8 caracteres
			//			if (dto.getNewPassword().length() < EmailEnum.MIN_CARACTERE_PASSWORD) {
			//				response.setStatus(functionalError.INVALID_FORMAT("Votre nouveau password doit etre au moins de 8 caractères. Merci de renseigner un password d'au moins 8 caractères.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// cryptage du NewPassword avant de l'inserer dans la BD
			dto.setNewPassword(Utilities.encryptPasswordBy_(dto.getLogin(), dto.getNewPassword()));

			if (entityToSave.getIsFirstlyConnexion() == null || entityToSave.getIsFirstlyConnexion()) {
				entityToSave.setIsFirstlyConnexion(false);
			}

			entityToSave.setPassword(dto.getNewPassword());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.saveAll((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), request.getUser(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto data = getFullInfos(entity, itemsSaved.size(), locale);
					if (data == null) continue;
					itemsDto.add(data);
				}
				response.setItems(itemsDto);

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password a bien été modifié.", locale));
				response.setHasError(false);

				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable(EmailEnum.date, dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						//context.setVariable(EmailEnum.nomUser, user.getNom());
						context.setVariable(EmailEnum.nomUser, (Utilities.notBlank(user.getPrenoms()) ? user.getPrenoms() : user.getLogin()));
						context.setVariable(EmailEnum.userName, user.getLogin());

						//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);

						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPassword User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * passwordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> passwordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin passwordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			//			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}




			// Verify if user to insert do not exist
			User entityToSave = null;

			if (dto.getIsUserMobile() != null && dto.getIsUserMobile()) {
				Role roleUserMobile = roleRepository.findByCode(RoleEnum.UTILISATEUR_MOBILE, false);
				if(roleUserMobile != null){
					entityToSave = userRepository.findByEmailAndRoleId(dto.getEmail(), roleUserMobile.getId(), false);
				}
			}else{
				entityToSave = userRepository.findByEmailAndEnseigneIsNotNull(dto.getEmail(), false);
			}
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			//			// verifier que le user connecter correspond à celui qui a oublié le password
			//			if (!entityToSave.getId().equals(utilisateur.getId())) {
			//				response.setStatus(functionalError.AUTH_FAIL("Utilisateur -> " + utilisateur.getId() +" n'a pas ce droit.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// setter token, isValidToken et tokenCreatedAt
			entityToSave.setToken(Utilities.randomString(50));
			GregorianCalendar calendar =new GregorianCalendar();

			calendar.setTime(Utilities.getCurrentDate());
			calendar.add(Calendar.HOUR, 24);
			entityToSave.setDateExpirationToken(calendar.getTime());
			entityToSave.setIsTokenValide(true);
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSave.getId());


			items.add(entityToSave);

			if (!items.isEmpty()) {

				// sauvegarde du user avec le champ token
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.saveAll((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				// envoi de mail avec le lien comportant le token et email

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());
					for (User user : itemsSaved) {


						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//					for (User user : items) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//					recipient.put("user", user.getNom());
						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//			        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_FORGET_PASSWORD;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateResetUserPassword();

						context.setVariable("email", user.getEmail());
						context.setVariable("nom", user.getNom());
						context.setVariable("token", user.getToken());
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate()));
						context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));
						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}


						// //app.link=http://ims-pp-webbo.ovh.swan.smile.ci
						//					context.setVariable("date", dateFormat.format(new Date()));


						//					context.setVariable("userName", entityToSave.getNom());
						//					context.setVariable("userName", entityToSave.getEmail());
						//					context.setVariable("url", paramsUtils.getUrlRootToken() + entityToSave.getEmail()+ "/"+entityToSave.getToken() );
						//					context.setVariable("expiration", dateFormat.format(entityToSave.getTokenCreatedAt()));
						//					context.setVariable("date", dateFormat.format(new Date()));
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail
					}

				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto data = getFullInfos(entity, itemsSaved.size(), locale);
					if (data == null) continue;
					itemsDto.add(data);
				}
				response.setItems(itemsDto);
				response.setStatus(functionalError.SUCCESS("Veuillez consulter votre mail pour saisir votre password.", locale));
				response.setHasError(false);
			}
			slf4jLogger.info("----end passwordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * checkEmailWithTokenIsValid User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> checkEmailWithTokenIsValid(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin checkEmailWithTokenIsValid User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("token", dto.getToken());
			fieldsToVerify.put("email", dto.getEmail());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verify if user to insert do not exist
			User entityToSave = null;

			entityToSave = userRepository.findUserByTokenAndEmail(dto.getToken(), dto.getEmail(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  + " avec le token "+ dto.getToken()+" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}else{
				if (entityToSave.getIsTokenValide() != null) {
					//if (!entityToSave.getIsTokenValide() || Utilities.getCurrentDate().after(entityToSave.getDateExpirationToken())  ) {
					if (!entityToSave.getIsTokenValide()) {
						response.setStatus(functionalError.DATA_NOT_EXIST("ce token a deja été utilisé ou a expiré", locale));
						response.setHasError(true);
						return response;
					}
				}else {
					response.setStatus(functionalError.AUTH_FAIL("Veuillez signifier d'abord que vous avez oublié votre password.", locale));
					response.setHasError(true);
					return response;
				}

				// setter ces valeurs a null car elles n'ont plus de sens
				entityToSave.setIsLocked(false);
				entityToSave.setToken(null);
				entityToSave.setIsTokenValide(false);
				//entityToSave.setDateExpirationToken(null);
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(entityToSave.getId());
				items.add(entityToSave);

				// maj les donnees en base
				items = userRepository.saveAll((Iterable<User>) items);
				if (!Utilities.isNotEmpty(items)) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				response.setItems(Arrays.asList(getFullInfos(items.get(0), items.size(), locale))); // transformer en liste 
				response.setHasError(false);

				//***********************************************
			}

			slf4jLogger.info("----end checkEmailWithTokenIsValid User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * changerPasswordOublier User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> changerPasswordOublier(Request<UserDto> request, Locale locale)  {
		slf4jLogger.info("----begin changerPasswordOublier User-----");

		response = new Response<UserDto>();

		try {
			List<User> items = new ArrayList<>();
			List<User> itemsAvant = new ArrayList<>();

			UserDto dto = request.getData();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("email", dto.getEmail());
			fieldsToVerify.put("newPassword", dto.getNewPassword());
			//			fieldsToVerify.put("password", dto.getPassword());
			//			fieldsToVerify.put("user", request.getUser());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if utilisateur exist
			//			User utilisateur = userRepository.findById(request.getUser(), false);
			//			if (utilisateur == null) {
			//				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser() +" n'existe pas.", locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			// Verify if user to insert do not exist
			User entityToSave = null;

			if (dto.getIsUserMobile() != null && dto.getIsUserMobile()) {
				Role roleUserMobile = roleRepository.findByCode(RoleEnum.UTILISATEUR_MOBILE, false);
				if(roleUserMobile != null){
					entityToSave = userRepository.findByEmailAndRoleId(dto.getEmail(), roleUserMobile.getId(), false);
				}
			}else{
				entityToSave = userRepository.findByEmailAndEnseigneIsNotNull(dto.getEmail(), false);
			}
			
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user-> " + dto.getEmail()  +" n'existe pas." , locale));
				response.setHasError(true);
				return response;
			}
			if ((entityToSave.getIsLocked() != null && entityToSave.getIsLocked())) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("L'utilisateur est verrouillé", locale));
				response.setHasError(true);
				return response;
			}

			// choisir la vraie url
			String appLink = "";
			//			if (entityToSave.getAdmin() != null ) {
			//				appLink = paramsUtils.getUrlRootAdmin();
			//			}else {
			//				appLink = paramsUtils.getUrlRootUser();
			//			}

			appLink = paramsUtils.getUrlRootUser();


			User entityToSaveOld = null;
			// pour historisation
			entityToSaveOld = (User) entityToSave.clone();
			itemsAvant.add(entityToSaveOld);

			// cryptage du password avant de le setter dans la BD
			entityToSave.setPassword(Utilities.encryptPasswordBy_(entityToSave.getLogin(), dto.getNewPassword()));

			Integer entityToSaveId = entityToSave.getId();

			if (entityToSave.getIsFirstlyConnexion() == null || entityToSave.getIsFirstlyConnexion()) {
				entityToSave.setIsFirstlyConnexion(false);
			}

			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(entityToSaveId);
			items.add(entityToSave);

			if (!items.isEmpty()) {
				List<User> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = userRepository.saveAll((Iterable<User>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("user", locale));
					response.setHasError(true);
					return response;
				}

				//				Response<HistoriqueActionUserDto> responseHistoriqueActionUser = new Response<>();
				//
				//				responseHistoriqueActionUser =  historiqueActionUserBusiness.callHistorisation(itemsAvant, itemsSaved, request.getUrlService(), entityToSave.getId(), locale);
				//
				//				if (responseHistoriqueActionUser.isHasError()) {
				//					response.setHasError(true);
				//					response.setStatus(responseHistoriqueActionUser.getStatus());
				//					return response;
				//				}
				List<UserDto> itemsDto = new ArrayList<UserDto>();
				for (User entity : itemsSaved) {
					UserDto data = getFullInfos(entity, itemsSaved.size(), locale);
					if (data == null) continue;
					itemsDto.add(data);
				}
				response.setItems(itemsDto);

				response.setStatus(functionalError.SUCCESS("Votre login et/ou password a bien été modifié.", locale));
				response.setHasError(false);

				//				// envoi de mail password changer avec succes
				//				if (Utilities.isNotEmpty(itemsSaved)) {
				//					
				//					//set mail to user
				//					Map<String, String> from=new HashMap<>();
				//		        	from.put("email", paramsUtils.getSmtpMailUsername());
				//		        	from.put("user", paramsUtils.getSmtpMailUser());
				//		        	//recipients
				//		        	List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
				////		        	for (User user : itemsSaved) {
				//		        	User user = itemsSaved.get(0);
				//		        	Map<String, String> recipient = new HashMap<String, String>();
				//		        	recipient = new HashMap<String, String>();
				//		        	recipient.put("email", user.getEmail());
				//		        	recipient.put("user", user.getLogin());
				////		        	recipient.put("user", user.getNom());
				//
				//		        	toRecipients.add(recipient); 
				////					}
				//		        	
				//		        	//subject
				//		        	// ajout du champ objet a la newsletter
				////		        	String subject = "NEWSLETTER DU VOLONTARIAT";
				//		        	String subject = EmailEnum.SUBJECT_RESET_PASSWORD_OK;
				//		        	String body = "";
				//		        	context = new Context();
				//		        	String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
				////		        	context.setVariable("email", user.getEmail());
				////					context.setVariable("login", user.getLogin());
				////					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
				//					context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}
				//					
				//		        	response = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
				//		        	// si jamais une exception c'est produite pour un mail
				//
				//				}

				// envoi de mail 

				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user", paramsUtils.getSmtpMailUser());

					for (User user : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", user.getEmail());
						recipient.put("user", user.getLogin());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_PASSWORD_RESET;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						String templateChangerPassword = paramsUtils.getTemplateSuccesChangePassword();
						//		        	context.setVariable("email", user.getEmail());
						//					context.setVariable("login", user.getLogin());
						//					context.setVariable("token", user.getToken());
						//					context.setVariable("expiration", dateFormat.format(user.getTokenCreatedAt()));
						context.setVariable("appLink",appLink); // @{|${appLink}/#/reset/${email}/${code}|}
						context.setVariable("date", dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}

						//context.setVariable(EmailEnum.nomUser, user.getNom());
						context.setVariable(EmailEnum.nomUser, (Utilities.notBlank(user.getPrenoms()) ? user.getPrenoms() : user.getLogin()));
						context.setVariable(EmailEnum.userName, user.getLogin());

						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,templateChangerPassword, locale);
						// si jamais une exception c'est produite pour un mail

					}

				}
			}

			slf4jLogger.info("----end changerPasswordOublier User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}





	/**
	 * getStats User by using UserDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<UserDto> getStats(Request<UserDto> request, Locale locale) {
		slf4jLogger.info("----begin getStats User-----");

		Response<UserDto> response = new Response<>();

		try {
			//List<AppelDoffres> items = null;
			UserDto itemDto = new UserDto();

			//Integer nbreMixte = 0 ;
			//Integer nbreMixte = 0 ;


			// formation des datas envoyées

			UserDto dto = request.getData() ;

			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("dateDebut", dto.getDateDebut());
			fieldsToVerify.put("dateFin", dto.getDateFin());
			//fieldsToVerify.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			Date dateDebut = dateFormatNew.parse(dto.getDateDebut());
			Date dateFin = dateFormatNew.parse(dto.getDateFin());
			if (dateDebut.after(dateFin)) {
				response.setStatus(functionalError.INVALID_DATA("La date de debut soit etre <= à la date de fin", locale));
				response.setHasError(true);
				return response;
			}

			//			PointDeVente existingPointDeVente = null;
			//			if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() >  0) {
			//				existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
			//				if (existingPointDeVente == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getPointDeVenteId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}
			//			}
			//			ProgrammeFideliteCarte existingProgrammeFideliteCarte = null;
			//			if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() >  0) {
			//				existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
			//				if (existingProgrammeFideliteCarte == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getProgrammeFideliteCarteId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}	
			//			}
			//			ProgrammeFideliteTampon existingProgrammeFideliteTampon = null;
			//			if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() >  0) {
			//				existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
			//				if (existingProgrammeFideliteTampon == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getProgrammeFideliteTamponId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}
			//			}


			// Verifier si la enseigne existe



			Integer  souscriptionId = dto.getSouscriptionId() ;


			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(dateDebut);
			//calendar.add(Calendar.MONTH, -11);
			//String currentDateString = "01" + dateFormatToSearch.format(calendar.getTime());			
			//Date dateFacture= dateFormatFront.parse(currentDateString);



			// 

			Date dateDebutNew ;
			Date dateFinNew ;
			Boolean isAnneeEcart = false ;

			Boolean isPdvIdNotNull = false ;
			Boolean isPdfCarteIdNotNull = false ;
			Boolean isPdfTamponIdNotNull = false ;


			// verifier si l'ecart depasse des annees
			calendar.add(Calendar.YEAR, 1); // annee suivante
			if (dateFin.after(calendar.getTime()) || calendar.getTime().equals(dateFin)) {
				isAnneeEcart = true ;
			}
			calendar.add(Calendar.YEAR, -1); // annee normale
			calendar.setTime(dateFormat.parse(dateFormatNew.format(calendar.getTime()) + " 23:59:59"));


			dateFin = dateFormat.parse(dto.getDateFin() + " 23:59:59"); // important pour la derniere requete


			// declaration des listes

			Integer  nbreAllUtilisateur  = 0;
			// Double  gainTotal = 0.0 ;
			Integer  nbreTotalEnseigne = 0 ;

			// bande 
			List<Double> nbreEnseigne = new ArrayList<>();
			List<Double> nbreNewEnseigne = new ArrayList<>();
			List<Double> nbreContratArreter = new ArrayList<>();
			List<Double> nbreEnseigneParContrat = new ArrayList<>();

			List<Double> gain = new ArrayList<>();


			List<String> abscisseGraphe = new ArrayList<>();

			// circulaire 
			List<_GraphCirculaireDto> inscritParPdfCarte = new ArrayList<>();
			List<String> legendCaPdfCarte= new ArrayList<>();



			// nbre all user 
			List<User> allUser = userRepository.findByRoleCode(RoleEnum.UTILISATEUR_MOBILE, false) ;
			nbreAllUtilisateur = Utilities.isNotEmpty(allUser) ?  allUser.size() : 0 ;

			// nbre all enseigne 
			List<Enseigne> allEnseigne = enseigneRepository.findByIsDeleted(false) ;
			nbreTotalEnseigne = Utilities.isNotEmpty(allEnseigne) ?  allEnseigne.size() : 0 ;

			// nbre all enseigne 
			//List<SouscriptionEnseigne> allSouscriptionEnseigne = souscriptionEnseigneRepository.findByIsActive(true, false) ;
//			List<HistoriqueSouscriptionEnseigne> allHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByIsDeleted(false) ;
//			if (Utilities.isNotEmpty(allSouscriptionEnseigne)) {
//				for (SouscriptionEnseigne souscriptionEnseigne : allSouscriptionEnseigne) {
//					double currentGain = Utilities.notBlank(souscriptionEnseigne.getPrixSpecifique()) ? Integer.parseInt(souscriptionEnseigne.getPrixSpecifique()) : 0 ;
//					gainTotal += currentGain;
//				}
//			}

			while(dateFin.after(calendar.getTime()) || dateFin.equals(calendar.getTime())) {


				String xGraphe = "" ;


				dateDebutNew = dateFormat.parse(dateFormatNew.format(calendar.getTime()) + " 00:00:00"); // important pour le debut afin d'eviter 23:59:59  

				if (isAnneeEcart) {
					xGraphe = ""+calendar.get(Calendar.YEAR) ;
					calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR)); // setter le dernier jour de l'annee

					//calendar.add(Calendar.YEAR, 1); // annee suivante
				}else {
					xGraphe = Utilities.getLibelleMonthByMonthId(calendar.get(Calendar.MONTH)) ;
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));// setter la date du mois à une valeur. La méthode est generale 

					//calendar.add(Calendar.MONTH, 1); // mois suivant
				}


				if (calendar.getTime().after(dateFin) || calendar.getTime().equals(dateFin)) { // voir si le mois suivant est inferieur à la date de fin
					dateFinNew = dateFin;
				}else {					
					dateFinNew = calendar.getTime();					
				}

				// setter le calendar a un jour suivant
				calendar.add(Calendar.DAY_OF_YEAR, 1); // mois suivant




				double currentNbreEnseigne = 0 ;
				double currentNbreEnseigneParContrat = 0 ;
				double currentNbreNewEnseigne = 0 ;
				double currentNbreContratArreter = 0 ;
				double currentGain = 0 ;



				if (dto.getSouscriptionId() != null && dto.getSouscriptionId() > 0) {
					List<Enseigne> enseignesParSouscription = historiqueSouscriptionEnseigneRepository.findAllEnseigneBySouscriptionIdAndCreatedAtSouscripBeetwen(dto.getSouscriptionId(), dateDebutNew, dateFinNew, false) ;
					if (Utilities.isNotEmpty(enseignesParSouscription)) {
						currentNbreEnseigneParContrat = enseignesParSouscription.size() ;
					}
				}else {
					List<Enseigne> enseignesParSouscription = historiqueSouscriptionEnseigneRepository.findAllEnseigneByCreatedAtSouscripBeetwen(dateDebutNew, dateFinNew, false) ;
					if (Utilities.isNotEmpty(enseignesParSouscription)) {
						currentNbreEnseigneParContrat = enseignesParSouscription.size() ;
					}
				}
				
				
				EtatSouscription  etatSouscriptionTerminer = etatSouscriptionRepository.findByCode(EtatEnum.TERMINER, false) ;
				EtatSouscription  etatSouscriptionAccepter = etatSouscriptionRepository.findByCode(EtatEnum.ACCEPTER, false) ;

				List<HistoriqueSouscriptionEnseigne> historiqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEtatSouscriptionBeetwenAndCreatedAtBeetwen(etatSouscriptionTerminer.getId(), etatSouscriptionAccepter.getId(), dateDebutNew, dateFinNew, false) ;
				if (Utilities.isNotEmpty(historiqueSouscriptionEnseigne)) {
					for (HistoriqueSouscriptionEnseigne data : historiqueSouscriptionEnseigne) {
						double localCurrentGain = data.getPrixSpecifique() != null ? data.getPrixSpecifique() : 0 ;
						currentGain += localCurrentGain;
					}
				}

				List<Enseigne> enseignes = enseigneRepository.findAllEnseigneByCreatedAtBefore(dateFinNew, false) ;
				if (Utilities.isNotEmpty(enseignes)) {
					currentNbreEnseigne = enseignes.size() ;
				}
				
				List<Enseigne> newEnseignes = enseigneRepository.findAllEnseigneByCreatedAtBeetwen(dateDebutNew, dateFinNew, false) ;
				if (Utilities.isNotEmpty(newEnseignes)) {
					currentNbreNewEnseigne = newEnseignes.size() ;
				}

				List<Enseigne> contratsArreter = enseigneRepository.findByIsActiveAndCreatedAtBeetwen(dateDebutNew, dateFinNew, false, false) ;
				if (Utilities.isNotEmpty(contratsArreter)) {
					currentNbreContratArreter = contratsArreter.size();
				}
				
				System.out.println("dateDebutNew " +  dateDebutNew);
				System.out.println("dateFinNew " +  dateFinNew);


				gain.add(currentGain) ;
				nbreNewEnseigne.add(currentNbreNewEnseigne) ;
				nbreEnseigne.add(currentNbreEnseigne) ;
				nbreContratArreter.add(currentNbreContratArreter) ;
				nbreEnseigneParContrat.add(currentNbreEnseigneParContrat) ;
				
				abscisseGraphe.add(xGraphe) ;
			}
			
			List<Enseigne> itemsEnseigne = enseigneRepository.findByIsLocked(false, false) ;


			// setter les valeurs
			itemDto.setAbscisseGraphe(abscisseGraphe);

			itemDto.setNbreContratArreter(nbreContratArreter);
			itemDto.setNbreEnseigneParContrat(nbreEnseigneParContrat);
			itemDto.setNbreNewEnseigne(nbreNewEnseigne);
			itemDto.setNbreEnseigne(nbreEnseigne);
			itemDto.setGain(gain);
			itemDto.setItemsEnseigne(EnseigneTransformer.INSTANCE.toDtos(itemsEnseigne));
			
			//			itemDto.setInscritParPdfCarte(inscritParPdfCarte);
			//			itemDto.setLegendCaPdfCarte(legendCaPdfCarte);



			response.setItems(Arrays.asList(itemDto));
			response.setHasError(false);

			slf4jLogger.info("----end getStats User-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}








	/**
	 * get full UserDto by using User as object.
	 *
	 * @param entity, locale
	 * @return UserDto
	 *
	 */
	private UserDto getFullInfos(User entity, Integer size, Locale locale) throws Exception {
		UserDto dto = UserTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		Carte dataCarte = carteRepository.findCarteByUserId(dto.getId(), false) ;
		if (dataCarte != null) {
			dto.setDataCarte(CarteTransformer.INSTANCE.toDto(dataCarte));
			dto.setCarteId(dataCarte.getId());
		}
		//dto.setPassword("");
		if (size > 1) {
			return dto;
		}


		// ajout de l'enseigne
		if (entity.getEnseigne() != null ) {
			dto.setDataEnseigne(EnseigneTransformer.INSTANCE.toDto(entity.getEnseigne()));
		}

		List<PointDeVente> datasPointDeVente = null ;
		// renvoi des listes de PointDeVente
		if (dto.getIsAdminEnseigne() != null && dto.getIsAdminEnseigne()) {
			datasPointDeVente = userEnseignePointDeVenteRepository.findByEnseigneId(dto.getEnseigneId(), false) ;
		} else {
			datasPointDeVente = userEnseignePointDeVenteRepository.findAllPointDeVenteByUserEnseigneId(dto.getId(), false) ;
		}
		if (Utilities.isNotEmpty(datasPointDeVente)) {
			dto.setFirstPointDeVenteId(datasPointDeVente.get(0).getId());
			dto.setDatasPointDeVente(PointDeVenteTransformer.INSTANCE.toDtos(datasPointDeVente)) ;
		}

		return dto;
	}
}
