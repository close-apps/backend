


/*
 * Java transformer for entity table souscription_programme_fidelite_tampon
 * Created on 2020-09-17 ( Time 10:11:12 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.enums.TypeActionEnum;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "souscription_programme_fidelite_tampon"
 *
 * @author Back-End developper
 *
 */
@Component
public class SouscriptionProgrammeFideliteTamponBusiness implements IBasicBusiness<Request<SouscriptionProgrammeFideliteTamponDto>, Response<SouscriptionProgrammeFideliteTamponDto>> {

	private Response<SouscriptionProgrammeFideliteTamponDto> response;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private TypeActionRepository typeActionRepository;
	
	@Autowired
	private EnseigneRepository enseigneRepository ;
	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository ;
	@Autowired
	private SouscriptionProgrammeFideliteBusiness souscriptionProgrammeFideliteBusiness ;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private NotePdfBusiness notePdfBusiness;


	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@Autowired
	private ParamsUtils paramsUtils;
	
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public SouscriptionProgrammeFideliteTamponBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create SouscriptionProgrammeFideliteTampon by using SouscriptionProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteTamponDto> create(Request<SouscriptionProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin create SouscriptionProgrammeFideliteTampon-----");

		response = new Response<SouscriptionProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteTampon> items = new ArrayList<SouscriptionProgrammeFideliteTampon>();
			List<SouscriptionProgrammeFideliteDto> datasSouscriptionProgrammeFidelite = new ArrayList<>();
			List<HistoriqueCarteProgrammeFideliteTampon> itemsHistoriqueCarteProgrammeFideliteTampon = new ArrayList<>();

			for (SouscriptionProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("carteId", dto.getCarteId());
				fieldsToVerify.put("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId());
				//fieldsToVerify.put("libelle", dto.getLibelle());


				//fieldsToVerify.put("code", dto.getCode());
				//fieldsToVerify.put("isLocked", dto.getIsLocked());
				//fieldsToVerify.put("rasoinLocked", dto.getRasoinLocked());
				//fieldsToVerify.put("unsubscribe", dto.getUnsubscribe());
				//fieldsToVerify.put("raisonUnsubscribe", dto.getRaisonUnsubscribe());
				//fieldsToVerify.put("nbreTamponActuelle", dto.getNbreTamponActuelle());
				//fieldsToVerify.put("nbreTamponPrecedent", dto.getNbreTamponPrecedent());

				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if souscriptionProgrammeFideliteTampon to insert do not exist
				SouscriptionProgrammeFideliteTampon existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				//				existingEntity = souscriptionProgrammeFideliteTamponRepository.findByCode(dto.getCode(), false);
				//				if (existingEntity != null) {
				//					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getCode(), locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				//				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
				//					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les souscriptionProgrammeFideliteTampons", locale));
				//					response.setHasError(true);
				//					return response;
				//				}

				// Verify if carte exist
				Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
				if (existingCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if programmeFideliteTampon exist
				ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
				if (existingProgrammeFideliteTampon == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
					response.setHasError(true);
					return response;
				}
				
				// verifier l'unicite de la souscription au PDF
				List<SouscriptionProgrammeFideliteTampon> listExistingEntity = souscriptionProgrammeFideliteTamponRepository.findByCarteIdAndProgrammeFideliteTamponId(dto.getCarteId(), dto.getProgrammeFideliteTamponId(), false);
				if (Utilities.isNotEmpty(listExistingEntity)) {
					response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				
				if (Utilities.notBlank(dto.getLibelle())) {
					existingEntity = souscriptionProgrammeFideliteTamponRepository.findByCarteIdAndLibelle(dto.getCarteId(), dto.getLibelle(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les souscriptionProgrammeFideliteTampons", locale));
						response.setHasError(true);
						return response;
					}
				}
		

				
				
				if (!Utilities.notBlank(dto.getLibelle())) {
					dto.setLibelle(existingProgrammeFideliteTampon.getLibelle()) ;
				}
				
				HistoriqueCarteProgrammeFideliteTampon historiqueCarteProgrammeFideliteTampon = new HistoriqueCarteProgrammeFideliteTampon();
				TypeAction typeAction = typeActionRepository.findByCode(TypeActionEnum.SOUSCRIPTION, false) ;


				historiqueCarteProgrammeFideliteTampon.setCarte(existingCarte);
				historiqueCarteProgrammeFideliteTampon.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				if (typeAction != null) {
					historiqueCarteProgrammeFideliteTampon.setTypeAction(typeAction);
				}
				
				
				historiqueCarteProgrammeFideliteTampon.setNbreTamponObtenu(existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() != null ? existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() : 0);
				historiqueCarteProgrammeFideliteTampon.setNbreTamponActuel(existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() != null ? existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() : 0);
				historiqueCarteProgrammeFideliteTampon.setNbreTamponAvantAction(0);

				historiqueCarteProgrammeFideliteTampon.setCreatedBy(request.getUser());
				historiqueCarteProgrammeFideliteTampon.setDateAction(Utilities.getCurrentDate()); // dateSouscription
				historiqueCarteProgrammeFideliteTampon.setCreatedAt(Utilities.getCurrentDate());
				historiqueCarteProgrammeFideliteTampon.setIsDeleted(false);

				// TODO : revoir l'heureAction




				// maj enseigne
				Enseigne enseignePdf = existingProgrammeFideliteTampon.getEnseigne() ;
				
				//enseignePdfCarte.setChiffreDaffaire(enseignePdfCarte.getChiffreDaffaire() != null ? enseignePdfCarte.getChiffreDaffaire() + dto.getChiffreDaffaire() : dto.getChiffreDaffaire());
				//enseignePdf.setNbreTransaction(enseignePdf.getNbreTransaction() != null ? enseignePdf.getNbreTransaction() + 1 : 1);
				enseignePdf.setRecompenseEngagerTampon(enseignePdf.getRecompenseEngagerTampon() != null ? enseignePdf.getRecompenseEngagerTampon() + 1 : 1);	

				enseignePdf.setUpdatedAt(Utilities.getCurrentDate());
						
				enseigneRepository.save(enseignePdf) ;
				
				
				// TODO : des verifications sur la validite du PDF (la souscription de l'enseigne)


				dto.setIsLocked(false);
				dto.setUnsubscribe(false);

				// le code de la souscription est generé


				// TODO : nbre de point initial dependra du PDF
				dto.setNbreTamponActuelle(existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() != null ? existingProgrammeFideliteTampon.getTamponObtenuApresSouscription() : 0);
				dto.setNbreTamponPrecedent(0);

				SouscriptionProgrammeFideliteTampon entityToSave = null;
				entityToSave = SouscriptionProgrammeFideliteTamponTransformer.INSTANCE.toEntity(dto, existingProgrammeFideliteTampon, existingCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setDateSouscription(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
				
				SouscriptionProgrammeFideliteDto dataSouscriptionProgrammeFideliteDto = new SouscriptionProgrammeFideliteDto() ;
				dataSouscriptionProgrammeFideliteDto.setCarteId(dto.getCarteId());
				dataSouscriptionProgrammeFideliteDto.setEnseigneId(existingProgrammeFideliteTampon.getEnseigne().getId());
				dataSouscriptionProgrammeFideliteDto.setProgrammeFideliteTamponId(dto.getProgrammeFideliteTamponId());
				datasSouscriptionProgrammeFidelite.add(dataSouscriptionProgrammeFideliteDto) ;
				
				
				historiqueCarteProgrammeFideliteTampon.setSouscriptionProgrammeFideliteTampon(entityToSave);
				itemsHistoriqueCarteProgrammeFideliteTampon.add(historiqueCarteProgrammeFideliteTampon) ;
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteTampon> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = souscriptionProgrammeFideliteTamponRepository.saveAll((Iterable<SouscriptionProgrammeFideliteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}
				
				// pour faciliter certains get 
				Request<SouscriptionProgrammeFideliteDto> req = new Request<>() ;
				req.setDatas(datasSouscriptionProgrammeFidelite);
				req.setUser(request.getUser());
				Response<SouscriptionProgrammeFideliteDto> res = souscriptionProgrammeFideliteBusiness.create(req, locale) ;
				if (res != null && res.isHasError()) {
					response.setStatus(res.getStatus());
					response.setHasError(true);
					return response;
				}
				
				List<HistoriqueCarteProgrammeFideliteTampon> itemsSavedHistoriqueCarteProgrammeFideliteTampon = null;
				if (!itemsHistoriqueCarteProgrammeFideliteTampon.isEmpty()) {
					// maj les donnees en base
					itemsSavedHistoriqueCarteProgrammeFideliteTampon = historiqueCarteProgrammeFideliteTamponRepository.saveAll((Iterable<HistoriqueCarteProgrammeFideliteTampon>) itemsHistoriqueCarteProgrammeFideliteTampon);
					if (itemsSavedHistoriqueCarteProgrammeFideliteTampon == null || itemsSavedHistoriqueCarteProgrammeFideliteTampon.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("historiqueCarteProgrammeFideliteTampon", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<SouscriptionProgrammeFideliteTamponDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteTamponDto>();
				for (SouscriptionProgrammeFideliteTampon entity : itemsSaved) {
					SouscriptionProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create SouscriptionProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SouscriptionProgrammeFideliteTampon by using SouscriptionProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteTamponDto> update(Request<SouscriptionProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin update SouscriptionProgrammeFideliteTampon-----");

		response = new Response<SouscriptionProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteTampon> items = new ArrayList<SouscriptionProgrammeFideliteTampon>();

			for (SouscriptionProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteTampon existe
				SouscriptionProgrammeFideliteTampon entityToSave = null;
				entityToSave = souscriptionProgrammeFideliteTamponRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if programmeFideliteTampon exist
				if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() > 0){
					ProgrammeFideliteTampon existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
					if (existingProgrammeFideliteTampon == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteTampon -> " + dto.getProgrammeFideliteTamponId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteTampon(existingProgrammeFideliteTampon);
				}
				// Verify if carte exist
				if (dto.getCarteId() != null && dto.getCarteId() > 0){
					Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
					if (existingCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCarte(existingCarte);
				}
				//				if (Utilities.notBlank(dto.getCode())) {
				//					SouscriptionProgrammeFideliteTampon existingEntity = souscriptionProgrammeFideliteTamponRepository.findByCode(dto.getCode(), false);
				//					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
				//						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getCode(), locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
				//						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les souscriptionProgrammeFideliteTampons", locale));
				//						response.setHasError(true);
				//						return response;
				//					}
				//					entityToSave.setCode(dto.getCode());
				//				}

				if (Utilities.notBlank(dto.getLibelle())) {
					SouscriptionProgrammeFideliteTampon existingEntity = souscriptionProgrammeFideliteTamponRepository.findByCarteIdAndLibelle(entityToSave.getCarte().getId(), dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les souscriptionProgrammeFideliteTampons", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (Utilities.notBlank(dto.getRaisonLocked())) {
					entityToSave.setRaisonLocked(dto.getRaisonLocked());
				}
				if (dto.getIsNotifed() != null) {
					entityToSave.setIsNotifed(dto.getIsNotifed());
				}
				if (Utilities.notBlank(dto.getMessageNotifed())) {
					entityToSave.setMessageNotifed(dto.getMessageNotifed());
				}
				if (dto.getUnsubscribe() != null) {
					entityToSave.setUnsubscribe(dto.getUnsubscribe());
				}
				if (dto.getIsShowLastNotification() != null) {
					entityToSave.setIsShowLastNotification(dto.getIsShowLastNotification());
				}
				if (Utilities.notBlank(dto.getRaisonUnsubscribe())) {
					entityToSave.setRaisonUnsubscribe(dto.getRaisonUnsubscribe());
				}
				if (dto.getNbreTamponActuelle() != null && dto.getNbreTamponActuelle() > 0) {
					entityToSave.setNbreTamponActuelle(dto.getNbreTamponActuelle());
				}
				if (dto.getNbreTamponPrecedent() != null && dto.getNbreTamponPrecedent() > 0) {
					entityToSave.setNbreTamponPrecedent(dto.getNbreTamponPrecedent());
				}
				//				if (Utilities.notBlank(dto.getDateSouscription())) {
				//					entityToSave.setDateSouscription(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSouscription()));
				//				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SouscriptionProgrammeFideliteTampon> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = souscriptionProgrammeFideliteTamponRepository.saveAll((Iterable<SouscriptionProgrammeFideliteTampon>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("souscriptionProgrammeFideliteTampon", locale));
					response.setHasError(true);
					return response;
				}
				List<SouscriptionProgrammeFideliteTamponDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteTamponDto>();
				for (SouscriptionProgrammeFideliteTampon entity : itemsSaved) {
					SouscriptionProgrammeFideliteTamponDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update SouscriptionProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete SouscriptionProgrammeFideliteTampon by using SouscriptionProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscriptionProgrammeFideliteTamponDto> delete(Request<SouscriptionProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete SouscriptionProgrammeFideliteTampon-----");

		response = new Response<SouscriptionProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteTampon> items = new ArrayList<SouscriptionProgrammeFideliteTampon>();

			for (SouscriptionProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteTampon existe
				SouscriptionProgrammeFideliteTampon existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findBySouscriptionProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findBySouscriptionProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteTamponRepository.saveAll((Iterable<SouscriptionProgrammeFideliteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete SouscriptionProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete SouscriptionProgrammeFideliteTampon by using SouscriptionProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<SouscriptionProgrammeFideliteTamponDto> forceDelete(Request<SouscriptionProgrammeFideliteTamponDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete SouscriptionProgrammeFideliteTampon-----");

		response = new Response<SouscriptionProgrammeFideliteTamponDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<SouscriptionProgrammeFideliteTampon> items = new ArrayList<SouscriptionProgrammeFideliteTampon>();

			for (SouscriptionProgrammeFideliteTamponDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la souscriptionProgrammeFideliteTampon existe
				SouscriptionProgrammeFideliteTampon existingEntity = null;
				existingEntity = souscriptionProgrammeFideliteTamponRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscriptionProgrammeFideliteTampon -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findBySouscriptionProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findBySouscriptionProgrammeFideliteTamponId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscriptionProgrammeFideliteTamponRepository.saveAll((Iterable<SouscriptionProgrammeFideliteTampon>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete SouscriptionProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get SouscriptionProgrammeFideliteTampon by using SouscriptionProgrammeFideliteTamponDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SouscriptionProgrammeFideliteTamponDto> getByCriteria(Request<SouscriptionProgrammeFideliteTamponDto> request, Locale locale) {
		slf4jLogger.info("----begin get SouscriptionProgrammeFideliteTampon-----");

		response = new Response<SouscriptionProgrammeFideliteTamponDto>();

		try {
			List<SouscriptionProgrammeFideliteTampon> items = null;
			items = souscriptionProgrammeFideliteTamponRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<SouscriptionProgrammeFideliteTamponDto> itemsDto = new ArrayList<SouscriptionProgrammeFideliteTamponDto>();
				for (SouscriptionProgrammeFideliteTampon entity : items) {
					SouscriptionProgrammeFideliteTamponDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(souscriptionProgrammeFideliteTamponRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("souscriptionProgrammeFideliteTampon", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get SouscriptionProgrammeFideliteTampon-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SouscriptionProgrammeFideliteTamponDto by using SouscriptionProgrammeFideliteTampon as object.
	 *
	 * @param entity, locale
	 * @return SouscriptionProgrammeFideliteTamponDto
	 *
	 */
	private SouscriptionProgrammeFideliteTamponDto getFullInfos(SouscriptionProgrammeFideliteTampon entity, Integer size, Locale locale) throws Exception {
		SouscriptionProgrammeFideliteTamponDto dto = SouscriptionProgrammeFideliteTamponTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		
		if (entity.getProgrammeFideliteTampon() != null && entity.getProgrammeFideliteTampon().getUrlImage() != null) {
			dto.setUrlLogo(paramsUtils.getBaseUrlImageApp() + entity.getProgrammeFideliteTampon().getUrlImage());
		}
		if (size > 1) {
			return dto;
		}

		// info carte
		Carte carte = carteRepository.findById(dto.getCarteId(), false) ;
		if (carte != null) {
			dto.setDataCarte(CarteTransformer.INSTANCE.toDto(carte));
		}
		// info programmeFideliteCarte
		ProgrammeFideliteTampon programmeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false) ;
		if (programmeFideliteTampon != null) {
			dto.setDataProgrammeFideliteTampon(ProgrammeFideliteTamponTransformer.INSTANCE.toDto(programmeFideliteTampon));
		}

		return dto;
	}
}
