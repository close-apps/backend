


/*
 * Java transformer for entity table information_regle_securite_carte
 * Created on 2020-08-17 ( Time 14:14:46 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "information_regle_securite_carte"
 *
 * @author Back-End developper
 *
 */
@Component
public class InformationRegleSecuriteCarteBusiness implements IBasicBusiness<Request<InformationRegleSecuriteCarteDto>, Response<InformationRegleSecuriteCarteDto>> {

	private Response<InformationRegleSecuriteCarteDto> response;
	@Autowired
	private InformationRegleSecuriteCarteRepository informationRegleSecuriteCarteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private DelitProgrammeFideliteRepository delitProgrammeFideliteRepository;
	@Autowired
	private UniteTempsRepository uniteTempsRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private DelitProgrammeFideliteBusiness delitProgrammeFideliteBusiness;



	public InformationRegleSecuriteCarteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create InformationRegleSecuriteCarte by using InformationRegleSecuriteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteCarteDto> create(Request<InformationRegleSecuriteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create InformationRegleSecuriteCarte-----");

		response = new Response<InformationRegleSecuriteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}



			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteCarte> items = new ArrayList<InformationRegleSecuriteCarte>();

			for (InformationRegleSecuriteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				//fieldsToVerify.put("plafondPoint", dto.getPlafondPoint());
				//fieldsToVerify.put("temps", dto.getTemps());
				//fieldsToVerify.put("nbreFoisCredite", dto.getNbreFoisCredite());
				//fieldsToVerify.put("tempsNbreFoisCredite", dto.getTempsNbreFoisCredite());
				//fieldsToVerify.put("nbreFoisDebiter", dto.getNbreFoisDebiter());
				fieldsToVerify.put("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId());
				fieldsToVerify.put("uniteTempsId", dto.getUniteTempsId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if informationRegleSecuriteCarte to insert do not exist
				InformationRegleSecuriteCarte existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("informationRegleSecuriteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				
				// Verify if programmeFideliteCarte exist
				ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
				if (existingProgrammeFideliteCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if uniteTemps exist
				UniteTemps existingUniteTemps = uniteTempsRepository.findById(dto.getUniteTempsId(), false);
				if (existingUniteTemps == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("uniteTemps -> " + dto.getUniteTempsId(), locale));
					response.setHasError(true);
					return response;
				}


				InformationRegleSecuriteCarte entityToSave = null;
				entityToSave = InformationRegleSecuriteCarteTransformer.INSTANCE.toEntity(dto, existingProgrammeFideliteCarte, existingUniteTemps);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<InformationRegleSecuriteCarte> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = informationRegleSecuriteCarteRepository.saveAll((Iterable<InformationRegleSecuriteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<InformationRegleSecuriteCarteDto> itemsDto = new ArrayList<InformationRegleSecuriteCarteDto>();
				for (InformationRegleSecuriteCarte entity : itemsSaved) {
					InformationRegleSecuriteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create InformationRegleSecuriteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update InformationRegleSecuriteCarte by using InformationRegleSecuriteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteCarteDto> update(Request<InformationRegleSecuriteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update InformationRegleSecuriteCarte-----");

		response = new Response<InformationRegleSecuriteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteCarte> items = new ArrayList<InformationRegleSecuriteCarte>();

			for (InformationRegleSecuriteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteCarte existe
				InformationRegleSecuriteCarte entityToSave = null;
				entityToSave = informationRegleSecuriteCarteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				
				// Verify if programmeFideliteCarte exist
				if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() > 0){
					ProgrammeFideliteCarte existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
					if (existingProgrammeFideliteCarte == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("programmeFideliteCarte -> " + dto.getProgrammeFideliteCarteId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProgrammeFideliteCarte(existingProgrammeFideliteCarte);
				}
				// Verify if uniteTemps exist
				if (dto.getUniteTempsId() != null && dto.getUniteTempsId() > 0){
					UniteTemps existingUniteTemps = uniteTempsRepository.findById(dto.getUniteTempsId(), false);
					if (existingUniteTemps == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("uniteTemps -> " + dto.getUniteTempsId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUniteTemps(existingUniteTemps);
				}
				if (dto.getPlafondSommeACrediter() != null && dto.getPlafondSommeACrediter() > 0) {
					entityToSave.setPlafondSommeACrediter(dto.getPlafondSommeACrediter());
				}
				if (dto.getNbreFoisCrediterAllPdv() != null && dto.getNbreFoisCrediterAllPdv() > 0) {
					entityToSave.setNbreFoisCrediterAllPdv(dto.getNbreFoisCrediterAllPdv());
				}
				if (dto.getNbreFoisCrediterParPdv() != null && dto.getNbreFoisCrediterParPdv() > 0) {
					entityToSave.setNbreFoisCrediterParPdv(dto.getNbreFoisCrediterParPdv());
				}
				

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<InformationRegleSecuriteCarte> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = informationRegleSecuriteCarteRepository.saveAll((Iterable<InformationRegleSecuriteCarte>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("informationRegleSecuriteCarte", locale));
					response.setHasError(true);
					return response;
				}
				List<InformationRegleSecuriteCarteDto> itemsDto = new ArrayList<InformationRegleSecuriteCarteDto>();
				for (InformationRegleSecuriteCarte entity : itemsSaved) {
					InformationRegleSecuriteCarteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update InformationRegleSecuriteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete InformationRegleSecuriteCarte by using InformationRegleSecuriteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InformationRegleSecuriteCarteDto> delete(Request<InformationRegleSecuriteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete InformationRegleSecuriteCarte-----");

		response = new Response<InformationRegleSecuriteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteCarte> items = new ArrayList<InformationRegleSecuriteCarte>();

			for (InformationRegleSecuriteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteCarte existe
				InformationRegleSecuriteCarte existingEntity = null;
				existingEntity = informationRegleSecuriteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFideliteCarte = delitProgrammeFideliteRepository.findByInformationRegleSecuriteCarteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFideliteCarte != null && !listOfDelitProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfDelitProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				informationRegleSecuriteCarteRepository.saveAll((Iterable<InformationRegleSecuriteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete InformationRegleSecuriteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete InformationRegleSecuriteCarte by using InformationRegleSecuriteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<InformationRegleSecuriteCarteDto> forceDelete(Request<InformationRegleSecuriteCarteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete InformationRegleSecuriteCarte-----");

		response = new Response<InformationRegleSecuriteCarteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<InformationRegleSecuriteCarte> items = new ArrayList<InformationRegleSecuriteCarte>();

			for (InformationRegleSecuriteCarteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la informationRegleSecuriteCarte existe
				InformationRegleSecuriteCarte existingEntity = null;
				existingEntity = informationRegleSecuriteCarteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteCarte -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// delitProgrammeFidelite
				List<DelitProgrammeFidelite> listOfDelitProgrammeFidelite = delitProgrammeFideliteRepository.findByInformationRegleSecuriteCarteId(existingEntity.getId(), false);
				if (listOfDelitProgrammeFidelite != null && !listOfDelitProgrammeFidelite.isEmpty()){
					Request<DelitProgrammeFideliteDto> deleteRequest = new Request<DelitProgrammeFideliteDto>();
					deleteRequest.setDatas(DelitProgrammeFideliteTransformer.INSTANCE.toDtos(listOfDelitProgrammeFidelite));
					deleteRequest.setUser(request.getUser());
					Response<DelitProgrammeFideliteDto> deleteResponse = delitProgrammeFideliteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				informationRegleSecuriteCarteRepository.saveAll((Iterable<InformationRegleSecuriteCarte>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete InformationRegleSecuriteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get InformationRegleSecuriteCarte by using InformationRegleSecuriteCarteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<InformationRegleSecuriteCarteDto> getByCriteria(Request<InformationRegleSecuriteCarteDto> request, Locale locale) {
		slf4jLogger.info("----begin get InformationRegleSecuriteCarte-----");

		response = new Response<InformationRegleSecuriteCarteDto>();

		try {
			List<InformationRegleSecuriteCarte> items = null;
			items = informationRegleSecuriteCarteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<InformationRegleSecuriteCarteDto> itemsDto = new ArrayList<InformationRegleSecuriteCarteDto>();
				for (InformationRegleSecuriteCarte entity : items) {
					InformationRegleSecuriteCarteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(informationRegleSecuriteCarteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("informationRegleSecuriteCarte", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get InformationRegleSecuriteCarte-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full InformationRegleSecuriteCarteDto by using InformationRegleSecuriteCarte as object.
	 *
	 * @param entity, locale
	 * @return InformationRegleSecuriteCarteDto
	 *
	 */
	private InformationRegleSecuriteCarteDto getFullInfos(InformationRegleSecuriteCarte entity, Integer size, Locale locale) throws Exception {
		InformationRegleSecuriteCarteDto dto = InformationRegleSecuriteCarteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
