


/*
 * Java transformer for entity table enseigne
 * Created on 2020-09-01 ( Time 02:39:21 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import com.closeapps.dao.entity.Enseigne;
import com.closeapps.dao.entity.EtatInscripition;
import com.closeapps.dao.entity.EtatInscripitionEnseigne;
import com.closeapps.dao.entity.EtatSouscription;
import com.closeapps.dao.entity.HistoriqueCarteProgrammeFideliteCarte;
import com.closeapps.dao.entity.HistoriqueCarteProgrammeFideliteTampon;
import com.closeapps.dao.entity.HistoriqueSouscriptionEnseigne;
import com.closeapps.dao.entity.ModePaiement;
import com.closeapps.dao.entity.NetPromoterScorUser;
import com.closeapps.dao.entity.NotePdf;
import com.closeapps.dao.entity.PointDeVente;
import com.closeapps.dao.entity.Produit;
import com.closeapps.dao.entity.ProgrammeFideliteCarte;
import com.closeapps.dao.entity.ProgrammeFideliteTampon;
import com.closeapps.dao.entity.Role;
import com.closeapps.dao.entity.SecteurActivite;
import com.closeapps.dao.entity.SecteurActiviteEnseigne;
import com.closeapps.dao.entity.Souscription;
import com.closeapps.dao.entity.SouscriptionProgrammeFideliteCarte;
import com.closeapps.dao.entity.SouscriptionProgrammeFideliteTampon;
import com.closeapps.dao.entity.User;
import com.closeapps.dao.repository.CarteRepository;
import com.closeapps.dao.repository.EnseigneRepository;
import com.closeapps.dao.repository.EtatInscripitionEnseigneRepository;
import com.closeapps.dao.repository.EtatInscripitionRepository;
import com.closeapps.dao.repository.EtatSouscriptionRepository;
import com.closeapps.dao.repository.HistoriqueCarteProgrammeFideliteCarteRepository;
import com.closeapps.dao.repository.HistoriqueCarteProgrammeFideliteTamponRepository;
import com.closeapps.dao.repository.HistoriqueSouscriptionEnseigneRepository;
import com.closeapps.dao.repository.ModePaiementRepository;
import com.closeapps.dao.repository.NetPromoterScorUserRepository;
import com.closeapps.dao.repository.NotePdfRepository;
import com.closeapps.dao.repository.PointDeVenteRepository;
import com.closeapps.dao.repository.ProduitRepository;
import com.closeapps.dao.repository.ProgrammeFideliteCarteRepository;
import com.closeapps.dao.repository.ProgrammeFideliteTamponRepository;
import com.closeapps.dao.repository.RoleRepository;
import com.closeapps.dao.repository.SecteurActiviteEnseigneRepository;
import com.closeapps.dao.repository.SecteurActiviteRepository;
import com.closeapps.dao.repository.SouscriptionProgrammeFideliteCarteRepository;
import com.closeapps.dao.repository.SouscriptionProgrammeFideliteRepository;
import com.closeapps.dao.repository.SouscriptionProgrammeFideliteTamponRepository;
import com.closeapps.dao.repository.SouscriptionRepository;
import com.closeapps.dao.repository.UserRepository;
import com.closeapps.helper.ExceptionUtils;
import com.closeapps.helper.FunctionalError;
import com.closeapps.helper.HostingUtils;
import com.closeapps.helper.ParamsUtils;
import com.closeapps.helper.TechnicalError;
import com.closeapps.helper.Utilities;
import com.closeapps.helper.Validate;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.dto.EnseigneDto;
import com.closeapps.helper.dto.EtatInscripitionEnseigneDto;
import com.closeapps.helper.dto.HistoriqueSouscriptionEnseigneDto;
import com.closeapps.helper.dto.NetPromoterScorUserDto;
import com.closeapps.helper.dto.NotePdfDto;
import com.closeapps.helper.dto.PointDeVenteDto;
import com.closeapps.helper.dto.ProduitDto;
import com.closeapps.helper.dto.ProgrammeFideliteCarteDto;
import com.closeapps.helper.dto.ProgrammeFideliteTamponDto;
import com.closeapps.helper.dto.SecteurActiviteDto;
import com.closeapps.helper.dto.SecteurActiviteEnseigneDto;
import com.closeapps.helper.dto.UserDto;
import com.closeapps.helper.dto.customize._GraphCirculaireDto;
import com.closeapps.helper.dto.customize._Image;
import com.closeapps.helper.dto.customize._RetourGroupByDto;
import com.closeapps.helper.dto.transformer.EnseigneTransformer;
import com.closeapps.helper.dto.transformer.EtatInscripitionEnseigneTransformer;
import com.closeapps.helper.dto.transformer.HistoriqueSouscriptionEnseigneTransformer;
import com.closeapps.helper.dto.transformer.NetPromoterScorUserTransformer;
import com.closeapps.helper.dto.transformer.NotePdfTransformer;
import com.closeapps.helper.dto.transformer.PointDeVenteTransformer;
import com.closeapps.helper.dto.transformer.ProduitTransformer;
import com.closeapps.helper.dto.transformer.ProgrammeFideliteCarteTransformer;
import com.closeapps.helper.dto.transformer.ProgrammeFideliteTamponTransformer;
import com.closeapps.helper.dto.transformer.SecteurActiviteEnseigneTransformer;
import com.closeapps.helper.dto.transformer.SecteurActiviteTransformer;
import com.closeapps.helper.dto.transformer.UserTransformer;
import com.closeapps.helper.enums.EmailEnum;
import com.closeapps.helper.enums.EtatEnum;
import com.closeapps.helper.enums.GlobalEnum;
import com.closeapps.helper.enums.ModePaiementEnum;
import com.closeapps.helper.enums.RoleEnum;
import com.closeapps.helper.enums.TypeActionEnum;
import com.closeapps.helper.enums.TypeNoteNpsEnum;
import com.closeapps.helper.enums.UniteTempsEnum;


/**
BUSINESS for table "enseigne"
 *
 * @author Back-End developper
 *
 */
@Component
@EnableAsync // pour activer le mode asynchrone
public class EnseigneBusiness implements IBasicBusiness<Request<EnseigneDto>, Response<EnseigneDto>> {

	private Response<EnseigneDto> response;
	@Autowired
	private EnseigneRepository enseigneRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private CarteRepository carteRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private ProgrammeFideliteTamponRepository programmeFideliteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteRepository souscriptionProgrammeFideliteRepository;
	@Autowired
	private ProgrammeFideliteCarteRepository programmeFideliteCarteRepository;
	@Autowired
	private HistoriqueSouscriptionEnseigneRepository historiqueSouscriptionEnseigneRepository;
	@Autowired
	private EtatInscripitionEnseigneRepository etatInscripitionEnseigneRepository;
	@Autowired
	private SecteurActiviteEnseigneRepository secteurActiviteEnseigneRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private EtatInscripitionRepository etatInscripitionRepository;
	@Autowired
	private SecteurActiviteRepository secteurActiviteRepository;
	@Autowired
	private NetPromoterScorUserRepository netPromoterScorUserRepository;
	@Autowired
	private NotePdfRepository notePdfRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteCarteRepository historiqueCarteProgrammeFideliteCarteRepository;
	@Autowired
	private HistoriqueCarteProgrammeFideliteTamponRepository historiqueCarteProgrammeFideliteTamponRepository;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private SouscriptionRepository souscriptionRepository;

	@Autowired
	private ModePaiementRepository modePaiementRepository;

	@Autowired
	private EtatSouscriptionRepository etatSouscriptionRepository;




	@Autowired
	private NetPromoterScorUserBusiness netPromoterScorUserBusiness;
	@Autowired
	private NotePdfBusiness notePdfBusiness;







	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private UserBusiness userBusiness;


	@Autowired
	private CarteBusiness carteBusiness;


	@Autowired
	private ProduitBusiness produitBusiness;


	@Autowired
	private ProgrammeFideliteTamponBusiness programmeFideliteTamponBusiness;


	@Autowired
	private ProgrammeFideliteCarteBusiness programmeFideliteCarteBusiness;


	@Autowired
	private HistoriqueSouscriptionEnseigneBusiness historiqueSouscriptionEnseigneBusiness;


	@Autowired
	private EtatInscripitionEnseigneBusiness etatInscripitionEnseigneBusiness;


	@Autowired
	private SecteurActiviteEnseigneBusiness secteurActiviteEnseigneBusiness;


	@Autowired
	private PointDeVenteBusiness pointDeVenteBusiness;

	@Autowired
	private ParamsUtils paramsUtils;

	private Context context;

	@Autowired
	private HostingUtils hostingUtils;

	private SimpleDateFormat dateFormatNew;



	public EnseigneBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormatNew = new SimpleDateFormat("dd/MM/yyyy");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EnseigneDto> create(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin create Enseigne-----");

		response = new Response<EnseigneDto>();
		List<String> listOfFilesCreate = new 	ArrayList<>();

		try {
			//    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//    fieldsToVerifyUser.put("user", request.getUser());
			//    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//      response.setHasError(true);
			//      return response;
			//    }
			//
			//    User utilisateur = userRepository.findById(request.getUser(), false);
			//    if (utilisateur == null) {
			//      response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
			//      response.setHasError(true);
			//      return response;
			//    }
			//    if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
			//      response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			//      response.setHasError(true);
			//      return response;
			//    }

			List<Enseigne> items = new ArrayList<>();
			List<User> itemsUserEnseigne = new ArrayList<>();
			List<EtatInscripitionEnseigne> itemsEtatInscripitionEnseigne = new ArrayList<>();
			List<SecteurActiviteEnseigne> itemsSecteurActiviteEnseigne = new ArrayList<>();



			for (EnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("nomAdmin", dto.getNomAdmin());
				fieldsToVerify.put("prenomsAdmin", dto.getPrenomsAdmin());
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("telephone", dto.getTelephone());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("roleGerantId", dto.getRoleGerantId());
				//fieldsToVerify.put("description", dto.getDescription());
				fieldsToVerify.put("datasSecteurActivite", dto.getDatasSecteurActivite());



				//        fieldsToVerify.put("urlLogo", dto.getUrlLogo());
				//        fieldsToVerify.put("facebook", dto.getFacebook());
				//        fieldsToVerify.put("instagram", dto.getInstagram());
				//        fieldsToVerify.put("twitter", dto.getTwitter());
				//        fieldsToVerify.put("couleur", dto.getCouleur());
				//        fieldsToVerify.put("isLocked", dto.getIsLocked());


				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
				if (!(dto.getDatasSecteurActivite().size() > 0)) {
					response.setStatus(functionalError.FIELD_EMPTY("datasSecteurActivite", locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if enseigne to insert do not exist
				Enseigne existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				// unicite du nom
				existingEntity = enseigneRepository.findEnseigneByNom(dto.getNom(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getNom(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les enseignes", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity = enseigneRepository.findByTelephone(dto.getTelephone(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getTelephone(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les enseignes", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = enseigneRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les enseignes", locale));
					response.setHasError(true);
					return response;
				}

				// verification de l'unicite du telephone et de l'email dans la table userEnseigne
				User existingUserEnseigne = null ;
				existingUserEnseigne = userRepository.findByTelephoneAndEnseigneIsNotNull(dto.getTelephone(), false);
				if (existingUserEnseigne != null) {
					response.setStatus(functionalError.DATA_EXIST("userEnseigne -> " + dto.getTelephone(), locale));
					response.setHasError(true);
					return response;
				}
				if (itemsUserEnseigne.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les userEnseignes", locale));
					response.setHasError(true);
					return response;
				}
				existingUserEnseigne = userRepository.findByEmailAndEnseigneIsNotNull(dto.getEmail(), false);
				if (existingUserEnseigne != null) {
					response.setStatus(functionalError.DATA_EXIST("userEnseigne -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (itemsUserEnseigne.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les userEnseignes", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if role exist
				Role existingRole = roleRepository.findById(dto.getRoleGerantId(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleGerantId(), locale));
					response.setHasError(true);
					return response;
				}

				// setter les informations concernant l'admin
				dto.setEmailAdmin(dto.getEmail());
				dto.setTelephoneAdmin(dto.getTelephone());



				// enregistrement du logo
				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();

					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);
					if(image2 != null){

						cheminFile =  Utilities.saveFile(image2, paramsUtils);
						//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
						if(cheminFile==null){
							response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
							response.setHasError(true);
							return response;
						}
						//dto.setUrlCarte(cheminFile.split("."+image2.getExtension())[0]);
						dto.setUrlLogo(cheminFile);
						listOfFilesCreate.add(cheminFile);
					}
					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}

				EtatInscripition existingEtatInscripition = etatInscripitionRepository.findByCode(EtatEnum.ENTRANT,
						false);


				// Verify if souscription exist
				Souscription existingSouscription = null;

				// Verify if modePaiement exist
				ModePaiement existingModePaiement = null;

				// Verify if etatSouscription exist
				EtatSouscription existingEtatSouscription = null;

				dto.setIsLocked(true); // car l'inscription va suivre pluiseurs process      
				dto.setIsAccepted(false); // car l'inscription va suivre pluiseurs process      
				dto.setIsActive(false); // pour la souscription     
				dto.setNbreUtilisateurMobile(0); // pour la souscription     
				dto.setNbreMaxiUserSouscription(0); // pour la souscription     
				Enseigne entityToSave = null;
				entityToSave = EnseigneTransformer.INSTANCE.toEntity(dto, existingRole, existingEtatSouscription, existingSouscription, existingModePaiement);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);


				// maj de l'etatInscription de l'enseigne
				EtatInscripitionEnseigne existingEtatInscripitionEnseigne = new EtatInscripitionEnseigne() ;
				existingEtatInscripitionEnseigne.setEnseigne(entityToSave);
				existingEtatInscripitionEnseigne.setEtatInscripition(existingEtatInscripition);
				existingEtatInscripitionEnseigne.setIsValid(true);
				existingEtatInscripitionEnseigne.setCreatedBy(request.getUser());
				existingEtatInscripitionEnseigne.setCreatedAt(Utilities.getCurrentDate());
				existingEtatInscripitionEnseigne.setIsDeleted(false);
				itemsEtatInscripitionEnseigne.add(existingEtatInscripitionEnseigne);


				// maj des secteurDactivite de l'enseigne

				for (SecteurActiviteDto data : dto.getDatasSecteurActivite()) {

					SecteurActivite existingSecteurActivite = secteurActiviteRepository.findById(data.getId(), false);
					//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
					if (existingSecteurActivite == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("secteurActivite -> " + data.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					SecteurActiviteEnseigne secteurActiviteEnseigne = new SecteurActiviteEnseigne() ;
					secteurActiviteEnseigne.setSecteurActivite(existingSecteurActivite);
					secteurActiviteEnseigne.setEnseigne(entityToSave);
					secteurActiviteEnseigne.setCreatedBy(request.getUser());
					secteurActiviteEnseigne.setCreatedAt(Utilities.getCurrentDate());
					secteurActiviteEnseigne.setIsDeleted(false);

					itemsSecteurActiviteEnseigne.add(secteurActiviteEnseigne) ;

				}


			}

			if (!items.isEmpty()) {
				List<Enseigne> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = enseigneRepository.saveAll((Iterable<Enseigne>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("enseigne", locale));
					response.setHasError(true);
					return response;
				}

				// save etatInscription enseigne
				if (!itemsEtatInscripitionEnseigne.isEmpty()) {
					List<EtatInscripitionEnseigne> itemsEtatInscripitionEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsEtatInscripitionEnseigneSaved = etatInscripitionEnseigneRepository.saveAll((Iterable<EtatInscripitionEnseigne>) itemsEtatInscripitionEnseigne);
					if (itemsEtatInscripitionEnseigneSaved == null || itemsEtatInscripitionEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("etatInscripitionEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}

				// save secteurActivite enseigne
				if (!itemsSecteurActiviteEnseigne.isEmpty()) {
					List<SecteurActiviteEnseigne> itemsSecteurActiviteEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsSecteurActiviteEnseigneSaved = secteurActiviteEnseigneRepository.saveAll((Iterable<SecteurActiviteEnseigne>) itemsSecteurActiviteEnseigne);
					if (itemsSecteurActiviteEnseigneSaved == null || itemsSecteurActiviteEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("secteurActiviteEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}
				List<EnseigneDto> itemsDto = new ArrayList<EnseigneDto>();
				for (Enseigne entity : itemsSaved) {
					EnseigneDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);


				// envoi de mail pour la demande d'inscription de l'entreprise

				// envoi de mail 
				if (Utilities.isNotEmpty(itemsSaved)) {

					//set mail to user
					Map<String, String> from=new HashMap<>();
					from.put("email", paramsUtils.getSmtpMailUsername());
					from.put("user" ,  paramsUtils.getSmtpMailUser());

					// choisir la vraie url
					String appLink = "";
					//			if (entityToSave.getAdmin() != null ) {
					//				appLink = paramsUtils.getUrlRootAdmin();
					//			}else {
					//				appLink = paramsUtils.getUrlRootUser();
					//			}

					appLink = paramsUtils.getUrlRootUser();

					for (Enseigne data : itemsSaved) {
						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email",  data.getEmail());
						recipient.put("user",  data.getNom());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";
						String subject = EmailEnum.SUBJECT_DEMANDE_CREATION_COMPTE;
						//String body = EmailEnum.BODY_PASSWORD_RESET;
						String body = "";
						context = new Context();
						//String template = paramsUtils.getTemplateCreationCompteParUser();
						String template = paramsUtils.getTemplateNousContacterRetourParDefaut();

						//context.setVariable("email", user.getEmail());
						//context.setVariable("login", user.getLogin());
						//context.setVariable("token", user.getToken());
						//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));


						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}

						context.setVariable(EmailEnum.nomEntreprise, data.getNom());
						//context.setVariable(EmailEnum.nomUser, user.getNom());
						//context.setVariable(EmailEnum.nomUser, (Utilities.notBlank(data.getPrenoms()) ? user.getPrenoms() : user.getLogin()));
						//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable(EmailEnum.date, dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
						// si jamais une exception c'est produite pour un mail

					}
				}


			}
			slf4jLogger.info("----end create Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EnseigneDto> update(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin update Enseigne-----");

		response = new Response<EnseigneDto>();
		List<String> listOfFilesCreate = new 	ArrayList<>();
		List<String> listOfOldFiles = new 	ArrayList<>();

		//Map<String, String>  oldEtatInscriptionCode = new  HashMap<String, String>();
		Map<Integer, Map<String, String>>  etatInscriptionCodex =  new HashMap<Integer, Map<String, String>>() ;
		Map<Integer, String>  mapEtatInscriptionCodeEnseigne =  new HashMap<Integer, String>() ;

		try {

			if (request.getUser() != null && request.getUser() > 0 ) {

				User utilisateur = userRepository.findById(request.getUser(), false);
				//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}

				// if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
				// && !utilisateur.getEnseigne().getIsActive()) {
				// 	response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!"+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				// 	response.setHasError(true);
				// 	return response;
				// }
			}


			List<Enseigne> items = new ArrayList<Enseigne>();
			List<EtatInscripitionEnseigne> itemsEtatInscripitionEnseigne = new ArrayList<EtatInscripitionEnseigne>();
			List<EtatInscripitionEnseigne> itemsDeleteEtatInscripitionEnseigne = new ArrayList<EtatInscripitionEnseigne>();
			List<SecteurActiviteEnseigne> itemsSecteurActiviteEnseigne = new ArrayList<SecteurActiviteEnseigne>();
			List<SecteurActiviteEnseigne> itemsDeleteSecteurActiviteEnseigne = new ArrayList<SecteurActiviteEnseigne>();


			for (EnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la enseigne existe
				Enseigne entityToSave = null;
				entityToSave = enseigneRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				// Verify if role exist
				if (dto.getRoleGerantId() != null && dto.getRoleGerantId() > 0){
					Role existingRole = roleRepository.findById(dto.getRoleGerantId(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role -> " + dto.getRoleGerantId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setRole(existingRole);
				}
				if (Utilities.notBlank(dto.getNom())) {

					Enseigne existingEntity = enseigneRepository.findEnseigneByNom(dto.getNom(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getNom(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getNom().equalsIgnoreCase(dto.getNom()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du nom '" + dto.getNom()+"' pour les enseignes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNom(dto.getNom());
				}

				if (Utilities.notBlank(dto.getTelephone())) {
					Enseigne existingEntity = enseigneRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone()+"' pour les enseignes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setTelephone(dto.getTelephone());
				}
				if (Utilities.notBlank(dto.getEmail())) {
					Enseigne existingEntity = enseigneRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("enseigne -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail()+"' pour les enseignes", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEmail(dto.getEmail());
				}

				if (Utilities.notBlank(dto.getEtatInscripitionCode())) {

					EtatInscripition existingEtatInscripition = etatInscripitionRepository.findByCode(dto.getEtatInscripitionCode(), false);

					if (existingEtatInscripition != null) {

						String oldEtatInscriptionCode  = "";
						//Map<String, String>  oldEtatInscriptionCode = new  HashMap<String, String>();


						List<EtatInscripitionEnseigne> etatInscripitionEnseignesValid = etatInscripitionEnseigneRepository.findAllByEtatInscripitionEnseigneByEnseigneIdAndIsValid(entityToSaveId, true, false) ;

						if (Utilities.isNotEmpty(etatInscripitionEnseignesValid)) {

							oldEtatInscriptionCode = etatInscripitionEnseignesValid.get(0).getEtatInscripition().getCode() ;

							for (EtatInscripitionEnseigne item : etatInscripitionEnseignesValid) {
								item.setDeletedBy(request.getUser());
								item.setDeletedAt(Utilities.getCurrentDate());
								item.setIsValid(false);
								item.setIsDeleted(true);

							}

							itemsDeleteEtatInscripitionEnseigne.addAll(etatInscripitionEnseignesValid) ;
						}
						EtatInscripitionEnseigne existingEtatInscripitionEnseigne = new EtatInscripitionEnseigne() ;
						existingEtatInscripitionEnseigne.setEnseigne(entityToSave);
						existingEtatInscripitionEnseigne.setEtatInscripition(existingEtatInscripition);
						existingEtatInscripitionEnseigne.setCreatedBy(request.getUser());
						existingEtatInscripitionEnseigne.setCreatedAt(Utilities.getCurrentDate());
						existingEtatInscripitionEnseigne.setIsValid(true);
						existingEtatInscripitionEnseigne.setIsDeleted(false);

						itemsEtatInscripitionEnseigne.add(existingEtatInscripitionEnseigne) ;

						if (Utilities.notBlank(oldEtatInscriptionCode) && !oldEtatInscriptionCode.equals(dto.getEtatInscripitionCode()) ) {
							mapEtatInscriptionCodeEnseigne.put(entityToSaveId, dto.getEtatInscripitionCode()) ;
						}

						//oldEtatInscriptionCode.put((Utilities.notBlank(ancienCode) ? ancienCode : dto.getEtatInscripitionCode() ), dto.getEtatInscripitionCode()) ;
						if (existingEtatInscripition.getCode().equals(EtatEnum.ACCEPTER)) {
							entityToSave.setIsLocked(false);
							entityToSave.setIsAccepted(true);
						}
					}
				}


				// gestion secteurActivite
				if (Utilities.isNotEmpty(dto.getDatasSecteurActivite())) {


					List<SecteurActiviteEnseigne> secteurActiviteEnseignesToDelete = secteurActiviteEnseigneRepository.findByEnseigneId(entityToSaveId, false) ;

					if (Utilities.isNotEmpty(secteurActiviteEnseignesToDelete)) {

						for (SecteurActiviteEnseigne secteurActiviteEnseigneToDelete : secteurActiviteEnseignesToDelete) {
							secteurActiviteEnseigneToDelete.setDeletedBy(request.getUser());
							secteurActiviteEnseigneToDelete.setDeletedAt(Utilities.getCurrentDate());
							secteurActiviteEnseigneToDelete.setIsDeleted(true);	
						}
						itemsDeleteSecteurActiviteEnseigne.addAll(secteurActiviteEnseignesToDelete) ;
					}




					for (SecteurActiviteDto data : dto.getDatasSecteurActivite()) {

						SecteurActivite existingSecteurActivite = secteurActiviteRepository.findById(data.getId(), false);
						//SecteurActivite existingSecteurActivite = secteurActiviteRepository.findByLibelle(data.getLibelle(), false);
						if (existingSecteurActivite == null) {
							response.setStatus(functionalError.DATA_NOT_EXIST("secteurActivite -> " + data.getLibelle(), locale));
							response.setHasError(true);
							return response;
						}
						SecteurActiviteEnseigne secteurActiviteEnseigne = new SecteurActiviteEnseigne() ;
						secteurActiviteEnseigne.setSecteurActivite(existingSecteurActivite);
						secteurActiviteEnseigne.setEnseigne(entityToSave);
						secteurActiviteEnseigne.setCreatedBy(request.getUser());
						secteurActiviteEnseigne.setCreatedAt(Utilities.getCurrentDate());
						secteurActiviteEnseigne.setIsDeleted(false);

						itemsSecteurActiviteEnseigne.add(secteurActiviteEnseigne) ;
					}
				}

				if (Utilities.notBlank(dto.getEmailAdmin())) {
					entityToSave.setEmailAdmin(dto.getEmailAdmin());
				}
				if (Utilities.notBlank(dto.getTelephoneAdmin())) {
					entityToSave.setTelephoneAdmin(dto.getTelephoneAdmin());
				}
				if (Utilities.notBlank(dto.getNomAdmin())) {
					entityToSave.setNomAdmin(dto.getNomAdmin());
				}
				if (Utilities.notBlank(dto.getPrenomsAdmin())) {
					entityToSave.setPrenomsAdmin(dto.getPrenomsAdmin());
				}				
				if (Utilities.notBlank(dto.getUrlLogo())) {
					entityToSave.setUrlLogo(dto.getUrlLogo());
				}
				if (Utilities.notBlank(dto.getDescription())) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (Utilities.notBlank(dto.getFacebook())) {
					entityToSave.setFacebook(dto.getFacebook());
				}
				if (Utilities.notBlank(dto.getInstagram())) {
					entityToSave.setInstagram(dto.getInstagram());
				}
				if (Utilities.notBlank(dto.getTwitter())) {
					entityToSave.setTwitter(dto.getTwitter());
				}
				if (Utilities.notBlank(dto.getSiteInternet ())) {
					entityToSave.setSiteInternet(dto.getSiteInternet());
				}
				if (Utilities.notBlank(dto.getCouleur())) {
					entityToSave.setCouleur(dto.getCouleur());
				}
				if (dto.getIsLocked() != null) {
					entityToSave.setIsLocked(dto.getIsLocked());
				}
				if (dto.getIsAccepted() != null) {
					entityToSave.setIsAccepted(dto.getIsAccepted());
				}




				/*
        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
          entityToSave.setCreatedBy(dto.getCreatedBy());
        }
        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
          entityToSave.setUpdatedBy(dto.getUpdatedBy());
        }
        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
          entityToSave.setDeletedBy(dto.getDeletedBy());
        }
        if (Utilities.notBlank(dto.getDeletedAt())) {
          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
        }
				 */


				if (Utilities.notBlank(dto.getFichierBase64()) && Utilities.notBlank(dto.getNomFichier()) ) {

					// TODO : reste la suppression du fichier sur le disque dur
					//listOfOldFiles.add(entityToSave.getUrlLogo());

					listOfOldFiles.add(entityToSave.getUrlLogo() );

					// gestion du logo associe à l'entreprise
					String cheminFile = null;
					String [] infosFichier = dto.getNomFichier().split("\\.");
					_Image image2 = new _Image();

					image2.setExtension(infosFichier[infosFichier.length - 1]);
					image2.setFileBase64(dto.getFichierBase64());
					image2.setFileName(dto.getNomFichier().split("\\." + image2.getExtension())[0]);

					cheminFile =  Utilities.saveFile(image2, paramsUtils);
					//cheminFile =  Utilities.saveFileCustom(image2, GlobalEnum.profils, paramsUtils);
					if(cheminFile==null){
						response.setStatus(functionalError.SAVE_FAIL("fichier", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setUrlLogo(cheminFile);
					//entityToSave.setUrlLogo(cheminFile.split("."+dto.getExtensionLogo())[0]);

					//entityToSave.setExtensionLogo(dto.getExtensionLogo());
					listOfFilesCreate.add(cheminFile);

					//Utilities.saveImage(data.getImageBase64(), "urlFichier", data.getImageExtension());
					//image.setImageUrl(data.getImageUrl());
				}

				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Enseigne> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = enseigneRepository.saveAll((Iterable<Enseigne>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("enseigne", locale));
					response.setHasError(true);
					return response;
				}

				// gestion etatInscription enseigne
				if (!itemsDeleteEtatInscripitionEnseigne.isEmpty()) {
					List<EtatInscripitionEnseigne> itemsDeleteEtatInscripitionEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsDeleteEtatInscripitionEnseigneSaved = etatInscripitionEnseigneRepository.saveAll((Iterable<EtatInscripitionEnseigne>) itemsDeleteEtatInscripitionEnseigne);
					if (itemsDeleteEtatInscripitionEnseigneSaved == null || itemsDeleteEtatInscripitionEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("etatInscripitionEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsEtatInscripitionEnseigne.isEmpty()) {
					List<EtatInscripitionEnseigne> itemsEtatInscripitionEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsEtatInscripitionEnseigneSaved = etatInscripitionEnseigneRepository.saveAll((Iterable<EtatInscripitionEnseigne>) itemsEtatInscripitionEnseigne);
					if (itemsEtatInscripitionEnseigneSaved == null || itemsEtatInscripitionEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("etatInscripitionEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}

				// gestion secteurActivite enseigne
				if (!itemsDeleteSecteurActiviteEnseigne.isEmpty()) {
					List<SecteurActiviteEnseigne> itemsDeleteSecteurActiviteEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsDeleteSecteurActiviteEnseigneSaved = secteurActiviteEnseigneRepository.saveAll((Iterable<SecteurActiviteEnseigne>) itemsDeleteSecteurActiviteEnseigne);
					if (itemsDeleteSecteurActiviteEnseigneSaved == null || itemsDeleteSecteurActiviteEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("secteurActiviteEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}
				if (!itemsSecteurActiviteEnseigne.isEmpty()) {
					List<SecteurActiviteEnseigne> itemsSecteurActiviteEnseigneSaved = null;
					// inserer les donnees en base de donnees
					itemsSecteurActiviteEnseigneSaved = secteurActiviteEnseigneRepository.saveAll((Iterable<SecteurActiviteEnseigne>) itemsSecteurActiviteEnseigne);
					if (itemsSecteurActiviteEnseigneSaved == null || itemsSecteurActiviteEnseigneSaved.isEmpty()) {
						response.setStatus(functionalError.SAVE_FAIL("secteurActiviteEnseigne", locale));
						response.setHasError(true);
						return response;
					}
				}


				List<EnseigneDto> itemsDto = new ArrayList<EnseigneDto>();
				for (Enseigne entity : itemsSaved) {
					EnseigneDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);




				if(mapEtatInscriptionCodeEnseigne.size() > 0) {

					//mapEtatInscriptionCodeEnseigne.forEach((k, v)->{


					for (Map.Entry<Integer, String> entry : mapEtatInscriptionCodeEnseigne.entrySet()) {

						Integer k = entry.getKey() ;
						String v = entry.getValue() ;



						// envoi de mail pour la validation de la demande d'inscription de l'entreprise

						// envoi de mail 

						//set mail to user
						Map<String, String> from=new HashMap<>();
						from.put("email", paramsUtils.getSmtpMailUsername());
						from.put("user" ,  paramsUtils.getSmtpMailUser());

						// choisir la vraie url
						String appLink = "";
						//			if (entityToSave.getAdmin() != null ) {
						//				appLink = paramsUtils.getUrlRootAdmin();
						//			}else {
						//				appLink = paramsUtils.getUrlRootUser();
						//			}

						appLink = paramsUtils.getUrlRootUser();



						Enseigne data = enseigneRepository.findById(k, false) ;

						//recipients
						List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
						//		        	for (User user : itemsSaved) {
						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email",  data.getEmail());
						recipient.put("user",  data.getNom());
						//		        	recipient.put("user", user.getNom());

						toRecipients.add(recipient); 
						//					}

						//subject
						// ajout du champ objet a la newsletter
						//		        	String subject = "NEWSLETTER DU VOLONTARIAT";


						String subject = "";
						String template = "";
						String body = "";
						String contenuMail = "";
						String bienvenueEntreprise = EmailEnum.bienvenue + data.getNom();
						context = new Context();

						switch (v) {
						case EtatEnum.ACCEPTER:
							subject = EmailEnum.SUBJECT_DEMANDE_CREATION_COMPTE_VALIDER;
							template = paramsUtils.getTemplateDemandeCreationCompte();
							contenuMail = EmailEnum.contenuMailAccepter ;
							bienvenueEntreprise = EmailEnum.bienvenueAccepter ;

							// creation de compte de l'administrateur
							UserDto userEnseigneDto = new UserDto() ;


							userEnseigneDto.setNom(data.getNomAdmin());
							userEnseigneDto.setEmail(data.getEmailAdmin());
							userEnseigneDto.setEnseigneId(data.getId());
							userEnseigneDto.setIsAdminEnseigne(true); // il est d'office l'admin de l'enseigne
							userEnseigneDto.setRoleId(data.getRole().getId()); // il a le role qui a ete choisi lors de la soumission de l'enseigne

							//							Role roleAdmin = roleRepository.findByCode(RoleEnum.ADMINISTRATEUR, false);
							//							if (roleAdmin != null) {
							//								userEnseigneDto.setRoleId(roleAdmin.getId());
							//							}

							// generation login/password
							String passwordGenerer = Utilities.generatePassowrdForUserEnseigne(userRepository, GlobalEnum.longueurDefaultPassword) ;
							//String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne, data.getRole().getId());
							String loginGenerer = Utilities.generateLoginUserEnseigne(userRepository, GlobalEnum.loginUserEnseigne);

							//							Date firstDate = Utilities.firstDateOfYear() ;
							//							Date lastDate = Utilities.lastDateOfYear() ;							
							//
							//							List<UserEnseigne> userEnseigneSysteme = userEnseigneRepository.findAllUserEnseigneByYear(firstDate, lastDate);
							//							Integer numeroUser = (Utilities.isNotEmpty(userEnseigneSysteme) ? userEnseigneSysteme.size() : 0);
							//
							//							String loginGenerer = Utilities.getLoginUserEnseigne(GlobalEnum.loginUserEnseigne, numeroUser);

							userEnseigneDto.setLogin(loginGenerer);
							userEnseigneDto.setPassword(passwordGenerer);

							userEnseigneDto.setPrenoms(data.getPrenomsAdmin());
							userEnseigneDto.setTelephone(data.getTelephoneAdmin());

							Request<UserDto> req = new Request<>() ;
							req.setDatas(Arrays.asList(userEnseigneDto));
							req.setUser(request.getUser());
							Response<UserDto> res = userBusiness.create(req, locale) ;
							if (res != null && res.isHasError()) {
								response.setStatus(res.getStatus());
								response.setHasError(true);
								return response;
							}


							break;

						case EtatEnum.REFUSER:
							subject = EmailEnum.SUBJECT_DEMANDE_CREATION_COMPTE_REFUSER;
							template = paramsUtils.getTemplateDemandeCreationCompte();
							contenuMail = EmailEnum.contenuMailRejeter  ;
							bienvenueEntreprise = EmailEnum.bienvenueRejeter  ;


							break;

						case EtatEnum.EN_COURS:
							subject = EmailEnum.SUBJECT_DEMANDE_CREATION_COMPTE_EN_COURS;
							template = paramsUtils.getTemplateDemandeCreationCompte();
							contenuMail = EmailEnum.contenuMailEnCours ;
							bienvenueEntreprise = EmailEnum.bienvenueEnCours  ;


							break;

						default:
							subject = EmailEnum.SUBJECT_DEMANDE_CREATION_COMPTE;
							template = paramsUtils.getTemplateDemandeCreationCompte();

							break;
						}

						//String body = EmailEnum.BODY_PASSWORD_RESET;

						//String template = paramsUtils.getTemplateCreationCompteParUser();

						//context.setVariable("email", user.getEmail());
						//context.setVariable("login", user.getLogin());
						//context.setVariable("token", user.getToken());
						//context.setVariable("expiration", dateFormat.format(user.getDateExpirationToken()));


						context.setVariable("appLink", appLink); // @{|${appLink}/#/reset/${email}/${code}|}

						context.setVariable(EmailEnum.bienvenueEntreprise, bienvenueEntreprise + data.getNom());
						context.setVariable(EmailEnum.contenuMail, contenuMail );
						//context.setVariable(EmailEnum.nomUser, user.getNom());
						//context.setVariable(EmailEnum.nomUser, (Utilities.notBlank(data.getPrenoms()) ? user.getPrenoms() : user.getLogin()));
						//context.setVariable(EmailEnum.domaineEntreprise, EmailEnum.DOMAINE_ENTREPRISE);
						context.setVariable(EmailEnum.date, dateFormat.format(Utilities.getCurrentDate())); // @{|${appLink}/#/reset/${email}/${code}|}
						Response<UserDto> responseEnvoiEmail = new Response<>();
						responseEnvoiEmail = hostingUtils.sendEmail(from, toRecipients, subject,body,null,context,template, locale);
						// si jamais une exception c'est produite pour un mail


						//});
					}

				}






			}

			slf4jLogger.info("----end update Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				Utilities.deleteFileOnSever(listOfFilesCreate, paramsUtils);
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}else {
				// suppression des anciens fichiers quand le processus de update c'est bien dérouler
				slf4jLogger.info("listOfOldFiles ... " + listOfOldFiles);
				slf4jLogger.info("listOfOldFiles.toString() ... " + listOfOldFiles.toString());

				Utilities.deleteFileOnSever(listOfOldFiles, paramsUtils);
			}
		}
		return response;
	}


	/**
	 * delete Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EnseigneDto> delete(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete Enseigne-----");

		response = new Response<EnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Enseigne> items = new ArrayList<Enseigne>();

			for (EnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la enseigne existe
				Enseigne existingEntity = null;
				existingEntity = enseigneRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// user
				List<User> listOfUser = userRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfUser != null && !listOfUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				//				// carte
				//				List<Carte> listOfCarte = carteRepository.findByEnseigneId(existingEntity.getId(), false);
				//				if (listOfCarte != null && !listOfCarte.isEmpty()){
				//					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCarte.size() + ")", locale));
				//					response.setHasError(true);
				//					return response;
				//				}
				// produit
				List<Produit> listOfProduit = produitRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProduit != null && !listOfProduit.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProduit.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// programmeFideliteTampon
				List<ProgrammeFideliteTampon> listOfProgrammeFideliteTampon = programmeFideliteTamponRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProgrammeFideliteTampon != null && !listOfProgrammeFideliteTampon.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProgrammeFideliteTampon.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// programmeFideliteCarte
				List<ProgrammeFideliteCarte> listOfProgrammeFideliteCarte = programmeFideliteCarteRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProgrammeFideliteCarte != null && !listOfProgrammeFideliteCarte.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProgrammeFideliteCarte.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// souscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfHistoriqueSouscriptionEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// etatInscripitionEnseigne
				List<EtatInscripitionEnseigne> listOfEtatInscripitionEnseigne = etatInscripitionEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfEtatInscripitionEnseigne != null && !listOfEtatInscripitionEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEtatInscripitionEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// secteurActiviteEnseigne
				List<SecteurActiviteEnseigne> listOfSecteurActiviteEnseigne = secteurActiviteEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfSecteurActiviteEnseigne != null && !listOfSecteurActiviteEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSecteurActiviteEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// pointDeVente
				List<PointDeVente> listOfPointDeVente = pointDeVenteRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfPointDeVente != null && !listOfPointDeVente.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPointDeVente.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNotePdf.size() + ")", locale));
					response.setHasError(true);
					return response;

				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNetPromoterScorUser.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				enseigneRepository.saveAll((Iterable<Enseigne>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<EnseigneDto> forceDelete(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete Enseigne-----");

		response = new Response<EnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			//UserEnseigne utilisateur = userEnseigneRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<Enseigne> items = new ArrayList<Enseigne>();

			for (EnseigneDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la enseigne existe
				Enseigne existingEntity = null;
				existingEntity = enseigneRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// user
				List<User> listOfUserEnseigne = userRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfUserEnseigne != null && !listOfUserEnseigne.isEmpty()){
					Request<UserDto> deleteRequest = new Request<>();
					deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUserEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// carte
				//				List<Carte> listOfCarte = carteRepository.findByEnseigneId(existingEntity.getId(), false);
				//				if (listOfCarte != null && !listOfCarte.isEmpty()){
				//					Request<CarteDto> deleteRequest = new Request<CarteDto>();
				//					deleteRequest.setDatas(CarteTransformer.INSTANCE.toDtos(listOfCarte));
				//					deleteRequest.setUser(request.getUser());
				//					Response<CarteDto> deleteResponse = carteBusiness.delete(deleteRequest, locale);
				//					if(deleteResponse.isHasError()){
				//						response.setStatus(deleteResponse.getStatus());
				//						response.setHasError(true);
				//						return response;
				//					}
				//				}
				// produit
				List<Produit> listOfProduit = produitRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProduit != null && !listOfProduit.isEmpty()){
					Request<ProduitDto> deleteRequest = new Request<ProduitDto>();
					deleteRequest.setDatas(ProduitTransformer.INSTANCE.toDtos(listOfProduit));
					deleteRequest.setUser(request.getUser());
					Response<ProduitDto> deleteResponse = produitBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// programmeFideliteTampon
				List<ProgrammeFideliteTampon> listOfProgrammeFideliteTampon = programmeFideliteTamponRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProgrammeFideliteTampon != null && !listOfProgrammeFideliteTampon.isEmpty()){
					Request<ProgrammeFideliteTamponDto> deleteRequest = new Request<ProgrammeFideliteTamponDto>();
					deleteRequest.setDatas(ProgrammeFideliteTamponTransformer.INSTANCE.toDtos(listOfProgrammeFideliteTampon));
					deleteRequest.setUser(request.getUser());
					Response<ProgrammeFideliteTamponDto> deleteResponse = programmeFideliteTamponBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// programmeFideliteCarte
				List<ProgrammeFideliteCarte> listOfProgrammeFideliteCarte = programmeFideliteCarteRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfProgrammeFideliteCarte != null && !listOfProgrammeFideliteCarte.isEmpty()){
					Request<ProgrammeFideliteCarteDto> deleteRequest = new Request<ProgrammeFideliteCarteDto>();
					deleteRequest.setDatas(ProgrammeFideliteCarteTransformer.INSTANCE.toDtos(listOfProgrammeFideliteCarte));
					deleteRequest.setUser(request.getUser());
					Response<ProgrammeFideliteCarteDto> deleteResponse = programmeFideliteCarteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// historiqueSouscriptionEnseigne
				List<HistoriqueSouscriptionEnseigne> listOfHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfHistoriqueSouscriptionEnseigne != null && !listOfHistoriqueSouscriptionEnseigne.isEmpty()){
					Request<HistoriqueSouscriptionEnseigneDto> deleteRequest = new Request<HistoriqueSouscriptionEnseigneDto>();
					deleteRequest.setDatas(HistoriqueSouscriptionEnseigneTransformer.INSTANCE.toDtos(listOfHistoriqueSouscriptionEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<HistoriqueSouscriptionEnseigneDto> deleteResponse = historiqueSouscriptionEnseigneBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// etatInscripitionEnseigne
				List<EtatInscripitionEnseigne> listOfEtatInscripitionEnseigne = etatInscripitionEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfEtatInscripitionEnseigne != null && !listOfEtatInscripitionEnseigne.isEmpty()){
					Request<EtatInscripitionEnseigneDto> deleteRequest = new Request<EtatInscripitionEnseigneDto>();
					deleteRequest.setDatas(EtatInscripitionEnseigneTransformer.INSTANCE.toDtos(listOfEtatInscripitionEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<EtatInscripitionEnseigneDto> deleteResponse = etatInscripitionEnseigneBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// secteurActiviteEnseigne
				List<SecteurActiviteEnseigne> listOfSecteurActiviteEnseigne = secteurActiviteEnseigneRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfSecteurActiviteEnseigne != null && !listOfSecteurActiviteEnseigne.isEmpty()){
					Request<SecteurActiviteEnseigneDto> deleteRequest = new Request<SecteurActiviteEnseigneDto>();
					deleteRequest.setDatas(SecteurActiviteEnseigneTransformer.INSTANCE.toDtos(listOfSecteurActiviteEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<SecteurActiviteEnseigneDto> deleteResponse = secteurActiviteEnseigneBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// pointDeVente
				List<PointDeVente> listOfPointDeVente = pointDeVenteRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfPointDeVente != null && !listOfPointDeVente.isEmpty()){
					Request<PointDeVenteDto> deleteRequest = new Request<PointDeVenteDto>();
					deleteRequest.setDatas(PointDeVenteTransformer.INSTANCE.toDtos(listOfPointDeVente));
					deleteRequest.setUser(request.getUser());
					Response<PointDeVenteDto> deleteResponse = pointDeVenteBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}

				// notePdf
				List<NotePdf> listOfNotePdf = notePdfRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfNotePdf != null && !listOfNotePdf.isEmpty()){
					Request<NotePdfDto> deleteRequest = new Request<NotePdfDto>();
					deleteRequest.setDatas(NotePdfTransformer.INSTANCE.toDtos(listOfNotePdf));
					deleteRequest.setUser(request.getUser());
					Response<NotePdfDto> deleteResponse = notePdfBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				// netPromoterScorUser
				List<NetPromoterScorUser> listOfNetPromoterScorUser = netPromoterScorUserRepository.findByEnseigneId(existingEntity.getId(), false);
				if (listOfNetPromoterScorUser != null && !listOfNetPromoterScorUser.isEmpty()){
					Request<NetPromoterScorUserDto> deleteRequest = new Request<NetPromoterScorUserDto>();
					deleteRequest.setDatas(NetPromoterScorUserTransformer.INSTANCE.toDtos(listOfNetPromoterScorUser));
					deleteRequest.setUser(request.getUser());
					Response<NetPromoterScorUserDto> deleteResponse = netPromoterScorUserBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				enseigneRepository.saveAll((Iterable<Enseigne>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * getBasicStat Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<EnseigneDto> getBasicStat(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin getBasicStat Enseigne-----");

		response = new Response<EnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			// if (utilisateur.getEnseigne() != null && utilisateur.getEnseigne().getIsActive() != null 
			// 	&& !utilisateur.getEnseigne().getIsActive()) {
			// 		response.setStatus(functionalError.INACTIF_PROFILE("Veuillez souscrire à une offre pour activer votre compte !!!"+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
			// 		response.setHasError(true);
			// 		return response;
			// 	}

			List<Enseigne> items = new ArrayList<>();
			List<User> itemsUserEnseigne = new ArrayList<>();
			List<EtatInscripitionEnseigne> itemsEtatInscripitionEnseigne = new ArrayList<>();
			List<SecteurActiviteEnseigne> itemsSecteurActiviteEnseigne = new ArrayList<>();



			EnseigneDto dto =  request.getData() ;


			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("id", dto.getId());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if enseigne to insert do not exist
			Enseigne existingEntity = null;

			// unicite du nom
			existingEntity = enseigneRepository.findById(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}




			// nbre PointDeVente
			List<PointDeVente>  itemsPointDeVente = pointDeVenteRepository.findByEnseigneId(dto.getId(), false) ;

			// nbre ProgrammeFideliteCarte
			List<ProgrammeFideliteCarte>  itemsProgrammeFideliteCarte = programmeFideliteCarteRepository.findByEnseigneId(dto.getId(), false) ;

			// nbre ProgrammeFideliteTampon
			List<ProgrammeFideliteTampon>  itemsProgrammeFideliteTampon = programmeFideliteTamponRepository.findByEnseigneId(dto.getId(), false) ;

			// nbre ProgrammeFideliteTampon
			//Long  nbreUserMobileEnseigne = souscriptionProgrammeFideliteRepository.findNbreUserMobileEnseigneByEnseigneId(dto.getId(),  false) ;


			EnseigneDto itemDto = EnseigneTransformer.INSTANCE.toDto(existingEntity);

			itemDto.setNbrePointDeVente( Utilities.isNotEmpty(itemsPointDeVente) ? itemsPointDeVente.size() :  0);
			itemDto.setNbreProgrammeFideliteTampon( Utilities.isNotEmpty(itemsProgrammeFideliteTampon) ? itemsProgrammeFideliteTampon.size() :  0);
			itemDto.setNbreProgrammeFideliteCarte( Utilities.isNotEmpty(itemsProgrammeFideliteCarte) ? itemsProgrammeFideliteCarte.size() :  0);
			//itemDto.setNbreUserMobileEnseigne(nbreUserMobileEnseigne.intValue());
			response.setItems(Arrays.asList(itemDto));
			response.setHasError(false);

			slf4jLogger.info("----end getBasicStat Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * souscriptionEnseigne Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<EnseigneDto> souscriptionEnseigne(Request<EnseigneDto> request, Locale locale)  {
		slf4jLogger.info("----begin souscriptionEnseigne Enseigne-----");

		response = new Response<EnseigneDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}
			EnseigneDto dto =  request.getData() ;


			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("id", dto.getId());
			fieldsToVerify.put("souscriptionId", dto.getSouscriptionId());
			//fieldsToVerify.put("modePaiementId", dto.getModePaiementId());

			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if enseigne to insert do not exist
			Enseigne existingEntity = null;

			// unicite du nom
			existingEntity = enseigneRepository.findById(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityId = existingEntity.getId();

			Souscription existingSouscription = null;

			// Verify if souscription exist
			existingSouscription = souscriptionRepository.findById(dto.getSouscriptionId(), false);
			if (existingSouscription == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("souscription -> " + dto.getSouscriptionId(), locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if modePaiement exist
			ModePaiement existingModePaiement = modePaiementRepository.findByCode(ModePaiementEnum.MONEY, false);

			// Verify if etatSouscription exist
			EtatSouscription existingEtatSouscription = etatSouscriptionRepository.findByCode(EtatEnum.ENTRANT, false);

			
			// Verify if historiqueSouscriptionEnseigne en cours de validation
			HistoriqueSouscriptionEnseigne existingHistoriqueSouscriptionEnseigne = null;

			// Verify if souscription exist
			existingHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneIdAndEtatSouscriptionId(dto.getId(),existingEtatSouscription.getId(), false);
			if (existingHistoriqueSouscriptionEnseigne != null) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("Une souscription est deja en cours de validation !!!", locale));
				response.setHasError(true);
				return response;
			}

			int currentPrix = 0 ;

			if (!existingSouscription.getPallierForfait().getCode().equals(UniteTempsEnum.CINQ_CENT_ET_PLUS)) {
				currentPrix = Integer.parseInt(existingSouscription.getPrix());
			}
			
			List<HistoriqueSouscriptionEnseigne> listHistoriqueSouscriptionEnseigne = null;

			Calendar calendar = new GregorianCalendar() ;
			
			
			// verifier si la  souscription correspond au nbre d'utilisateur de l'enseigne
			Souscription oldSouscription = existingEntity.getSouscription();
			if (oldSouscription != null && existingEntity.getNbreUtilisateurMobile() != null 
					&& existingEntity.getNbreUtilisateurMobile() > existingSouscription.getNbreMaxiUser()) {
				response.setStatus(functionalError.DISALLOWED_OPERATION("Vous avez "+ existingEntity.getNbreUtilisateurMobile() +" abonnées actuellement. Cette offre ne correspond pas à votre profil !!!", locale));
				response.setHasError(true);
				return response;	
			}


			listHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneIdAndIsActiveAndDateExpirationBefore(entityId, calendar.getTime(), true, false);
			if (Utilities.isNotEmpty(listHistoriqueSouscriptionEnseigne)) {
				// maj souscriptionEnseigne 
				calendar.setTime(listHistoriqueSouscriptionEnseigne.get(0).getDateExpiration());

				// faire le controle si la souscription courante a deja depasser 20 jours
				calendar.add(Calendar.DAY_OF_MONTH, -GlobalEnum.JOURS_ADMIS_POUR_CHANGER_SOUSCRIP) ;
				if (calendar.getTime().after(Utilities.getCurrentDate())) {
					response.setStatus(functionalError.DISALLOWED_OPERATION("Votre souscription actuelle n'a pas depassée 20 jours. Merci de contacter le service assistance !!!", locale));
					response.setHasError(true);
					return response;					
				}
			}else {
				// creation souscriptionEnseigne 
				EtatSouscription etatSouscriptionTerminer = etatSouscriptionRepository.findByCode(EtatEnum.TERMINER, false);

				listHistoriqueSouscriptionEnseigne = historiqueSouscriptionEnseigneRepository.findByEnseigneIdAndIsActive(entityId, true, false);
				if (Utilities.isNotEmpty(listHistoriqueSouscriptionEnseigne)) {
					for (HistoriqueSouscriptionEnseigne data : listHistoriqueSouscriptionEnseigne) {
						data.setIsActive(false);
						data.setUpdatedBy(request.getUser());
						data.setUpdatedAt(Utilities.getCurrentDate());
						data.setEtatSouscription(etatSouscriptionTerminer);
					}
					
					existingEntity.setIsActive(false);
					existingEntity.setEtatSouscription(etatSouscriptionTerminer);
					existingEntity.setUpdatedAt(Utilities.getCurrentDate());

					enseigneRepository.save(existingEntity) ;
					historiqueSouscriptionEnseigneRepository.saveAll(listHistoriqueSouscriptionEnseigne) ;
				}
				
			}
			
			// historiqueSouscriptionEnseigneToSave
			HistoriqueSouscriptionEnseigne historiqueSouscriptionEnseigneToSave = new HistoriqueSouscriptionEnseigne();

			// 
			historiqueSouscriptionEnseigneToSave.setSouscription(existingSouscription);
			historiqueSouscriptionEnseigneToSave.setModePaiement(existingModePaiement);
			historiqueSouscriptionEnseigneToSave.setEtatSouscription(existingEtatSouscription);
			historiqueSouscriptionEnseigneToSave.setEnseigne(existingEntity);
			
			historiqueSouscriptionEnseigneToSave.setIsActive(false);
			historiqueSouscriptionEnseigneToSave.setPrixSpecifique(currentPrix);
			historiqueSouscriptionEnseigneToSave.setNbreSouscription(existingEntity.getNbreSouscription() != null ? existingEntity.getNbreSouscription() + 1 : 1);
			historiqueSouscriptionEnseigneToSave.setNbreUtilisateurMobile(existingEntity.getNbreUtilisateurMobile() != null ? existingEntity.getNbreUtilisateurMobile() + 1 : 1);

			
			historiqueSouscriptionEnseigneToSave.setCreatedBy(request.getUser());
			historiqueSouscriptionEnseigneToSave.setCreatedAt(Utilities.getCurrentDate());
			historiqueSouscriptionEnseigneToSave.setIsDeleted(false);
			
			
			
			historiqueSouscriptionEnseigneRepository.save(historiqueSouscriptionEnseigneToSave) ;
			
			response.setHasError(false);

			slf4jLogger.info("----end souscriptionEnseigne Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	
	
	


	/**
	 * getStatistiques EnseigneDto by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<EnseigneDto> getStatistiques(Request<EnseigneDto> request, Locale locale) {
		slf4jLogger.info("----begin getStatistiques Enseigne-----");

		Response<EnseigneDto> response = new Response<>();

		try {
			//List<AppelDoffres> items = null;
			EnseigneDto itemDto = new EnseigneDto();

			//Integer nbreMixte = 0 ;
			//Integer nbreMixte = 0 ;


			// formation des datas envoyées

			EnseigneDto dto = request.getData() ;

			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("id", dto.getId());
			fieldsToVerify.put("dateDebut", dto.getDateDebut());
			fieldsToVerify.put("dateFin", dto.getDateFin());
			//fieldsToVerify.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			Date dateDebut = dateFormatNew.parse(dto.getDateDebut());
			Date dateFin = dateFormatNew.parse(dto.getDateFin());
			if (dateDebut.after(dateFin)) {
				response.setStatus(functionalError.INVALID_DATA("La date de debut soit etre <= à la date de fin", locale));
				response.setHasError(true);
				return response;
			}

			//			PointDeVente existingPointDeVente = null;
			//			if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() >  0) {
			//				existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
			//				if (existingPointDeVente == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getPointDeVenteId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}
			//			}
			//			ProgrammeFideliteCarte existingProgrammeFideliteCarte = null;
			//			if (dto.getProgrammeFideliteCarteId() != null && dto.getProgrammeFideliteCarteId() >  0) {
			//				existingProgrammeFideliteCarte = programmeFideliteCarteRepository.findById(dto.getProgrammeFideliteCarteId(), false);
			//				if (existingProgrammeFideliteCarte == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getProgrammeFideliteCarteId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}	
			//			}
			//			ProgrammeFideliteTampon existingProgrammeFideliteTampon = null;
			//			if (dto.getProgrammeFideliteTamponId() != null && dto.getProgrammeFideliteTamponId() >  0) {
			//				existingProgrammeFideliteTampon = programmeFideliteTamponRepository.findById(dto.getProgrammeFideliteTamponId(), false);
			//				if (existingProgrammeFideliteTampon == null) {
			//					response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getProgrammeFideliteTamponId(), locale));
			//					response.setHasError(true);
			//					return response;
			//				}
			//			}


			// Verifier si la enseigne existe
			Enseigne entityToSave = null;
			entityToSave = enseigneRepository.findById(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("enseigne -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}


			Integer entityToSaveId = entityToSave.getId();

			Integer  programmeFideliteCarteId = dto.getProgrammeFideliteCarteId() ;
			Integer  programmeFideliteTamponId = dto.getProgrammeFideliteTamponId() ;
			Integer  pointDeVenteId = dto.getPointDeVenteId();


			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(dateDebut);
			//calendar.add(Calendar.MONTH, -11);
			//String currentDateString = "01" + dateFormatToSearch.format(calendar.getTime());			
			//Date dateFacture= dateFormatFront.parse(currentDateString);



			// 

			Date dateDebutNew ;
			Date dateFinNew ;
			Boolean isAnneeEcart = false ;

			Boolean isPdvIdNotNull = false ;
			Boolean isPdfCarteIdNotNull = false ;
			Boolean isPdfTamponIdNotNull = false ;


			// verifier si l'ecart depasse des annees
			calendar.add(Calendar.YEAR, 1); // annee suivante
			if (dateFin.after(calendar.getTime()) || calendar.getTime().equals(dateFin)) {
				isAnneeEcart = true ;
			}
			calendar.add(Calendar.YEAR, -1); // annee normale
			calendar.setTime(dateFormat.parse(dateFormatNew.format(calendar.getTime()) + " 23:59:59"));


			dateFin = dateFormat.parse(dto.getDateFin() + " 23:59:59"); // important pour la derniere requete


			// declaration des listes

			Integer  nbreTotalTransaction = 0 ;
			Integer  nbreTransactionParPdfCarte  = 0;
			Integer  nbreTransactionParPdfTampon = 0 ;
			Integer  nbreTransactionPointDeVente = 0 ;

			// bande 
			List<Double> recompenseEngagePdfTampon = new ArrayList<>();
			List<Double> recompenseEngagePdfCarte = new ArrayList<>();
			List<Double> recompenseRemboursePdfTampon = new ArrayList<>();
			List<Double> recompenseRemboursePdfCarte = new ArrayList<>();
			List<Double> recompenseEnAttentePdfTampon = new ArrayList<>();
			List<Double> recompenseEnAttentePdfCarte = new ArrayList<>();
			List<Double> npsPdfTampon = new ArrayList<>();
			List<Double> npsPdfCarte = new ArrayList<>();
			List<Double> tauxRetentioPdfTampon = new ArrayList<>();
			List<Double> tauxRetentioPdfCarte = new ArrayList<>();
			List<Double> tauxAttrioPdfTampon = new ArrayList<>();
			List<Double> tauxAttrioPdfCarte = new ArrayList<>();
			List<Double> tauxReachatPdfTampon = new ArrayList<>();
			List<Double> tauxReachatPdfCarte = new ArrayList<>();
			List<Integer> nbreInscritPdfTampon = new ArrayList<>();
			List<Integer> nbreInscritPdfCarte = new ArrayList<>();
			List<Double> chiffreDaffairePdfCarte = new ArrayList<>();
			List<String> abscisseGraphe = new ArrayList<>();

			// circulaire 
			List<_GraphCirculaireDto> chiffreDaffaireParPdfCarte = new ArrayList<>();
			List<_GraphCirculaireDto>inscritParPdfTampon = new ArrayList<>();
			List<_GraphCirculaireDto> inscritParPdfCarte = new ArrayList<>();
			List<String> legendCaPdfCarte= new ArrayList<>();
			List<String> legendPdfTampon= new ArrayList<>();
			List<String> legendPdfCarte= new ArrayList<>();



			// nbre transaction pdfCarte
			if (programmeFideliteCarteId != null && programmeFideliteCarteId >  0) {
				isPdfCarteIdNotNull = true ;
				List<HistoriqueCarteProgrammeFideliteCarte> listTransactionParPdfCarte = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteCarteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;
				nbreTransactionParPdfCarte = Utilities.isNotEmpty(listTransactionParPdfCarte) ?  listTransactionParPdfCarte.size() : 0 ;
			}else {
				List<HistoriqueCarteProgrammeFideliteCarte> listTransactionParPdfCarte = historiqueCarteProgrammeFideliteCarteRepository.findByTypeActionCodeAndCreatedAtBetween(TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;
				nbreTransactionParPdfCarte = Utilities.isNotEmpty(listTransactionParPdfCarte) ? listTransactionParPdfCarte.size() : 0 ;
			}

			// nbre transaction pdfTampon
			if (programmeFideliteTamponId != null && programmeFideliteTamponId >  0) {
				isPdfTamponIdNotNull = true ;
				List<HistoriqueCarteProgrammeFideliteTampon> listTransactionParPdfTampon = historiqueCarteProgrammeFideliteTamponRepository.findByPdfTamponIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteTamponId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;
				nbreTransactionParPdfTampon = Utilities.isNotEmpty(listTransactionParPdfTampon) ?  listTransactionParPdfTampon.size() : 0 ;
			}else {
				List<HistoriqueCarteProgrammeFideliteTampon> listTransactionParPdfTampon = historiqueCarteProgrammeFideliteTamponRepository.findByTypeActionCodeAndCreatedAtBetween(TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;
				nbreTransactionParPdfTampon = Utilities.isNotEmpty(listTransactionParPdfTampon) ?  listTransactionParPdfTampon.size() : 0 ;
			}
			// nbre transaction pdv
			if (pointDeVenteId != null && pointDeVenteId >  0) {
				isPdvIdNotNull = true ;
				List<HistoriqueCarteProgrammeFideliteTampon> listTransactionParPdfTampon = historiqueCarteProgrammeFideliteTamponRepository.findByPdvIdAndTypeActionCodeAndCreatedAtBetween(pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;
				List<HistoriqueCarteProgrammeFideliteCarte> listTransactionParPdfCarte = historiqueCarteProgrammeFideliteCarteRepository.findByPdvIdAndTypeActionCodeAndCreatedAtBetween(pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebut, dateFin, false) ;

				nbreTransactionPointDeVente = Utilities.isNotEmpty(listTransactionParPdfTampon) ?  listTransactionParPdfTampon.size() : 0 ;
				nbreTransactionPointDeVente += Utilities.isNotEmpty(listTransactionParPdfCarte) ?  listTransactionParPdfCarte.size() : 0 ;
			}else {
				nbreTransactionPointDeVente = nbreTransactionParPdfTampon + nbreTransactionParPdfCarte ;
			}
			nbreTotalTransaction = nbreTransactionParPdfTampon + nbreTransactionParPdfCarte ;


			// inscrit par pdfTampon
			List<ProgrammeFideliteTampon> pdfTamponEnseigne = programmeFideliteTamponRepository.findByEnseigneId(entityToSaveId, false) ;


			if (Utilities.isNotEmpty(pdfTamponEnseigne)) {
				slf4jLogger.info("----pdfTamponEnseigne -----" + pdfTamponEnseigne.size());

				for (ProgrammeFideliteTampon pdf : pdfTamponEnseigne) {
					List<SouscriptionProgrammeFideliteTampon> souscriptions = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndCreatedAtBetween(pdf.getId(), dateDebut, dateFin, false) ;
					inscritParPdfTampon.add(new _GraphCirculaireDto(pdf.getLibelle(), Utilities.isNotEmpty(souscriptions) ? souscriptions.size() : 0)) ;
					//inscritParPdfTampon.put(pdf.getLibelle(), Utilities.isNotEmpty(souscriptions) ? souscriptions.size() : 0) ;
					legendPdfTampon.add(pdf.getLibelle()) ;
				}
			} else {
				// l'enseigne n'a pas de pdfTampon
			}

			// inscrit par pdfCarte
			List<ProgrammeFideliteCarte> pdfCarteEnseigne = programmeFideliteCarteRepository.findByEnseigneId(entityToSaveId, false) ;
			if (Utilities.isNotEmpty(pdfCarteEnseigne)) {
				slf4jLogger.info("----pdfCarteEnseigne -----" + pdfCarteEnseigne.size());

				for (ProgrammeFideliteCarte pdf : pdfCarteEnseigne) {
					List<SouscriptionProgrammeFideliteCarte> souscriptions = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndCreatedAtBetween(pdf.getId(), dateDebut, dateFin, false) ;
					inscritParPdfCarte.add(new _GraphCirculaireDto(pdf.getLibelle(), Utilities.isNotEmpty(souscriptions) ? souscriptions.size() : 0)) ;
					//inscritParPdfCarte.put(pdf.getLibelle(), Utilities.isNotEmpty(souscriptions) ? souscriptions.size() : 0) ;
					legendPdfCarte.add(pdf.getLibelle()) ;
				}
			} else {
				// l'enseigne n'a pas de PdfCarte
			}

			// chiffreDaffaire par pdfCarte
			List<_RetourGroupByDto> listChiffreDaffaireParPdfCarte = historiqueCarteProgrammeFideliteCarteRepository.findByDateActionBetweenAndGroupByPdfCarte(dateDebut, dateFin, false) ;


			if (Utilities.isNotEmpty(listChiffreDaffaireParPdfCarte)) {
				_RetourGroupByDto first = (_RetourGroupByDto) listChiffreDaffaireParPdfCarte.get(0) ;
				chiffreDaffaireParPdfCarte.add(new _GraphCirculaireDto(first.getProgrammeFideliteCarte().getLibelle(),  first.getChiffreDaffaire().intValue())) ;
				//chiffreDaffaireParPdfCarte.put( first.getProgrammeFideliteCarte().getLibelle(),  first.getChiffreDaffaire().intValue()) ;
				legendCaPdfCarte.add(first.getProgrammeFideliteCarte().getLibelle()) ;
			} else {
				// pas de chiffreDaffaire par pdfCarte
			}

			while(dateFin.after(calendar.getTime()) || dateFin.equals(calendar.getTime())) {


				String xGraphe = "" ;


				dateDebutNew = dateFormat.parse(dateFormatNew.format(calendar.getTime()) + " 00:00:00"); // important pour le debut afin d'eviter 23:59:59  

				if (isAnneeEcart) {
					xGraphe = ""+calendar.get(Calendar.YEAR) ;
					calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR)); // setter le dernier jour de l'annee

					//calendar.add(Calendar.YEAR, 1); // annee suivante
				}else {
					xGraphe = Utilities.getLibelleMonthByMonthId(calendar.get(Calendar.MONTH)) ;
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));// setter la date du mois à une valeur. La méthode est generale 

					//calendar.add(Calendar.MONTH, 1); // mois suivant
				}


				if (calendar.getTime().after(dateFin) || calendar.getTime().equals(dateFin)) { // voir si le mois suivant est inferieur à la date de fin
					dateFinNew = dateFin;
				}else {					
					dateFinNew = calendar.getTime();					
				}

				// setter le calendar a un jour suivant
				calendar.add(Calendar.DAY_OF_YEAR, 1); // mois suivant




				int nbreInscrit = 0 ;
				double tauxRetention = 0 ;
				double tauxAttrition = 0 ;
				double tauxReachat = 0 ;
				double recompensesEngagees = 0 ;
				double chiffreDaffaire = 0 ;
				double recompensesRemboursees = 0 ;
				double recompensesEnAttente = 0 ;
				double nps = 0 ;



				if (isPdfCarteIdNotNull) {

					// nbre inscrit
					List<SouscriptionProgrammeFideliteCarte> nbreInscrits = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndCreatedAtBetween(programmeFideliteCarteId, dateDebutNew, dateFinNew, false) ;
					nbreInscrit = Utilities.isNotEmpty(nbreInscrits) ? nbreInscrits.size() : 0 ;

					// taux attrition && taux retention 
					List<SouscriptionProgrammeFideliteCarte> nbreInscritsDebut = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndCreatedAtBefore(programmeFideliteCarteId, dateDebutNew, false) ;

					if (Utilities.isNotEmpty(nbreInscritsDebut)) {
						List<SouscriptionProgrammeFideliteCarte> nbreInscritsFin = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndCreatedAtBefore(programmeFideliteCarteId, dateFinNew, false) ;
						tauxRetention = Utilities.isNotEmpty(nbreInscritsDebut) ? ( ( nbreInscritsFin.size() -  nbreInscrit ) / nbreInscritsDebut.size() )  * 100  : 0 ;

						List<SouscriptionProgrammeFideliteCarte> nbrePerdu = souscriptionProgrammeFideliteCarteRepository.findByProgrammeFideliteCarteIdAndCreatedAtBetween(programmeFideliteCarteId, dateDebutNew, dateFinNew, true) ;
						tauxAttrition = Utilities.isNotEmpty(nbrePerdu) ? (nbrePerdu.size() / nbreInscritsDebut.size()) * 100 : 0 ;
					}

					// nps
					List<NetPromoterScorUser> allNps = netPromoterScorUserRepository.findByProgrammeFideliteCarteId(programmeFideliteCarteId, false) ; 
					List<NetPromoterScorUser> nbrePromoters = netPromoterScorUserRepository.findByPdfCarteIdAndTypeNoteNpsCodeAndCreatedAtBetween(programmeFideliteCarteId, TypeNoteNpsEnum.PROMOTERS, dateDebutNew, dateFinNew, false) ;
					List<NetPromoterScorUser> nbreDetractors = netPromoterScorUserRepository.findByPdfCarteIdAndTypeNoteNpsCodeAndCreatedAtBetween(programmeFideliteCarteId, TypeNoteNpsEnum.DETRACTORS, dateDebutNew, dateFinNew, false) ;

					if (Utilities.isNotEmpty(allNps)) {
						int npsDetractors = Utilities.isNotEmpty(nbreDetractors) ? nbreDetractors.size() : 0 ;
						int npsPromoters = Utilities.isNotEmpty(nbrePromoters) ? nbrePromoters.size() : 0 ;
						nps =  ((npsPromoters - npsDetractors) / allNps.size()) * 100;
					} 




					if (isPdvIdNotNull) {
						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteCarteRepository.countUserByPdfCarteIdAndPdvIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteCarteId, pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						List<_RetourGroupByDto> listRecompensesEngages = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndPdvIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.PARTAGER,TypeActionEnum.RECU_PAR_PARTAGER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesEngages)) {
							_RetourGroupByDto first = listRecompensesEngages.get(0) ;
							recompensesEngagees = first.getChiffreDaffaire() ;
						}

						// recompense rembourse
						List<_RetourGroupByDto> listRecompensesRembourses = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndPdvIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, pointDeVenteId, TypeActionEnum.DEBITER, dateDebutNew, dateFinNew, false) ;

						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							_RetourGroupByDto first = listRecompensesRembourses.get(0) ;
							recompensesRemboursees = first.getChiffreDaffaire() ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire 
						List<_RetourGroupByDto> listChiffreDaffaire = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndPdvIdAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, pointDeVenteId, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							chiffreDaffaire = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						} 

					}else {
						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteCarteRepository.countUserByPdfCarteIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteCarteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						List<_RetourGroupByDto> listRecompensesEngages = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, TypeActionEnum.DEBITER, TypeActionEnum.PARTAGER,TypeActionEnum.RECU_PAR_PARTAGER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesEngages)) {
							recompensesEngagees = listRecompensesEngages.get(0).getChiffreDaffaire() ;
						} 


						// recompense rembourse
						List<_RetourGroupByDto> listRecompensesRembourses = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, TypeActionEnum.DEBITER, dateDebutNew, dateFinNew, false) ;

						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							recompensesRemboursees = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						} 

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire 
						List<_RetourGroupByDto> listChiffreDaffaire = historiqueCarteProgrammeFideliteCarteRepository.findByPdfCarteIdAndDateActionBetweenAndGroupByPdfCarte(programmeFideliteCarteId, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							chiffreDaffaire = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						} 

					}

				}else {
					// nbre inscrit
					List<SouscriptionProgrammeFideliteCarte> nbreInscrits = souscriptionProgrammeFideliteCarteRepository.findByCreatedAtBetween(dateDebutNew, dateFinNew, false) ;
					nbreInscrit = Utilities.isNotEmpty(nbreInscrits) ? nbreInscrits.size() : 0 ;

					// taux attrition && taux retention 
					List<SouscriptionProgrammeFideliteCarte> nbreInscritsDebut = souscriptionProgrammeFideliteCarteRepository.findByCreatedAtBefore( dateDebutNew, false) ;

					if (Utilities.isNotEmpty(nbreInscritsDebut)) {
						List<SouscriptionProgrammeFideliteCarte> nbreInscritsFin = souscriptionProgrammeFideliteCarteRepository.findByCreatedAtBefore(dateFinNew, false) ;
						tauxRetention = Utilities.isNotEmpty(nbreInscritsDebut) ? ( ( nbreInscritsFin.size() -  nbreInscrit ) / nbreInscritsDebut.size() )  * 100  : 0 ;

						List<SouscriptionProgrammeFideliteCarte> nbrePerdu = souscriptionProgrammeFideliteCarteRepository.findByCreatedAtBetween(dateDebutNew, dateFinNew, true) ;
						tauxAttrition = Utilities.isNotEmpty(nbrePerdu) ? (nbrePerdu.size() / nbreInscritsDebut.size()) * 100 : 0 ;
					}

					// nps
					List<NetPromoterScorUser> allNps = netPromoterScorUserRepository.findByIsDeletedWithPdfCarteNotNull(false) ; 
					List<NetPromoterScorUser> nbrePromoters = netPromoterScorUserRepository.findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfCarteNotNull(TypeNoteNpsEnum.PROMOTERS, dateDebutNew, dateFinNew, false) ;
					List<NetPromoterScorUser> nbreDetractors = netPromoterScorUserRepository.findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfCarteNotNull(TypeNoteNpsEnum.DETRACTORS, dateDebutNew, dateFinNew, false) ;

					if (Utilities.isNotEmpty(allNps)) {
						int npsDetractors = Utilities.isNotEmpty(nbreDetractors) ? nbreDetractors.size() : 0 ;
						int npsPromoters = Utilities.isNotEmpty(nbrePromoters) ? nbrePromoters.size() : 0 ;
						nps =  ((npsPromoters - npsDetractors) / allNps.size()) * 100;
					} 



					if (isPdvIdNotNull) {
						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteCarteRepository.countUserByPdvIdAndTypeActionCodeAndCreatedAtBetween(pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						List<_RetourGroupByDto> listRecompensesEngages = historiqueCarteProgrammeFideliteCarteRepository.findByPdvIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte( pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.PARTAGER,TypeActionEnum.RECU_PAR_PARTAGER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesEngages)) {
							recompensesEngagees = listRecompensesEngages.get(0).getChiffreDaffaire() ;
						}


						// recompense rembourse
						List<_RetourGroupByDto> listRecompensesRembourses = historiqueCarteProgrammeFideliteCarteRepository.findByPdvIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(pointDeVenteId, TypeActionEnum.DEBITER, dateDebutNew, dateFinNew, false) ;

						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							recompensesRemboursees = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire 
						List<_RetourGroupByDto> listChiffreDaffaire = historiqueCarteProgrammeFideliteCarteRepository.findByPdvIdAndDateActionBetweenAndGroupByPdfCarte(pointDeVenteId, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							chiffreDaffaire = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						} 

					}else {
						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteCarteRepository.countUserByTypeActionCodeAndCreatedAtBetween(TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						List<_RetourGroupByDto> listRecompensesEngages = historiqueCarteProgrammeFideliteCarteRepository.findByTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(TypeActionEnum.DEBITER, TypeActionEnum.PARTAGER,TypeActionEnum.RECU_PAR_PARTAGER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesEngages)) {
							recompensesEngagees = listRecompensesEngages.get(0).getChiffreDaffaire() ;
						} 


						// recompense rembourse
						List<_RetourGroupByDto> listRecompensesRembourses = historiqueCarteProgrammeFideliteCarteRepository.findByTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(TypeActionEnum.DEBITER, dateDebutNew, dateFinNew, false) ;

						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							recompensesRemboursees = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire 
						List<_RetourGroupByDto> listChiffreDaffaire = historiqueCarteProgrammeFideliteCarteRepository.findByDateActionBetweenAndGroupByPdfCarte(dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(listRecompensesRembourses)) {
							chiffreDaffaire = listRecompensesRembourses.get(0).getChiffreDaffaire() ;
						} 
					}
				}

				// setter les values avant de continuer

				recompenseEngagePdfCarte.add(recompensesEngagees) ;
				recompenseRemboursePdfCarte.add(recompensesRemboursees) ;
				recompenseEnAttentePdfCarte.add(recompensesEnAttente) ;
				npsPdfCarte.add(nps) ;
				tauxRetentioPdfCarte.add(tauxRetention) ;
				tauxAttrioPdfCarte.add(tauxAttrition) ;
				tauxReachatPdfCarte.add(tauxReachat) ;
				nbreInscritPdfCarte.add(nbreInscrit) ;
				chiffreDaffairePdfCarte.add(chiffreDaffaire) ;


				nbreInscrit = 0 ;
				tauxRetention = 0 ;
				tauxAttrition = 0 ;
				tauxReachat = 0 ;
				recompensesEngagees = 0 ;
				chiffreDaffaire = 0 ;
				recompensesRemboursees = 0 ;
				recompensesEnAttente = 0 ;
				nps = 0 ;


				if (isPdfTamponIdNotNull) {


					// nbre inscrit
					List<SouscriptionProgrammeFideliteTampon> nbreInscrits = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndCreatedAtBetween(programmeFideliteTamponId, dateDebutNew, dateFinNew, false) ;
					nbreInscrit = Utilities.isNotEmpty(nbreInscrits) ? nbreInscrits.size() : 0 ;

					// taux attrition && taux retention 
					List<SouscriptionProgrammeFideliteTampon> nbreInscritsDebut = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndCreatedAtBefore(programmeFideliteTamponId, dateDebutNew, false) ;

					if (Utilities.isNotEmpty(nbreInscritsDebut)) {
						List<SouscriptionProgrammeFideliteTampon> nbreInscritsFin = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndCreatedAtBefore(programmeFideliteTamponId, dateFinNew, false) ;
						tauxRetention = Utilities.isNotEmpty(nbreInscritsDebut) ? ( ( nbreInscritsFin.size() -  nbreInscrit ) / nbreInscritsDebut.size() )  * 100  : 0 ;

						List<SouscriptionProgrammeFideliteTampon> nbrePerdu = souscriptionProgrammeFideliteTamponRepository.findByProgrammeFideliteTamponIdAndCreatedAtBetween(programmeFideliteTamponId, dateDebutNew, dateFinNew, true) ;
						tauxAttrition = Utilities.isNotEmpty(nbrePerdu) ? (nbrePerdu.size() / nbreInscritsDebut.size()) * 100 : 0 ;
					}

					// nps
					List<NetPromoterScorUser> allNps = netPromoterScorUserRepository.findByProgrammeFideliteTamponId(programmeFideliteTamponId, false) ; 
					List<NetPromoterScorUser> nbrePromoters = netPromoterScorUserRepository.findByPdfTamponIdAndTypeNoteNpsCodeAndCreatedAtBetween(programmeFideliteTamponId, TypeNoteNpsEnum.PROMOTERS, dateDebutNew, dateFinNew, false) ;
					List<NetPromoterScorUser> nbreDetractors = netPromoterScorUserRepository.findByPdfTamponIdAndTypeNoteNpsCodeAndCreatedAtBetween(programmeFideliteTamponId, TypeNoteNpsEnum.DETRACTORS, dateDebutNew, dateFinNew, false) ;

					if (Utilities.isNotEmpty(allNps)) {
						int npsDetractors = Utilities.isNotEmpty(nbreDetractors) ? nbreDetractors.size() : 0 ;
						int npsPromoters = Utilities.isNotEmpty(nbrePromoters) ? nbrePromoters.size() : 0 ;
						nps =  ((npsPromoters - npsDetractors) / allNps.size()) * 100;
					} 




					if (isPdvIdNotNull) {
						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndPdvIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteTamponId, pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						Long nbreRecompensesEngages = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndPdvIdAndCreatedAtBetweenForEngager(programmeFideliteTamponId, pointDeVenteId, dateDebutNew, dateFinNew, false) ;
						if (nbreRecompensesEngages != null) {
							recompensesEngagees = nbreRecompensesEngages ;
						}

						// recompense rembourse
						Long nbreRecompensesRembourses = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndPdvIdAndCreatedAtBetweenForRembourser(programmeFideliteTamponId, pointDeVenteId, dateDebutNew, dateFinNew, false) ;

						if (nbreRecompensesRembourses != null) {
							recompensesRemboursees = nbreRecompensesRembourses ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire pas prix en compte


					}else {

						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndTypeActionCodeAndCreatedAtBetween(programmeFideliteTamponId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						Long nbreRecompensesEngages = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndCreatedAtBetweenForEngager(programmeFideliteTamponId, dateDebutNew, dateFinNew, false) ;
						if (nbreRecompensesEngages != null) {
							recompensesEngagees = nbreRecompensesEngages ;
						}

						// recompense rembourse
						Long nbreRecompensesRembourses = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdfTamponIdAndCreatedAtBetweenForRembourser(programmeFideliteTamponId, dateDebutNew, dateFinNew, false) ;

						if (nbreRecompensesRembourses != null) {
							recompensesRemboursees = nbreRecompensesRembourses ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;
						// chiffre d'affaire pas prix en compte

					}		
				}else {
					// nbre inscrit
					List<SouscriptionProgrammeFideliteTampon> nbreInscrits = souscriptionProgrammeFideliteTamponRepository.findByCreatedAtBetween(dateDebutNew, dateFinNew, false) ;
					nbreInscrit = Utilities.isNotEmpty(nbreInscrits) ? nbreInscrits.size() : 0 ;

					// taux attrition && taux retention 
					List<SouscriptionProgrammeFideliteTampon> nbreInscritsDebut = souscriptionProgrammeFideliteTamponRepository.findByCreatedAtBefore(dateDebutNew, false) ;

					if (Utilities.isNotEmpty(nbreInscritsDebut)) {
						List<SouscriptionProgrammeFideliteTampon> nbreInscritsFin = souscriptionProgrammeFideliteTamponRepository.findByCreatedAtBefore(dateFinNew, false) ;
						tauxRetention = Utilities.isNotEmpty(nbreInscritsDebut) ? ( ( nbreInscritsFin.size() -  nbreInscrit ) / nbreInscritsDebut.size() )  * 100  : 0 ;

						List<SouscriptionProgrammeFideliteTampon> nbrePerdu = souscriptionProgrammeFideliteTamponRepository.findByCreatedAtBetween(dateDebutNew, dateFinNew, true) ;
						tauxAttrition = Utilities.isNotEmpty(nbrePerdu) ? (nbrePerdu.size() / nbreInscritsDebut.size()) * 100 : 0 ;
					}

					// nps
					List<NetPromoterScorUser> allNps = netPromoterScorUserRepository.findByIsDeletedWithPdfTamponNotNull(false) ; 
					List<NetPromoterScorUser> nbrePromoters = netPromoterScorUserRepository.findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfTamponNotNull(TypeNoteNpsEnum.PROMOTERS, dateDebutNew, dateFinNew, false) ;
					List<NetPromoterScorUser> nbreDetractors = netPromoterScorUserRepository.findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfTamponNotNull(TypeNoteNpsEnum.DETRACTORS, dateDebutNew, dateFinNew, false) ;

					if (Utilities.isNotEmpty(allNps)) {
						int npsDetractors = Utilities.isNotEmpty(nbreDetractors) ? nbreDetractors.size() : 0 ;
						int npsPromoters = Utilities.isNotEmpty(nbrePromoters) ? nbrePromoters.size() : 0 ;
						nps =  ((npsPromoters - npsDetractors) / allNps.size()) * 100;
					} 

					if (isPdvIdNotNull) {

						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdvIdAndTypeActionCodeAndCreatedAtBetween(pointDeVenteId, TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						Long nbreRecompensesEngages = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdvIdAndCreatedAtBetweenForEngager(pointDeVenteId, dateDebutNew, dateFinNew, false) ;
						if (nbreRecompensesEngages != null) {
							recompensesEngagees = nbreRecompensesEngages ;
						}

						// recompense rembourse
						Long nbreRecompensesRembourses = historiqueCarteProgrammeFideliteTamponRepository.countUserByPdvIdAndCreatedAtBetweenForRembourser(pointDeVenteId, dateDebutNew, dateFinNew, false) ;

						if (nbreRecompensesRembourses != null) {
							recompensesRemboursees = nbreRecompensesRembourses ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire pas prix en compte



					}else {

						// taux  reachat
						Long numerateurTauxReachat = historiqueCarteProgrammeFideliteTamponRepository.countUserByTypeActionCodeAndCreatedAtBetween(TypeActionEnum.DEBITER, TypeActionEnum.CREDITER, dateDebutNew, dateFinNew, false) ;
						if (Utilities.isNotEmpty(nbreInscritsDebut)) {
							tauxReachat = (numerateurTauxReachat / nbreInscritsDebut.size()) * 100 ;
						}

						// recompense engages
						Long nbreRecompensesEngages = historiqueCarteProgrammeFideliteTamponRepository.countUserByCreatedAtBetweenForEngager(dateDebutNew, dateFinNew, false) ;
						if (nbreRecompensesEngages != null) {
							recompensesEngagees = nbreRecompensesEngages ;
						}

						// recompense rembourse
						Long nbreRecompensesRembourses = historiqueCarteProgrammeFideliteTamponRepository.countUserByCreatedAtBetweenForRembourser(dateDebutNew, dateFinNew, false) ;

						if (nbreRecompensesRembourses != null) {
							recompensesRemboursees = nbreRecompensesRembourses ;
						}

						// recompense en attente de remboursement
						recompensesEnAttente = recompensesEngagees - recompensesRemboursees ;

						// chiffre d'affaire pas prix en compte

					}
				}
				recompenseEngagePdfTampon.add(recompensesEngagees) ;
				recompenseRemboursePdfTampon.add(recompensesRemboursees) ;
				recompenseEnAttentePdfTampon.add(recompensesEnAttente) ;
				npsPdfTampon.add(nps) ;
				tauxRetentioPdfTampon.add(tauxRetention) ;
				tauxAttrioPdfTampon.add(tauxAttrition) ;
				tauxReachatPdfTampon.add(tauxReachat) ;
				//chiffreDaffairePdfTampon.add(chiffreDaffaire) ;
				nbreInscritPdfTampon.add(nbreInscrit) ;




				System.out.println("dateDebutNew " +  dateDebutNew);
				System.out.println("dateFinNew " +  dateFinNew);


				abscisseGraphe.add(xGraphe) ;
			}


			// setter les valeurs
			itemDto.setAbscisseGraphe(abscisseGraphe);

			itemDto.setNbreTotalTransaction(nbreTotalTransaction);
			itemDto.setNbreTransactionParPdfTampon(nbreTransactionParPdfTampon);
			itemDto.setNbreTransactionParPdfCarte(nbreTransactionParPdfCarte);
			itemDto.setNbreTransactionPointDeVente(nbreTransactionPointDeVente);

			itemDto.setInscritParPdfCarte(inscritParPdfCarte);
			itemDto.setLegendPdfCarte(legendPdfCarte);
			itemDto.setInscritParPdfTampon(inscritParPdfTampon);
			itemDto.setLegendPdfTampon(legendPdfTampon);
			itemDto.setChiffreDaffaireParPdfCarte(chiffreDaffaireParPdfCarte);
			itemDto.setLegendCaPdfCarte(legendCaPdfCarte);


			itemDto.setChiffreDaffairePdfCarte(chiffreDaffairePdfCarte);
			itemDto.setNbreInscritPdfCarte(nbreInscritPdfCarte);
			itemDto.setNbreInscritPdfTampon(nbreInscritPdfTampon);
			itemDto.setTauxReachatPdfCarte(tauxReachatPdfCarte);
			itemDto.setTauxReachatPdfTampon(tauxReachatPdfTampon);
			itemDto.setTauxAttrioPdfCarte(tauxAttrioPdfCarte);
			itemDto.setTauxAttrioPdfTampon(tauxAttrioPdfTampon);
			itemDto.setTauxRetentioPdfCarte(tauxRetentioPdfCarte);
			itemDto.setTauxRetentioPdfTampon(tauxRetentioPdfTampon);
			itemDto.setNpsPdfCarte(npsPdfCarte);
			itemDto.setNpsPdfTampon(npsPdfTampon);
			itemDto.setRecompenseEnAttentePdfCarte(recompenseEnAttentePdfCarte);
			itemDto.setRecompenseEnAttentePdfTampon(recompenseEnAttentePdfTampon);
			itemDto.setRecompenseRemboursePdfCarte(recompenseRemboursePdfCarte);
			itemDto.setRecompenseRemboursePdfTampon(recompenseRemboursePdfTampon);
			itemDto.setRecompenseEngagePdfCarte(recompenseEngagePdfCarte);
			itemDto.setRecompenseEngagePdfTampon(recompenseEngagePdfTampon);


			response.setItems(Arrays.asList(itemDto));
			response.setHasError(false);

			slf4jLogger.info("----end getStatistiques Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}




	/**
	 * get Enseigne by using EnseigneDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<EnseigneDto> 
	getByCriteria(Request<EnseigneDto> request, Locale locale) {
		slf4jLogger.info("----begin get Enseigne-----");

		response = new Response<EnseigneDto>();

		try {
			List<Enseigne> items = null;
			items = enseigneRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<EnseigneDto> itemsDto = new ArrayList<EnseigneDto>();
				for (Enseigne entity : items) {
					EnseigneDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(enseigneRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("enseigne", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get Enseigne-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	

	/**
	 * get full EnseigneDto by using Enseigne as object.
	 *
	 * @param entity, locale
	 * @return EnseigneDto
	 *
	 */
	private EnseigneDto getFullInfos(Enseigne entity, Integer size, Locale locale) throws Exception {
		EnseigneDto dto = EnseigneTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		// recuperer l'etat courant de l'enseigne
		EtatInscripitionEnseigne etatInscripitionEnseigne = etatInscripitionEnseigneRepository.findEtatInscripitionEnseigneByEnseigneIdAndIsValid(dto.getId(), true, false) ;
		if (etatInscripitionEnseigne != null) {
			dto.setEtatInscripitionCode(etatInscripitionEnseigne.getEtatInscripition().getCode());
			dto.setEtatInscripitionLibelle(etatInscripitionEnseigne.getEtatInscripition().getLibelle());
		}

		if (dto.getUrlLogo() != null) {
			dto.setUrlLogo(paramsUtils.getBaseUrlImageApp() + dto.getUrlLogo());
		}
		if (size > 1) {
			return dto;
		}

		List<SecteurActiviteEnseigne> datasSecteurActiviteEnseigne = secteurActiviteEnseigneRepository.findByEnseigneId(dto.getId(), false);

		if (Utilities.isNotEmpty(datasSecteurActiviteEnseigne)) {
			List<SecteurActivite>  datasSecteurActivite = new ArrayList<SecteurActivite>() ;
			for (SecteurActiviteEnseigne secteurActiviteEnseigne : datasSecteurActiviteEnseigne) {
				datasSecteurActivite.add(secteurActiviteEnseigne.getSecteurActivite()) ;
			}
			dto.setDatasSecteurActivite(SecteurActiviteTransformer.INSTANCE.toDtos(datasSecteurActivite));
		}

		return dto;
	}
}
