


/*
 * Java transformer for entity table delit_programme_fidelite
 * Created on 2021-08-16 ( Time 17:55:17 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "delit_programme_fidelite"
 *
 * @author Back-End developper
 *
 */
@Component
public class DelitProgrammeFideliteBusiness implements IBasicBusiness<Request<DelitProgrammeFideliteDto>, Response<DelitProgrammeFideliteDto>> {

	private Response<DelitProgrammeFideliteDto> response;
	@Autowired
	private DelitProgrammeFideliteRepository delitProgrammeFideliteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private InformationRegleSecuriteTamponRepository informationRegleSecuriteTamponRepository;
	@Autowired
	private InformationRegleSecuriteCarteRepository informationRegleSecuriteCarteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteCarteRepository souscriptionProgrammeFideliteCarteRepository;
	@Autowired
	private SouscriptionProgrammeFideliteTamponRepository souscriptionProgrammeFideliteTamponRepository;
	@Autowired
	private PointDeVenteRepository pointDeVenteRepository;
	@Autowired
	private CarteRepository carteRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;



	public DelitProgrammeFideliteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DelitProgrammeFideliteDto> create(Request<DelitProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin create DelitProgrammeFidelite-----");

		response = new Response<DelitProgrammeFideliteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DelitProgrammeFidelite> items = new ArrayList<DelitProgrammeFidelite>();

			for (DelitProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("dateDelit", dto.getDateDelit());
				fieldsToVerify.put("heureDelit", dto.getHeureDelit());
				fieldsToVerify.put("numeroDelit", dto.getNumeroDelit());
				fieldsToVerify.put("isNotifed", dto.getIsNotifed());
				fieldsToVerify.put("isApplicateSanction", dto.getIsApplicateSanction());
				fieldsToVerify.put("carteId", dto.getCarteId());
				fieldsToVerify.put("informationRegleSecuriteCarteId", dto.getInformationRegleSecuriteCarteId());
				fieldsToVerify.put("informationRegleSecuriteTamponId", dto.getInformationRegleSecuriteTamponId());
				fieldsToVerify.put("pointDeVenteId", dto.getPointDeVenteId());
				fieldsToVerify.put("createdBy", dto.getCreatedBy());
				fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if delitProgrammeFidelite to insert do not exist
				DelitProgrammeFidelite existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("delitProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if informationRegleSecuriteTampon exist
				InformationRegleSecuriteTampon existingInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findById(dto.getInformationRegleSecuriteTamponId(), false);
				if (existingInformationRegleSecuriteTampon == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteTampon -> " + dto.getInformationRegleSecuriteTamponId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if informationRegleSecuriteCarte exist
				InformationRegleSecuriteCarte existingInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findById(dto.getInformationRegleSecuriteCarteId(), false);
				if (existingInformationRegleSecuriteCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("informationRegleSecuriteCarte -> " + dto.getInformationRegleSecuriteCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if pointDeVente exist
				PointDeVente existingPointDeVente = pointDeVenteRepository.findById(dto.getPointDeVenteId(), false);
				if (existingPointDeVente == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pointDeVente -> " + dto.getPointDeVenteId(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if carte exist
				Carte existingCarte = carteRepository.findById(dto.getCarteId(), false);
				if (existingCarte == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("carte -> " + dto.getCarteId(), locale));
					response.setHasError(true);
					return response;
				}
				DelitProgrammeFidelite entityToSave = null;
				entityToSave = DelitProgrammeFideliteTransformer.INSTANCE.toEntity(dto, existingInformationRegleSecuriteTampon, existingInformationRegleSecuriteCarte, existingPointDeVente, existingCarte);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<DelitProgrammeFidelite> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = delitProgrammeFideliteRepository.saveAll((Iterable<DelitProgrammeFidelite>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("delitProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}
				List<DelitProgrammeFideliteDto> itemsDto = new ArrayList<DelitProgrammeFideliteDto>();
				for (DelitProgrammeFidelite entity : itemsSaved) {
					DelitProgrammeFideliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DelitProgrammeFideliteDto> update(Request<DelitProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin update DelitProgrammeFidelite-----");

		response = new Response<DelitProgrammeFideliteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DelitProgrammeFidelite> items = new ArrayList<DelitProgrammeFidelite>();

			for (DelitProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la delitProgrammeFidelite existe
				DelitProgrammeFidelite entityToSave = null;
				entityToSave = delitProgrammeFideliteRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("delitProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Integer entityToSaveId = entityToSave.getId();
				Integer pdfTamponId = null ;
				Integer pdfCarteId = null  ;

				InformationRegleSecuriteCarte existingInformationRegleSecuriteCarte ;
				InformationRegleSecuriteTampon existingInformationRegleSecuriteTampon ;

				if (dto.getInformationRegleSecuriteCarteId() != null && dto.getInformationRegleSecuriteCarteId() > 0) {
					existingInformationRegleSecuriteCarte = informationRegleSecuriteCarteRepository.findById(dto.getInformationRegleSecuriteCarteId(), false);
					if (existingInformationRegleSecuriteCarte != null) {
						pdfCarteId = existingInformationRegleSecuriteCarte.getProgrammeFideliteCarte().getId() ;
					}
				} else {
					if (dto.getInformationRegleSecuriteTamponId() != null && dto.getInformationRegleSecuriteTamponId() > 0) {
						existingInformationRegleSecuriteTampon = informationRegleSecuriteTamponRepository.findById(dto.getInformationRegleSecuriteTamponId(), false);
						if (existingInformationRegleSecuriteTampon != null) {
							pdfTamponId = existingInformationRegleSecuriteTampon.getProgrammeFideliteTampon().getId() ;
						}
					}
				}



				if (dto.getIsBlocageUserInPDFAndPDV() != null ) {

					List<DelitProgrammeFidelite> delitProgrammeFidelites = new ArrayList<>() ;

					if (pdfCarteId != null) {
						delitProgrammeFidelites = delitProgrammeFideliteRepository.findByCarteIdAndPdvIdAndPdfCarteId(dto.getCarteId(), dto.getPointDeVenteId(), pdfCarteId, false) ;

					} else {
						if (pdfTamponId != null) {
							delitProgrammeFidelites = delitProgrammeFideliteRepository.findByCarteIdAndPdvIdAndPdfTamponId(dto.getCarteId(), dto.getPointDeVenteId(), pdfTamponId, false) ;
						}
					}
					if (Utilities.isNotEmpty(delitProgrammeFidelites)) {
						for (DelitProgrammeFidelite data : delitProgrammeFidelites) {
							data.setIsApplicateSanction(dto.getIsBlocageUserInPDFAndPDV());
							data.setUpdatedBy(request.getUser());
							data.setUpdatedAt(Utilities.getCurrentDate());
							if (Utilities.notBlank(dto.getMessageLocked()) ) {
								data.setMessageLocked(dto.getMessageLocked());
							}
						}
						items.addAll(delitProgrammeFidelites);
					}
				} else {
					if (dto.getIsNotifed() != null) {
						entityToSave.setIsNotifed(dto.getIsNotifed());
					}
					if (Utilities.notBlank(dto.getMessageNotifed()) ) {
						entityToSave.setMessageNotifed(dto.getMessageNotifed());
					}
					entityToSave.setUpdatedBy(request.getUser());
					entityToSave.setUpdatedAt(Utilities.getCurrentDate());
					items.add(entityToSave);
				}

				if (pdfCarteId != null) {

					SouscriptionProgrammeFideliteCarte souscriptionProgrammeFideliteCarte = souscriptionProgrammeFideliteCarteRepository.findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(dto.getCarteId(), pdfCarteId, false) ;

					if (souscriptionProgrammeFideliteCarte != null) {
						souscriptionProgrammeFideliteCarte.setUpdatedBy(request.getUser());
						souscriptionProgrammeFideliteCarte.setUpdatedAt(Utilities.getCurrentDate());

						if (dto.getIsBlocageUserInPDFAndPDV() != null) {
							souscriptionProgrammeFideliteCarte.setIsLocked(dto.getIsBlocageUserInPDFAndPDV());
							if (Utilities.notBlank(dto.getMessageLocked()) ) {
								souscriptionProgrammeFideliteCarte.setRaisonLocked(dto.getMessageLocked());
							}
						}else {
							if (dto.getIsNotifed() != null) {
								souscriptionProgrammeFideliteCarte.setIsNotifed(dto.getIsNotifed());
							}
							if (Utilities.notBlank(dto.getMessageNotifed()) ) {
								souscriptionProgrammeFideliteCarte.setMessageNotifed(dto.getMessageNotifed());
							}
						}

						souscriptionProgrammeFideliteCarteRepository.save(souscriptionProgrammeFideliteCarte) ;
					}
				} else {
					if (pdfTamponId != null) {
						SouscriptionProgrammeFideliteTampon souscriptionProgrammeFideliteTampon = souscriptionProgrammeFideliteTamponRepository.findSouscriptionProgrammeFideliteTamponByCarteIdAndProgrammeFideliteTamponId(dto.getCarteId(), pdfTamponId, false) ;

						if (souscriptionProgrammeFideliteTampon != null) {
							souscriptionProgrammeFideliteTampon.setUpdatedBy(request.getUser());
							souscriptionProgrammeFideliteTampon.setUpdatedAt(Utilities.getCurrentDate());

							if (dto.getIsBlocageUserInPDFAndPDV() != null) {
								souscriptionProgrammeFideliteTampon.setIsLocked(dto.getIsBlocageUserInPDFAndPDV());
								if (Utilities.notBlank(dto.getMessageLocked()) ) {
									souscriptionProgrammeFideliteTampon.setRaisonLocked(dto.getMessageLocked());
								}
							}else {
								if (dto.getIsNotifed() != null) {
									souscriptionProgrammeFideliteTampon.setIsNotifed(dto.getIsNotifed());
								}
								if (Utilities.notBlank(dto.getMessageNotifed()) ) {
									souscriptionProgrammeFideliteTampon.setMessageNotifed(dto.getMessageNotifed());
								}
							}
							souscriptionProgrammeFideliteTamponRepository.save(souscriptionProgrammeFideliteTampon) ;
						}
					}
				}
			}

			if (!items.isEmpty()) {
				List<DelitProgrammeFidelite> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = delitProgrammeFideliteRepository.saveAll((Iterable<DelitProgrammeFidelite>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("delitProgrammeFidelite", locale));
					response.setHasError(true);
					return response;
				}
				List<DelitProgrammeFideliteDto> itemsDto = new ArrayList<DelitProgrammeFideliteDto>();
				for (DelitProgrammeFidelite entity : itemsSaved) {
					DelitProgrammeFideliteDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<DelitProgrammeFideliteDto> delete(Request<DelitProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete DelitProgrammeFidelite-----");

		response = new Response<DelitProgrammeFideliteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DelitProgrammeFidelite> items = new ArrayList<DelitProgrammeFidelite>();

			for (DelitProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la delitProgrammeFidelite existe
				DelitProgrammeFidelite existingEntity = null;
				existingEntity = delitProgrammeFideliteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("delitProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				delitProgrammeFideliteRepository.saveAll((Iterable<DelitProgrammeFidelite>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<DelitProgrammeFideliteDto> forceDelete(Request<DelitProgrammeFideliteDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete DelitProgrammeFidelite-----");

		response = new Response<DelitProgrammeFideliteDto>();

		try {
			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			fieldsToVerifyUser.put("user", request.getUser());
			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			User utilisateur = userRepository.findById(request.getUser(), false);
			if (utilisateur == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
				response.setHasError(true);
				return response;
			}
			if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
				response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
				response.setHasError(true);
				return response;
			}

			List<DelitProgrammeFidelite> items = new ArrayList<DelitProgrammeFidelite>();

			for (DelitProgrammeFideliteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la delitProgrammeFidelite existe
				DelitProgrammeFidelite existingEntity = null;
				existingEntity = delitProgrammeFideliteRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("delitProgrammeFidelite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				delitProgrammeFideliteRepository.saveAll((Iterable<DelitProgrammeFidelite>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<DelitProgrammeFideliteDto> getByCriteria(Request<DelitProgrammeFideliteDto> request, Locale locale) {
		slf4jLogger.info("----begin get DelitProgrammeFidelite-----");

		response = new Response<DelitProgrammeFideliteDto>();

		try {
			List<DelitProgrammeFidelite> items = null;
			items = delitProgrammeFideliteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<DelitProgrammeFideliteDto> itemsDto = new ArrayList<DelitProgrammeFideliteDto>();
				for (DelitProgrammeFidelite entity : items) {
					DelitProgrammeFideliteDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(delitProgrammeFideliteRepository.countCustom(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("delitProgrammeFidelite", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * get DelitProgrammeFidelite by using DelitProgrammeFideliteDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response<UserDto> getByCriteriaCustom(Request<DelitProgrammeFideliteDto> request, Locale locale) {
		slf4jLogger.info("----begin getByCriteriaCustom DelitProgrammeFidelite-----");

		Response<UserDto> responseCustom = new Response<UserDto>();

		try {
			List<User> items = null;
			items = delitProgrammeFideliteRepository.getByCriteriaCustom(request, em, locale);
			if (items != null && !items.isEmpty()) {
				//List<UserDto> itemsDto = new ArrayList<UserDto>();

				responseCustom.setItems(UserTransformer.INSTANCE.toDtos(items));
				responseCustom.setCount(delitProgrammeFideliteRepository.count(request, em, locale));
				responseCustom.setHasError(false);
			} else {
				responseCustom.setStatus(functionalError.DATA_EMPTY("delitProgrammeFidelite", locale));
				responseCustom.setHasError(false);
				return responseCustom;
			}

			slf4jLogger.info("----end getByCriteriaCustom DelitProgrammeFidelite-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(responseCustom, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(responseCustom, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(responseCustom, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(responseCustom, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(responseCustom, locale, e);
		} finally {
			if (responseCustom.isHasError() && responseCustom.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", responseCustom.getStatus().getCode(), responseCustom.getStatus().getMessage());
				throw new RuntimeException(responseCustom.getStatus().getCode() + ";" + responseCustom.getStatus().getMessage());
			}
		}
		return responseCustom;
	}

	/**
	 * get full DelitProgrammeFideliteDto by using DelitProgrammeFidelite as object.
	 *
	 * @param entity, locale
	 * @return DelitProgrammeFideliteDto
	 *
	 */
	private DelitProgrammeFideliteDto getFullInfos(DelitProgrammeFidelite entity, Integer size, Locale locale) throws Exception {
		DelitProgrammeFideliteDto dto = DelitProgrammeFideliteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
