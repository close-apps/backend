


/*
 * Java transformer for entity table etat_inscripition
 * Created on 2020-08-17 ( Time 14:14:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2020 SalaamSolutions. All Rights Reserved.
 */

package com.closeapps.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.transformer.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.IBasicBusiness;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.dao.entity.*;
import com.closeapps.helper.*;
import com.closeapps.dao.repository.*;

/**
BUSINESS for table "etat_inscripition"
 *
 * @author Back-End developper
 *
 */
@Component
public class EtatInscripitionBusiness implements IBasicBusiness<Request<EtatInscripitionDto>, Response<EtatInscripitionDto>> {

	private Response<EtatInscripitionDto> response;
	@Autowired
	private EtatInscripitionRepository etatInscripitionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EtatInscripitionEnseigneRepository etatInscripitionEnseigneRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private Logger slf4jLogger;
	private SimpleDateFormat dateFormat;


	@Autowired
	private EtatInscripitionEnseigneBusiness etatInscripitionEnseigneBusiness;



	public EtatInscripitionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}


	/**
	 * create EtatInscripition by using EtatInscripitionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatInscripitionDto> create(Request<EtatInscripitionDto> request, Locale locale)  {
		slf4jLogger.info("----begin create EtatInscripition-----");

		response = new Response<EtatInscripitionDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {


				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<EtatInscripition> items = new ArrayList<EtatInscripition>();

			for (EtatInscripitionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("code", dto.getCode());

				//        fieldsToVerify.put("createdBy", dto.getCreatedBy());
				//        fieldsToVerify.put("updatedBy", dto.getUpdatedBy());
				//        fieldsToVerify.put("deletedBy", dto.getDeletedBy());
				//        fieldsToVerify.put("deletedAt", dto.getDeletedAt());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verify if etatInscripition to insert do not exist
				EtatInscripition existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etatInscripition -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = etatInscripitionRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etatInscripition -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les etatInscripitions", locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = etatInscripitionRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("etatInscripition -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les etatInscripitions", locale));
					response.setHasError(true);
					return response;
				}

				EtatInscripition entityToSave = null;
				entityToSave = EtatInscripitionTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<EtatInscripition> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = etatInscripitionRepository.saveAll((Iterable<EtatInscripition>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("etatInscripition", locale));
					response.setHasError(true);
					return response;
				}
				List<EtatInscripitionDto> itemsDto = new ArrayList<EtatInscripitionDto>();
				for (EtatInscripition entity : itemsSaved) {
					EtatInscripitionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end create EtatInscripition-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update EtatInscripition by using EtatInscripitionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatInscripitionDto> update(Request<EtatInscripitionDto> request, Locale locale)  {
		slf4jLogger.info("----begin update EtatInscripition-----");

		response = new Response<EtatInscripitionDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {


				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<EtatInscripition> items = new ArrayList<EtatInscripition>();

			for (EtatInscripitionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etatInscripition existe
				EtatInscripition entityToSave = null;
				entityToSave = etatInscripitionRepository.findById(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etatInscripition -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}


				Integer entityToSaveId = entityToSave.getId();

				if (Utilities.notBlank(dto.getLibelle())) {
					EtatInscripition existingEntity = etatInscripitionRepository.findByLibelle(dto.getLibelle(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("etatInscripition -> " + dto.getLibelle(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les etatInscripitions", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (Utilities.notBlank(dto.getCode())) {
					EtatInscripition existingEntity = etatInscripitionRepository.findByCode(dto.getCode(), false);
					if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
						response.setStatus(functionalError.DATA_EXIST("etatInscripition -> " + dto.getCode(), locale));
						response.setHasError(true);
						return response;
					}
					if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les etatInscripitions", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCode(dto.getCode());
				}
				//        if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				//          entityToSave.setCreatedBy(dto.getCreatedBy());
				//        }
				//        if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				//          entityToSave.setUpdatedBy(dto.getUpdatedBy());
				//        }
				//        if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				//          entityToSave.setDeletedBy(dto.getDeletedBy());
				//        }
				//        if (Utilities.notBlank(dto.getDeletedAt())) {
				//          entityToSave.setDeletedAt(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeletedAt()));
				//        }
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<EtatInscripition> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = etatInscripitionRepository.saveAll((Iterable<EtatInscripition>) items);
				if (itemsSaved == null || itemsSaved.isEmpty()) {
					response.setStatus(functionalError.SAVE_FAIL("etatInscripition", locale));
					response.setHasError(true);
					return response;
				}
				List<EtatInscripitionDto> itemsDto = new ArrayList<EtatInscripitionDto>();
				for (EtatInscripition entity : itemsSaved) {
					EtatInscripitionDto dto = getFullInfos(entity, itemsSaved.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end update EtatInscripition-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * delete EtatInscripition by using EtatInscripitionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<EtatInscripitionDto> delete(Request<EtatInscripitionDto> request, Locale locale)  {
		slf4jLogger.info("----begin delete EtatInscripition-----");

		response = new Response<EtatInscripitionDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {


				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<EtatInscripition> items = new ArrayList<EtatInscripition>();

			for (EtatInscripitionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etatInscripition existe
				EtatInscripition existingEntity = null;
				existingEntity = etatInscripitionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etatInscripition -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// etatInscripitionEnseigne
				List<EtatInscripitionEnseigne> listOfEtatInscripitionEnseigne = etatInscripitionEnseigneRepository.findByEtatInscripitionId(existingEntity.getId(), false);
				if (listOfEtatInscripitionEnseigne != null && !listOfEtatInscripitionEnseigne.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEtatInscripitionEnseigne.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				etatInscripitionRepository.saveAll((Iterable<EtatInscripition>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end delete EtatInscripition-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}


	/**
	 * forceDelete EtatInscripition by using EtatInscripitionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	//@Override
	public Response<EtatInscripitionDto> forceDelete(Request<EtatInscripitionDto> request, Locale locale)  {
		slf4jLogger.info("----begin forceDelete EtatInscripition-----");

		response = new Response<EtatInscripitionDto>();

		try {
			//			Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
			//			fieldsToVerifyUser.put("user", request.getUser());
			//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
			//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
			//				response.setHasError(true);
			//				return response;
			//			}

			if (request.getUser() != null && request.getUser() > 0 ) {


				User utilisateur = userRepository.findById(request.getUser(), false);
				if (utilisateur == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
					response.setHasError(true);
					return response;
				}
				if (utilisateur.getIsLocked() != null && utilisateur.getIsLocked()) {
					response.setStatus(functionalError.USER_IS_LOCKED("L'utilisateur "+utilisateur.getLogin()+" "+" est verouille(e)" , locale));
					response.setHasError(true);
					return response;
				}
			}

			List<EtatInscripition> items = new ArrayList<EtatInscripition>();

			for (EtatInscripitionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				//fieldsToVerify.put("user", request.getUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				/*
        // Verify if utilisateur exist
        User utilisateur = userRepository.findById(request.getUser(), false);
        if (utilisateur == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur -> " + request.getUser(), locale));
          response.setHasError(true);
          return response;
        }
				 */

				// Verifier si la etatInscripition existe
				EtatInscripition existingEntity = null;
				existingEntity = etatInscripitionRepository.findById(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("etatInscripition -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// etatInscripitionEnseigne
				List<EtatInscripitionEnseigne> listOfEtatInscripitionEnseigne = etatInscripitionEnseigneRepository.findByEtatInscripitionId(existingEntity.getId(), false);
				if (listOfEtatInscripitionEnseigne != null && !listOfEtatInscripitionEnseigne.isEmpty()){
					Request<EtatInscripitionEnseigneDto> deleteRequest = new Request<EtatInscripitionEnseigneDto>();
					deleteRequest.setDatas(EtatInscripitionEnseigneTransformer.INSTANCE.toDtos(listOfEtatInscripitionEnseigne));
					deleteRequest.setUser(request.getUser());
					Response<EtatInscripitionEnseigneDto> deleteResponse = etatInscripitionEnseigneBusiness.delete(deleteRequest, locale);
					if(deleteResponse.isHasError()){
						response.setStatus(deleteResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}


				existingEntity.setDeletedBy(request.getUser());
				existingEntity.setDeletedAt(Utilities.getCurrentDate());
				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				etatInscripitionRepository.saveAll((Iterable<EtatInscripition>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end forceDelete EtatInscripition-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}



	/**
	 * get EtatInscripition by using EtatInscripitionDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<EtatInscripitionDto> getByCriteria(Request<EtatInscripitionDto> request, Locale locale) {
		slf4jLogger.info("----begin get EtatInscripition-----");

		response = new Response<EtatInscripitionDto>();

		try {
			List<EtatInscripition> items = null;
			items = etatInscripitionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<EtatInscripitionDto> itemsDto = new ArrayList<EtatInscripitionDto>();
				for (EtatInscripition entity : items) {
					EtatInscripitionDto dto = getFullInfos(entity, items.size(), locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItems(itemsDto);
				response.setCount(etatInscripitionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("etatInscripition", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end get EtatInscripition-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full EtatInscripitionDto by using EtatInscripition as object.
	 *
	 * @param entity, locale
	 * @return EtatInscripitionDto
	 *
	 */
	private EtatInscripitionDto getFullInfos(EtatInscripition entity, Integer size, Locale locale) throws Exception {
		EtatInscripitionDto dto = EtatInscripitionTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
