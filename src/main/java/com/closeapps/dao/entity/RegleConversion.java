/*
 * Created on 2020-08-17 ( Time 14:14:49 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.closeapps.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

import lombok.*;

/**
 * Persistent class for entity stored in table "regle_conversion"
 *
* @author Back-End developper
   *
 */

@Entity
@Table(name="regle_conversion" )
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class RegleConversion implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * 
	 */
    @Column(name="valeur_acheter", length=255)
    private String     valeurAcheter ;

	/*
	 * 
	 */
    @Column(name="point_gagner", length=255)
    private String     pointGagner  ;

	/*
	 * 
	 */
    @Column(name="somme_correspondant", length=255)
    private String     sommeCorrespondant ;

	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	// "programmeFideliteCarteId" (column "programme_fidelite_carte_id") is not defined by itself because used as FK in a link 
	// "produitId" (column "produit_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="produit_id", referencedColumnName="id")
        private Produit produit;
    @ManyToOne
    @JoinColumn(name="programme_fidelite_carte_id", referencedColumnName="id")
        private ProgrammeFideliteCarte programmeFideliteCarte;

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
