/*
 * Created on 2021-08-18 ( Time 16:55:12 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.closeapps.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

import lombok.*;

/**
 * Persistent class for entity stored in table "souscription_programme_fidelite_tampon"
 *
* @author Back-End developper
   *
 */

@Entity
@Table(name="souscription_programme_fidelite_tampon" )
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class SouscriptionProgrammeFideliteTampon implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * pour distinguer les users chez le client
	 */
    @Column(name="code", length=255)
    private String     code         ;

	/*
	 * 
	 */
    @Column(name="libelle", length=255)
    private String     libelle      ;

	/*
	 * 
	 */
    @Column(name="is_show_last_notification")
    private Boolean    isShowLastNotification ;

	/*
	 * 
	 */
    @Column(name="is_locked")
    private Boolean    isLocked     ;

	/*
	 * 
	 */
    @Column(name="raison_locked")
    private String     raisonLocked ;

	/*
	 * 
	 */
    @Column(name="unsubscribe")
    private Boolean    unsubscribe  ;

	/*
	 * 
	 */
    @Column(name="raison_unsubscribe")
    private String     raisonUnsubscribe ;

	/*
	 * 
	 */
    @Column(name="is_notifed")
    private Boolean    isNotifed    ;

	/*
	 * 
	 */
    @Column(name="message_notifed")
    private String     messageNotifed ;

	/*
	 * 
	 */
    @Column(name="nbre_tampon_actuelle")
    private Integer    nbreTamponActuelle ;

	/*
	 * 
	 */
    @Column(name="nbre_tampon_precedent")
    private Integer    nbreTamponPrecedent ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_souscription")
    private Date       dateSouscription ;

	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	// "carteId" (column "carte_id") is not defined by itself because used as FK in a link 
	// "programmeFideliteTamponId" (column "programme_fidelite_tampon_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="programme_fidelite_tampon_id", referencedColumnName="id")
        private ProgrammeFideliteTampon programmeFideliteTampon;
    @ManyToOne
    @JoinColumn(name="carte_id", referencedColumnName="id")
        private Carte carte;

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
