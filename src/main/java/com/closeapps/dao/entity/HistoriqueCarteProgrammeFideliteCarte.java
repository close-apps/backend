/*
 * Created on 2021-08-26 ( Time 17:47:23 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.closeapps.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.*;

/**
 * Persistent class for entity stored in table "historique_carte_programme_fidelite_carte"
 *
* @author Back-End developper
   *
 */

@Entity
@Table(name="historique_carte_programme_fidelite_carte" )
@Data // getters setters
@AllArgsConstructor // constructeur avec arguments
@NoArgsConstructor // constructeur sans arguments
public class HistoriqueCarteProgrammeFideliteCarte implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
	/*
	 * 
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
	/*
	 * 
	 */
    @Column(name="created_by")
    private Integer    createdBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

	/*
	 * 
	 */
    @Column(name="updated_by")
    private Integer    updatedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

	/*
	 * 
	 */
    @Column(name="deleted_by")
    private Integer    deletedBy    ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="deleted_at")
    private Date       deletedAt    ;

	/*
	 * 
	 */
    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	/*
	 * 
	 */
    @Column(name="nbre_point_operation")
    private Integer    nbrePointOperation ;

	/*
	 * 
	 */
    @Column(name="nbre_point_actuel")
    private Integer    nbrePointActuel ;

	/*
	 * 
	 */
    @Column(name="description")
    private String     description  ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_action")
    private Date       dateAction   ;

	/*
	 * 
	 */
    @Temporal(TemporalType.TIME)
    @Column(name="heure_action")
    private Date       heureAction  ;

	/*
	 * 
	 */
    @Column(name="nbre_point_avant_action")
    private Integer    nbrePointAvantAction ;

	/*
	 * 
	 */
    @Column(name="somme_operation")
    private Integer    sommeOperation ;

	/*
	 * 
	 */
    @Column(name="chiffre_daffaire")
    private Integer    chiffreDaffaire ;

	// "carteId" (column "carte_id") is not defined by itself because used as FK in a link 
	// "programmeFideliteCarteId" (column "programme_fidelite_carte_id") is not defined by itself because used as FK in a link 
	// "typeActionId" (column "type_action_id") is not defined by itself because used as FK in a link 
	// "pointDeVenteId" (column "point_de_vente_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="programme_fidelite_carte_id", referencedColumnName="id")
        private ProgrammeFideliteCarte programmeFideliteCarte;
    @ManyToOne
    @JoinColumn(name="type_action_id", referencedColumnName="id")
        private TypeAction typeAction;
    @ManyToOne
    @JoinColumn(name="carte_id", referencedColumnName="id")
        private Carte carte;
    @ManyToOne
    @JoinColumn(name="point_de_vente_id", referencedColumnName="id")
        private PointDeVente pointDeVente;

	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
