
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._ProgrammeFideliteCarteRepository;

/**
 * Repository : ProgrammeFideliteCarte.
 */
@Repository
public interface ProgrammeFideliteCarteRepository extends JpaRepository<ProgrammeFideliteCarte, Integer>, _ProgrammeFideliteCarteRepository {
	/**
	 * Finds ProgrammeFideliteCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ProgrammeFideliteCarte whose id is equals to the given id. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.id = :id and e.isDeleted = :isDeleted")
	ProgrammeFideliteCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProgrammeFideliteCarte by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object ProgrammeFideliteCarte whose libelle is equals to the given libelle. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	ProgrammeFideliteCarte findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object ProgrammeFideliteCarte whose description is equals to the given description. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.description = :description and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using conditionsDutilisation as a search criteria.
	 *
	 * @param conditionsDutilisation
	 * @return An Object ProgrammeFideliteCarte whose conditionsDutilisation is equals to the given conditionsDutilisation. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.conditionsDutilisation = :conditionsDutilisation and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByConditionsDutilisation(@Param("conditionsDutilisation")String conditionsDutilisation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using pourXAcheter as a search criteria.
	 *
	 * @param pourXAcheter
	 * @return An Object ProgrammeFideliteCarte whose pourXAcheter is equals to the given pourXAcheter. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.pourXAcheter = :pourXAcheter and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByPourXAcheter(@Param("pourXAcheter")Integer pourXAcheter, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using nbrePointObtenu as a search criteria.
	 *
	 * @param nbrePointObtenu
	 * @return An Object ProgrammeFideliteCarte whose nbrePointObtenu is equals to the given nbrePointObtenu. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.nbrePointObtenu = :nbrePointObtenu and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByNbrePointObtenu(@Param("nbrePointObtenu")Integer nbrePointObtenu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using correspondanceNbrePoint as a search criteria.
	 *
	 * @param correspondanceNbrePoint
	 * @return An Object ProgrammeFideliteCarte whose correspondanceNbrePoint is equals to the given correspondanceNbrePoint. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.correspondanceNbrePoint = :correspondanceNbrePoint and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByCorrespondanceNbrePoint(@Param("correspondanceNbrePoint")Integer correspondanceNbrePoint, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using isDispoInAllPdv as a search criteria.
	 *
	 * @param isDispoInAllPdv
	 * @return An Object ProgrammeFideliteCarte whose isDispoInAllPdv is equals to the given isDispoInAllPdv. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.isDispoInAllPdv = :isDispoInAllPdv and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByIsDispoInAllPdv(@Param("isDispoInAllPdv")Boolean isDispoInAllPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object ProgrammeFideliteCarte whose isLocked is equals to the given isLocked. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using pointObtenuParParrain as a search criteria.
	 *
	 * @param pointObtenuParParrain
	 * @return An Object ProgrammeFideliteCarte whose pointObtenuParParrain is equals to the given pointObtenuParParrain. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.pointObtenuParParrain = :pointObtenuParParrain and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByPointObtenuParParrain(@Param("pointObtenuParParrain")Integer pointObtenuParParrain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using pointObtenuParFilleul as a search criteria.
	 *
	 * @param pointObtenuParFilleul
	 * @return An Object ProgrammeFideliteCarte whose pointObtenuParFilleul is equals to the given pointObtenuParFilleul. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.pointObtenuParFilleul = :pointObtenuParFilleul and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByPointObtenuParFilleul(@Param("pointObtenuParFilleul")Integer pointObtenuParFilleul, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using pointObtenuApresSouscription as a search criteria.
	 *
	 * @param pointObtenuApresSouscription
	 * @return An Object ProgrammeFideliteCarte whose pointObtenuApresSouscription is equals to the given pointObtenuApresSouscription. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.pointObtenuApresSouscription = :pointObtenuApresSouscription and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByPointObtenuApresSouscription(@Param("pointObtenuApresSouscription")Integer pointObtenuApresSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using nbreDeSouscrit as a search criteria.
	 *
	 * @param nbreDeSouscrit
	 * @return An Object ProgrammeFideliteCarte whose nbreDeSouscrit is equals to the given nbreDeSouscrit. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.nbreDeSouscrit = :nbreDeSouscrit and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByNbreDeSouscrit(@Param("nbreDeSouscrit")Integer nbreDeSouscrit, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using isDesactived as a search criteria.
	 *
	 * @param isDesactived
	 * @return An Object ProgrammeFideliteCarte whose isDesactived is equals to the given isDesactived. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.isDesactived = :isDesactived and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByIsDesactived(@Param("isDesactived")Boolean isDesactived, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using urlCarte as a search criteria.
	 *
	 * @param urlCarte
	 * @return An Object ProgrammeFideliteCarte whose urlCarte is equals to the given urlCarte. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.urlCarte = :urlCarte and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByUrlCarte(@Param("urlCarte")String urlCarte, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ProgrammeFideliteCarte whose createdBy is equals to the given createdBy. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ProgrammeFideliteCarte whose createdAt is equals to the given createdAt. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ProgrammeFideliteCarte whose updatedBy is equals to the given updatedBy. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ProgrammeFideliteCarte whose updatedAt is equals to the given updatedAt. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ProgrammeFideliteCarte whose deletedBy is equals to the given deletedBy. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ProgrammeFideliteCarte whose deletedAt is equals to the given deletedAt. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ProgrammeFideliteCarte whose isDeleted is equals to the given isDeleted. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProgrammeFideliteCarte by using typeProgrammeFideliteCarteId as a search criteria.
	 *
	 * @param typeProgrammeFideliteCarteId
	 * @return A list of Object ProgrammeFideliteCarte whose typeProgrammeFideliteCarteId is equals to the given typeProgrammeFideliteCarteId. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.typeProgrammeFideliteCarte.id = :typeProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByTypeProgrammeFideliteCarteId(@Param("typeProgrammeFideliteCarteId")Integer typeProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProgrammeFideliteCarte by using typeProgrammeFideliteCarteId as a search criteria.
   *
   * @param typeProgrammeFideliteCarteId
   * @return An Object ProgrammeFideliteCarte whose typeProgrammeFideliteCarteId is equals to the given typeProgrammeFideliteCarteId. If
   *         no ProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from ProgrammeFideliteCarte e where e.typeProgrammeFideliteCarte.id = :typeProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
  ProgrammeFideliteCarte findProgrammeFideliteCarteByTypeProgrammeFideliteCarteId(@Param("typeProgrammeFideliteCarteId")Integer typeProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds ProgrammeFideliteCarte by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object ProgrammeFideliteCarte whose enseigneId is equals to the given enseigneId. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProgrammeFideliteCarte by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object ProgrammeFideliteCarte whose enseigneId is equals to the given enseigneId. If
   *         no ProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from ProgrammeFideliteCarte e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  ProgrammeFideliteCarte findProgrammeFideliteCarteByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ProgrammeFideliteCarte by using programmeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ProgrammeFideliteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<ProgrammeFideliteCarte> getByCriteria(Request<ProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ProgrammeFideliteCarte> query = em.createQuery(req, ProgrammeFideliteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ProgrammeFideliteCarte by using programmeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ProgrammeFideliteCarte
	 *
	 */
	public default Long count(Request<ProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ProgrammeFideliteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ProgrammeFideliteCarteDto dto = request.getData() != null ? request.getData() : new ProgrammeFideliteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ProgrammeFideliteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getConditionsDutilisation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("conditionsDutilisation", dto.getConditionsDutilisation(), "e.conditionsDutilisation", "String", dto.getConditionsDutilisationParam(), param, index, locale));
			}
			if (dto.getPourXAcheter()!= null && dto.getPourXAcheter() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pourXAcheter", dto.getPourXAcheter(), "e.pourXAcheter", "Integer", dto.getPourXAcheterParam(), param, index, locale));
			}
			if (dto.getNbrePointObtenu()!= null && dto.getNbrePointObtenu() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointObtenu", dto.getNbrePointObtenu(), "e.nbrePointObtenu", "Integer", dto.getNbrePointObtenuParam(), param, index, locale));
			}
			if (dto.getCorrespondanceNbrePoint()!= null && dto.getCorrespondanceNbrePoint() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("correspondanceNbrePoint", dto.getCorrespondanceNbrePoint(), "e.correspondanceNbrePoint", "Integer", dto.getCorrespondanceNbrePointParam(), param, index, locale));
			}
			if (dto.getIsDispoInAllPdv()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDispoInAllPdv", dto.getIsDispoInAllPdv(), "e.isDispoInAllPdv", "Boolean", dto.getIsDispoInAllPdvParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getPointObtenuParParrain()!= null && dto.getPointObtenuParParrain() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointObtenuParParrain", dto.getPointObtenuParParrain(), "e.pointObtenuParParrain", "Integer", dto.getPointObtenuParParrainParam(), param, index, locale));
			}
			if (dto.getPointObtenuParFilleul()!= null && dto.getPointObtenuParFilleul() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointObtenuParFilleul", dto.getPointObtenuParFilleul(), "e.pointObtenuParFilleul", "Integer", dto.getPointObtenuParFilleulParam(), param, index, locale));
			}
			if (dto.getPointObtenuApresSouscription()!= null && dto.getPointObtenuApresSouscription() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointObtenuApresSouscription", dto.getPointObtenuApresSouscription(), "e.pointObtenuApresSouscription", "Integer", dto.getPointObtenuApresSouscriptionParam(), param, index, locale));
			}
			if (dto.getNbreDeSouscrit()!= null && dto.getNbreDeSouscrit() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreDeSouscrit", dto.getNbreDeSouscrit(), "e.nbreDeSouscrit", "Integer", dto.getNbreDeSouscritParam(), param, index, locale));
			}
			if (dto.getIsDesactived()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDesactived", dto.getIsDesactived(), "e.isDesactived", "Boolean", dto.getIsDesactivedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlCarte())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlCarte", dto.getUrlCarte(), "e.urlCarte", "String", dto.getUrlCarteParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getTypeProgrammeFideliteCarteId()!= null && dto.getTypeProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeProgrammeFideliteCarteId", dto.getTypeProgrammeFideliteCarteId(), "e.typeProgrammeFideliteCarte.id", "Integer", dto.getTypeProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeProgrammeFideliteCarteLibelle", dto.getTypeProgrammeFideliteCarteLibelle(), "e.typeProgrammeFideliteCarte.libelle", "String", dto.getTypeProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeProgrammeFideliteCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeProgrammeFideliteCarteCode", dto.getTypeProgrammeFideliteCarteCode(), "e.typeProgrammeFideliteCarte.code", "String", dto.getTypeProgrammeFideliteCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
