
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._PointDeVenteRepository;

/**
 * Repository : PointDeVente.
 */
@Repository
public interface PointDeVenteRepository extends JpaRepository<PointDeVente, Integer>, _PointDeVenteRepository {
	/**
	 * Finds PointDeVente by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PointDeVente whose id is equals to the given id. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.id = :id and e.isDeleted = :isDeleted")
	PointDeVente findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PointDeVente by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object PointDeVente whose nom is equals to the given nom. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<PointDeVente> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object PointDeVente whose description is equals to the given description. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.description = :description and e.isDeleted = :isDeleted")
	List<PointDeVente> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object PointDeVente whose code is equals to the given code. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.code = :code and e.isDeleted = :isDeleted")
	PointDeVente findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using libelleCommune as a search criteria.
	 *
	 * @param libelleCommune
	 * @return An Object PointDeVente whose libelleCommune is equals to the given libelleCommune. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.libelleCommune = :libelleCommune and e.isDeleted = :isDeleted")
	List<PointDeVente> findByLibelleCommune(@Param("libelleCommune")String libelleCommune, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using ville as a search criteria.
	 *
	 * @param ville
	 * @return An Object PointDeVente whose ville is equals to the given ville. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.ville = :ville and e.isDeleted = :isDeleted")
	List<PointDeVente> findByVille(@Param("ville")String ville, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using isAllCarteDipso as a search criteria.
	 *
	 * @param isAllCarteDipso
	 * @return An Object PointDeVente whose isAllCarteDipso is equals to the given isAllCarteDipso. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.isAllCarteDipso = :isAllCarteDipso and e.isDeleted = :isDeleted")
	List<PointDeVente> findByIsAllCarteDipso(@Param("isAllCarteDipso")Boolean isAllCarteDipso, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using isAllTamponDipso as a search criteria.
	 *
	 * @param isAllTamponDipso
	 * @return An Object PointDeVente whose isAllTamponDipso is equals to the given isAllTamponDipso. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.isAllTamponDipso = :isAllTamponDipso and e.isDeleted = :isDeleted")
	List<PointDeVente> findByIsAllTamponDipso(@Param("isAllTamponDipso")Boolean isAllTamponDipso, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PointDeVente whose createdBy is equals to the given createdBy. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PointDeVente> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PointDeVente whose createdAt is equals to the given createdAt. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PointDeVente> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PointDeVente whose updatedBy is equals to the given updatedBy. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PointDeVente> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PointDeVente whose updatedAt is equals to the given updatedAt. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PointDeVente> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PointDeVente whose deletedBy is equals to the given deletedBy. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PointDeVente> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PointDeVente whose deletedAt is equals to the given deletedAt. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PointDeVente> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVente by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PointDeVente whose isDeleted is equals to the given isDeleted. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.isDeleted = :isDeleted")
	List<PointDeVente> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PointDeVente by using gpsPointDeVenteId as a search criteria.
	 *
	 * @param gpsPointDeVenteId
	 * @return A list of Object PointDeVente whose gpsPointDeVenteId is equals to the given gpsPointDeVenteId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.gpsPointDeVente.id = :gpsPointDeVenteId and e.isDeleted = :isDeleted")
	List<PointDeVente> findByGpsPointDeVenteId(@Param("gpsPointDeVenteId")Integer gpsPointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVente by using gpsPointDeVenteId as a search criteria.
   *
   * @param gpsPointDeVenteId
   * @return An Object PointDeVente whose gpsPointDeVenteId is equals to the given gpsPointDeVenteId. If
   *         no PointDeVente is found, this method returns null.
   */
  @Query("select e from PointDeVente e where e.gpsPointDeVente.id = :gpsPointDeVenteId and e.isDeleted = :isDeleted")
  PointDeVente findPointDeVenteByGpsPointDeVenteId(@Param("gpsPointDeVenteId")Integer gpsPointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PointDeVente by using communeId as a search criteria.
	 *
	 * @param communeId
	 * @return A list of Object PointDeVente whose communeId is equals to the given communeId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.commune.id = :communeId and e.isDeleted = :isDeleted")
	List<PointDeVente> findByCommuneId(@Param("communeId")Integer communeId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVente by using communeId as a search criteria.
   *
   * @param communeId
   * @return An Object PointDeVente whose communeId is equals to the given communeId. If
   *         no PointDeVente is found, this method returns null.
   */
  @Query("select e from PointDeVente e where e.commune.id = :communeId and e.isDeleted = :isDeleted")
  PointDeVente findPointDeVenteByCommuneId(@Param("communeId")Integer communeId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PointDeVente by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object PointDeVente whose userId is equals to the given userId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<PointDeVente> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVente by using userId as a search criteria.
   *
   * @param userId
   * @return An Object PointDeVente whose userId is equals to the given userId. If
   *         no PointDeVente is found, this method returns null.
   */
  @Query("select e from PointDeVente e where e.user.id = :userId and e.isDeleted = :isDeleted")
  PointDeVente findPointDeVenteByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PointDeVente by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object PointDeVente whose enseigneId is equals to the given enseigneId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<PointDeVente> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVente by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object PointDeVente whose enseigneId is equals to the given enseigneId. If
   *         no PointDeVente is found, this method returns null.
   */
  @Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  PointDeVente findPointDeVenteByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PointDeVente by using pointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PointDeVente
	 * @throws DataAccessException,ParseException
	 */
	public default List<PointDeVente> getByCriteria(Request<PointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PointDeVente> query = em.createQuery(req, PointDeVente.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PointDeVente by using pointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PointDeVente
	 *
	 */
	public default Long count(Request<PointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PointDeVenteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PointDeVenteDto dto = request.getData() != null ? request.getData() : new PointDeVenteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PointDeVenteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PointDeVenteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelleCommune())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelleCommune", dto.getLibelleCommune(), "e.libelleCommune", "String", dto.getLibelleCommuneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getVille())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("ville", dto.getVille(), "e.ville", "String", dto.getVilleParam(), param, index, locale));
			}
			if (dto.getIsAllCarteDipso()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAllCarteDipso", dto.getIsAllCarteDipso(), "e.isAllCarteDipso", "Boolean", dto.getIsAllCarteDipsoParam(), param, index, locale));
			}
			if (dto.getIsAllTamponDipso()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAllTamponDipso", dto.getIsAllTamponDipso(), "e.isAllTamponDipso", "Boolean", dto.getIsAllTamponDipsoParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getGpsPointDeVenteId()!= null && dto.getGpsPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("gpsPointDeVenteId", dto.getGpsPointDeVenteId(), "e.gpsPointDeVente.id", "Integer", dto.getGpsPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getCommuneId()!= null && dto.getCommuneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeId", dto.getCommuneId(), "e.commune.id", "Integer", dto.getCommuneIdParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommuneLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeLibelle", dto.getCommuneLibelle(), "e.commune.libelle", "String", dto.getCommuneLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommuneCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeCode", dto.getCommuneCode(), "e.commune.code", "String", dto.getCommuneCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
