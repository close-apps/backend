
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._ProgrammeFideliteTamponRepository;

/**
 * Repository : ProgrammeFideliteTampon.
 */
@Repository
public interface ProgrammeFideliteTamponRepository extends JpaRepository<ProgrammeFideliteTampon, Integer>, _ProgrammeFideliteTamponRepository {
	/**
	 * Finds ProgrammeFideliteTampon by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ProgrammeFideliteTampon whose id is equals to the given id. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.id = :id and e.isDeleted = :isDeleted")
	ProgrammeFideliteTampon findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProgrammeFideliteTampon by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object ProgrammeFideliteTampon whose libelle is equals to the given libelle. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	ProgrammeFideliteTampon findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object ProgrammeFideliteTampon whose description is equals to the given description. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.description = :description and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using conditionsDutilisation as a search criteria.
	 *
	 * @param conditionsDutilisation
	 * @return An Object ProgrammeFideliteTampon whose conditionsDutilisation is equals to the given conditionsDutilisation. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.conditionsDutilisation = :conditionsDutilisation and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByConditionsDutilisation(@Param("conditionsDutilisation")String conditionsDutilisation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using nbreTampon as a search criteria.
	 *
	 * @param nbreTampon
	 * @return An Object ProgrammeFideliteTampon whose nbreTampon is equals to the given nbreTampon. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.nbreTampon = :nbreTampon and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByNbreTampon(@Param("nbreTampon")Integer nbreTampon, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using nbrePallier as a search criteria.
	 *
	 * @param nbrePallier
	 * @return An Object ProgrammeFideliteTampon whose nbrePallier is equals to the given nbrePallier. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.nbrePallier = :nbrePallier and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByNbrePallier(@Param("nbrePallier")Integer nbrePallier, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using urlImage as a search criteria.
	 *
	 * @param urlImage
	 * @return An Object ProgrammeFideliteTampon whose urlImage is equals to the given urlImage. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.urlImage = :urlImage and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByUrlImage(@Param("urlImage")String urlImage, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using gain as a search criteria.
	 *
	 * @param gain
	 * @return An Object ProgrammeFideliteTampon whose gain is equals to the given gain. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.gain = :gain and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByGain(@Param("gain")String gain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using valeurMaxGain as a search criteria.
	 *
	 * @param valeurMaxGain
	 * @return An Object ProgrammeFideliteTampon whose valeurMaxGain is equals to the given valeurMaxGain. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.valeurMaxGain = :valeurMaxGain and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByValeurMaxGain(@Param("valeurMaxGain")Integer valeurMaxGain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using isValeurMaxGain as a search criteria.
	 *
	 * @param isValeurMaxGain
	 * @return An Object ProgrammeFideliteTampon whose isValeurMaxGain is equals to the given isValeurMaxGain. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.isValeurMaxGain = :isValeurMaxGain and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByIsValeurMaxGain(@Param("isValeurMaxGain")Boolean isValeurMaxGain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using tamponObtenuApresSouscription as a search criteria.
	 *
	 * @param tamponObtenuApresSouscription
	 * @return An Object ProgrammeFideliteTampon whose tamponObtenuApresSouscription is equals to the given tamponObtenuApresSouscription. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.tamponObtenuApresSouscription = :tamponObtenuApresSouscription and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByTamponObtenuApresSouscription(@Param("tamponObtenuApresSouscription")Integer tamponObtenuApresSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using codeBarre as a search criteria.
	 *
	 * @param codeBarre
	 * @return An Object ProgrammeFideliteTampon whose codeBarre is equals to the given codeBarre. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.codeBarre = :codeBarre and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByCodeBarre(@Param("codeBarre")String codeBarre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using qrCode as a search criteria.
	 *
	 * @param qrCode
	 * @return An Object ProgrammeFideliteTampon whose qrCode is equals to the given qrCode. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.qrCode = :qrCode and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByQrCode(@Param("qrCode")String qrCode, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using isDispoInAllPdv as a search criteria.
	 *
	 * @param isDispoInAllPdv
	 * @return An Object ProgrammeFideliteTampon whose isDispoInAllPdv is equals to the given isDispoInAllPdv. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.isDispoInAllPdv = :isDispoInAllPdv and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByIsDispoInAllPdv(@Param("isDispoInAllPdv")Boolean isDispoInAllPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object ProgrammeFideliteTampon whose isLocked is equals to the given isLocked. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using isDesactived as a search criteria.
	 *
	 * @param isDesactived
	 * @return An Object ProgrammeFideliteTampon whose isDesactived is equals to the given isDesactived. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.isDesactived = :isDesactived and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByIsDesactived(@Param("isDesactived")Boolean isDesactived, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ProgrammeFideliteTampon whose createdBy is equals to the given createdBy. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ProgrammeFideliteTampon whose createdAt is equals to the given createdAt. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ProgrammeFideliteTampon whose updatedBy is equals to the given updatedBy. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ProgrammeFideliteTampon whose updatedAt is equals to the given updatedAt. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ProgrammeFideliteTampon whose deletedBy is equals to the given deletedBy. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ProgrammeFideliteTampon whose deletedAt is equals to the given deletedAt. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProgrammeFideliteTampon by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ProgrammeFideliteTampon whose isDeleted is equals to the given isDeleted. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProgrammeFideliteTampon by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object ProgrammeFideliteTampon whose enseigneId is equals to the given enseigneId. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteTampon e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProgrammeFideliteTampon by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object ProgrammeFideliteTampon whose enseigneId is equals to the given enseigneId. If
   *         no ProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from ProgrammeFideliteTampon e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  ProgrammeFideliteTampon findProgrammeFideliteTamponByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ProgrammeFideliteTampon by using programmeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ProgrammeFideliteTampon
	 * @throws DataAccessException,ParseException
	 */
	public default List<ProgrammeFideliteTampon> getByCriteria(Request<ProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ProgrammeFideliteTampon> query = em.createQuery(req, ProgrammeFideliteTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ProgrammeFideliteTampon by using programmeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ProgrammeFideliteTampon
	 *
	 */
	public default Long count(Request<ProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ProgrammeFideliteTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ProgrammeFideliteTamponDto dto = request.getData() != null ? request.getData() : new ProgrammeFideliteTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ProgrammeFideliteTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getConditionsDutilisation())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("conditionsDutilisation", dto.getConditionsDutilisation(), "e.conditionsDutilisation", "String", dto.getConditionsDutilisationParam(), param, index, locale));
			}
			if (dto.getNbreTampon()!= null && dto.getNbreTampon() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTampon", dto.getNbreTampon(), "e.nbreTampon", "Integer", dto.getNbreTamponParam(), param, index, locale));
			}
			if (dto.getNbrePallier()!= null && dto.getNbrePallier() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePallier", dto.getNbrePallier(), "e.nbrePallier", "Integer", dto.getNbrePallierParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlImage())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlImage", dto.getUrlImage(), "e.urlImage", "String", dto.getUrlImageParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getGain())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("gain", dto.getGain(), "e.gain", "String", dto.getGainParam(), param, index, locale));
			}
			if (dto.getValeurMaxGain()!= null && dto.getValeurMaxGain() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeurMaxGain", dto.getValeurMaxGain(), "e.valeurMaxGain", "Integer", dto.getValeurMaxGainParam(), param, index, locale));
			}
			if (dto.getIsValeurMaxGain()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValeurMaxGain", dto.getIsValeurMaxGain(), "e.isValeurMaxGain", "Boolean", dto.getIsValeurMaxGainParam(), param, index, locale));
			}
			if (dto.getTamponObtenuApresSouscription()!= null && dto.getTamponObtenuApresSouscription() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("tamponObtenuApresSouscription", dto.getTamponObtenuApresSouscription(), "e.tamponObtenuApresSouscription", "Integer", dto.getTamponObtenuApresSouscriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCodeBarre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("codeBarre", dto.getCodeBarre(), "e.codeBarre", "String", dto.getCodeBarreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getQrCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("qrCode", dto.getQrCode(), "e.qrCode", "String", dto.getQrCodeParam(), param, index, locale));
			}
			if (dto.getIsDispoInAllPdv()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDispoInAllPdv", dto.getIsDispoInAllPdv(), "e.isDispoInAllPdv", "Boolean", dto.getIsDispoInAllPdvParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsDesactived()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDesactived", dto.getIsDesactived(), "e.isDesactived", "Boolean", dto.getIsDesactivedParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
