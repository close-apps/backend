
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._HistoriqueCarteProgrammeFideliteTamponRepository;

/**
 * Repository : HistoriqueCarteProgrammeFideliteTampon.
 */
@Repository
public interface HistoriqueCarteProgrammeFideliteTamponRepository extends JpaRepository<HistoriqueCarteProgrammeFideliteTampon, Integer>, _HistoriqueCarteProgrammeFideliteTamponRepository {
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose id is equals to the given id. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.id = :id and e.isDeleted = :isDeleted")
	HistoriqueCarteProgrammeFideliteTampon findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose createdBy is equals to the given createdBy. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose createdAt is equals to the given createdAt. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose updatedBy is equals to the given updatedBy. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose updatedAt is equals to the given updatedAt. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose deletedBy is equals to the given deletedBy. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose deletedAt is equals to the given deletedAt. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose isDeleted is equals to the given isDeleted. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using nbreTamponObtenu as a search criteria.
	 *
	 * @param nbreTamponObtenu
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose nbreTamponObtenu is equals to the given nbreTamponObtenu. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.nbreTamponObtenu = :nbreTamponObtenu and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByNbreTamponObtenu(@Param("nbreTamponObtenu")Integer nbreTamponObtenu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using nbreTamponActuel as a search criteria.
	 *
	 * @param nbreTamponActuel
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose nbreTamponActuel is equals to the given nbreTamponActuel. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.nbreTamponActuel = :nbreTamponActuel and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByNbreTamponActuel(@Param("nbreTamponActuel")Integer nbreTamponActuel, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using dateAction as a search criteria.
	 *
	 * @param dateAction
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose dateAction is equals to the given dateAction. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.dateAction = :dateAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByDateAction(@Param("dateAction")Date dateAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using heureAction as a search criteria.
	 *
	 * @param heureAction
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose heureAction is equals to the given heureAction. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.heureAction = :heureAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByHeureAction(@Param("heureAction")Date heureAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using nbreTamponAvantAction as a search criteria.
	 *
	 * @param nbreTamponAvantAction
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose nbreTamponAvantAction is equals to the given nbreTamponAvantAction. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.nbreTamponAvantAction = :nbreTamponAvantAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByNbreTamponAvantAction(@Param("nbreTamponAvantAction")Integer nbreTamponAvantAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose description is equals to the given description. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.description = :description and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using chiffreDaffaire as a search criteria.
	 *
	 * @param chiffreDaffaire
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose chiffreDaffaire is equals to the given chiffreDaffaire. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.chiffreDaffaire = :chiffreDaffaire and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByChiffreDaffaire(@Param("chiffreDaffaire")Integer chiffreDaffaire, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose carteId is equals to the given carteId. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteTampon by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object HistoriqueCarteProgrammeFideliteTampon whose carteId is equals to the given carteId. If
   *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteTampon findHistoriqueCarteProgrammeFideliteTamponByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteTampon by using pointDeVenteId as a search criteria.
   *
   * @param pointDeVenteId
   * @return An Object HistoriqueCarteProgrammeFideliteTampon whose pointDeVenteId is equals to the given pointDeVenteId. If
   *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteTampon findHistoriqueCarteProgrammeFideliteTamponByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteTampon by using programmeFideliteTamponId as a search criteria.
   *
   * @param programmeFideliteTamponId
   * @return An Object HistoriqueCarteProgrammeFideliteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
   *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteTampon findHistoriqueCarteProgrammeFideliteTamponByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionId as a search criteria.
	 *
	 * @param typeActionId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionId is equals to the given typeActionId. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.typeAction.id = :typeActionId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByTypeActionId(@Param("typeActionId")Integer typeActionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteTampon by using typeActionId as a search criteria.
   *
   * @param typeActionId
   * @return An Object HistoriqueCarteProgrammeFideliteTampon whose typeActionId is equals to the given typeActionId. If
   *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.typeAction.id = :typeActionId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteTampon findHistoriqueCarteProgrammeFideliteTamponByTypeActionId(@Param("typeActionId")Integer typeActionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using souscriptionProgrammeFideliteTamponId as a search criteria.
	 *
	 * @param souscriptionProgrammeFideliteTamponId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose souscriptionProgrammeFideliteTamponId is equals to the given souscriptionProgrammeFideliteTamponId. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.souscriptionProgrammeFideliteTampon.id = :souscriptionProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findBySouscriptionProgrammeFideliteTamponId(@Param("souscriptionProgrammeFideliteTamponId")Integer souscriptionProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteTampon by using souscriptionProgrammeFideliteTamponId as a search criteria.
   *
   * @param souscriptionProgrammeFideliteTamponId
   * @return An Object HistoriqueCarteProgrammeFideliteTampon whose souscriptionProgrammeFideliteTamponId is equals to the given souscriptionProgrammeFideliteTamponId. If
   *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.souscriptionProgrammeFideliteTampon.id = :souscriptionProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteTampon findHistoriqueCarteProgrammeFideliteTamponBySouscriptionProgrammeFideliteTamponId(@Param("souscriptionProgrammeFideliteTamponId")Integer souscriptionProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of HistoriqueCarteProgrammeFideliteTampon by using historiqueCarteProgrammeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of HistoriqueCarteProgrammeFideliteTampon
	 * @throws DataAccessException,ParseException
	 */
	public default List<HistoriqueCarteProgrammeFideliteTampon> getByCriteria(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from HistoriqueCarteProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<HistoriqueCarteProgrammeFideliteTampon> query = em.createQuery(req, HistoriqueCarteProgrammeFideliteTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of HistoriqueCarteProgrammeFideliteTampon by using historiqueCarteProgrammeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of HistoriqueCarteProgrammeFideliteTampon
	 *
	 */
	public default Long count(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from HistoriqueCarteProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<HistoriqueCarteProgrammeFideliteTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		HistoriqueCarteProgrammeFideliteTamponDto dto = request.getData() != null ? request.getData() : new HistoriqueCarteProgrammeFideliteTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (HistoriqueCarteProgrammeFideliteTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(HistoriqueCarteProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getNbreTamponObtenu()!= null && dto.getNbreTamponObtenu() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTamponObtenu", dto.getNbreTamponObtenu(), "e.nbreTamponObtenu", "Integer", dto.getNbreTamponObtenuParam(), param, index, locale));
			}
			if (dto.getNbreTamponActuel()!= null && dto.getNbreTamponActuel() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTamponActuel", dto.getNbreTamponActuel(), "e.nbreTamponActuel", "Integer", dto.getNbreTamponActuelParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateAction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateAction", dto.getDateAction(), "e.dateAction", "Date", dto.getDateActionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getHeureAction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("heureAction", dto.getHeureAction(), "e.heureAction", "Date", dto.getHeureActionParam(), param, index, locale));
			}
			if (dto.getNbreTamponAvantAction()!= null && dto.getNbreTamponAvantAction() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTamponAvantAction", dto.getNbreTamponAvantAction(), "e.nbreTamponAvantAction", "Integer", dto.getNbreTamponAvantActionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (dto.getChiffreDaffaire()!= null && dto.getChiffreDaffaire() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("chiffreDaffaire", dto.getChiffreDaffaire(), "e.chiffreDaffaire", "Integer", dto.getChiffreDaffaireParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getTypeActionId()!= null && dto.getTypeActionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionId", dto.getTypeActionId(), "e.typeAction.id", "Integer", dto.getTypeActionIdParam(), param, index, locale));
			}
			if (dto.getSouscriptionProgrammeFideliteTamponId()!= null && dto.getSouscriptionProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponId", dto.getSouscriptionProgrammeFideliteTamponId(), "e.souscriptionProgrammeFideliteTampon.id", "Integer", dto.getSouscriptionProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeActionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionLibelle", dto.getTypeActionLibelle(), "e.typeAction.libelle", "String", dto.getTypeActionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeActionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionCode", dto.getTypeActionCode(), "e.typeAction.code", "String", dto.getTypeActionCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteTamponCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponCode", dto.getSouscriptionProgrammeFideliteTamponCode(), "e.souscriptionProgrammeFideliteTampon.code", "String", dto.getSouscriptionProgrammeFideliteTamponCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponLibelle", dto.getSouscriptionProgrammeFideliteTamponLibelle(), "e.souscriptionProgrammeFideliteTampon.libelle", "String", dto.getSouscriptionProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
