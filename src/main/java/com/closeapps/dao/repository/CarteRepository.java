
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._CarteRepository;

/**
 * Repository : Carte.
 */
@Repository
public interface CarteRepository extends JpaRepository<Carte, Integer>, _CarteRepository {
	/**
	 * Finds Carte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Carte whose id is equals to the given id. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.id = :id and e.isDeleted = :isDeleted")
	Carte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Carte by using codeBarre as a search criteria.
	 *
	 * @param codeBarre
	 * @return An Object Carte whose codeBarre is equals to the given codeBarre. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.codeBarre = :codeBarre and e.isDeleted = :isDeleted")
	List<Carte> findByCodeBarre(@Param("codeBarre")String codeBarre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using qrCode as a search criteria.
	 *
	 * @param qrCode
	 * @return An Object Carte whose qrCode is equals to the given qrCode. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.qrCode = :qrCode and e.isDeleted = :isDeleted")
	List<Carte> findByQrCode(@Param("qrCode")String qrCode, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object Carte whose code is equals to the given code. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.code = :code and e.isDeleted = :isDeleted")
	Carte findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using urlImage as a search criteria.
	 *
	 * @param urlImage
	 * @return An Object Carte whose urlImage is equals to the given urlImage. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.urlImage = :urlImage and e.isDeleted = :isDeleted")
	List<Carte> findByUrlImage(@Param("urlImage")String urlImage, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Carte whose createdBy is equals to the given createdBy. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Carte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Carte whose createdAt is equals to the given createdAt. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Carte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Carte whose updatedBy is equals to the given updatedBy. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Carte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Carte whose updatedAt is equals to the given updatedAt. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Carte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Carte whose deletedBy is equals to the given deletedBy. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Carte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Carte whose deletedAt is equals to the given deletedAt. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Carte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object Carte whose isLocked is equals to the given isLocked. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<Carte> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Carte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Carte whose isDeleted is equals to the given isDeleted. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.isDeleted = :isDeleted")
	List<Carte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Carte by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object Carte whose userId is equals to the given userId. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<Carte> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Carte by using userId as a search criteria.
   *
   * @param userId
   * @return An Object Carte whose userId is equals to the given userId. If
   *         no Carte is found, this method returns null.
   */
  @Query("select e from Carte e where e.user.id = :userId and e.isDeleted = :isDeleted")
  Carte findCarteByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Carte by using carteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Carte
	 * @throws DataAccessException,ParseException
	 */
	public default List<Carte> getByCriteria(Request<CarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Carte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Carte> query = em.createQuery(req, Carte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Carte by using carteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Carte
	 *
	 */
	public default Long count(Request<CarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Carte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		CarteDto dto = request.getData() != null ? request.getData() : new CarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCodeBarre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("codeBarre", dto.getCodeBarre(), "e.codeBarre", "String", dto.getCodeBarreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getQrCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("qrCode", dto.getQrCode(), "e.qrCode", "String", dto.getQrCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlImage())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlImage", dto.getUrlImage(), "e.urlImage", "String", dto.getUrlImageParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
