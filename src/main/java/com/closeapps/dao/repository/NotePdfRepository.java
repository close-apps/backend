
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._NotePdfRepository;

/**
 * Repository : NotePdf.
 */
@Repository
public interface NotePdfRepository extends JpaRepository<NotePdf, Integer>, _NotePdfRepository {
	/**
	 * Finds NotePdf by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object NotePdf whose id is equals to the given id. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.id = :id and e.isDeleted = :isDeleted")
	NotePdf findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NotePdf by using note as a search criteria.
	 *
	 * @param note
	 * @return An Object NotePdf whose note is equals to the given note. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.note = :note and e.isDeleted = :isDeleted")
	List<NotePdf> findByNote(@Param("note")Integer note, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using commentaire as a search criteria.
	 *
	 * @param commentaire
	 * @return An Object NotePdf whose commentaire is equals to the given commentaire. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.commentaire = :commentaire and e.isDeleted = :isDeleted")
	List<NotePdf> findByCommentaire(@Param("commentaire")String commentaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using retourEnseigne as a search criteria.
	 *
	 * @param retourEnseigne
	 * @return An Object NotePdf whose retourEnseigne is equals to the given retourEnseigne. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.retourEnseigne = :retourEnseigne and e.isDeleted = :isDeleted")
	List<NotePdf> findByRetourEnseigne(@Param("retourEnseigne")String retourEnseigne, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using dateAction as a search criteria.
	 *
	 * @param dateAction
	 * @return An Object NotePdf whose dateAction is equals to the given dateAction. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.dateAction = :dateAction and e.isDeleted = :isDeleted")
	List<NotePdf> findByDateAction(@Param("dateAction")Date dateAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object NotePdf whose createdBy is equals to the given createdBy. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<NotePdf> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object NotePdf whose createdAt is equals to the given createdAt. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<NotePdf> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object NotePdf whose updatedBy is equals to the given updatedBy. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<NotePdf> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object NotePdf whose updatedAt is equals to the given updatedAt. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<NotePdf> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object NotePdf whose deletedBy is equals to the given deletedBy. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<NotePdf> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object NotePdf whose deletedAt is equals to the given deletedAt. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<NotePdf> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotePdf by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object NotePdf whose isDeleted is equals to the given isDeleted. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.isDeleted = :isDeleted")
	List<NotePdf> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NotePdf by using souscriptionProgrammeFideliteTamponId as a search criteria.
	 *
	 * @param souscriptionProgrammeFideliteTamponId
	 * @return A list of Object NotePdf whose souscriptionProgrammeFideliteTamponId is equals to the given souscriptionProgrammeFideliteTamponId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.souscriptionProgrammeFideliteTampon.id = :souscriptionProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
	List<NotePdf> findBySouscriptionProgrammeFideliteTamponId(@Param("souscriptionProgrammeFideliteTamponId")Integer souscriptionProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using souscriptionProgrammeFideliteTamponId as a search criteria.
   *
   * @param souscriptionProgrammeFideliteTamponId
   * @return An Object NotePdf whose souscriptionProgrammeFideliteTamponId is equals to the given souscriptionProgrammeFideliteTamponId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.souscriptionProgrammeFideliteTampon.id = :souscriptionProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfBySouscriptionProgrammeFideliteTamponId(@Param("souscriptionProgrammeFideliteTamponId")Integer souscriptionProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotePdf by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object NotePdf whose userId is equals to the given userId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<NotePdf> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using userId as a search criteria.
   *
   * @param userId
   * @return An Object NotePdf whose userId is equals to the given userId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.user.id = :userId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotePdf by using souscriptionProgrammeFideliteCarteId as a search criteria.
	 *
	 * @param souscriptionProgrammeFideliteCarteId
	 * @return A list of Object NotePdf whose souscriptionProgrammeFideliteCarteId is equals to the given souscriptionProgrammeFideliteCarteId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.souscriptionProgrammeFideliteCarte.id = :souscriptionProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
	List<NotePdf> findBySouscriptionProgrammeFideliteCarteId(@Param("souscriptionProgrammeFideliteCarteId")Integer souscriptionProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using souscriptionProgrammeFideliteCarteId as a search criteria.
   *
   * @param souscriptionProgrammeFideliteCarteId
   * @return An Object NotePdf whose souscriptionProgrammeFideliteCarteId is equals to the given souscriptionProgrammeFideliteCarteId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.souscriptionProgrammeFideliteCarte.id = :souscriptionProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfBySouscriptionProgrammeFideliteCarteId(@Param("souscriptionProgrammeFideliteCarteId")Integer souscriptionProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotePdf by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object NotePdf whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<NotePdf> findByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using programmeFideliteTamponId as a search criteria.
   *
   * @param programmeFideliteTamponId
   * @return An Object NotePdf whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotePdf by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object NotePdf whose enseigneId is equals to the given enseigneId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<NotePdf> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object NotePdf whose enseigneId is equals to the given enseigneId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotePdf by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object NotePdf whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no NotePdf is found, this method returns null.
	 */
	@Query("select e from NotePdf e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<NotePdf> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotePdf by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object NotePdf whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no NotePdf is found, this method returns null.
   */
  @Query("select e from NotePdf e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  NotePdf findNotePdfByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of NotePdf by using notePdfDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of NotePdf
	 * @throws DataAccessException,ParseException
	 */
	public default List<NotePdf> getByCriteria(Request<NotePdfDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from NotePdf e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<NotePdf> query = em.createQuery(req, NotePdf.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of NotePdf by using notePdfDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of NotePdf
	 *
	 */
	public default Long count(Request<NotePdfDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from NotePdf e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<NotePdfDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		NotePdfDto dto = request.getData() != null ? request.getData() : new NotePdfDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (NotePdfDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(NotePdfDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getNote()!= null && dto.getNote() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("note", dto.getNote(), "e.note", "Integer", dto.getNoteParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommentaire())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("commentaire", dto.getCommentaire(), "e.commentaire", "String", dto.getCommentaireParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRetourEnseigne())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("retourEnseigne", dto.getRetourEnseigne(), "e.retourEnseigne", "String", dto.getRetourEnseigneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateAction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateAction", dto.getDateAction(), "e.dateAction", "Date", dto.getDateActionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSouscriptionProgrammeFideliteTamponId()!= null && dto.getSouscriptionProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponId", dto.getSouscriptionProgrammeFideliteTamponId(), "e.souscriptionProgrammeFideliteTampon.id", "Integer", dto.getSouscriptionProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (dto.getSouscriptionProgrammeFideliteCarteId()!= null && dto.getSouscriptionProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteCarteId", dto.getSouscriptionProgrammeFideliteCarteId(), "e.souscriptionProgrammeFideliteCarte.id", "Integer", dto.getSouscriptionProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteTamponCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponCode", dto.getSouscriptionProgrammeFideliteTamponCode(), "e.souscriptionProgrammeFideliteTampon.code", "String", dto.getSouscriptionProgrammeFideliteTamponCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteTamponLibelle", dto.getSouscriptionProgrammeFideliteTamponLibelle(), "e.souscriptionProgrammeFideliteTampon.libelle", "String", dto.getSouscriptionProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteCarteCode", dto.getSouscriptionProgrammeFideliteCarteCode(), "e.souscriptionProgrammeFideliteCarte.code", "String", dto.getSouscriptionProgrammeFideliteCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscriptionProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionProgrammeFideliteCarteLibelle", dto.getSouscriptionProgrammeFideliteCarteLibelle(), "e.souscriptionProgrammeFideliteCarte.libelle", "String", dto.getSouscriptionProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
