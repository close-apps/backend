
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._EnseigneRepository;

/**
 * Repository : Enseigne.
 */
@Repository
public interface EnseigneRepository extends JpaRepository<Enseigne, Integer>, _EnseigneRepository {
	/**
	 * Finds Enseigne by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Enseigne whose id is equals to the given id. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.id = :id and e.isDeleted = :isDeleted")
	Enseigne findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Enseigne by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object Enseigne whose nom is equals to the given nom. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<Enseigne> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object Enseigne whose telephone is equals to the given telephone. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	Enseigne findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object Enseigne whose email is equals to the given email. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.email = :email and e.isDeleted = :isDeleted")
	Enseigne findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using urlLogo as a search criteria.
	 *
	 * @param urlLogo
	 * @return An Object Enseigne whose urlLogo is equals to the given urlLogo. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.urlLogo = :urlLogo and e.isDeleted = :isDeleted")
	List<Enseigne> findByUrlLogo(@Param("urlLogo")String urlLogo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object Enseigne whose description is equals to the given description. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.description = :description and e.isDeleted = :isDeleted")
	List<Enseigne> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using facebook as a search criteria.
	 *
	 * @param facebook
	 * @return An Object Enseigne whose facebook is equals to the given facebook. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.facebook = :facebook and e.isDeleted = :isDeleted")
	List<Enseigne> findByFacebook(@Param("facebook")String facebook, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using instagram as a search criteria.
	 *
	 * @param instagram
	 * @return An Object Enseigne whose instagram is equals to the given instagram. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.instagram = :instagram and e.isDeleted = :isDeleted")
	List<Enseigne> findByInstagram(@Param("instagram")String instagram, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using twitter as a search criteria.
	 *
	 * @param twitter
	 * @return An Object Enseigne whose twitter is equals to the given twitter. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.twitter = :twitter and e.isDeleted = :isDeleted")
	List<Enseigne> findByTwitter(@Param("twitter")String twitter, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using siteInternet as a search criteria.
	 *
	 * @param siteInternet
	 * @return An Object Enseigne whose siteInternet is equals to the given siteInternet. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.siteInternet = :siteInternet and e.isDeleted = :isDeleted")
	List<Enseigne> findBySiteInternet(@Param("siteInternet")String siteInternet, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using couleur as a search criteria.
	 *
	 * @param couleur
	 * @return An Object Enseigne whose couleur is equals to the given couleur. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.couleur = :couleur and e.isDeleted = :isDeleted")
	List<Enseigne> findByCouleur(@Param("couleur")String couleur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object Enseigne whose isLocked is equals to the given isLocked. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<Enseigne> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using isAccepted as a search criteria.
	 *
	 * @param isAccepted
	 * @return An Object Enseigne whose isAccepted is equals to the given isAccepted. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.isAccepted = :isAccepted and e.isDeleted = :isDeleted")
	List<Enseigne> findByIsAccepted(@Param("isAccepted")Boolean isAccepted, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Enseigne whose createdBy is equals to the given createdBy. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Enseigne> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Enseigne whose createdAt is equals to the given createdAt. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Enseigne> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Enseigne whose updatedBy is equals to the given updatedBy. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Enseigne> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Enseigne whose updatedAt is equals to the given updatedAt. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Enseigne> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Enseigne whose deletedBy is equals to the given deletedBy. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Enseigne> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Enseigne whose deletedAt is equals to the given deletedAt. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Enseigne> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Enseigne whose isDeleted is equals to the given isDeleted. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.isDeleted = :isDeleted")
	List<Enseigne> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using nomAdmin as a search criteria.
	 *
	 * @param nomAdmin
	 * @return An Object Enseigne whose nomAdmin is equals to the given nomAdmin. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nomAdmin = :nomAdmin and e.isDeleted = :isDeleted")
	List<Enseigne> findByNomAdmin(@Param("nomAdmin")String nomAdmin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using prenomsAdmin as a search criteria.
	 *
	 * @param prenomsAdmin
	 * @return An Object Enseigne whose prenomsAdmin is equals to the given prenomsAdmin. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.prenomsAdmin = :prenomsAdmin and e.isDeleted = :isDeleted")
	List<Enseigne> findByPrenomsAdmin(@Param("prenomsAdmin")String prenomsAdmin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using emailAdmin as a search criteria.
	 *
	 * @param emailAdmin
	 * @return An Object Enseigne whose emailAdmin is equals to the given emailAdmin. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.emailAdmin = :emailAdmin and e.isDeleted = :isDeleted")
	List<Enseigne> findByEmailAdmin(@Param("emailAdmin")String emailAdmin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using telephoneAdmin as a search criteria.
	 *
	 * @param telephoneAdmin
	 * @return An Object Enseigne whose telephoneAdmin is equals to the given telephoneAdmin. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.telephoneAdmin = :telephoneAdmin and e.isDeleted = :isDeleted")
	List<Enseigne> findByTelephoneAdmin(@Param("telephoneAdmin")String telephoneAdmin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using chiffreDaffaire as a search criteria.
	 *
	 * @param chiffreDaffaire
	 * @return An Object Enseigne whose chiffreDaffaire is equals to the given chiffreDaffaire. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.chiffreDaffaire = :chiffreDaffaire and e.isDeleted = :isDeleted")
	List<Enseigne> findByChiffreDaffaire(@Param("chiffreDaffaire")Long chiffreDaffaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using nbreTransaction as a search criteria.
	 *
	 * @param nbreTransaction
	 * @return An Object Enseigne whose nbreTransaction is equals to the given nbreTransaction. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nbreTransaction = :nbreTransaction and e.isDeleted = :isDeleted")
	List<Enseigne> findByNbreTransaction(@Param("nbreTransaction")Long nbreTransaction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using recompenseEngagerCarte as a search criteria.
	 *
	 * @param recompenseEngagerCarte
	 * @return An Object Enseigne whose recompenseEngagerCarte is equals to the given recompenseEngagerCarte. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.recompenseEngagerCarte = :recompenseEngagerCarte and e.isDeleted = :isDeleted")
	List<Enseigne> findByRecompenseEngagerCarte(@Param("recompenseEngagerCarte")Long recompenseEngagerCarte, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using recompenseEngagerTampon as a search criteria.
	 *
	 * @param recompenseEngagerTampon
	 * @return An Object Enseigne whose recompenseEngagerTampon is equals to the given recompenseEngagerTampon. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.recompenseEngagerTampon = :recompenseEngagerTampon and e.isDeleted = :isDeleted")
	List<Enseigne> findByRecompenseEngagerTampon(@Param("recompenseEngagerTampon")Long recompenseEngagerTampon, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using recompenseRembouserCarte as a search criteria.
	 *
	 * @param recompenseRembouserCarte
	 * @return An Object Enseigne whose recompenseRembouserCarte is equals to the given recompenseRembouserCarte. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.recompenseRembouserCarte = :recompenseRembouserCarte and e.isDeleted = :isDeleted")
	List<Enseigne> findByRecompenseRembouserCarte(@Param("recompenseRembouserCarte")Long recompenseRembouserCarte, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using recompenseRembouserTampon as a search criteria.
	 *
	 * @param recompenseRembouserTampon
	 * @return An Object Enseigne whose recompenseRembouserTampon is equals to the given recompenseRembouserTampon. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.recompenseRembouserTampon = :recompenseRembouserTampon and e.isDeleted = :isDeleted")
	List<Enseigne> findByRecompenseRembouserTampon(@Param("recompenseRembouserTampon")Long recompenseRembouserTampon, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using nbreMaxiUserSouscription as a search criteria.
	 *
	 * @param nbreMaxiUserSouscription
	 * @return An Object Enseigne whose nbreMaxiUserSouscription is equals to the given nbreMaxiUserSouscription. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nbreMaxiUserSouscription = :nbreMaxiUserSouscription and e.isDeleted = :isDeleted")
	List<Enseigne> findByNbreMaxiUserSouscription(@Param("nbreMaxiUserSouscription")Integer nbreMaxiUserSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using nbreUtilisateurMobile as a search criteria.
	 *
	 * @param nbreUtilisateurMobile
	 * @return An Object Enseigne whose nbreUtilisateurMobile is equals to the given nbreUtilisateurMobile. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nbreUtilisateurMobile = :nbreUtilisateurMobile and e.isDeleted = :isDeleted")
	List<Enseigne> findByNbreUtilisateurMobile(@Param("nbreUtilisateurMobile")Integer nbreUtilisateurMobile, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using nbreSouscription as a search criteria.
	 *
	 * @param nbreSouscription
	 * @return An Object Enseigne whose nbreSouscription is equals to the given nbreSouscription. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nbreSouscription = :nbreSouscription and e.isDeleted = :isDeleted")
	List<Enseigne> findByNbreSouscription(@Param("nbreSouscription")Integer nbreSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using prixSpecifique as a search criteria.
	 *
	 * @param prixSpecifique
	 * @return An Object Enseigne whose prixSpecifique is equals to the given prixSpecifique. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.prixSpecifique = :prixSpecifique and e.isDeleted = :isDeleted")
	List<Enseigne> findByPrixSpecifique(@Param("prixSpecifique")Integer prixSpecifique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using dateExpirationSouscription as a search criteria.
	 *
	 * @param dateExpirationSouscription
	 * @return An Object Enseigne whose dateExpirationSouscription is equals to the given dateExpirationSouscription. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.dateExpirationSouscription = :dateExpirationSouscription and e.isDeleted = :isDeleted")
	List<Enseigne> findByDateExpirationSouscription(@Param("dateExpirationSouscription")Date dateExpirationSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Enseigne by using isActive as a search criteria.
	 *
	 * @param isActive
	 * @return An Object Enseigne whose isActive is equals to the given isActive. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<Enseigne> findByIsActive(@Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Enseigne by using roleGerantId as a search criteria.
	 *
	 * @param roleGerantId
	 * @return A list of Object Enseigne whose roleGerantId is equals to the given roleGerantId. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.role.id = :roleGerantId and e.isDeleted = :isDeleted")
	List<Enseigne> findByRoleGerantId(@Param("roleGerantId")Integer roleGerantId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Enseigne by using roleGerantId as a search criteria.
   *
   * @param roleGerantId
   * @return An Object Enseigne whose roleGerantId is equals to the given roleGerantId. If
   *         no Enseigne is found, this method returns null.
   */
  @Query("select e from Enseigne e where e.role.id = :roleGerantId and e.isDeleted = :isDeleted")
  Enseigne findEnseigneByRoleGerantId(@Param("roleGerantId")Integer roleGerantId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Enseigne by using etatSouscriptionId as a search criteria.
	 *
	 * @param etatSouscriptionId
	 * @return A list of Object Enseigne whose etatSouscriptionId is equals to the given etatSouscriptionId. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.etatSouscription.id = :etatSouscriptionId and e.isDeleted = :isDeleted")
	List<Enseigne> findByEtatSouscriptionId(@Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Enseigne by using etatSouscriptionId as a search criteria.
   *
   * @param etatSouscriptionId
   * @return An Object Enseigne whose etatSouscriptionId is equals to the given etatSouscriptionId. If
   *         no Enseigne is found, this method returns null.
   */
  @Query("select e from Enseigne e where e.etatSouscription.id = :etatSouscriptionId and e.isDeleted = :isDeleted")
  Enseigne findEnseigneByEtatSouscriptionId(@Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Enseigne by using souscriptionId as a search criteria.
	 *
	 * @param souscriptionId
	 * @return A list of Object Enseigne whose souscriptionId is equals to the given souscriptionId. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.souscription.id = :souscriptionId and e.isDeleted = :isDeleted")
	List<Enseigne> findBySouscriptionId(@Param("souscriptionId")Integer souscriptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Enseigne by using souscriptionId as a search criteria.
   *
   * @param souscriptionId
   * @return An Object Enseigne whose souscriptionId is equals to the given souscriptionId. If
   *         no Enseigne is found, this method returns null.
   */
  @Query("select e from Enseigne e where e.souscription.id = :souscriptionId and e.isDeleted = :isDeleted")
  Enseigne findEnseigneBySouscriptionId(@Param("souscriptionId")Integer souscriptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Enseigne by using modePaiementId as a search criteria.
	 *
	 * @param modePaiementId
	 * @return A list of Object Enseigne whose modePaiementId is equals to the given modePaiementId. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.modePaiement.id = :modePaiementId and e.isDeleted = :isDeleted")
	List<Enseigne> findByModePaiementId(@Param("modePaiementId")Integer modePaiementId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Enseigne by using modePaiementId as a search criteria.
   *
   * @param modePaiementId
   * @return An Object Enseigne whose modePaiementId is equals to the given modePaiementId. If
   *         no Enseigne is found, this method returns null.
   */
  @Query("select e from Enseigne e where e.modePaiement.id = :modePaiementId and e.isDeleted = :isDeleted")
  Enseigne findEnseigneByModePaiementId(@Param("modePaiementId")Integer modePaiementId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Enseigne by using enseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Enseigne
	 * @throws DataAccessException,ParseException
	 */
	public default List<Enseigne> getByCriteria(Request<EnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Enseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Enseigne> query = em.createQuery(req, Enseigne.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Enseigne by using enseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Enseigne
	 *
	 */
	public default Long count(Request<EnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Enseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<EnseigneDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		EnseigneDto dto = request.getData() != null ? request.getData() : new EnseigneDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (EnseigneDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(EnseigneDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlLogo())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlLogo", dto.getUrlLogo(), "e.urlLogo", "String", dto.getUrlLogoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFacebook())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("facebook", dto.getFacebook(), "e.facebook", "String", dto.getFacebookParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getInstagram())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("instagram", dto.getInstagram(), "e.instagram", "String", dto.getInstagramParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTwitter())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("twitter", dto.getTwitter(), "e.twitter", "String", dto.getTwitterParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSiteInternet())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("siteInternet", dto.getSiteInternet(), "e.siteInternet", "String", dto.getSiteInternetParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCouleur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("couleur", dto.getCouleur(), "e.couleur", "String", dto.getCouleurParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsAccepted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAccepted", dto.getIsAccepted(), "e.isAccepted", "Boolean", dto.getIsAcceptedParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNomAdmin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nomAdmin", dto.getNomAdmin(), "e.nomAdmin", "String", dto.getNomAdminParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenomsAdmin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenomsAdmin", dto.getPrenomsAdmin(), "e.prenomsAdmin", "String", dto.getPrenomsAdminParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmailAdmin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("emailAdmin", dto.getEmailAdmin(), "e.emailAdmin", "String", dto.getEmailAdminParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephoneAdmin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephoneAdmin", dto.getTelephoneAdmin(), "e.telephoneAdmin", "String", dto.getTelephoneAdminParam(), param, index, locale));
			}
			if (dto.getChiffreDaffaire()!= null && dto.getChiffreDaffaire() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("chiffreDaffaire", dto.getChiffreDaffaire(), "e.chiffreDaffaire", "Long", dto.getChiffreDaffaireParam(), param, index, locale));
			}
			if (dto.getNbreTransaction()!= null && dto.getNbreTransaction() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTransaction", dto.getNbreTransaction(), "e.nbreTransaction", "Long", dto.getNbreTransactionParam(), param, index, locale));
			}
			if (dto.getRecompenseEngagerCarte()!= null && dto.getRecompenseEngagerCarte() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("recompenseEngagerCarte", dto.getRecompenseEngagerCarte(), "e.recompenseEngagerCarte", "Long", dto.getRecompenseEngagerCarteParam(), param, index, locale));
			}
			if (dto.getRecompenseEngagerTampon()!= null && dto.getRecompenseEngagerTampon() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("recompenseEngagerTampon", dto.getRecompenseEngagerTampon(), "e.recompenseEngagerTampon", "Long", dto.getRecompenseEngagerTamponParam(), param, index, locale));
			}
			if (dto.getRecompenseRembouserCarte()!= null && dto.getRecompenseRembouserCarte() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("recompenseRembouserCarte", dto.getRecompenseRembouserCarte(), "e.recompenseRembouserCarte", "Long", dto.getRecompenseRembouserCarteParam(), param, index, locale));
			}
			if (dto.getRecompenseRembouserTampon()!= null && dto.getRecompenseRembouserTampon() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("recompenseRembouserTampon", dto.getRecompenseRembouserTampon(), "e.recompenseRembouserTampon", "Long", dto.getRecompenseRembouserTamponParam(), param, index, locale));
			}
			if (dto.getNbreMaxiUserSouscription()!= null && dto.getNbreMaxiUserSouscription() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreMaxiUserSouscription", dto.getNbreMaxiUserSouscription(), "e.nbreMaxiUserSouscription", "Integer", dto.getNbreMaxiUserSouscriptionParam(), param, index, locale));
			}
			if (dto.getNbreUtilisateurMobile()!= null && dto.getNbreUtilisateurMobile() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreUtilisateurMobile", dto.getNbreUtilisateurMobile(), "e.nbreUtilisateurMobile", "Integer", dto.getNbreUtilisateurMobileParam(), param, index, locale));
			}
			if (dto.getNbreSouscription()!= null && dto.getNbreSouscription() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreSouscription", dto.getNbreSouscription(), "e.nbreSouscription", "Integer", dto.getNbreSouscriptionParam(), param, index, locale));
			}
			if (dto.getPrixSpecifique()!= null && dto.getPrixSpecifique() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prixSpecifique", dto.getPrixSpecifique(), "e.prixSpecifique", "Integer", dto.getPrixSpecifiqueParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateExpirationSouscription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateExpirationSouscription", dto.getDateExpirationSouscription(), "e.dateExpirationSouscription", "Date", dto.getDateExpirationSouscriptionParam(), param, index, locale));
			}
			if (dto.getIsActive()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isActive", dto.getIsActive(), "e.isActive", "Boolean", dto.getIsActiveParam(), param, index, locale));
			}
			if (dto.getRoleGerantId()!= null && dto.getRoleGerantId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleGerantId", dto.getRoleGerantId(), "e.role.id", "Integer", dto.getRoleGerantIdParam(), param, index, locale));
			}
			if (dto.getEtatSouscriptionId()!= null && dto.getEtatSouscriptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionId", dto.getEtatSouscriptionId(), "e.etatSouscription.id", "Integer", dto.getEtatSouscriptionIdParam(), param, index, locale));
			}
			if (dto.getSouscriptionId()!= null && dto.getSouscriptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionId", dto.getSouscriptionId(), "e.souscription.id", "Integer", dto.getSouscriptionIdParam(), param, index, locale));
			}
			if (dto.getModePaiementId()!= null && dto.getModePaiementId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementId", dto.getModePaiementId(), "e.modePaiement.id", "Integer", dto.getModePaiementIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleLibelle", dto.getRoleLibelle(), "e.role.libelle", "String", dto.getRoleLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleCode", dto.getRoleCode(), "e.role.code", "String", dto.getRoleCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatSouscriptionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionLibelle", dto.getEtatSouscriptionLibelle(), "e.etatSouscription.libelle", "String", dto.getEtatSouscriptionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatSouscriptionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionCode", dto.getEtatSouscriptionCode(), "e.etatSouscription.code", "String", dto.getEtatSouscriptionCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getModePaiementLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementLibelle", dto.getModePaiementLibelle(), "e.modePaiement.libelle", "String", dto.getModePaiementLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getModePaiementCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementCode", dto.getModePaiementCode(), "e.modePaiement.code", "String", dto.getModePaiementCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
