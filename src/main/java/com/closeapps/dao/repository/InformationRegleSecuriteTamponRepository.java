
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._InformationRegleSecuriteTamponRepository;

/**
 * Repository : InformationRegleSecuriteTampon.
 */
@Repository
public interface InformationRegleSecuriteTamponRepository extends JpaRepository<InformationRegleSecuriteTampon, Integer>, _InformationRegleSecuriteTamponRepository {
	/**
	 * Finds InformationRegleSecuriteTampon by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object InformationRegleSecuriteTampon whose id is equals to the given id. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.id = :id and e.isDeleted = :isDeleted")
	InformationRegleSecuriteTampon findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds InformationRegleSecuriteTampon by using nbreMaxiParPdv as a search criteria.
	 *
	 * @param nbreMaxiParPdv
	 * @return An Object InformationRegleSecuriteTampon whose nbreMaxiParPdv is equals to the given nbreMaxiParPdv. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.nbreMaxiParPdv = :nbreMaxiParPdv and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByNbreMaxiParPdv(@Param("nbreMaxiParPdv")Integer nbreMaxiParPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using nbreMaxiAllPdv as a search criteria.
	 *
	 * @param nbreMaxiAllPdv
	 * @return An Object InformationRegleSecuriteTampon whose nbreMaxiAllPdv is equals to the given nbreMaxiAllPdv. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.nbreMaxiAllPdv = :nbreMaxiAllPdv and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByNbreMaxiAllPdv(@Param("nbreMaxiAllPdv")Integer nbreMaxiAllPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object InformationRegleSecuriteTampon whose createdBy is equals to the given createdBy. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object InformationRegleSecuriteTampon whose createdAt is equals to the given createdAt. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object InformationRegleSecuriteTampon whose updatedBy is equals to the given updatedBy. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object InformationRegleSecuriteTampon whose updatedAt is equals to the given updatedAt. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object InformationRegleSecuriteTampon whose deletedBy is equals to the given deletedBy. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object InformationRegleSecuriteTampon whose deletedAt is equals to the given deletedAt. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteTampon by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object InformationRegleSecuriteTampon whose isDeleted is equals to the given isDeleted. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds InformationRegleSecuriteTampon by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object InformationRegleSecuriteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one InformationRegleSecuriteTampon by using programmeFideliteTamponId as a search criteria.
   *
   * @param programmeFideliteTamponId
   * @return An Object InformationRegleSecuriteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
   *         no InformationRegleSecuriteTampon is found, this method returns null.
   */
  @Query("select e from InformationRegleSecuriteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
  InformationRegleSecuriteTampon findInformationRegleSecuriteTamponByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds InformationRegleSecuriteTampon by using uniteTempsId as a search criteria.
	 *
	 * @param uniteTempsId
	 * @return A list of Object InformationRegleSecuriteTampon whose uniteTempsId is equals to the given uniteTempsId. If
	 *         no InformationRegleSecuriteTampon is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteTampon e where e.uniteTemps.id = :uniteTempsId and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteTampon> findByUniteTempsId(@Param("uniteTempsId")Integer uniteTempsId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one InformationRegleSecuriteTampon by using uniteTempsId as a search criteria.
   *
   * @param uniteTempsId
   * @return An Object InformationRegleSecuriteTampon whose uniteTempsId is equals to the given uniteTempsId. If
   *         no InformationRegleSecuriteTampon is found, this method returns null.
   */
  @Query("select e from InformationRegleSecuriteTampon e where e.uniteTemps.id = :uniteTempsId and e.isDeleted = :isDeleted")
  InformationRegleSecuriteTampon findInformationRegleSecuriteTamponByUniteTempsId(@Param("uniteTempsId")Integer uniteTempsId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of InformationRegleSecuriteTampon by using informationRegleSecuriteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of InformationRegleSecuriteTampon
	 * @throws DataAccessException,ParseException
	 */
	public default List<InformationRegleSecuriteTampon> getByCriteria(Request<InformationRegleSecuriteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from InformationRegleSecuriteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<InformationRegleSecuriteTampon> query = em.createQuery(req, InformationRegleSecuriteTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of InformationRegleSecuriteTampon by using informationRegleSecuriteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of InformationRegleSecuriteTampon
	 *
	 */
	public default Long count(Request<InformationRegleSecuriteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from InformationRegleSecuriteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<InformationRegleSecuriteTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		InformationRegleSecuriteTamponDto dto = request.getData() != null ? request.getData() : new InformationRegleSecuriteTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (InformationRegleSecuriteTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(InformationRegleSecuriteTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getNbreMaxiParPdv()!= null && dto.getNbreMaxiParPdv() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreMaxiParPdv", dto.getNbreMaxiParPdv(), "e.nbreMaxiParPdv", "Integer", dto.getNbreMaxiParPdvParam(), param, index, locale));
			}
			if (dto.getNbreMaxiAllPdv()!= null && dto.getNbreMaxiAllPdv() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreMaxiAllPdv", dto.getNbreMaxiAllPdv(), "e.nbreMaxiAllPdv", "Integer", dto.getNbreMaxiAllPdvParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getUniteTempsId()!= null && dto.getUniteTempsId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsId", dto.getUniteTempsId(), "e.uniteTemps.id", "Integer", dto.getUniteTempsIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUniteTempsLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsLibelle", dto.getUniteTempsLibelle(), "e.uniteTemps.libelle", "String", dto.getUniteTempsLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUniteTempsCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsCode", dto.getUniteTempsCode(), "e.uniteTemps.code", "String", dto.getUniteTempsCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
