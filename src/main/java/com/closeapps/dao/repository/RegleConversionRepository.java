
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._RegleConversionRepository;

/**
 * Repository : RegleConversion.
 */
@Repository
public interface RegleConversionRepository extends JpaRepository<RegleConversion, Integer>, _RegleConversionRepository {
	/**
	 * Finds RegleConversion by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object RegleConversion whose id is equals to the given id. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.id = :id and e.isDeleted = :isDeleted")
	RegleConversion findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds RegleConversion by using valeurAcheter as a search criteria.
	 *
	 * @param valeurAcheter
	 * @return An Object RegleConversion whose valeurAcheter is equals to the given valeurAcheter. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.valeurAcheter = :valeurAcheter and e.isDeleted = :isDeleted")
	List<RegleConversion> findByValeurAcheter(@Param("valeurAcheter")String valeurAcheter, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using pointGagner as a search criteria.
	 *
	 * @param pointGagner
	 * @return An Object RegleConversion whose pointGagner is equals to the given pointGagner. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.pointGagner = :pointGagner and e.isDeleted = :isDeleted")
	List<RegleConversion> findByPointGagner(@Param("pointGagner")String pointGagner, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using sommeCorrespondant as a search criteria.
	 *
	 * @param sommeCorrespondant
	 * @return An Object RegleConversion whose sommeCorrespondant is equals to the given sommeCorrespondant. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.sommeCorrespondant = :sommeCorrespondant and e.isDeleted = :isDeleted")
	List<RegleConversion> findBySommeCorrespondant(@Param("sommeCorrespondant")String sommeCorrespondant, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object RegleConversion whose createdBy is equals to the given createdBy. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<RegleConversion> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object RegleConversion whose createdAt is equals to the given createdAt. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<RegleConversion> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object RegleConversion whose updatedBy is equals to the given updatedBy. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<RegleConversion> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object RegleConversion whose updatedAt is equals to the given updatedAt. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<RegleConversion> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object RegleConversion whose deletedBy is equals to the given deletedBy. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<RegleConversion> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object RegleConversion whose deletedAt is equals to the given deletedAt. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<RegleConversion> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds RegleConversion by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object RegleConversion whose isDeleted is equals to the given isDeleted. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.isDeleted = :isDeleted")
	List<RegleConversion> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds RegleConversion by using produitId as a search criteria.
	 *
	 * @param produitId
	 * @return A list of Object RegleConversion whose produitId is equals to the given produitId. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
	List<RegleConversion> findByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one RegleConversion by using produitId as a search criteria.
   *
   * @param produitId
   * @return An Object RegleConversion whose produitId is equals to the given produitId. If
   *         no RegleConversion is found, this method returns null.
   */
  @Query("select e from RegleConversion e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
  RegleConversion findRegleConversionByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds RegleConversion by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object RegleConversion whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no RegleConversion is found, this method returns null.
	 */
	@Query("select e from RegleConversion e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<RegleConversion> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one RegleConversion by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object RegleConversion whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no RegleConversion is found, this method returns null.
   */
  @Query("select e from RegleConversion e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  RegleConversion findRegleConversionByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of RegleConversion by using regleConversionDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of RegleConversion
	 * @throws DataAccessException,ParseException
	 */
	public default List<RegleConversion> getByCriteria(Request<RegleConversionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from RegleConversion e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<RegleConversion> query = em.createQuery(req, RegleConversion.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of RegleConversion by using regleConversionDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of RegleConversion
	 *
	 */
	public default Long count(Request<RegleConversionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from RegleConversion e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<RegleConversionDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		RegleConversionDto dto = request.getData() != null ? request.getData() : new RegleConversionDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (RegleConversionDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(RegleConversionDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getValeurAcheter())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("valeurAcheter", dto.getValeurAcheter(), "e.valeurAcheter", "String", dto.getValeurAcheterParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointGagner())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointGagner", dto.getPointGagner(), "e.pointGagner", "String", dto.getPointGagnerParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSommeCorrespondant())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sommeCorrespondant", dto.getSommeCorrespondant(), "e.sommeCorrespondant", "String", dto.getSommeCorrespondantParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getProduitId()!= null && dto.getProduitId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitId", dto.getProduitId(), "e.produit.id", "Integer", dto.getProduitIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProduitLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitLibelle", dto.getProduitLibelle(), "e.produit.libelle", "String", dto.getProduitLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
