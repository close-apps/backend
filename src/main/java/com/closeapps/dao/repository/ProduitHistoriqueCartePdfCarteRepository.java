
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._ProduitHistoriqueCartePdfCarteRepository;

/**
 * Repository : ProduitHistoriqueCartePdfCarte.
 */
@Repository
public interface ProduitHistoriqueCartePdfCarteRepository extends JpaRepository<ProduitHistoriqueCartePdfCarte, Integer>, _ProduitHistoriqueCartePdfCarteRepository {
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ProduitHistoriqueCartePdfCarte whose id is equals to the given id. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.id = :id and e.isDeleted = :isDeleted")
	ProduitHistoriqueCartePdfCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ProduitHistoriqueCartePdfCarte whose createdBy is equals to the given createdBy. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ProduitHistoriqueCartePdfCarte whose createdAt is equals to the given createdAt. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ProduitHistoriqueCartePdfCarte whose updatedBy is equals to the given updatedBy. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ProduitHistoriqueCartePdfCarte whose updatedAt is equals to the given updatedAt. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ProduitHistoriqueCartePdfCarte whose deletedBy is equals to the given deletedBy. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ProduitHistoriqueCartePdfCarte whose deletedAt is equals to the given deletedAt. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ProduitHistoriqueCartePdfCarte whose isDeleted is equals to the given isDeleted. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using historiqueCarteProgrammeFideliteCarteId as a search criteria.
	 *
	 * @param historiqueCarteProgrammeFideliteCarteId
	 * @return A list of Object ProduitHistoriqueCartePdfCarte whose historiqueCarteProgrammeFideliteCarteId is equals to the given historiqueCarteProgrammeFideliteCarteId. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.historiqueCarteProgrammeFideliteCarte.id = :historiqueCarteProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByHistoriqueCarteProgrammeFideliteCarteId(@Param("historiqueCarteProgrammeFideliteCarteId")Integer historiqueCarteProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProduitHistoriqueCartePdfCarte by using historiqueCarteProgrammeFideliteCarteId as a search criteria.
   *
   * @param historiqueCarteProgrammeFideliteCarteId
   * @return An Object ProduitHistoriqueCartePdfCarte whose historiqueCarteProgrammeFideliteCarteId is equals to the given historiqueCarteProgrammeFideliteCarteId. If
   *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
   */
  @Query("select e from ProduitHistoriqueCartePdfCarte e where e.historiqueCarteProgrammeFideliteCarte.id = :historiqueCarteProgrammeFideliteCarteId and e.isDeleted = :isDeleted")
  ProduitHistoriqueCartePdfCarte findProduitHistoriqueCartePdfCarteByHistoriqueCarteProgrammeFideliteCarteId(@Param("historiqueCarteProgrammeFideliteCarteId")Integer historiqueCarteProgrammeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds ProduitHistoriqueCartePdfCarte by using produitId as a search criteria.
	 *
	 * @param produitId
	 * @return A list of Object ProduitHistoriqueCartePdfCarte whose produitId is equals to the given produitId. If
	 *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfCarte e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfCarte> findByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProduitHistoriqueCartePdfCarte by using produitId as a search criteria.
   *
   * @param produitId
   * @return An Object ProduitHistoriqueCartePdfCarte whose produitId is equals to the given produitId. If
   *         no ProduitHistoriqueCartePdfCarte is found, this method returns null.
   */
  @Query("select e from ProduitHistoriqueCartePdfCarte e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
  ProduitHistoriqueCartePdfCarte findProduitHistoriqueCartePdfCarteByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ProduitHistoriqueCartePdfCarte by using produitHistoriqueCartePdfCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ProduitHistoriqueCartePdfCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<ProduitHistoriqueCartePdfCarte> getByCriteria(Request<ProduitHistoriqueCartePdfCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ProduitHistoriqueCartePdfCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ProduitHistoriqueCartePdfCarte> query = em.createQuery(req, ProduitHistoriqueCartePdfCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ProduitHistoriqueCartePdfCarte by using produitHistoriqueCartePdfCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ProduitHistoriqueCartePdfCarte
	 *
	 */
	public default Long count(Request<ProduitHistoriqueCartePdfCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ProduitHistoriqueCartePdfCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ProduitHistoriqueCartePdfCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ProduitHistoriqueCartePdfCarteDto dto = request.getData() != null ? request.getData() : new ProduitHistoriqueCartePdfCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ProduitHistoriqueCartePdfCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ProduitHistoriqueCartePdfCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getHistoriqueCarteProgrammeFideliteCarteId()!= null && dto.getHistoriqueCarteProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("historiqueCarteProgrammeFideliteCarteId", dto.getHistoriqueCarteProgrammeFideliteCarteId(), "e.historiqueCarteProgrammeFideliteCarte.id", "Integer", dto.getHistoriqueCarteProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (dto.getProduitId()!= null && dto.getProduitId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitId", dto.getProduitId(), "e.produit.id", "Integer", dto.getProduitIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProduitLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitLibelle", dto.getProduitLibelle(), "e.produit.libelle", "String", dto.getProduitLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
