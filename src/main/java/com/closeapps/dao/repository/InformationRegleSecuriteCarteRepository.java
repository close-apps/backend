
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._InformationRegleSecuriteCarteRepository;

/**
 * Repository : InformationRegleSecuriteCarte.
 */
@Repository
public interface InformationRegleSecuriteCarteRepository extends JpaRepository<InformationRegleSecuriteCarte, Integer>, _InformationRegleSecuriteCarteRepository {
	/**
	 * Finds InformationRegleSecuriteCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object InformationRegleSecuriteCarte whose id is equals to the given id. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.id = :id and e.isDeleted = :isDeleted")
	InformationRegleSecuriteCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds InformationRegleSecuriteCarte by using plafondSommeACrediter as a search criteria.
	 *
	 * @param plafondSommeACrediter
	 * @return An Object InformationRegleSecuriteCarte whose plafondSommeACrediter is equals to the given plafondSommeACrediter. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.plafondSommeACrediter = :plafondSommeACrediter and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByPlafondSommeACrediter(@Param("plafondSommeACrediter")Integer plafondSommeACrediter, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using nbreFoisCrediterParPdv as a search criteria.
	 *
	 * @param nbreFoisCrediterParPdv
	 * @return An Object InformationRegleSecuriteCarte whose nbreFoisCrediterParPdv is equals to the given nbreFoisCrediterParPdv. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.nbreFoisCrediterParPdv = :nbreFoisCrediterParPdv and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByNbreFoisCrediterParPdv(@Param("nbreFoisCrediterParPdv")Integer nbreFoisCrediterParPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using nbreFoisCrediterAllPdv as a search criteria.
	 *
	 * @param nbreFoisCrediterAllPdv
	 * @return An Object InformationRegleSecuriteCarte whose nbreFoisCrediterAllPdv is equals to the given nbreFoisCrediterAllPdv. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.nbreFoisCrediterAllPdv = :nbreFoisCrediterAllPdv and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByNbreFoisCrediterAllPdv(@Param("nbreFoisCrediterAllPdv")Integer nbreFoisCrediterAllPdv, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object InformationRegleSecuriteCarte whose createdBy is equals to the given createdBy. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object InformationRegleSecuriteCarte whose createdAt is equals to the given createdAt. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object InformationRegleSecuriteCarte whose updatedBy is equals to the given updatedBy. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object InformationRegleSecuriteCarte whose updatedAt is equals to the given updatedAt. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object InformationRegleSecuriteCarte whose deletedBy is equals to the given deletedBy. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object InformationRegleSecuriteCarte whose deletedAt is equals to the given deletedAt. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds InformationRegleSecuriteCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object InformationRegleSecuriteCarte whose isDeleted is equals to the given isDeleted. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds InformationRegleSecuriteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object InformationRegleSecuriteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one InformationRegleSecuriteCarte by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object InformationRegleSecuriteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no InformationRegleSecuriteCarte is found, this method returns null.
   */
  @Query("select e from InformationRegleSecuriteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  InformationRegleSecuriteCarte findInformationRegleSecuriteCarteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds InformationRegleSecuriteCarte by using uniteTempsId as a search criteria.
	 *
	 * @param uniteTempsId
	 * @return A list of Object InformationRegleSecuriteCarte whose uniteTempsId is equals to the given uniteTempsId. If
	 *         no InformationRegleSecuriteCarte is found, this method returns null.
	 */
	@Query("select e from InformationRegleSecuriteCarte e where e.uniteTemps.id = :uniteTempsId and e.isDeleted = :isDeleted")
	List<InformationRegleSecuriteCarte> findByUniteTempsId(@Param("uniteTempsId")Integer uniteTempsId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one InformationRegleSecuriteCarte by using uniteTempsId as a search criteria.
   *
   * @param uniteTempsId
   * @return An Object InformationRegleSecuriteCarte whose uniteTempsId is equals to the given uniteTempsId. If
   *         no InformationRegleSecuriteCarte is found, this method returns null.
   */
  @Query("select e from InformationRegleSecuriteCarte e where e.uniteTemps.id = :uniteTempsId and e.isDeleted = :isDeleted")
  InformationRegleSecuriteCarte findInformationRegleSecuriteCarteByUniteTempsId(@Param("uniteTempsId")Integer uniteTempsId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of InformationRegleSecuriteCarte by using informationRegleSecuriteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of InformationRegleSecuriteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<InformationRegleSecuriteCarte> getByCriteria(Request<InformationRegleSecuriteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from InformationRegleSecuriteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<InformationRegleSecuriteCarte> query = em.createQuery(req, InformationRegleSecuriteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of InformationRegleSecuriteCarte by using informationRegleSecuriteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of InformationRegleSecuriteCarte
	 *
	 */
	public default Long count(Request<InformationRegleSecuriteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from InformationRegleSecuriteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<InformationRegleSecuriteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		InformationRegleSecuriteCarteDto dto = request.getData() != null ? request.getData() : new InformationRegleSecuriteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (InformationRegleSecuriteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(InformationRegleSecuriteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getPlafondSommeACrediter()!= null && dto.getPlafondSommeACrediter() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("plafondSommeACrediter", dto.getPlafondSommeACrediter(), "e.plafondSommeACrediter", "Integer", dto.getPlafondSommeACrediterParam(), param, index, locale));
			}
			if (dto.getNbreFoisCrediterParPdv()!= null && dto.getNbreFoisCrediterParPdv() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreFoisCrediterParPdv", dto.getNbreFoisCrediterParPdv(), "e.nbreFoisCrediterParPdv", "Integer", dto.getNbreFoisCrediterParPdvParam(), param, index, locale));
			}
			if (dto.getNbreFoisCrediterAllPdv()!= null && dto.getNbreFoisCrediterAllPdv() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreFoisCrediterAllPdv", dto.getNbreFoisCrediterAllPdv(), "e.nbreFoisCrediterAllPdv", "Integer", dto.getNbreFoisCrediterAllPdvParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (dto.getUniteTempsId()!= null && dto.getUniteTempsId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsId", dto.getUniteTempsId(), "e.uniteTemps.id", "Integer", dto.getUniteTempsIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUniteTempsLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsLibelle", dto.getUniteTempsLibelle(), "e.uniteTemps.libelle", "String", dto.getUniteTempsLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUniteTempsCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("uniteTempsCode", dto.getUniteTempsCode(), "e.uniteTemps.code", "String", dto.getUniteTempsCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
