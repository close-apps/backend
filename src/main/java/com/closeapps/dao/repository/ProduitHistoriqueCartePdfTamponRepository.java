
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._ProduitHistoriqueCartePdfTamponRepository;

/**
 * Repository : ProduitHistoriqueCartePdfTampon.
 */
@Repository
public interface ProduitHistoriqueCartePdfTamponRepository extends JpaRepository<ProduitHistoriqueCartePdfTampon, Integer>, _ProduitHistoriqueCartePdfTamponRepository {
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object ProduitHistoriqueCartePdfTampon whose id is equals to the given id. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.id = :id and e.isDeleted = :isDeleted")
	ProduitHistoriqueCartePdfTampon findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ProduitHistoriqueCartePdfTampon whose createdBy is equals to the given createdBy. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object ProduitHistoriqueCartePdfTampon whose createdAt is equals to the given createdAt. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object ProduitHistoriqueCartePdfTampon whose updatedBy is equals to the given updatedBy. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object ProduitHistoriqueCartePdfTampon whose updatedAt is equals to the given updatedAt. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object ProduitHistoriqueCartePdfTampon whose deletedBy is equals to the given deletedBy. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object ProduitHistoriqueCartePdfTampon whose deletedAt is equals to the given deletedAt. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object ProduitHistoriqueCartePdfTampon whose isDeleted is equals to the given isDeleted. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using historiqueCarteProgrammeFideliteTamponId as a search criteria.
	 *
	 * @param historiqueCarteProgrammeFideliteTamponId
	 * @return A list of Object ProduitHistoriqueCartePdfTampon whose historiqueCarteProgrammeFideliteTamponId is equals to the given historiqueCarteProgrammeFideliteTamponId. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.historiqueCarteProgrammeFideliteTampon.id = :historiqueCarteProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByHistoriqueCarteProgrammeFideliteTamponId(@Param("historiqueCarteProgrammeFideliteTamponId")Integer historiqueCarteProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProduitHistoriqueCartePdfTampon by using historiqueCarteProgrammeFideliteTamponId as a search criteria.
   *
   * @param historiqueCarteProgrammeFideliteTamponId
   * @return An Object ProduitHistoriqueCartePdfTampon whose historiqueCarteProgrammeFideliteTamponId is equals to the given historiqueCarteProgrammeFideliteTamponId. If
   *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
   */
  @Query("select e from ProduitHistoriqueCartePdfTampon e where e.historiqueCarteProgrammeFideliteTampon.id = :historiqueCarteProgrammeFideliteTamponId and e.isDeleted = :isDeleted")
  ProduitHistoriqueCartePdfTampon findProduitHistoriqueCartePdfTamponByHistoriqueCarteProgrammeFideliteTamponId(@Param("historiqueCarteProgrammeFideliteTamponId")Integer historiqueCarteProgrammeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds ProduitHistoriqueCartePdfTampon by using produitId as a search criteria.
	 *
	 * @param produitId
	 * @return A list of Object ProduitHistoriqueCartePdfTampon whose produitId is equals to the given produitId. If
	 *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
	 */
	@Query("select e from ProduitHistoriqueCartePdfTampon e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
	List<ProduitHistoriqueCartePdfTampon> findByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one ProduitHistoriqueCartePdfTampon by using produitId as a search criteria.
   *
   * @param produitId
   * @return An Object ProduitHistoriqueCartePdfTampon whose produitId is equals to the given produitId. If
   *         no ProduitHistoriqueCartePdfTampon is found, this method returns null.
   */
  @Query("select e from ProduitHistoriqueCartePdfTampon e where e.produit.id = :produitId and e.isDeleted = :isDeleted")
  ProduitHistoriqueCartePdfTampon findProduitHistoriqueCartePdfTamponByProduitId(@Param("produitId")Integer produitId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of ProduitHistoriqueCartePdfTampon by using produitHistoriqueCartePdfTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of ProduitHistoriqueCartePdfTampon
	 * @throws DataAccessException,ParseException
	 */
	public default List<ProduitHistoriqueCartePdfTampon> getByCriteria(Request<ProduitHistoriqueCartePdfTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from ProduitHistoriqueCartePdfTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<ProduitHistoriqueCartePdfTampon> query = em.createQuery(req, ProduitHistoriqueCartePdfTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of ProduitHistoriqueCartePdfTampon by using produitHistoriqueCartePdfTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of ProduitHistoriqueCartePdfTampon
	 *
	 */
	public default Long count(Request<ProduitHistoriqueCartePdfTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from ProduitHistoriqueCartePdfTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ProduitHistoriqueCartePdfTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ProduitHistoriqueCartePdfTamponDto dto = request.getData() != null ? request.getData() : new ProduitHistoriqueCartePdfTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ProduitHistoriqueCartePdfTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ProduitHistoriqueCartePdfTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getHistoriqueCarteProgrammeFideliteTamponId()!= null && dto.getHistoriqueCarteProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("historiqueCarteProgrammeFideliteTamponId", dto.getHistoriqueCarteProgrammeFideliteTamponId(), "e.historiqueCarteProgrammeFideliteTampon.id", "Integer", dto.getHistoriqueCarteProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getProduitId()!= null && dto.getProduitId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitId", dto.getProduitId(), "e.produit.id", "Integer", dto.getProduitIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProduitLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("produitLibelle", dto.getProduitLibelle(), "e.produit.libelle", "String", dto.getProduitLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
