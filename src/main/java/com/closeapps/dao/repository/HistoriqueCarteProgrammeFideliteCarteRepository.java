
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._HistoriqueCarteProgrammeFideliteCarteRepository;

/**
 * Repository : HistoriqueCarteProgrammeFideliteCarte.
 */
@Repository
public interface HistoriqueCarteProgrammeFideliteCarteRepository extends JpaRepository<HistoriqueCarteProgrammeFideliteCarte, Integer>, _HistoriqueCarteProgrammeFideliteCarteRepository {
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose id is equals to the given id. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.id = :id and e.isDeleted = :isDeleted")
	HistoriqueCarteProgrammeFideliteCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose createdBy is equals to the given createdBy. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose createdAt is equals to the given createdAt. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose updatedBy is equals to the given updatedBy. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose updatedAt is equals to the given updatedAt. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose deletedBy is equals to the given deletedBy. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose deletedAt is equals to the given deletedAt. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose isDeleted is equals to the given isDeleted. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using nbrePointOperation as a search criteria.
	 *
	 * @param nbrePointOperation
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose nbrePointOperation is equals to the given nbrePointOperation. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.nbrePointOperation = :nbrePointOperation and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByNbrePointOperation(@Param("nbrePointOperation")Integer nbrePointOperation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using nbrePointActuel as a search criteria.
	 *
	 * @param nbrePointActuel
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose nbrePointActuel is equals to the given nbrePointActuel. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.nbrePointActuel = :nbrePointActuel and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByNbrePointActuel(@Param("nbrePointActuel")Integer nbrePointActuel, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose description is equals to the given description. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.description = :description and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using dateAction as a search criteria.
	 *
	 * @param dateAction
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose dateAction is equals to the given dateAction. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.dateAction = :dateAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByDateAction(@Param("dateAction")Date dateAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using heureAction as a search criteria.
	 *
	 * @param heureAction
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose heureAction is equals to the given heureAction. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.heureAction = :heureAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByHeureAction(@Param("heureAction")Date heureAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using nbrePointAvantAction as a search criteria.
	 *
	 * @param nbrePointAvantAction
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose nbrePointAvantAction is equals to the given nbrePointAvantAction. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.nbrePointAvantAction = :nbrePointAvantAction and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByNbrePointAvantAction(@Param("nbrePointAvantAction")Integer nbrePointAvantAction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using sommeOperation as a search criteria.
	 *
	 * @param sommeOperation
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose sommeOperation is equals to the given sommeOperation. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.sommeOperation = :sommeOperation and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findBySommeOperation(@Param("sommeOperation")Integer sommeOperation, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using chiffreDaffaire as a search criteria.
	 *
	 * @param chiffreDaffaire
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose chiffreDaffaire is equals to the given chiffreDaffaire. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire = :chiffreDaffaire and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByChiffreDaffaire(@Param("chiffreDaffaire")Integer chiffreDaffaire, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object HistoriqueCarteProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteCarte findHistoriqueCarteProgrammeFideliteCarteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionId as a search criteria.
	 *
	 * @param typeActionId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionId is equals to the given typeActionId. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.typeAction.id = :typeActionId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByTypeActionId(@Param("typeActionId")Integer typeActionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteCarte by using typeActionId as a search criteria.
   *
   * @param typeActionId
   * @return An Object HistoriqueCarteProgrammeFideliteCarte whose typeActionId is equals to the given typeActionId. If
   *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.typeAction.id = :typeActionId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteCarte findHistoriqueCarteProgrammeFideliteCarteByTypeActionId(@Param("typeActionId")Integer typeActionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose carteId is equals to the given carteId. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteCarte by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object HistoriqueCarteProgrammeFideliteCarte whose carteId is equals to the given carteId. If
   *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteCarte findHistoriqueCarteProgrammeFideliteCarteByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueCarteProgrammeFideliteCarte by using pointDeVenteId as a search criteria.
   *
   * @param pointDeVenteId
   * @return An Object HistoriqueCarteProgrammeFideliteCarte whose pointDeVenteId is equals to the given pointDeVenteId. If
   *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
  HistoriqueCarteProgrammeFideliteCarte findHistoriqueCarteProgrammeFideliteCarteByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of HistoriqueCarteProgrammeFideliteCarte by using historiqueCarteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of HistoriqueCarteProgrammeFideliteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<HistoriqueCarteProgrammeFideliteCarte> getByCriteria(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from HistoriqueCarteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<HistoriqueCarteProgrammeFideliteCarte> query = em.createQuery(req, HistoriqueCarteProgrammeFideliteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of HistoriqueCarteProgrammeFideliteCarte by using historiqueCarteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of HistoriqueCarteProgrammeFideliteCarte
	 *
	 */
	public default Long count(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from HistoriqueCarteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<HistoriqueCarteProgrammeFideliteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		HistoriqueCarteProgrammeFideliteCarteDto dto = request.getData() != null ? request.getData() : new HistoriqueCarteProgrammeFideliteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (HistoriqueCarteProgrammeFideliteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(HistoriqueCarteProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getNbrePointOperation()!= null && dto.getNbrePointOperation() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointOperation", dto.getNbrePointOperation(), "e.nbrePointOperation", "Integer", dto.getNbrePointOperationParam(), param, index, locale));
			}
			if (dto.getNbrePointActuel()!= null && dto.getNbrePointActuel() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointActuel", dto.getNbrePointActuel(), "e.nbrePointActuel", "Integer", dto.getNbrePointActuelParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateAction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateAction", dto.getDateAction(), "e.dateAction", "Date", dto.getDateActionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getHeureAction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("heureAction", dto.getHeureAction(), "e.heureAction", "Date", dto.getHeureActionParam(), param, index, locale));
			}
			if (dto.getNbrePointAvantAction()!= null && dto.getNbrePointAvantAction() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointAvantAction", dto.getNbrePointAvantAction(), "e.nbrePointAvantAction", "Integer", dto.getNbrePointAvantActionParam(), param, index, locale));
			}
			if (dto.getSommeOperation()!= null && dto.getSommeOperation() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sommeOperation", dto.getSommeOperation(), "e.sommeOperation", "Integer", dto.getSommeOperationParam(), param, index, locale));
			}
			if (dto.getChiffreDaffaire()!= null && dto.getChiffreDaffaire() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("chiffreDaffaire", dto.getChiffreDaffaire(), "e.chiffreDaffaire", "Integer", dto.getChiffreDaffaireParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (dto.getTypeActionId()!= null && dto.getTypeActionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionId", dto.getTypeActionId(), "e.typeAction.id", "Integer", dto.getTypeActionIdParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeActionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionLibelle", dto.getTypeActionLibelle(), "e.typeAction.libelle", "String", dto.getTypeActionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTypeActionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeActionCode", dto.getTypeActionCode(), "e.typeAction.code", "String", dto.getTypeActionCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
