
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._EtatInscripitionEnseigneRepository;

/**
 * Repository : EtatInscripitionEnseigne.
 */
@Repository
public interface EtatInscripitionEnseigneRepository extends JpaRepository<EtatInscripitionEnseigne, Integer>, _EtatInscripitionEnseigneRepository {
	/**
	 * Finds EtatInscripitionEnseigne by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object EtatInscripitionEnseigne whose id is equals to the given id. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.id = :id and e.isDeleted = :isDeleted")
	EtatInscripitionEnseigne findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds EtatInscripitionEnseigne by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object EtatInscripitionEnseigne whose createdBy is equals to the given createdBy. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object EtatInscripitionEnseigne whose createdAt is equals to the given createdAt. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object EtatInscripitionEnseigne whose updatedBy is equals to the given updatedBy. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object EtatInscripitionEnseigne whose updatedAt is equals to the given updatedAt. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object EtatInscripitionEnseigne whose deletedBy is equals to the given deletedBy. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object EtatInscripitionEnseigne whose deletedAt is equals to the given deletedAt. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using isValid as a search criteria.
	 *
	 * @param isValid
	 * @return An Object EtatInscripitionEnseigne whose isValid is equals to the given isValid. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.isValid = :isValid and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByIsValidx(@Param("isValid")Boolean isValid, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds EtatInscripitionEnseigne by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object EtatInscripitionEnseigne whose isDeleted is equals to the given isDeleted. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds EtatInscripitionEnseigne by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object EtatInscripitionEnseigne whose enseigneId is equals to the given enseigneId. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one EtatInscripitionEnseigne by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object EtatInscripitionEnseigne whose enseigneId is equals to the given enseigneId. If
   *         no EtatInscripitionEnseigne is found, this method returns null.
   */
  @Query("select e from EtatInscripitionEnseigne e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  EtatInscripitionEnseigne findEtatInscripitionEnseigneByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds EtatInscripitionEnseigne by using etatInscripitionId as a search criteria.
	 *
	 * @param etatInscripitionId
	 * @return A list of Object EtatInscripitionEnseigne whose etatInscripitionId is equals to the given etatInscripitionId. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.etatInscripition.id = :etatInscripitionId and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findByEtatInscripitionId(@Param("etatInscripitionId")Integer etatInscripitionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one EtatInscripitionEnseigne by using etatInscripitionId as a search criteria.
   *
   * @param etatInscripitionId
   * @return An Object EtatInscripitionEnseigne whose etatInscripitionId is equals to the given etatInscripitionId. If
   *         no EtatInscripitionEnseigne is found, this method returns null.
   */
  @Query("select e from EtatInscripitionEnseigne e where e.etatInscripition.id = :etatInscripitionId and e.isDeleted = :isDeleted")
  EtatInscripitionEnseigne findEtatInscripitionEnseigneByEtatInscripitionId(@Param("etatInscripitionId")Integer etatInscripitionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of EtatInscripitionEnseigne by using etatInscripitionEnseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of EtatInscripitionEnseigne
	 * @throws DataAccessException,ParseException
	 */
	public default List<EtatInscripitionEnseigne> getByCriteria(Request<EtatInscripitionEnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from EtatInscripitionEnseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<EtatInscripitionEnseigne> query = em.createQuery(req, EtatInscripitionEnseigne.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of EtatInscripitionEnseigne by using etatInscripitionEnseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of EtatInscripitionEnseigne
	 *
	 */
	public default Long count(Request<EtatInscripitionEnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from EtatInscripitionEnseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<EtatInscripitionEnseigneDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		EtatInscripitionEnseigneDto dto = request.getData() != null ? request.getData() : new EtatInscripitionEnseigneDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (EtatInscripitionEnseigneDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(EtatInscripitionEnseigneDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsValid()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValid", dto.getIsValid(), "e.isValid", "Boolean", dto.getIsValidParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getEtatInscripitionId()!= null && dto.getEtatInscripitionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatInscripitionId", dto.getEtatInscripitionId(), "e.etatInscripition.id", "Integer", dto.getEtatInscripitionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatInscripitionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatInscripitionLibelle", dto.getEtatInscripitionLibelle(), "e.etatInscripition.libelle", "String", dto.getEtatInscripitionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatInscripitionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatInscripitionCode", dto.getEtatInscripitionCode(), "e.etatInscripition.code", "String", dto.getEtatInscripitionCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
