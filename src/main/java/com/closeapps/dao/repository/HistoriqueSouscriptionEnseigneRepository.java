
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._HistoriqueSouscriptionEnseigneRepository;

/**
 * Repository : HistoriqueSouscriptionEnseigne.
 */
@Repository
public interface HistoriqueSouscriptionEnseigneRepository extends JpaRepository<HistoriqueSouscriptionEnseigne, Integer>, _HistoriqueSouscriptionEnseigneRepository {
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object HistoriqueSouscriptionEnseigne whose id is equals to the given id. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.id = :id and e.isDeleted = :isDeleted")
	HistoriqueSouscriptionEnseigne findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueSouscriptionEnseigne by using nbreSouscription as a search criteria.
	 *
	 * @param nbreSouscription
	 * @return An Object HistoriqueSouscriptionEnseigne whose nbreSouscription is equals to the given nbreSouscription. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.nbreSouscription = :nbreSouscription and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByNbreSouscription(@Param("nbreSouscription")Integer nbreSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using nbreUtilisateurMobile as a search criteria.
	 *
	 * @param nbreUtilisateurMobile
	 * @return An Object HistoriqueSouscriptionEnseigne whose nbreUtilisateurMobile is equals to the given nbreUtilisateurMobile. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.nbreUtilisateurMobile = :nbreUtilisateurMobile and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByNbreUtilisateurMobile(@Param("nbreUtilisateurMobile")Integer nbreUtilisateurMobile, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using commentaire as a search criteria.
	 *
	 * @param commentaire
	 * @return An Object HistoriqueSouscriptionEnseigne whose commentaire is equals to the given commentaire. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.commentaire = :commentaire and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByCommentaire(@Param("commentaire")String commentaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using prixSpecifique as a search criteria.
	 *
	 * @param prixSpecifique
	 * @return An Object HistoriqueSouscriptionEnseigne whose prixSpecifique is equals to the given prixSpecifique. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.prixSpecifique = :prixSpecifique and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByPrixSpecifique(@Param("prixSpecifique")Integer prixSpecifique, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object HistoriqueSouscriptionEnseigne whose createdBy is equals to the given createdBy. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object HistoriqueSouscriptionEnseigne whose createdAt is equals to the given createdAt. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object HistoriqueSouscriptionEnseigne whose updatedBy is equals to the given updatedBy. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object HistoriqueSouscriptionEnseigne whose updatedAt is equals to the given updatedAt. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object HistoriqueSouscriptionEnseigne whose deletedBy is equals to the given deletedBy. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object HistoriqueSouscriptionEnseigne whose deletedAt is equals to the given deletedAt. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using isActive as a search criteria.
	 *
	 * @param isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose isActive is equals to the given isActive. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByIsActive(@Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param dateExpiration
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.dateExpiration = :dateExpiration and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByDateExpiration(@Param("dateExpiration")Date dateExpiration, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object HistoriqueSouscriptionEnseigne whose isDeleted is equals to the given isDeleted. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueSouscriptionEnseigne by using souscriptionId as a search criteria.
	 *
	 * @param souscriptionId
	 * @return A list of Object HistoriqueSouscriptionEnseigne whose souscriptionId is equals to the given souscriptionId. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.souscription.id = :souscriptionId and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findBySouscriptionId(@Param("souscriptionId")Integer souscriptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueSouscriptionEnseigne by using souscriptionId as a search criteria.
   *
   * @param souscriptionId
   * @return An Object HistoriqueSouscriptionEnseigne whose souscriptionId is equals to the given souscriptionId. If
   *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
   */
  @Query("select e from HistoriqueSouscriptionEnseigne e where e.souscription.id = :souscriptionId and e.isDeleted = :isDeleted")
  HistoriqueSouscriptionEnseigne findHistoriqueSouscriptionEnseigneBySouscriptionId(@Param("souscriptionId")Integer souscriptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueSouscriptionEnseigne by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object HistoriqueSouscriptionEnseigne whose enseigneId is equals to the given enseigneId. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueSouscriptionEnseigne by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object HistoriqueSouscriptionEnseigne whose enseigneId is equals to the given enseigneId. If
   *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
   */
  @Query("select e from HistoriqueSouscriptionEnseigne e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  HistoriqueSouscriptionEnseigne findHistoriqueSouscriptionEnseigneByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueSouscriptionEnseigne by using modePaiementId as a search criteria.
	 *
	 * @param modePaiementId
	 * @return A list of Object HistoriqueSouscriptionEnseigne whose modePaiementId is equals to the given modePaiementId. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.modePaiement.id = :modePaiementId and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByModePaiementId(@Param("modePaiementId")Integer modePaiementId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueSouscriptionEnseigne by using modePaiementId as a search criteria.
   *
   * @param modePaiementId
   * @return An Object HistoriqueSouscriptionEnseigne whose modePaiementId is equals to the given modePaiementId. If
   *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
   */
  @Query("select e from HistoriqueSouscriptionEnseigne e where e.modePaiement.id = :modePaiementId and e.isDeleted = :isDeleted")
  HistoriqueSouscriptionEnseigne findHistoriqueSouscriptionEnseigneByModePaiementId(@Param("modePaiementId")Integer modePaiementId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds HistoriqueSouscriptionEnseigne by using etatSouscriptionId as a search criteria.
	 *
	 * @param etatSouscriptionId
	 * @return A list of Object HistoriqueSouscriptionEnseigne whose etatSouscriptionId is equals to the given etatSouscriptionId. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.etatSouscription.id = :etatSouscriptionId and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByEtatSouscriptionId(@Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one HistoriqueSouscriptionEnseigne by using etatSouscriptionId as a search criteria.
   *
   * @param etatSouscriptionId
   * @return An Object HistoriqueSouscriptionEnseigne whose etatSouscriptionId is equals to the given etatSouscriptionId. If
   *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
   */
  @Query("select e from HistoriqueSouscriptionEnseigne e where e.etatSouscription.id = :etatSouscriptionId and e.isDeleted = :isDeleted")
  HistoriqueSouscriptionEnseigne findHistoriqueSouscriptionEnseigneByEtatSouscriptionId(@Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of HistoriqueSouscriptionEnseigne by using historiqueSouscriptionEnseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of HistoriqueSouscriptionEnseigne
	 * @throws DataAccessException,ParseException
	 */
	public default List<HistoriqueSouscriptionEnseigne> getByCriteria(Request<HistoriqueSouscriptionEnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from HistoriqueSouscriptionEnseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<HistoriqueSouscriptionEnseigne> query = em.createQuery(req, HistoriqueSouscriptionEnseigne.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of HistoriqueSouscriptionEnseigne by using historiqueSouscriptionEnseigneDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of HistoriqueSouscriptionEnseigne
	 *
	 */
	public default Long count(Request<HistoriqueSouscriptionEnseigneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from HistoriqueSouscriptionEnseigne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<HistoriqueSouscriptionEnseigneDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		HistoriqueSouscriptionEnseigneDto dto = request.getData() != null ? request.getData() : new HistoriqueSouscriptionEnseigneDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (HistoriqueSouscriptionEnseigneDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(HistoriqueSouscriptionEnseigneDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getNbreSouscription()!= null && dto.getNbreSouscription() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreSouscription", dto.getNbreSouscription(), "e.nbreSouscription", "Integer", dto.getNbreSouscriptionParam(), param, index, locale));
			}
			if (dto.getNbreUtilisateurMobile()!= null && dto.getNbreUtilisateurMobile() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreUtilisateurMobile", dto.getNbreUtilisateurMobile(), "e.nbreUtilisateurMobile", "Integer", dto.getNbreUtilisateurMobileParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommentaire())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("commentaire", dto.getCommentaire(), "e.commentaire", "String", dto.getCommentaireParam(), param, index, locale));
			}
			if (dto.getPrixSpecifique()!= null && dto.getPrixSpecifique() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prixSpecifique", dto.getPrixSpecifique(), "e.prixSpecifique", "Integer", dto.getPrixSpecifiqueParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsActive()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isActive", dto.getIsActive(), "e.isActive", "Boolean", dto.getIsActiveParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateExpiration())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateExpiration", dto.getDateExpiration(), "e.dateExpiration", "Date", dto.getDateExpirationParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getSouscriptionId()!= null && dto.getSouscriptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscriptionId", dto.getSouscriptionId(), "e.souscription.id", "Integer", dto.getSouscriptionIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getModePaiementId()!= null && dto.getModePaiementId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementId", dto.getModePaiementId(), "e.modePaiement.id", "Integer", dto.getModePaiementIdParam(), param, index, locale));
			}
			if (dto.getEtatSouscriptionId()!= null && dto.getEtatSouscriptionId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionId", dto.getEtatSouscriptionId(), "e.etatSouscription.id", "Integer", dto.getEtatSouscriptionIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getModePaiementLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementLibelle", dto.getModePaiementLibelle(), "e.modePaiement.libelle", "String", dto.getModePaiementLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getModePaiementCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modePaiementCode", dto.getModePaiementCode(), "e.modePaiement.code", "String", dto.getModePaiementCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatSouscriptionLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionLibelle", dto.getEtatSouscriptionLibelle(), "e.etatSouscription.libelle", "String", dto.getEtatSouscriptionLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEtatSouscriptionCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("etatSouscriptionCode", dto.getEtatSouscriptionCode(), "e.etatSouscription.code", "String", dto.getEtatSouscriptionCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
