
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._GpsPointDeVenteRepository;

/**
 * Repository : GpsPointDeVente.
 */
@Repository
public interface GpsPointDeVenteRepository extends JpaRepository<GpsPointDeVente, Integer>, _GpsPointDeVenteRepository {
	/**
	 * Finds GpsPointDeVente by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object GpsPointDeVente whose id is equals to the given id. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.id = :id and e.isDeleted = :isDeleted")
	GpsPointDeVente findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds GpsPointDeVente by using longititude as a search criteria.
	 *
	 * @param longititude
	 * @return An Object GpsPointDeVente whose longititude is equals to the given longititude. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.longititude = :longititude and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByLongititude(@Param("longititude")String longititude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using latitude as a search criteria.
	 *
	 * @param latitude
	 * @return An Object GpsPointDeVente whose latitude is equals to the given latitude. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.latitude = :latitude and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByLatitude(@Param("latitude")String latitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object GpsPointDeVente whose createdBy is equals to the given createdBy. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object GpsPointDeVente whose createdAt is equals to the given createdAt. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object GpsPointDeVente whose updatedBy is equals to the given updatedBy. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object GpsPointDeVente whose updatedAt is equals to the given updatedAt. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object GpsPointDeVente whose deletedBy is equals to the given deletedBy. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object GpsPointDeVente whose deletedAt is equals to the given deletedAt. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds GpsPointDeVente by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object GpsPointDeVente whose isDeleted is equals to the given isDeleted. If
	 *         no GpsPointDeVente is found, this method returns null.
	 */
	@Query("select e from GpsPointDeVente e where e.isDeleted = :isDeleted")
	List<GpsPointDeVente> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of GpsPointDeVente by using gpsPointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of GpsPointDeVente
	 * @throws DataAccessException,ParseException
	 */
	public default List<GpsPointDeVente> getByCriteria(Request<GpsPointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from GpsPointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<GpsPointDeVente> query = em.createQuery(req, GpsPointDeVente.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of GpsPointDeVente by using gpsPointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of GpsPointDeVente
	 *
	 */
	public default Long count(Request<GpsPointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from GpsPointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<GpsPointDeVenteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		GpsPointDeVenteDto dto = request.getData() != null ? request.getData() : new GpsPointDeVenteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (GpsPointDeVenteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(GpsPointDeVenteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLongititude())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("longititude", dto.getLongititude(), "e.longititude", "String", dto.getLongititudeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLatitude())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("latitude", dto.getLatitude(), "e.latitude", "String", dto.getLatitudeParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
