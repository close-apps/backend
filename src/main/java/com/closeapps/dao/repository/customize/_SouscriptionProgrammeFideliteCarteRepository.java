package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : SouscriptionProgrammeFideliteCarte.
 */
@Repository
public interface _SouscriptionProgrammeFideliteCarteRepository {
	default List<String> _generateCriteria(SouscriptionProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (dto.getUserId()!= null && dto.getUserId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.carte.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
		}

		if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.programmeFideliteCarte.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
		}
		
		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}
		return listOfQuery;
	}

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId, isLocked as a search criteria.
	 *
	 * @param programmeFideliteCarteId, isLocked
	 * @return An Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId, isLocked is equals to the given programmeFideliteCarteId, isLocked. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByProgrammeFideliteCarteIdAndIsLocked(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using carteId, libelle as a search criteria.
	 *
	 * @param carteId, libelle
	 * @return An Object SouscriptionProgrammeFideliteCarte whose carteId, libelle is equals to the given carteId, libelle. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.libelle = :libelle and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findByCarteIdAndLibelle(@Param("carteId")Integer carteId, @Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using carteId, libelle as a search criteria.
	 *
	 * @param carteId, libelle
	 * @return An Object SouscriptionProgrammeFideliteCarte whose carteId, libelle is equals to the given carteId, libelle. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.libelle = :libelle and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByUserTelephone(@Param("carteId")Integer carteId, @Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using userEmail as a search criteria.
	 *
	 * @param userEmail
	 * @return An Object SouscriptionProgrammeFideliteCarte whose userEmail is equals to the given userEmail. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.user.email = :userEmail and e.programmeFideliteCarte.id = :programmeFideliteCarteId  and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByUserEmailAndByProgrammeFideliteCarteId(@Param("userEmail")String userEmail , @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using userTelephone as a search criteria.
	 *
	 * @param userTelephone
	 * @return An Object SouscriptionProgrammeFideliteCarte whose userTelephone is equals to the given userTelephone. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.user.telephone = :userTelephone and e.programmeFideliteCarte.id = :programmeFideliteCarteId  and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByUserTelephoneAndByProgrammeFideliteCarteId(@Param("userTelephone")String userTelephone , @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds one SouscriptionProgrammeFideliteCarte by using carteId and programmeFideliteCarteIdas a search criteria.
	 *
	 * @param carteId, programmeFideliteCarteId
	 * @return An Object SouscriptionProgrammeFideliteCarte whose carteId and programmeFideliteCarteId is equals to the given carteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByCarteIdAndProgrammeFideliteCarteId(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds one SouscriptionProgrammeFideliteCarte by using carteId and programmeFideliteCarteIdas a search criteria.
	 *
	 * @param carteId, programmeFideliteCarteId
	 * @return An Object SouscriptionProgrammeFideliteCarte whose carteId and programmeFideliteCarteId is equals to the given carteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCarteIdAndProgrammeFideliteCarteId(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByProgrammeFideliteCarteIdAndCreatedAtBetween(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.createdAt <= :dateCreation and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByProgrammeFideliteCarteIdAndCreatedAtBefore(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("dateCreation")Date dateCreation, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.createdAt <= :dateCreation and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCreatedAtBefore(@Param("dateCreation")Date dateCreation, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCreatedAtBetween(@Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

}
