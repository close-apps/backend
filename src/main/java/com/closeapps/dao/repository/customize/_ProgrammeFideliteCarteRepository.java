package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : ProgrammeFideliteCarte.
 */
@Repository
public interface _ProgrammeFideliteCarteRepository {
	default List<String> _generateCriteria(ProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}
		
		return listOfQuery;
	}
	
	/**
	 * Finds ProgrammeFideliteCarte by using libelle, enseigneId as a search criteria.
	 *
	 * @param libelle, enseigneId
	 * @return An Object ProgrammeFideliteCarte whose libelle, enseigneId is equals to the given libelle, enseigneId. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.libelle = :libelle and e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	ProgrammeFideliteCarte findByLibelleAndEnseigneId(@Param("libelle")String libelle, @Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds ProgrammeFideliteCarte by using enseigneId, isDispoInAllPdv as a search criteria.
	 *
	 * @param enseigneId, isDispoInAllPdv
	 * @return A list of Object ProgrammeFideliteCarte whose enseigneId, isDispoInAllPdv is equals to the given enseigneId, isDispoInAllPdv. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from ProgrammeFideliteCarte e where e.enseigne.id = :enseigneId and e.isDispoInAllPdv = :isDispoInAllPdv and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findByEnseigneIdAndIsDispoInAllPdv(@Param("enseigneId")Integer enseigneId, @Param("isDispoInAllPdv")Boolean isDispoInAllPdv, @Param("isDeleted")Boolean isDeleted);

}
