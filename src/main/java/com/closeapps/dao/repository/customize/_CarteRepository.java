package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : Carte.
 */
@Repository
public interface _CarteRepository {
	default List<String> _generateCriteria(CarteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Carte by using etatId as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose etatId is equals to the given etatId. If
	 *         no Carte is found, this method returns null.
	 */
	@Query("select e from Carte e where e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<Carte> findAllCarteByYear(@Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	/**
	   * Finds one Carte by using telephone as a search criteria.
	   *
	   * @param telephone
	   * @return An Object Carte whose telephone is equals to the given telephone. If
	   *         no Carte is found, this method returns null.
	   */
	  @Query("select e from Carte e where e.user.telephone = :telephone and e.isDeleted = :isDeleted")
	  Carte findCarteByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);

	
	  /**
	   * Finds one Carte by using email as a search criteria.
	   *
	   * @param email
	   * @return An Object Carte whose email is equals to the given email. If
	   *         no Carte is found, this method returns null.
	   */
	  @Query("select e from Carte e where e.user.email = :email and e.isDeleted = :isDeleted")
	  Carte findCarteByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);

	
	  

		/**
		 * Finds Carte by using userLogin as a search criteria.
		 *
		 * @param userLogin
		 * @return A list of Object Carte whose userLogin is equals to the given userLogin. If
		 *         no Carte is found, this method returns null.
		 */
		@Query("select e from Carte e where e.user.login = :userLogin and e.isDeleted = :isDeleted")
		List<Carte> findByUserLogin(@Param("userLogin")String userLogin, @Param("isDeleted")Boolean isDeleted);

	
		/**
		 * Finds Carte by using userEmail as a search criteria.
		 *
		 * @param userEmail
		 * @return A list of Object Carte whose userEmail is equals to the given userEmail. If
		 *         no Carte is found, this method returns null.
		 */
		@Query("select e from Carte e where e.user.email = :userEmail and e.isDeleted = :isDeleted")
		List<Carte> findByUserEmail(@Param("userEmail")String userEmail, @Param("isDeleted")Boolean isDeleted);

	
}
