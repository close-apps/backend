package com.closeapps.dao.repository.customize;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.dao.entity.User;
import com.closeapps.helper.dto.UserDto;


/**
 * Repository customize : User.
 */
@Repository
public interface _UserRepository {
	default List<String> _generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param login, password
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.password = :password and e.isDeleted = :isDeleted")
	User findByLoginAndPassword(@Param("login")String login, @Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds User by using etatId as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose etatId is equals to the given etatId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<User> findAllUserByYearx(@Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	

	
	/**
	 * Finds User by using roleId, firstDate, lastDate as a search criteria.
	 *
	 * @param roleId, firstDate, lastDate
	 * @return A list of Object User whose roleId, firstDate, lastDate is equals to the given roleId, firstDate, lastDate. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.id = :roleId and e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<User> findAllUserByYearAndRoleIdx(@Param("roleId")Integer roleId, @Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	/**
	 * Finds User by using roleCode, firstDate, lastDate as a search criteria.
	 *
	 * @param roleCode, firstDate, lastDate
	 * @return A list of Object User whose roleCode, firstDate, lastDate is equals to the given roleCode, firstDate, lastDate. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.code = :roleCode and e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<User> findAllUserByYearAndRoleCode(@Param("roleCode")String roleCode, @Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	
//	/**
//	 * Finds User by using roleCode, firstDate, lastDate as a search criteria.
//	 *
//	 * @param roleCode, firstDate, lastDate
//	 * @return A list of Object User whose roleCode, firstDate, lastDate is equals to the given roleCode, firstDate, lastDate. If
//	 *         no User is found, this method returns null.
//	 */
//	@Query("select e from User e where e.role.code <> :roleCode and e.createdAt >= :firstDate and e.createdAt <= :lastDate")
//	List<User> findAllUserByYearAndRoleCodeDifferent(@Param("roleCode")String roleCode, @Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	
	/**
	 * Finds User by using roleCode, firstDate, lastDate as a search criteria.
	 *
	 * @param roleCode, firstDate, lastDate
	 * @return A list of Object User whose roleCode, firstDate, lastDate is equals to the given roleCode, firstDate, lastDate. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.enseigne IS NOT NULL and e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<User> findAllUserByYearAndEnseigneNotNull(@Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param token, email
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.email = :email and e.isDeleted = :isDeleted")
	User findUserByTokenAndEmail(@Param("token")String token, @Param("email")String email, @Param("isDeleted")Boolean isDeleted);
		
		
	/**
	 * Finds User by using email and enseigneId as a search criteria.
	 *
	 * @param email, enseigneId
	 * @return An Object User whose email and enseigneId ares equals to the given email and enseigneId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.enseigne.id <> :enseigneId and e.isDeleted = :isDeleted")
	List<User> findByEmailAndEnseigneIdDifferentx(@Param("email")String email, @Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds User by using telephone and enseigneId as a search criteria.
	 *
	 * @param telephone, enseigneId
	 * @return An Object User whose telephone and enseigneId ares equals to the given telephone and enseigneId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.enseigne.id <> :enseigneId and e.isDeleted = :isDeleted")
	List<User> findByTelephoneAndEnseigneIdDifferentx(@Param("telephone")String telephone, @Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);
		
	
	
	/**
	 * Finds User by using emailGoogle as a search criteria.
	 *
	 * @param emailGoogle
	 * @return An Object User whose emailGoogle is equals to the given emailGoogle. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.emailGoogle = :emailGoogle and e.isDeleted = :isDeleted")
	User findUserByEmailGoogle(@Param("emailGoogle")String emailGoogle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephoneGoogle as a search criteria.
	 *
	 * @param telephoneGoogle
	 * @return An Object User whose telephoneGoogle is equals to the given telephoneGoogle. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephoneGoogle = :telephoneGoogle and e.isDeleted = :isDeleted")
	User findUserByTelephoneGoogle(@Param("telephoneGoogle")String telephoneGoogle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using emailFacebook as a search criteria.
	 *
	 * @param emailFacebook
	 * @return An Object User whose emailFacebook is equals to the given emailFacebook. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.emailFacebook = :emailFacebook and e.isDeleted = :isDeleted")
	User findUserByEmailFacebook(@Param("emailFacebook")String emailFacebook, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephoneFacebook as a search criteria.
	 *
	 * @param telephoneFacebook
	 * @return An Object User whose telephoneFacebook is equals to the given telephoneFacebook. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephoneFacebook = :telephoneFacebook and e.isDeleted = :isDeleted")
	User findUserByTelephoneFacebook(@Param("telephoneFacebook")String telephoneFacebook, @Param("isDeleted")Boolean isDeleted);
	
	
	
	/**
	 * Finds User by using email, roleId as a search criteria.
	 *
	 * @param email, roleId
	 * @return An Object User whose email, roleId is equals to the given email, roleId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.role.id = :roleId and e.isDeleted = :isDeleted")
	User findByEmailAndRoleId(@Param("email")String email, @Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephone, roleId as a search criteria.
	 *
	 * @param telephone, roleId
	 * @return An Object User whose telephone, roleId is equals to the given telephone, roleId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.role.id = :roleId and e.isDeleted = :isDeleted")
	User findByTelephoneAndRoleId(@Param("telephone")String telephone, @Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using login, roleId as a search criteria.
	 *
	 * @param login, roleId
	 * @return An Object User whose login, roleId is equals to the given login, roleId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.role.id = :roleId and e.isDeleted = :isDeleted")
	User findByLoginAndRoleId(@Param("login")String login, @Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds User by using roleCode as a search criteria.
	 *
	 * @param roleCode
	 * @return A list of Object User whose roleCode is equals to the given roleCode. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.code = :roleCode and e.isDeleted = :isDeleted")
	List<User> findByRoleCode(@Param("roleCode")String roleCode, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object User whose email is equals to the given email. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.enseigne IS NOT NULL and e.email = :email and e.isDeleted = :isDeleted")
	User findByEmailAndEnseigneIsNotNull(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object User whose telephone is equals to the given telephone. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.enseigne IS NOT NULL and e.telephone = :telephone and e.isDeleted = :isDeleted")
	User findByTelephoneAndEnseigneIsNotNull(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
}
