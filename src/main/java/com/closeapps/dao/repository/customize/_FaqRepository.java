package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : Faq.
 */
@Repository
public interface _FaqRepository {
	default List<String> _generateCriteria(FaqDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Faq by using titre as a search criteria.
	 *
	 * @param titre
	 * @return An Object Faq whose titre is equals to the given titre. If
	 *         no Faq is found, this method returns null.
	 */
	@Query("select e from Faq e where e.titre = :titre and e.isDeleted = :isDeleted")
	Faq findFaqByTitre(@Param("titre")String titre, @Param("isDeleted")Boolean isDeleted);
	
}
