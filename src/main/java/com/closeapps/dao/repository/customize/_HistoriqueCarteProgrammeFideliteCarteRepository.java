package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.dto.customize._RetourGroupByDto;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : HistoriqueCarteProgrammeFideliteCarte.
 */
@Repository
public interface _HistoriqueCarteProgrammeFideliteCarteRepository {
	default List<String> _generateCriteria(HistoriqueCarteProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (Utilities.notBlank(dto.getCurrentDateAction())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("currentDateAction", dto.getCurrentDateAction(), "e.dateAction", "OtherDate", dto.getCurrentDateActionParam(), param, index, locale));
		}
		
		if (dto.getUserId() != null && dto.getUserId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.carte.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
		}
		

		return listOfQuery;
	}


	/**
	 * Finds one HistoriqueCarteProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param pdfCarteId, carteId, typeActionCode, createdBy
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :pdfCarteId and e.carte.id = :carteId and e.typeAction.code = :typeActionCode and e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	HistoriqueCarteProgrammeFideliteCarte uniciteParrainage(@Param("pdfCarteId")Integer pdfCarteId,@Param("carteId")Integer carteId, @Param("typeActionCode")String typeActionCode, @Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using carteId, programmeFideliteCarteId, heureAction as a search criteria.
	 *
	 * @param carteId, programmeFideliteTamponId, heureAction
	 * @return An Object HistoriqueCarteProgrammeFideliteCarte whose carteId, programmeFideliteCarteId, heureAction is equals to the given carteId, programmeFideliteCarteId, heureAction. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.heureAction >= :heureAction and e.carte.id = :carteId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCarteIdAndPdfCarteIdAndUpperHeureAction(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("heureAction")Date heureAction, @Param("isDeleted")Boolean isDeleted);

	
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.id = :typeActionId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCarteIdAndPdfCarteIdAndPdvIdAndTypeActionIdAndDateActionBetween(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.typeAction.id = :typeActionId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteCarte> findByCarteIdAndPdfCarteIdAndTypeActionIdAndDateActionBetween(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select count(e.carte.id) as nbreLigne, sum(e.nbrePointActuel) as totalPoint, sum(e.nbrePointOperation) as totalPoint2 from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.id = :typeActionId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.carte.id")
	List<Object> findByCarteIdAndPdfCarteIdAndPdvIdAndTypeActionIdAndDateActionBetween2(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	
	@Query("select count(e.carte.id) as nbreLigne, sum(e.nbrePointActuel) as totalPoint, sum(e.nbrePointOperation) as totalPoint2 from HistoriqueCarteProgrammeFideliteCarte e where e.carte.id = :carteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.id = :typeActionId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.carte.id")
	List<Object> findByCarteIdAndPdfCarteIdAndPdvIdAndTypeActionIdAndDateActionBetween3(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteCarte> findByTypeActionCodeAndCreatedAtBetween(@Param("typeActionCode1")String typeActionCode1,  @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteCarte> findByPdfCarteIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteCarte> findByPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByDateActionBetweenAndGroupByPdfCarte(@Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.pointDeVente.id = :pointDeVenteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdvIdAndDateActionBetweenAndGroupByPdfCarte(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.pointDeVente.id = :pointDeVenteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndPdvIdAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.typeAction.code = :typeActionCode and e.chiffreDaffaire IS NOT NULL and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(@Param("typeActionCode")String typeActionCode, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.typeAction.code = :typeActionCode and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeActionCode")String typeActionCode, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.pointDeVente.id = :pointDeVenteId and e.typeAction.code = :typeActionCode and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdvIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode")String typeActionCode, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.code = :typeActionCode and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndPdvIdAndTypeActionCodeAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode")String typeActionCode, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code != :typeActionCode1 or e.typeAction.code != :typeActionCode2 or e.typeAction.code != :typeActionCode3) and (e.chiffreDaffaire IS NOT NULL and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted) GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(@Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("typeActionCode3")String typeActionCode3,  @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code != :typeActionCode1 or e.typeAction.code != :typeActionCode2 or e.typeAction.code != :typeActionCode3) and (e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted) GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("typeActionCode3")String typeActionCode3,  @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code != :typeActionCode1 or e.typeAction.code != :typeActionCode2 or e.typeAction.code != :typeActionCode3) and ( e.chiffreDaffaire IS NOT NULL and e.pointDeVente.id = :pointDeVenteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted) GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdvIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("typeActionCode3")String typeActionCode3,  @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	@Query("select new com.closeapps.helper.dto.customize._RetourGroupByDto(e.programmeFideliteCarte,  sum(e.chiffreDaffaire)) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code != :typeActionCode1 or e.typeAction.code != :typeActionCode2 or e.typeAction.code != :typeActionCode3) and (e.chiffreDaffaire IS NOT NULL and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.pointDeVente.id = :pointDeVenteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted) GROUP BY e.programmeFideliteCarte")
	List<_RetourGroupByDto> findByPdfCarteIdAndPdvIdAndTypeActionCodeDifferentAndDateActionBetweenAndGroupByPdfCarte(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("typeActionCode3")String typeActionCode3, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdfCarteIdAndPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdfCarteIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteCarte by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteCarte whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteCarte e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByTypeActionCodeAndCreatedAtBetween(@Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);


}
