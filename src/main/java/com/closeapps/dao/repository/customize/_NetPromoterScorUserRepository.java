package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : NetPromoterScorUser.
 */
@Repository
public interface _NetPromoterScorUserRepository {
	default List<String> _generateCriteria(NetPromoterScorUserDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	/**
	 * Finds NetPromoterScorUser by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object NetPromoterScorUser whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If 
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.typeNoteNps.code = :typeNoteNpsCode and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByPdfCarteIdAndTypeNoteNpsCodeAndCreatedAtBetween(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("typeNoteNpsCode")String typeNoteNpsCode, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NetPromoterScorUser by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object NetPromoterScorUser whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If 
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.typeNoteNps.code = :typeNoteNpsCode and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByPdfTamponIdAndTypeNoteNpsCodeAndCreatedAtBetween(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("typeNoteNpsCode")String typeNoteNpsCode, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

//	/**
//	 * Finds NetPromoterScorUser by using programmeFideliteCarteId as a search criteria.
//	 *
//	 * @param programmeFideliteCarteId
//	 * @return A list of Object NetPromoterScorUser whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If 
//	 *         no NetPromoterScorUser is found, this method returns null.
//	 */
//	@Query("select e from NetPromoterScorUser e where e.typeNoteNps.code = :typeNoteNpsCode and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
//	List<NetPromoterScorUser> findByTypeNoteNpsCodeAndCreatedAtBetween(@Param("typeNoteNpsCode")String typeNoteNpsCode, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NetPromoterScorUser by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object NetPromoterScorUser whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If 
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteCarte IS NOT NULL and e.typeNoteNps.code = :typeNoteNpsCode and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfCarteNotNull(@Param("typeNoteNpsCode")String typeNoteNpsCode, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds NetPromoterScorUser by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object NetPromoterScorUser whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If 
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteTampon IS NOT NULL and e.typeNoteNps.code = :typeNoteNpsCode and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByTypeNoteNpsCodeAndCreatedAtBetweenWithPdfTamponNotNull(@Param("typeNoteNpsCode")String typeNoteNpsCode, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NetPromoterScorUser by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object NetPromoterScorUser whose isDeleted is equals to the given isDeleted. If
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteTampon IS NOT NULL and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByIsDeletedWithPdfTamponNotNull(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NetPromoterScorUser by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object NetPromoterScorUser whose isDeleted is equals to the given isDeleted. If
	 *         no NetPromoterScorUser is found, this method returns null.
	 */
	@Query("select e from NetPromoterScorUser e where e.programmeFideliteCarte IS NOT NULL and e.isDeleted = :isDeleted")
	List<NetPromoterScorUser> findByIsDeletedWithPdfCarteNotNull(@Param("isDeleted")Boolean isDeleted);

}
