package com.closeapps.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.dao.entity.PointDeVente;
import com.closeapps.dao.entity.User;
import com.closeapps.dao.entity.UserEnseignePointDeVente;
import com.closeapps.helper.dto.UserEnseignePointDeVenteDto;

/**
 * Repository customize : UserEnseignePointDeVente.
 */
@Repository
public interface _UserEnseignePointDeVenteRepository {
	default List<String> _generateCriteria(UserEnseignePointDeVenteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	/**
	 * Finds User by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object User whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e.user from UserEnseignePointDeVente e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<User> findAllUserEnseigneByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds PointDeVente by using userEnseigneId as a search criteria.
	 *
	 * @param userEnseigneId
	 * @return A list of Object PointDeVente whose userEnseigneId is equals to the given userEnseigneId. If
	 *         no UserEnseigne is found, this method returns null.
	 */
	@Query("select e.pointDeVente from UserEnseignePointDeVente e where e.user.id = :userEnseigneId and e.isDeleted = :isDeleted")
	List<PointDeVente> findAllPointDeVenteByUserEnseigneId(@Param("userEnseigneId")Integer userEnseigneId, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds UserEnseignePointDeVente by using userEnseigneId, pointDeVenteId as a search criteria.
	 *
	 * @param userEnseigneId, pointDeVenteId
	 * @return A list of Object UserEnseignePointDeVente whose userEnseigneId, pointDeVenteId is equals to the given userEnseigneId, pointDeVenteId. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.user.id = :userEnseigneId and e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByUserEnseigneIdAndPointDeVenteId(@Param("userEnseigneId")Integer userEnseigneId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds pointDeVente by using userEnseigneId, pointDeVenteId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object pointDeVente whose userEnseigneId, pointDeVenteId is equals to the given userEnseigneId, pointDeVenteId. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e.pointDeVente from UserEnseignePointDeVente e where e.user.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<PointDeVente> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

}
