package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : EtatInscripitionEnseigne.
 */
@Repository
public interface _EtatInscripitionEnseigneRepository {
	default List<String> _generateCriteria(EtatInscripitionEnseigneDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds EtatInscripitionEnseigne by using isValid as a search criteria.
	 *
	 * @param isValid
	 * @return An Object EtatInscripitionEnseigne whose isValid is equals to the given isValid. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.isValid = :isValid and e.isDeleted = :isDeleted")
	EtatInscripitionEnseigne findEtatInscripitionEnseigneByIsValidx(@Param("isValid")Boolean isValid, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds EtatInscripitionEnseigne by using isValid as a search criteria.
	 *
	 * @param isValid
	 * @return An Object EtatInscripitionEnseigne whose isValid is equals to the given isValid. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.enseigne.id = :enseigneId and e.isValid = :isValid and e.isDeleted = :isDeleted")
	EtatInscripitionEnseigne findEtatInscripitionEnseigneByEnseigneIdAndIsValid(@Param("enseigneId")Integer enseigneId, @Param("isValid")Boolean isValid, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds EtatInscripitionEnseigne by using isValid as a search criteria.
	 *
	 * @param isValid
	 * @return An Object EtatInscripitionEnseigne whose isValid is equals to the given isValid. If
	 *         no EtatInscripitionEnseigne is found, this method returns null.
	 */
	@Query("select e from EtatInscripitionEnseigne e where e.enseigne.id = :enseigneId and e.isValid = :isValid and e.isDeleted = :isDeleted")
	List<EtatInscripitionEnseigne> findAllByEtatInscripitionEnseigneByEnseigneIdAndIsValid(@Param("enseigneId")Integer enseigneId, @Param("isValid")Boolean isValid, @Param("isDeleted")Boolean isDeleted);
	
	
}
