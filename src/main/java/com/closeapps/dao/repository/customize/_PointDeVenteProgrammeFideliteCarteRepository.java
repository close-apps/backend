package com.closeapps.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.dao.entity.PointDeVente;
import com.closeapps.dao.entity.PointDeVenteProgrammeFideliteCarte;
import com.closeapps.dao.entity.PointDeVenteProgrammeFideliteTampon;
import com.closeapps.dao.entity.ProgrammeFideliteCarte;
import com.closeapps.helper.CriteriaUtils;
import com.closeapps.helper.Utilities;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.dto.PointDeVenteProgrammeFideliteCarteDto;

/**
 * Repository customize : PointDeVenteProgrammeFideliteCarte.
 */
@Repository
public interface _PointDeVenteProgrammeFideliteCarteRepository {
	default List<String> _generateCriteria(PointDeVenteProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (dto.getUserPDVId() != null && dto.getUserPDVId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userPDVId", dto.getUserPDVId(), "e.pointDeVente.user.id", "Integer", dto.getUserPDVIdParam(), param, index, locale));
		}

		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.programmeFideliteCarte.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}

		return listOfQuery;
	}


	/**
	 * Finds ProgrammeFideliteCarte by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object ProgrammeFideliteCarte whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no ProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e.programmeFideliteCarte from PointDeVenteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteCarte> findAllProgrammeFideliteCarteByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PointDeVente by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object PointDeVente whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select DISTINCT e.pointDeVente from PointDeVenteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<PointDeVente> findAllPointDeVenteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds one PointDeVenteProgrammeFideliteCarte by using pointDeVenteId and programmeFideliteCarteId as a search criteria.
	 *
	 * @param pointDeVenteId, programmeFideliteCarteId
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose pointDeVenteId and programmeFideliteCarteId is equals to the given pointDeVenteId and programmeFideliteCarteId. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null. 
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and  e.isDeleted = :isDeleted")
	PointDeVenteProgrammeFideliteCarte findPdvPdfCarteByPdvIdAndPdfCarteIdx(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using pointDeVenteId and programmeFideliteCarteId as a search criteria.
	 *
	 * @param pointDeVenteId, programmeFideliteCarteId
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose pointDeVenteId and programmeFideliteCarteId is equals to the given pointDeVenteId and programmeFideliteCarteId. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null. 
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and  e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByPdvIdAndPdfCarteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

	
	
	/**
	 * Finds List of PointDeVenteProgrammeFideliteCarte by using pointDeVenteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PointDeVenteProgrammeFideliteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<ProgrammeFideliteCarte> getByCriteriaCustom(Request<PointDeVenteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select DISTINCT e.programmeFideliteCarte from PointDeVenteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by e.programmeFideliteCarte desc";
		//req += " order by e.id desc";
		TypedQuery<ProgrammeFideliteCarte> query = em.createQuery(req, ProgrammeFideliteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PointDeVenteProgrammeFideliteCarte by using pointDeVenteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PointDeVenteProgrammeFideliteCarte
	 *
	 */
	public default Long countCustom(Request<PointDeVenteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(DISTINCT e.programmeFideliteCarte.id) from PointDeVenteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}
	
	
	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PointDeVenteProgrammeFideliteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PointDeVenteProgrammeFideliteCarteDto dto = request.getData() != null ? request.getData() : new PointDeVenteProgrammeFideliteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PointDeVenteProgrammeFideliteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}
	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PointDeVenteProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}



}
