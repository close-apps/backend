package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : PointDeVenteProgrammeFideliteTampon.
 */
@Repository
public interface _PointDeVenteProgrammeFideliteTamponRepository {
	default List<String> _generateCriteria(PointDeVenteProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (dto.getUserPDVId() != null && dto.getUserPDVId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userPDVId", dto.getUserPDVId(), "e.pointDeVente.user.id", "Integer", dto.getUserPDVIdParam(), param, index, locale));
		}
		
		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.programmeFideliteTampon.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}


		return listOfQuery;
	}

	/**
	 * Finds ProgrammeFideliteTampon by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object ProgrammeFideliteTampon whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no ProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e.programmeFideliteTampon from PointDeVenteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<ProgrammeFideliteTampon> findAllProgrammeFideliteTamponByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PointDeVente by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object PointDeVente whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e.pointDeVente from PointDeVenteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<PointDeVente> findAllPointDeVenteByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds one PointDeVenteProgrammeFideliteTampon by using pointDeVenteId and programmeFideliteTamponId as a search criteria.
	 *
	 * @param pointDeVenteId, programmeFideliteTamponId
	 * @return An Object PointDeVenteProgrammeFideliteTampon whose pointDeVenteId and programmeFideliteTamponId is equals to the given pointDeVenteId and programmeFideliteTamponId. If
	 *         no PointDeVenteProgrammeFideliteTampon is found, this method returns null. 
	 */
	@Query("select e from PointDeVenteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and  e.isDeleted = :isDeleted")
	PointDeVenteProgrammeFideliteTampon findPdvPdfTamponByPdvIdAndPdfTamponIdx(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds  PointDeVenteProgrammeFideliteTampon by using pointDeVenteId and programmeFideliteTamponId as a search criteria.
	 *
	 * @param pointDeVenteId, programmeFideliteTamponId
	 * @return An Object PointDeVenteProgrammeFideliteTampon whose pointDeVenteId and programmeFideliteTamponId is equals to the given pointDeVenteId and programmeFideliteTamponId. If
	 *         no PointDeVenteProgrammeFideliteTampon is found, this method returns null. 
	 */
	@Query("select e from PointDeVenteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and  e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteTampon> findByPdvIdAndPdfTamponId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	public default List<ProgrammeFideliteTampon> getByCriteriaCustom(Request<PointDeVenteProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select DISTINCT e.programmeFideliteTampon from PointDeVenteProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by e.programmeFideliteTampon desc";
		// req += " order by e.id desc";
		TypedQuery<ProgrammeFideliteTampon> query = em.createQuery(req, ProgrammeFideliteTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}


	public default Long countCustom(Request<PointDeVenteProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(DISTINCT e.programmeFideliteTampon.id) from PointDeVenteProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PointDeVenteProgrammeFideliteTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PointDeVenteProgrammeFideliteTamponDto dto = request.getData() != null ? request.getData() : new PointDeVenteProgrammeFideliteTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PointDeVenteProgrammeFideliteTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PointDeVenteProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
