package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : SouscriptionProgrammeFideliteTampon.
 */
@Repository
public interface _SouscriptionProgrammeFideliteTamponRepository {
	default List<String> _generateCriteria(SouscriptionProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		if (dto.getUserId()!= null && dto.getUserId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.carte.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
		}

		return listOfQuery;
	}
	
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using programmeFideliteTamponId, isLocked as a search criteria.
	 *
	 * @param programmeFideliteTamponId, isLocked
	 * @return An Object SouscriptionProgrammeFideliteCarte whose programmeFideliteTamponId, isLocked is equals to the given programmeFideliteTamponId, isLocked. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByProgrammeFideliteTamponIdAndIsLocked(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using carteId, libelle as a search criteria.
	 *
	 * @param carteId, libelle
	 * @return An Object SouscriptionProgrammeFideliteTampon whose carteId, libelle is equals to the given carteId, libelle. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.carte.id = :carteId and e.libelle = :libelle and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteTampon findByCarteIdAndLibelle(@Param("carteId")Integer carteId, @Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	   * Finds one SouscriptionProgrammeFideliteTampon by using carteId and programmeFideliteTamponId a search criteria.
	   *
	   * @param carteId, programmeFideliteTamponId
	   * @return An Object SouscriptionProgrammeFideliteCarte whose carteId and programmeFideliteTamponId is equals to the given carteId and programmeFideliteTamponId. If
	   *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	   */
	  @Query("select e from SouscriptionProgrammeFideliteTampon e where e.carte.id = :carteId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	  SouscriptionProgrammeFideliteTampon findSouscriptionProgrammeFideliteTamponByCarteIdAndProgrammeFideliteTamponId(@Param("carteId")Integer carteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

	  /**
	   * Finds one SouscriptionProgrammeFideliteTampon by using carteId and programmeFideliteTamponId a search criteria.
	   *
	   * @param carteId, programmeFideliteTamponId
	   * @return An Object SouscriptionProgrammeFideliteCarte whose carteId and programmeFideliteTamponId is equals to the given carteId and programmeFideliteTamponId. If
	   *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	   */
	  @Query("select e from SouscriptionProgrammeFideliteTampon e where e.carte.id = :carteId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	  List<SouscriptionProgrammeFideliteTampon> findByCarteIdAndProgrammeFideliteTamponId(@Param("carteId")Integer carteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	  /**
		 * Finds SouscriptionProgrammeFideliteTampon by using programmeFideliteTamponId as a search criteria.
		 *
		 * @param programmeFideliteCarteId
		 * @return A list of Object SouscriptionProgrammeFideliteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
		 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
		 */
		@Query("select e from SouscriptionProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
		List<SouscriptionProgrammeFideliteTampon> findByProgrammeFideliteTamponIdAndCreatedAtBetween(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

		
		
		
		
		
		
		/**
		 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
		 *
		 * @param programmeFideliteCarteId
		 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
		 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
		 */
		@Query("select e from SouscriptionProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.createdAt <= :dateCreation and e.isDeleted = :isDeleted")
		List<SouscriptionProgrammeFideliteTampon> findByProgrammeFideliteTamponIdAndCreatedAtBefore(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("dateCreation")Date dateCreation, @Param("isDeleted")Boolean isDeleted);

		/**
		 * Finds SouscriptionProgrammeFideliteTampon by using programmeFideliteCarteId as a search criteria.
		 *
		 * @param programmeFideliteCarteId
		 * @return A list of Object SouscriptionProgrammeFideliteTampon whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
		 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
		 */
		@Query("select e from SouscriptionProgrammeFideliteTampon e where e.createdAt <= :dateCreation and e.isDeleted = :isDeleted")
		List<SouscriptionProgrammeFideliteTampon> findByCreatedAtBefore(@Param("dateCreation")Date dateCreation, @Param("isDeleted")Boolean isDeleted);

		
		/**
		 * Finds SouscriptionProgrammeFideliteTampon by using programmeFideliteCarteId as a search criteria.
		 *
		 * @param programmeFideliteCarteId
		 * @return A list of Object SouscriptionProgrammeFideliteTampon whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
		 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
		 */
		@Query("select e from SouscriptionProgrammeFideliteTampon e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
		List<SouscriptionProgrammeFideliteTampon> findByCreatedAtBetween(@Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	  
		
}
