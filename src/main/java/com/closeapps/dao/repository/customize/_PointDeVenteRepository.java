package com.closeapps.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.dao.entity.PointDeVente;
import com.closeapps.dao.entity.ProgrammeFideliteCarte;
import com.closeapps.helper.CriteriaUtils;
import com.closeapps.helper.dto.PointDeVenteDto;

/**
 * Repository customize : PointDeVente.
 */
@Repository
public interface _PointDeVenteRepository {
	default List<String> _generateCriteria(PointDeVenteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}
		return listOfQuery;
		
		
	}
	
	
	/**
	 * Finds PointDeVente by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object PointDeVente whose nom is equals to the given nom. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.nom = :nom and e.isDeleted = :isDeleted")
	PointDeVente findPointDeVenteByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds PointDeVente by using enseigneId, nom as a search criteria.
	 *
	 * @param enseigneId, nom
	 * @return An Object PointDeVente whose enseigneId, nom is equals to the given enseigneId, nom. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.nom = :nom and e.isDeleted = :isDeleted")
	PointDeVente findPointDeVenteByEnseigneIdAndNom(@Param("enseigneId")Integer enseigneId, @Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds PointDeVente by using enseigneId, code as a search criteria.
	 *
	 * @param enseigneId, code
	 * @return An Object PointDeVente whose enseigneId, code is equals to the given enseigneId, code. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.code = :code and e.isDeleted = :isDeleted")
	PointDeVente findPointDeVenteByEnseigneIdAndCode(@Param("enseigneId")Integer enseigneId, @Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	
	
	
	/**
	 * Finds PointDeVente by using enseigneId, isAllCarteDipso as a search criteria.
	 *
	 * @param enseigneId, isAllCarteDipso
	 * @return A list of Object PointDeVente whose enseigneId, isAllCarteDipso is equals to the given enseigneId, isAllCarteDipso. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.isAllCarteDipso = :isAllCarteDipso and e.isDeleted = :isDeleted")
	List<PointDeVente> findByEnseigneIdAndIsAllCarteDipso(@Param("enseigneId")Integer enseigneId, @Param("isAllCarteDipso")Boolean isAllCarteDipso, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds PointDeVente by using enseigneId, isAllTamponDipso as a search criteria.
	 *
	 * @param enseigneId, isAllTamponDipso
	 * @return A list of Object PointDeVente whose enseigneId, isAllTamponDipso is equals to the given enseigneId, isAllTamponDipso. If
	 *         no PointDeVente is found, this method returns null.
	 */
	@Query("select e from PointDeVente e where e.enseigne.id = :enseigneId and e.isAllTamponDipso = :isAllTamponDipso and e.isDeleted = :isDeleted")
	List<PointDeVente> findByEnseigneIdAndIsAllTamponDipso(@Param("enseigneId")Integer enseigneId, @Param("isAllTamponDipso")Boolean isAllTamponDipso, @Param("isDeleted")Boolean isDeleted);

}
