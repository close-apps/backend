package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : HistoriqueSouscriptionEnseigne.
 */
@Repository
public interface _HistoriqueSouscriptionEnseigneRepository {
	default List<String> _generateCriteria(HistoriqueSouscriptionEnseigneDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, dateExpiration, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.enseigne.id = :enseigneId and e.dateExpiration >= :dateExpiration and e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByEnseigneIdAndIsActiveAndDateExpirationBefore(@Param("enseigneId")Integer enseigneId,  @Param("dateExpiration")Date dateExpiration, @Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.enseigne.id = :enseigneId and e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByEnseigneIdAndIsActive(@Param("enseigneId")Integer enseigneId,  @Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.enseigne.id = :enseigneId and e.etatSouscription.id = :etatSouscriptionId and e.isDeleted = :isDeleted")
	HistoriqueSouscriptionEnseigne findByEnseigneIdAndEtatSouscriptionId(@Param("enseigneId")Integer enseigneId,  @Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, dateExpiration, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e.enseigne from HistoriqueSouscriptionEnseigne e where e.souscription.id = :souscriptionId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<Enseigne> findAllEnseigneBySouscriptionIdAndCreatedAtSouscripBeetwen(@Param("souscriptionId")Integer souscriptionId,  @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, dateExpiration, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e.enseigne from HistoriqueSouscriptionEnseigne e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<Enseigne> findAllEnseigneByCreatedAtSouscripBeetwen(@Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using dateExpiration as a search criteria.
	 *
	 * @param enseigneId, dateExpiration, isActive
	 * @return An Object HistoriqueSouscriptionEnseigne whose dateExpiration is equals to the given dateExpiration. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted and (e.etatSouscription.id = :etatSouscriptionId1 or e.etatSouscription.id = :etatSouscriptionId2)")
	List<HistoriqueSouscriptionEnseigne> findByEtatSouscriptionBeetwenAndCreatedAtBeetwen(@Param("etatSouscriptionId1")Integer etatSouscriptionId1, @Param("etatSouscriptionId2")Integer etatSouscriptionId2,  @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);
	
	
	/**
	 * Finds HistoriqueSouscriptionEnseigne by using etatSouscriptionId as a search criteria.
	 *
	 * @param etatSouscriptionId
	 * @return A list of Object HistoriqueSouscriptionEnseigne whose etatSouscriptionId is equals to the given etatSouscriptionId. If
	 *         no HistoriqueSouscriptionEnseigne is found, this method returns null.
	 */
	@Query("select e from HistoriqueSouscriptionEnseigne e where e.etatSouscription.id = :etatSouscriptionId and e.isActive = :isActive and e.dateExpiration < :dateExpiration and e.isDeleted = :isDeleted")
	List<HistoriqueSouscriptionEnseigne> findByEtatSouscriptionIdAndIsActiveAndDateExpirationBefore(@Param("etatSouscriptionId")Integer etatSouscriptionId, @Param("isActive")Boolean isActive, @Param("dateExpiration")Date dateExpiration, @Param("isDeleted")Boolean isDeleted);

}
