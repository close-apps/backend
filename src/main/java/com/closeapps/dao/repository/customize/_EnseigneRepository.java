package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : Enseigne.
 */
@Repository
public interface _EnseigneRepository {
	default List<String> _generateCriteria(EnseigneDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	/**
	 * Finds Enseigne by using firstDate and lastDate as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose firstDate and lastDate are equals to the given firstDate and lastDate. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdAt >= :firstDate and e.createdAt <= :lastDate")
	List<Enseigne> findAllUserEnseigneByYear(@Param("firstDate")Date firstDate, @Param("lastDate")Date lastDate);
	
	
	/**
	 * Finds Enseigne by using firstDate and lastDate as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose firstDate and lastDate are equals to the given firstDate and lastDate. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<Enseigne> findAllEnseigneByCreatedAtBefore(@Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Enseigne by using firstDate and lastDate as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose firstDate and lastDate are equals to the given firstDate and lastDate. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	List<Enseigne> findAllEnseigneByCreatedAtBeetwen(@Param("dateDebut")Date dateDebut, @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Enseigne by using firstDate and lastDate as a search criteria.
	 *
	 * @param firstDate, lastDate
	 * @return A list of Object User whose firstDate and lastDate are equals to the given firstDate and lastDate. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<Enseigne> findByIsActiveAndCreatedAtBeetwen(@Param("dateDebut")Date dateDebut, @Param("dateFin")Date dateFin, @Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	
	/**
	 * Finds Enseigne by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object Enseigne whose nom is equals to the given nom. If
	 *         no Enseigne is found, this method returns null.
	 */
	@Query("select e from Enseigne e where e.nom = :nom and e.isDeleted = :isDeleted")
	Enseigne findEnseigneByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	
}
