package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : DelitProgrammeFidelite.
 */
@Repository
public interface _DelitProgrammeFideliteRepository {
	default List<String> _generateCriteria(DelitProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		if (dto.getUserId()!= null && dto.getUserId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.carte.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
		}
		
		if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.informationRegleSecuriteCarte.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
		}
		if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.informationRegleSecuriteTampon.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
		}

		return listOfQuery;
	}
	
	
	
	/**
	 * Finds DelitProgrammeFidelite by using informationRegleSecuriteTamponId as a search criteria.
	 *
	 * @param informationRegleSecuriteTamponId
	 * @return A list of Object DelitProgrammeFidelite whose informationRegleSecuriteTamponId is equals to the given informationRegleSecuriteTamponId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.carte.id = :caretId and e.pointDeVente.id = :pointDeVenteId and e.informationRegleSecuriteCarte.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByCarteIdAndPdvIdAndPdfCarteId(@Param("caretId")Integer caretId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds DelitProgrammeFidelite by using informationRegleSecuriteTamponId as a search criteria.
	 *
	 * @param informationRegleSecuriteTamponId
	 * @return A list of Object DelitProgrammeFidelite whose informationRegleSecuriteTamponId is equals to the given informationRegleSecuriteTamponId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.carte.id = :caretId and e.pointDeVente.id = :pointDeVenteId and e.informationRegleSecuriteTampon.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByCarteIdAndPdvIdAndPdfTamponId(@Param("caretId")Integer caretId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds List of DelitProgrammeFidelite by using delitProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of DelitProgrammeFidelite
	 * @throws DataAccessException,ParseException
	 */
	public default List<User> getByCriteriaCustom(Request<DelitProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select DISTINCT e.carte.user from DelitProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		// req += " order by e.carte.user desc";
		//req += " order by e.id desc";
		TypedQuery<User> query = em.createQuery(req, User.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
	
	
	/**
	 * Finds count of DelitProgrammeFidelite by using delitProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of DelitProgrammeFidelite
	 *
	 */
	public default Long countCustom(Request<DelitProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(DISTINCT e.carte) from DelitProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}
	
	
	
	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DelitProgrammeFideliteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DelitProgrammeFideliteDto dto = request.getData() != null ? request.getData() : new DelitProgrammeFideliteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DelitProgrammeFideliteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DelitProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateDelit", dto.getDateDelit(), "e.dateDelit", "Date", dto.getDateDelitParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getHeureDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("heureDelit", dto.getHeureDelit(), "e.heureDelit", "Date", dto.getHeureDelitParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroDelit", dto.getNumeroDelit(), "e.numeroDelit", "String", dto.getNumeroDelitParam(), param, index, locale));
			}
			if (dto.getIsNotifed()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isNotifed", dto.getIsNotifed(), "e.isNotifed", "Boolean", dto.getIsNotifedParam(), param, index, locale));
			}
			if (dto.getIsApplicateSanction()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isApplicateSanction", dto.getIsApplicateSanction(), "e.isApplicateSanction", "Boolean", dto.getIsApplicateSanctionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getInformationRegleSecuriteTamponId()!= null && dto.getInformationRegleSecuriteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("informationRegleSecuriteTamponId", dto.getInformationRegleSecuriteTamponId(), "e.informationRegleSecuriteTampon.id", "Integer", dto.getInformationRegleSecuriteTamponIdParam(), param, index, locale));
			}
			if (dto.getInformationRegleSecuriteCarteId()!= null && dto.getInformationRegleSecuriteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("informationRegleSecuriteCarteId", dto.getInformationRegleSecuriteCarteId(), "e.informationRegleSecuriteCarte.id", "Integer", dto.getInformationRegleSecuriteCarteIdParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
