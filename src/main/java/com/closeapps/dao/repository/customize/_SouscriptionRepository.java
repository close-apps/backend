package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : Souscription.
 */
@Repository
public interface _SouscriptionRepository {
	default List<String> _generateCriteria(SouscriptionDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}





	/**
	 * Finds List of Souscription by using souscriptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Souscription
	 * @throws DataAccessException,ParseException
	 */
	public default List<Souscription> getByCriteriaCustom(Request<SouscriptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Souscription e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id asc";
		TypedQuery<Souscription> query = em.createQuery(req, Souscription.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Souscription by using souscriptionDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Souscription
	 *
	 */
	public default Long count(Request<SouscriptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Souscription e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscriptionDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SouscriptionDto dto = request.getData() != null ? request.getData() : new SouscriptionDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscriptionDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscriptionDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlImage())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlImage", dto.getUrlImage(), "e.urlImage", "String", dto.getUrlImageParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrix())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prix", dto.getPrix(), "e.prix", "String", dto.getPrixParam(), param, index, locale));
			}
			if (dto.getPallierForfaitId()!= null && dto.getPallierForfaitId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pallierForfaitId", dto.getPallierForfaitId(), "e.pallierForfait.id", "Integer", dto.getPallierForfaitIdParam(), param, index, locale));
			}
			if (dto.getDureeForfaitId()!= null && dto.getDureeForfaitId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dureeForfaitId", dto.getDureeForfaitId(), "e.dureeForfait.id", "Integer", dto.getDureeForfaitIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPallierForfaitLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pallierForfaitLibelle", dto.getPallierForfaitLibelle(), "e.pallierForfait.libelle", "String", dto.getPallierForfaitLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPallierForfaitCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pallierForfaitCode", dto.getPallierForfaitCode(), "e.pallierForfait.code", "String", dto.getPallierForfaitCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDureeForfaitLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dureeForfaitLibelle", dto.getDureeForfaitLibelle(), "e.dureeForfait.libelle", "String", dto.getDureeForfaitLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDureeForfaitCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dureeForfaitCode", dto.getDureeForfaitCode(), "e.dureeForfait.code", "String", dto.getDureeForfaitCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
