package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : HistoriqueCarteProgrammeFideliteTampon.
 */
@Repository
public interface _HistoriqueCarteProgrammeFideliteTamponRepository {
	default List<String> _generateCriteria(HistoriqueCarteProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		if (Utilities.notBlank(dto.getCurrentDateAction())) {
			listOfQuery.add(CriteriaUtils.generateCriteria("currentDateAction", dto.getCurrentDateAction(), "e.dateAction", "OtherDate", dto.getCurrentDateActionParam(), param, index, locale));
		}
		
		if (dto.getUserId() != null && dto.getUserId() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.carte.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
		}
		
		
		return listOfQuery;
	}
	
	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using carteId, programmeFideliteTamponId, heureAction as a search criteria.
	 *
	 * @param carteId, programmeFideliteTamponId, heureAction
	 * @return An Object HistoriqueCarteProgrammeFideliteTampon whose carteId, programmeFideliteTamponId, heureAction is equals to the given carteId, programmeFideliteTamponId, heureAction. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.heureAction >= :heureAction and e.carte.id = :carteId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCarteIdAndPdfTamponIdAndUpperHeureAction(@Param("carteId")Integer carteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("heureAction")Date heureAction, @Param("isDeleted")Boolean isDeleted);
	
	
	
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.carte.id = :carteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.id = :typeActionId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCarteIdAndPdfTamponIdAndPdvIdAndTypeActionIdAndDateActionBetween(@Param("carteId")Integer carteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where e.carte.id = :carteId and e.typeAction.id = :typeActionId and e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted")
	List<HistoriqueCarteProgrammeFideliteTampon> findByCarteIdAndPdfTamponIdAndTypeActionIdAndDateActionBetween(@Param("carteId")Integer carteId, @Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	
//	@Query("select count(e.carte.id) as nbreLigne, sum(e.nbrePointActuel) as totalPoint, sum(e.nbrePointOperation) as totalPoint2 from HistoriqueCarteProgrammeFideliteTampon e where e.carte.id = :carteId and e.pointDeVente.id = :pointDeVenteId and e.typeAction.id = :typeActionId and e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.dateAction >= :dateActionDebut and e.dateAction <= :dateActionFin and e.isDeleted = :isDeleted GROUP BY e.carte.id")
//	List<Object> findByCarteIdAndPdfCarteIdAndPdvIdAndTypeActionIdAndDateActionBetween2(@Param("carteId")Integer carteId, @Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionId")Integer typeActionId, @Param("dateActionDebut")Date dateActionDebut, @Param("dateActionFin")Date dateActionFin, @Param("isDeleted")Boolean isDeleted);
	

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteTampon> findByTypeActionCodeAndCreatedAtBetween(@Param("typeActionCode1")String typeActionCode1,  @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteTampon> findByPdfTamponIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	List<HistoriqueCarteProgrammeFideliteTampon> findByPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByTypeActionCodeAndCreatedAtBetween(@Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.nbreTamponActuel > :valeurComparer and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("valeurComparer")Integer valeurComparer, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.pointDeVente.id = :pointDeVenteId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdfTamponIdAndPdvIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.pointDeVente.id = :pointDeVenteId and e.nbreTamponActuel = 0 and e.nbreTamponAvantAction > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle = 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponPrecedent > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdfTamponIdAndPdvIdAndCreatedAtBetweenForRembourser(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.nbreTamponActuel = 0 and e.nbreTamponAvantAction > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle = 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponPrecedent > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdvIdAndCreatedAtBetweenForRembourser(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.nbreTamponActuel = 0 and e.nbreTamponAvantAction > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle = 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponPrecedent > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByCreatedAtBetweenForRembourser(@Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.pointDeVente.id = :pointDeVenteId and e.nbreTamponActuel > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdfTamponIdAndPdvIdAndCreatedAtBetweenForEngager(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.pointDeVente.id = :pointDeVenteId and e.nbreTamponActuel > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdvIdAndCreatedAtBetweenForEngager(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where (e.typeAction.code = :typeActionCode1 or e.typeAction.code = :typeActionCode2) and (e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted)")
	Long countUserByPdfTamponIdAndTypeActionCodeAndCreatedAtBetween(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("typeActionCode1")String typeActionCode1, @Param("typeActionCode2")String typeActionCode2, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.nbreTamponActuel = 0 and e.nbreTamponAvantAction > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle = 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponPrecedent > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdfTamponIdAndCreatedAtBetweenForRembourser(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.nbreTamponActuel > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByPdfTamponIdAndCreatedAtBetweenForEngager(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds HistoriqueCarteProgrammeFideliteTampon by using typeActionCode as a search criteria.
	 *
	 * @param typeActionCode
	 * @return A list of Object HistoriqueCarteProgrammeFideliteTampon whose typeActionCode is equals to the given typeActionCode. If
	 *         no HistoriqueCarteProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte) from HistoriqueCarteProgrammeFideliteTampon e where e.nbreTamponActuel > 0 and e.souscriptionProgrammeFideliteTampon.nbreTamponActuelle > 0 and e.createdAt >= :dateDebut and e.createdAt <= :dateFin and e.isDeleted = :isDeleted")
	Long countUserByCreatedAtBetweenForEngager(@Param("dateDebut")Date dateDebut,  @Param("dateFin")Date dateFin, @Param("isDeleted")Boolean isDeleted);

	
}
