package com.closeapps.dao.repository.customize;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;

/**
 * Repository customize : SouscriptionProgrammeFidelite.
 */
@Repository
public interface _SouscriptionProgrammeFideliteRepository {
	default List<String> _generateCriteria(SouscriptionProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE
		
		if (dto.getEnseigneIdForTampon() != null && dto.getEnseigneIdForTampon() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("enseigneIdForTampon", dto.getEnseigneIdForTampon(), "e.programmeFideliteTampon.enseigne.id", "Integer", dto.getEnseigneIdForTamponParam(), param, index, locale));
		}
		
		if (dto.getEnseigneIdForCarte() != null && dto.getEnseigneIdForCarte() > 0) {
			listOfQuery.add(CriteriaUtils.generateCriteria("enseigneIdForCarte", dto.getEnseigneIdForCarte(), "e.programmeFideliteCarte.enseigne.id", "Integer", dto.getEnseigneIdForCarteParam(), param, index, locale));
		}	
		
		if (dto.getIsActiveEnseigne() != null) {
			listOfQuery.add(CriteriaUtils.generateCriteria("isActiveEnseigne", dto.getIsActiveEnseigne(), "e.enseigne.isActive", "Boolean", dto.getIsActiveEnseigneParam(), param, index, locale));
		}

//		if (dto.getPointDeVenteId() != null && dto.getPointDeVenteId() > 0) {
//			listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.programmeFideliteCarte.enseigne.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
//		}


		return listOfQuery;
	}
	

	/**
	 * Finds SouscriptionProgrammeFidelite by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object SouscriptionProgrammeFidelite whose enseigneId is equals to the given enseigneId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.enseigne.id = :enseigneId and e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByEnseigneIdAndCarteId(@Param("enseigneId")Integer enseigneId, @Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

	
	/**
	 * Finds SouscriptionProgrammeFidelite by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object SouscriptionProgrammeFidelite whose enseigneId is equals to the given enseigneId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select count(DISTINCT e.carte.id) from SouscriptionProgrammeFidelite e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	Long findNbreUserMobileEnseigneByEnseigneId(@Param("enseigneId")Integer enseigneId,  @Param("isDeleted")Boolean isDeleted);

	
	
	
	public default List<Carte> getByCriteriaCustom(Request<SouscriptionProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select DISTINCT e.carte from SouscriptionProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		//req += " order by e.id desc";
		//req += " order by e.carte desc";
		TypedQuery<Carte> query = em.createQuery(req, Carte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}
	public default Long countCustom(Request<SouscriptionProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(DISTINCT e.carte.id) from SouscriptionProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}
	
	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscriptionProgrammeFideliteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SouscriptionProgrammeFideliteDto dto = request.getData() != null ? request.getData() : new SouscriptionProgrammeFideliteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscriptionProgrammeFideliteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscriptionProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSouscription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSouscription", dto.getDateSouscription(), "e.dateSouscription", "Date", dto.getDateSouscriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
