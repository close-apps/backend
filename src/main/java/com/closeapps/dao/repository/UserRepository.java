
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._UserRepository;

/**
 * Repository : User.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>, _UserRepository {
	/**
	 * Finds User by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object User whose id is equals to the given id. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.id = :id and e.isDeleted = :isDeleted")
	User findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using nom as a search criteria.
	 *
	 * @param nom
	 * @return An Object User whose nom is equals to the given nom. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<User> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using prenoms as a search criteria.
	 *
	 * @param prenoms
	 * @return An Object User whose prenoms is equals to the given prenoms. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.prenoms = :prenoms and e.isDeleted = :isDeleted")
	List<User> findByPrenoms(@Param("prenoms")String prenoms, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using email as a search criteria.
	 *
	 * @param email
	 * @return An Object User whose email is equals to the given email. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.isDeleted = :isDeleted")
	User findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephone as a search criteria.
	 *
	 * @param telephone
	 * @return An Object User whose telephone is equals to the given telephone. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	User findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using login as a search criteria.
	 *
	 * @param login
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.isDeleted = :isDeleted")
	User findByLogin(@Param("login")String login, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using password as a search criteria.
	 *
	 * @param password
	 * @return An Object User whose password is equals to the given password. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.password = :password and e.isDeleted = :isDeleted")
	List<User> findByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using matricule as a search criteria.
	 *
	 * @param matricule
	 * @return An Object User whose matricule is equals to the given matricule. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	User findByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using token as a search criteria.
	 *
	 * @param token
	 * @return An Object User whose token is equals to the given token. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.token = :token and e.isDeleted = :isDeleted")
	List<User> findByToken(@Param("token")String token, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateExpirationToken as a search criteria.
	 *
	 * @param dateExpirationToken
	 * @return An Object User whose dateExpirationToken is equals to the given dateExpirationToken. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateExpirationToken = :dateExpirationToken and e.isDeleted = :isDeleted")
	List<User> findByDateExpirationToken(@Param("dateExpirationToken")Date dateExpirationToken, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isTokenValide as a search criteria.
	 *
	 * @param isTokenValide
	 * @return An Object User whose isTokenValide is equals to the given isTokenValide. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isTokenValide = :isTokenValide and e.isDeleted = :isDeleted")
	List<User> findByIsTokenValide(@Param("isTokenValide")Boolean isTokenValide, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object User whose isLocked is equals to the given isLocked. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<User> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isFirstlyConnexion as a search criteria.
	 *
	 * @param isFirstlyConnexion
	 * @return An Object User whose isFirstlyConnexion is equals to the given isFirstlyConnexion. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isFirstlyConnexion = :isFirstlyConnexion and e.isDeleted = :isDeleted")
	List<User> findByIsFirstlyConnexion(@Param("isFirstlyConnexion")Boolean isFirstlyConnexion, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isAdminEnseigne as a search criteria.
	 *
	 * @param isAdminEnseigne
	 * @return An Object User whose isAdminEnseigne is equals to the given isAdminEnseigne. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isAdminEnseigne = :isAdminEnseigne and e.isDeleted = :isDeleted")
	List<User> findByIsAdminEnseigne(@Param("isAdminEnseigne")Boolean isAdminEnseigne, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using byFirebaseUser as a search criteria.
	 *
	 * @param byFirebaseUser
	 * @return An Object User whose byFirebaseUser is equals to the given byFirebaseUser. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.byFirebaseUser = :byFirebaseUser and e.isDeleted = :isDeleted")
	List<User> findByByFirebaseUser(@Param("byFirebaseUser")Boolean byFirebaseUser, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using emailGoogle as a search criteria.
	 *
	 * @param emailGoogle
	 * @return An Object User whose emailGoogle is equals to the given emailGoogle. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.emailGoogle = :emailGoogle and e.isDeleted = :isDeleted")
	List<User> findByEmailGoogle(@Param("emailGoogle")String emailGoogle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephoneGoogle as a search criteria.
	 *
	 * @param telephoneGoogle
	 * @return An Object User whose telephoneGoogle is equals to the given telephoneGoogle. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephoneGoogle = :telephoneGoogle and e.isDeleted = :isDeleted")
	List<User> findByTelephoneGoogle(@Param("telephoneGoogle")String telephoneGoogle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using emailFacebook as a search criteria.
	 *
	 * @param emailFacebook
	 * @return An Object User whose emailFacebook is equals to the given emailFacebook. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.emailFacebook = :emailFacebook and e.isDeleted = :isDeleted")
	List<User> findByEmailFacebook(@Param("emailFacebook")String emailFacebook, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephoneFacebook as a search criteria.
	 *
	 * @param telephoneFacebook
	 * @return An Object User whose telephoneFacebook is equals to the given telephoneFacebook. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephoneFacebook = :telephoneFacebook and e.isDeleted = :isDeleted")
	List<User> findByTelephoneFacebook(@Param("telephoneFacebook")String telephoneFacebook, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using passwordGenerate as a search criteria.
	 *
	 * @param passwordGenerate
	 * @return An Object User whose passwordGenerate is equals to the given passwordGenerate. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.passwordGenerate = :passwordGenerate and e.isDeleted = :isDeleted")
	List<User> findByPasswordGenerate(@Param("passwordGenerate")String passwordGenerate, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object User whose createdBy is equals to the given createdBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<User> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object User whose createdAt is equals to the given createdAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<User> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object User whose updatedBy is equals to the given updatedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<User> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object User whose updatedAt is equals to the given updatedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<User> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object User whose deletedBy is equals to the given deletedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<User> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object User whose deletedAt is equals to the given deletedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<User> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object User whose isDeleted is equals to the given isDeleted. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isDeleted = :isDeleted")
	List<User> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object User whose enseigneId is equals to the given enseigneId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<User> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object User whose enseigneId is equals to the given enseigneId. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  User findUserByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using roleId as a search criteria.
	 *
	 * @param roleId
	 * @return A list of Object User whose roleId is equals to the given roleId. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.id = :roleId and e.isDeleted = :isDeleted")
	List<User> findByRoleId(@Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using roleId as a search criteria.
   *
   * @param roleId
   * @return An Object User whose roleId is equals to the given roleId. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.role.id = :roleId and e.isDeleted = :isDeleted")
  User findUserByRoleId(@Param("roleId")Integer roleId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of User by using userDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of User
	 * @throws DataAccessException,ParseException
	 */
	public default List<User> getByCriteria(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<User> query = em.createQuery(req, User.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of User by using userDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of User
	 *
	 */
	public default Long count(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<UserDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		UserDto dto = request.getData() != null ? request.getData() : new UserDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (UserDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenoms", dto.getPrenoms(), "e.prenoms", "String", dto.getPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmail())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("login", dto.getLogin(), "e.login", "String", dto.getLoginParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPassword())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("password", dto.getPassword(), "e.password", "String", dto.getPasswordParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatricule())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matricule", dto.getMatricule(), "e.matricule", "String", dto.getMatriculeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getToken())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("token", dto.getToken(), "e.token", "String", dto.getTokenParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateExpirationToken())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateExpirationToken", dto.getDateExpirationToken(), "e.dateExpirationToken", "Date", dto.getDateExpirationTokenParam(), param, index, locale));
			}
			if (dto.getIsTokenValide()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isTokenValide", dto.getIsTokenValide(), "e.isTokenValide", "Boolean", dto.getIsTokenValideParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (dto.getIsFirstlyConnexion()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isFirstlyConnexion", dto.getIsFirstlyConnexion(), "e.isFirstlyConnexion", "Boolean", dto.getIsFirstlyConnexionParam(), param, index, locale));
			}
			if (dto.getIsAdminEnseigne()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isAdminEnseigne", dto.getIsAdminEnseigne(), "e.isAdminEnseigne", "Boolean", dto.getIsAdminEnseigneParam(), param, index, locale));
			}
			if (dto.getByFirebaseUser()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("byFirebaseUser", dto.getByFirebaseUser(), "e.byFirebaseUser", "Boolean", dto.getByFirebaseUserParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmailGoogle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("emailGoogle", dto.getEmailGoogle(), "e.emailGoogle", "String", dto.getEmailGoogleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephoneGoogle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephoneGoogle", dto.getTelephoneGoogle(), "e.telephoneGoogle", "String", dto.getTelephoneGoogleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmailFacebook())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("emailFacebook", dto.getEmailFacebook(), "e.emailFacebook", "String", dto.getEmailFacebookParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephoneFacebook())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephoneFacebook", dto.getTelephoneFacebook(), "e.telephoneFacebook", "String", dto.getTelephoneFacebookParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPasswordGenerate())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("passwordGenerate", dto.getPasswordGenerate(), "e.passwordGenerate", "String", dto.getPasswordGenerateParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getRoleId()!= null && dto.getRoleId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleId", dto.getRoleId(), "e.role.id", "Integer", dto.getRoleIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleLibelle", dto.getRoleLibelle(), "e.role.libelle", "String", dto.getRoleLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRoleCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleCode", dto.getRoleCode(), "e.role.code", "String", dto.getRoleCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
