
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._PartenairesRepository;

/**
 * Repository : Partenaires.
 */
@Repository
public interface PartenairesRepository extends JpaRepository<Partenaires, Integer>, _PartenairesRepository {
	/**
	 * Finds Partenaires by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Partenaires whose id is equals to the given id. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.id = :id and e.isDeleted = :isDeleted")
	Partenaires findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Partenaires by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object Partenaires whose libelle is equals to the given libelle. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Partenaires findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using urlLogo as a search criteria.
	 *
	 * @param urlLogo
	 * @return An Object Partenaires whose urlLogo is equals to the given urlLogo. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.urlLogo = :urlLogo and e.isDeleted = :isDeleted")
	List<Partenaires> findByUrlLogo(@Param("urlLogo")String urlLogo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object Partenaires whose description is equals to the given description. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.description = :description and e.isDeleted = :isDeleted")
	List<Partenaires> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Partenaires whose createdBy is equals to the given createdBy. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Partenaires> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Partenaires whose createdAt is equals to the given createdAt. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Partenaires> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Partenaires whose updatedBy is equals to the given updatedBy. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Partenaires> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Partenaires whose updatedAt is equals to the given updatedAt. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Partenaires> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Partenaires whose deletedBy is equals to the given deletedBy. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Partenaires> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Partenaires whose deletedAt is equals to the given deletedAt. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Partenaires> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Partenaires by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Partenaires whose isDeleted is equals to the given isDeleted. If
	 *         no Partenaires is found, this method returns null.
	 */
	@Query("select e from Partenaires e where e.isDeleted = :isDeleted")
	List<Partenaires> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Partenaires by using partenairesDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Partenaires
	 * @throws DataAccessException,ParseException
	 */
	public default List<Partenaires> getByCriteria(Request<PartenairesDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Partenaires e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Partenaires> query = em.createQuery(req, Partenaires.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Partenaires by using partenairesDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Partenaires
	 *
	 */
	public default Long count(Request<PartenairesDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Partenaires e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PartenairesDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PartenairesDto dto = request.getData() != null ? request.getData() : new PartenairesDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PartenairesDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PartenairesDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlLogo())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlLogo", dto.getUrlLogo(), "e.urlLogo", "String", dto.getUrlLogoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
