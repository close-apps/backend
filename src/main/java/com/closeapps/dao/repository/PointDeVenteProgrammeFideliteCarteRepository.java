
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._PointDeVenteProgrammeFideliteCarteRepository;

/**
 * Repository : PointDeVenteProgrammeFideliteCarte.
 */
@Repository
public interface PointDeVenteProgrammeFideliteCarteRepository extends JpaRepository<PointDeVenteProgrammeFideliteCarte, Integer>, _PointDeVenteProgrammeFideliteCarteRepository {
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose id is equals to the given id. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.id = :id and e.isDeleted = :isDeleted")
	PointDeVenteProgrammeFideliteCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose createdBy is equals to the given createdBy. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose createdAt is equals to the given createdAt. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose updatedBy is equals to the given updatedBy. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose updatedAt is equals to the given updatedAt. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose deletedBy is equals to the given deletedBy. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose deletedAt is equals to the given deletedAt. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object PointDeVenteProgrammeFideliteCarte whose isDeleted is equals to the given isDeleted. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object PointDeVenteProgrammeFideliteCarte whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVenteProgrammeFideliteCarte by using pointDeVenteId as a search criteria.
   *
   * @param pointDeVenteId
   * @return An Object PointDeVenteProgrammeFideliteCarte whose pointDeVenteId is equals to the given pointDeVenteId. If
   *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from PointDeVenteProgrammeFideliteCarte e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
  PointDeVenteProgrammeFideliteCarte findPointDeVenteProgrammeFideliteCarteByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds PointDeVenteProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object PointDeVenteProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from PointDeVenteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<PointDeVenteProgrammeFideliteCarte> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one PointDeVenteProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object PointDeVenteProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no PointDeVenteProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from PointDeVenteProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  PointDeVenteProgrammeFideliteCarte findPointDeVenteProgrammeFideliteCarteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of PointDeVenteProgrammeFideliteCarte by using pointDeVenteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of PointDeVenteProgrammeFideliteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<PointDeVenteProgrammeFideliteCarte> getByCriteria(Request<PointDeVenteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from PointDeVenteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<PointDeVenteProgrammeFideliteCarte> query = em.createQuery(req, PointDeVenteProgrammeFideliteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of PointDeVenteProgrammeFideliteCarte by using pointDeVenteProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of PointDeVenteProgrammeFideliteCarte
	 *
	 */
	public default Long count(Request<PointDeVenteProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from PointDeVenteProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PointDeVenteProgrammeFideliteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		PointDeVenteProgrammeFideliteCarteDto dto = request.getData() != null ? request.getData() : new PointDeVenteProgrammeFideliteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PointDeVenteProgrammeFideliteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PointDeVenteProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
