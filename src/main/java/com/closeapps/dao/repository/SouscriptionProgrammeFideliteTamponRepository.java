
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._SouscriptionProgrammeFideliteTamponRepository;

/**
 * Repository : SouscriptionProgrammeFideliteTampon.
 */
@Repository
public interface SouscriptionProgrammeFideliteTamponRepository extends JpaRepository<SouscriptionProgrammeFideliteTampon, Integer>, _SouscriptionProgrammeFideliteTamponRepository {
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object SouscriptionProgrammeFideliteTampon whose id is equals to the given id. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.id = :id and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteTampon findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object SouscriptionProgrammeFideliteTampon whose code is equals to the given code. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.code = :code and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteTampon findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object SouscriptionProgrammeFideliteTampon whose libelle is equals to the given libelle. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteTampon findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using isShowLastNotification as a search criteria.
	 *
	 * @param isShowLastNotification
	 * @return An Object SouscriptionProgrammeFideliteTampon whose isShowLastNotification is equals to the given isShowLastNotification. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.isShowLastNotification = :isShowLastNotification and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByIsShowLastNotification(@Param("isShowLastNotification")Boolean isShowLastNotification, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object SouscriptionProgrammeFideliteTampon whose isLocked is equals to the given isLocked. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using raisonLocked as a search criteria.
	 *
	 * @param raisonLocked
	 * @return An Object SouscriptionProgrammeFideliteTampon whose raisonLocked is equals to the given raisonLocked. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.raisonLocked = :raisonLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByRaisonLocked(@Param("raisonLocked")String raisonLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using unsubscribe as a search criteria.
	 *
	 * @param unsubscribe
	 * @return An Object SouscriptionProgrammeFideliteTampon whose unsubscribe is equals to the given unsubscribe. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.unsubscribe = :unsubscribe and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByUnsubscribe(@Param("unsubscribe")Boolean unsubscribe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using raisonUnsubscribe as a search criteria.
	 *
	 * @param raisonUnsubscribe
	 * @return An Object SouscriptionProgrammeFideliteTampon whose raisonUnsubscribe is equals to the given raisonUnsubscribe. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.raisonUnsubscribe = :raisonUnsubscribe and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByRaisonUnsubscribe(@Param("raisonUnsubscribe")String raisonUnsubscribe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using isNotifed as a search criteria.
	 *
	 * @param isNotifed
	 * @return An Object SouscriptionProgrammeFideliteTampon whose isNotifed is equals to the given isNotifed. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.isNotifed = :isNotifed and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByIsNotifed(@Param("isNotifed")Boolean isNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using messageNotifed as a search criteria.
	 *
	 * @param messageNotifed
	 * @return An Object SouscriptionProgrammeFideliteTampon whose messageNotifed is equals to the given messageNotifed. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.messageNotifed = :messageNotifed and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByMessageNotifed(@Param("messageNotifed")String messageNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using nbreTamponActuelle as a search criteria.
	 *
	 * @param nbreTamponActuelle
	 * @return An Object SouscriptionProgrammeFideliteTampon whose nbreTamponActuelle is equals to the given nbreTamponActuelle. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.nbreTamponActuelle = :nbreTamponActuelle and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByNbreTamponActuelle(@Param("nbreTamponActuelle")Integer nbreTamponActuelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using nbreTamponPrecedent as a search criteria.
	 *
	 * @param nbreTamponPrecedent
	 * @return An Object SouscriptionProgrammeFideliteTampon whose nbreTamponPrecedent is equals to the given nbreTamponPrecedent. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.nbreTamponPrecedent = :nbreTamponPrecedent and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByNbreTamponPrecedent(@Param("nbreTamponPrecedent")Integer nbreTamponPrecedent, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using dateSouscription as a search criteria.
	 *
	 * @param dateSouscription
	 * @return An Object SouscriptionProgrammeFideliteTampon whose dateSouscription is equals to the given dateSouscription. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.dateSouscription = :dateSouscription and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByDateSouscription(@Param("dateSouscription")Date dateSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object SouscriptionProgrammeFideliteTampon whose createdBy is equals to the given createdBy. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object SouscriptionProgrammeFideliteTampon whose createdAt is equals to the given createdAt. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object SouscriptionProgrammeFideliteTampon whose updatedBy is equals to the given updatedBy. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object SouscriptionProgrammeFideliteTampon whose updatedAt is equals to the given updatedAt. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object SouscriptionProgrammeFideliteTampon whose deletedBy is equals to the given deletedBy. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object SouscriptionProgrammeFideliteTampon whose deletedAt is equals to the given deletedAt. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object SouscriptionProgrammeFideliteTampon whose isDeleted is equals to the given isDeleted. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object SouscriptionProgrammeFideliteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFideliteTampon by using programmeFideliteTamponId as a search criteria.
   *
   * @param programmeFideliteTamponId
   * @return An Object SouscriptionProgrammeFideliteTampon whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
   *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFideliteTampon e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFideliteTampon findSouscriptionProgrammeFideliteTamponByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFideliteTampon by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object SouscriptionProgrammeFideliteTampon whose carteId is equals to the given carteId. If
	 *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteTampon e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteTampon> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFideliteTampon by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object SouscriptionProgrammeFideliteTampon whose carteId is equals to the given carteId. If
   *         no SouscriptionProgrammeFideliteTampon is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFideliteTampon e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFideliteTampon findSouscriptionProgrammeFideliteTamponByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of SouscriptionProgrammeFideliteTampon by using souscriptionProgrammeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SouscriptionProgrammeFideliteTampon
	 * @throws DataAccessException,ParseException
	 */
	public default List<SouscriptionProgrammeFideliteTampon> getByCriteria(Request<SouscriptionProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SouscriptionProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<SouscriptionProgrammeFideliteTampon> query = em.createQuery(req, SouscriptionProgrammeFideliteTampon.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of SouscriptionProgrammeFideliteTampon by using souscriptionProgrammeFideliteTamponDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SouscriptionProgrammeFideliteTampon
	 *
	 */
	public default Long count(Request<SouscriptionProgrammeFideliteTamponDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SouscriptionProgrammeFideliteTampon e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscriptionProgrammeFideliteTamponDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SouscriptionProgrammeFideliteTamponDto dto = request.getData() != null ? request.getData() : new SouscriptionProgrammeFideliteTamponDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscriptionProgrammeFideliteTamponDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscriptionProgrammeFideliteTamponDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getIsShowLastNotification()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isShowLastNotification", dto.getIsShowLastNotification(), "e.isShowLastNotification", "Boolean", dto.getIsShowLastNotificationParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRaisonLocked())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("raisonLocked", dto.getRaisonLocked(), "e.raisonLocked", "String", dto.getRaisonLockedParam(), param, index, locale));
			}
			if (dto.getUnsubscribe()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("unsubscribe", dto.getUnsubscribe(), "e.unsubscribe", "Boolean", dto.getUnsubscribeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRaisonUnsubscribe())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("raisonUnsubscribe", dto.getRaisonUnsubscribe(), "e.raisonUnsubscribe", "String", dto.getRaisonUnsubscribeParam(), param, index, locale));
			}
			if (dto.getIsNotifed()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isNotifed", dto.getIsNotifed(), "e.isNotifed", "Boolean", dto.getIsNotifedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMessageNotifed())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("messageNotifed", dto.getMessageNotifed(), "e.messageNotifed", "String", dto.getMessageNotifedParam(), param, index, locale));
			}
			if (dto.getNbreTamponActuelle()!= null && dto.getNbreTamponActuelle() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTamponActuelle", dto.getNbreTamponActuelle(), "e.nbreTamponActuelle", "Integer", dto.getNbreTamponActuelleParam(), param, index, locale));
			}
			if (dto.getNbreTamponPrecedent()!= null && dto.getNbreTamponPrecedent() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreTamponPrecedent", dto.getNbreTamponPrecedent(), "e.nbreTamponPrecedent", "Integer", dto.getNbreTamponPrecedentParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSouscription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSouscription", dto.getDateSouscription(), "e.dateSouscription", "Date", dto.getDateSouscriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
