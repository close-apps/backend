
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._SouscriptionProgrammeFideliteCarteRepository;

/**
 * Repository : SouscriptionProgrammeFideliteCarte.
 */
@Repository
public interface SouscriptionProgrammeFideliteCarteRepository extends JpaRepository<SouscriptionProgrammeFideliteCarte, Integer>, _SouscriptionProgrammeFideliteCarteRepository {
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object SouscriptionProgrammeFideliteCarte whose id is equals to the given id. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.id = :id and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object SouscriptionProgrammeFideliteCarte whose code is equals to the given code. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.code = :code and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object SouscriptionProgrammeFideliteCarte whose libelle is equals to the given libelle. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFideliteCarte findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using isShowLastNotification as a search criteria.
	 *
	 * @param isShowLastNotification
	 * @return An Object SouscriptionProgrammeFideliteCarte whose isShowLastNotification is equals to the given isShowLastNotification. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.isShowLastNotification = :isShowLastNotification and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByIsShowLastNotification(@Param("isShowLastNotification")Boolean isShowLastNotification, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using isLocked as a search criteria.
	 *
	 * @param isLocked
	 * @return An Object SouscriptionProgrammeFideliteCarte whose isLocked is equals to the given isLocked. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.isLocked = :isLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByIsLocked(@Param("isLocked")Boolean isLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using raisonLocked as a search criteria.
	 *
	 * @param raisonLocked
	 * @return An Object SouscriptionProgrammeFideliteCarte whose raisonLocked is equals to the given raisonLocked. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.raisonLocked = :raisonLocked and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByRaisonLocked(@Param("raisonLocked")String raisonLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using unsubscribe as a search criteria.
	 *
	 * @param unsubscribe
	 * @return An Object SouscriptionProgrammeFideliteCarte whose unsubscribe is equals to the given unsubscribe. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.unsubscribe = :unsubscribe and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByUnsubscribe(@Param("unsubscribe")Boolean unsubscribe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using emailParrain as a search criteria.
	 *
	 * @param emailParrain
	 * @return An Object SouscriptionProgrammeFideliteCarte whose emailParrain is equals to the given emailParrain. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.emailParrain = :emailParrain and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByEmailParrain(@Param("emailParrain")String emailParrain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using telephoneParrain as a search criteria.
	 *
	 * @param telephoneParrain
	 * @return An Object SouscriptionProgrammeFideliteCarte whose telephoneParrain is equals to the given telephoneParrain. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.telephoneParrain = :telephoneParrain and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByTelephoneParrain(@Param("telephoneParrain")String telephoneParrain, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using raisonUnsubscribe as a search criteria.
	 *
	 * @param raisonUnsubscribe
	 * @return An Object SouscriptionProgrammeFideliteCarte whose raisonUnsubscribe is equals to the given raisonUnsubscribe. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.raisonUnsubscribe = :raisonUnsubscribe and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByRaisonUnsubscribe(@Param("raisonUnsubscribe")String raisonUnsubscribe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using isNotifed as a search criteria.
	 *
	 * @param isNotifed
	 * @return An Object SouscriptionProgrammeFideliteCarte whose isNotifed is equals to the given isNotifed. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.isNotifed = :isNotifed and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByIsNotifed(@Param("isNotifed")Boolean isNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using messageNotifed as a search criteria.
	 *
	 * @param messageNotifed
	 * @return An Object SouscriptionProgrammeFideliteCarte whose messageNotifed is equals to the given messageNotifed. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.messageNotifed = :messageNotifed and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByMessageNotifed(@Param("messageNotifed")String messageNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using nbrePointActuelle as a search criteria.
	 *
	 * @param nbrePointActuelle
	 * @return An Object SouscriptionProgrammeFideliteCarte whose nbrePointActuelle is equals to the given nbrePointActuelle. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.nbrePointActuelle = :nbrePointActuelle and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByNbrePointActuelle(@Param("nbrePointActuelle")Integer nbrePointActuelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using nbrePointPrecedent as a search criteria.
	 *
	 * @param nbrePointPrecedent
	 * @return An Object SouscriptionProgrammeFideliteCarte whose nbrePointPrecedent is equals to the given nbrePointPrecedent. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.nbrePointPrecedent = :nbrePointPrecedent and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByNbrePointPrecedent(@Param("nbrePointPrecedent")Integer nbrePointPrecedent, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using dateSouscription as a search criteria.
	 *
	 * @param dateSouscription
	 * @return An Object SouscriptionProgrammeFideliteCarte whose dateSouscription is equals to the given dateSouscription. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.dateSouscription = :dateSouscription and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByDateSouscription(@Param("dateSouscription")Date dateSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object SouscriptionProgrammeFideliteCarte whose createdBy is equals to the given createdBy. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object SouscriptionProgrammeFideliteCarte whose createdAt is equals to the given createdAt. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object SouscriptionProgrammeFideliteCarte whose updatedBy is equals to the given updatedBy. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object SouscriptionProgrammeFideliteCarte whose updatedAt is equals to the given updatedAt. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object SouscriptionProgrammeFideliteCarte whose deletedBy is equals to the given deletedBy. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object SouscriptionProgrammeFideliteCarte whose deletedAt is equals to the given deletedAt. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object SouscriptionProgrammeFideliteCarte whose isDeleted is equals to the given isDeleted. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose carteId is equals to the given carteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFideliteCarte by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object SouscriptionProgrammeFideliteCarte whose carteId is equals to the given carteId. If
   *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFideliteCarte e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFideliteCarte> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFideliteCarte by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object SouscriptionProgrammeFideliteCarte whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no SouscriptionProgrammeFideliteCarte is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFideliteCarte e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFideliteCarte findSouscriptionProgrammeFideliteCarteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of SouscriptionProgrammeFideliteCarte by using souscriptionProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SouscriptionProgrammeFideliteCarte
	 * @throws DataAccessException,ParseException
	 */
	public default List<SouscriptionProgrammeFideliteCarte> getByCriteria(Request<SouscriptionProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SouscriptionProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<SouscriptionProgrammeFideliteCarte> query = em.createQuery(req, SouscriptionProgrammeFideliteCarte.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of SouscriptionProgrammeFideliteCarte by using souscriptionProgrammeFideliteCarteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SouscriptionProgrammeFideliteCarte
	 *
	 */
	public default Long count(Request<SouscriptionProgrammeFideliteCarteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SouscriptionProgrammeFideliteCarte e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscriptionProgrammeFideliteCarteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SouscriptionProgrammeFideliteCarteDto dto = request.getData() != null ? request.getData() : new SouscriptionProgrammeFideliteCarteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscriptionProgrammeFideliteCarteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscriptionProgrammeFideliteCarteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getIsShowLastNotification()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isShowLastNotification", dto.getIsShowLastNotification(), "e.isShowLastNotification", "Boolean", dto.getIsShowLastNotificationParam(), param, index, locale));
			}
			if (dto.getIsLocked()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocked", dto.getIsLocked(), "e.isLocked", "Boolean", dto.getIsLockedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRaisonLocked())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("raisonLocked", dto.getRaisonLocked(), "e.raisonLocked", "String", dto.getRaisonLockedParam(), param, index, locale));
			}
			if (dto.getUnsubscribe()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("unsubscribe", dto.getUnsubscribe(), "e.unsubscribe", "Boolean", dto.getUnsubscribeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEmailParrain())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("emailParrain", dto.getEmailParrain(), "e.emailParrain", "String", dto.getEmailParrainParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephoneParrain())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephoneParrain", dto.getTelephoneParrain(), "e.telephoneParrain", "String", dto.getTelephoneParrainParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRaisonUnsubscribe())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("raisonUnsubscribe", dto.getRaisonUnsubscribe(), "e.raisonUnsubscribe", "String", dto.getRaisonUnsubscribeParam(), param, index, locale));
			}
			if (dto.getIsNotifed()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isNotifed", dto.getIsNotifed(), "e.isNotifed", "Boolean", dto.getIsNotifedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMessageNotifed())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("messageNotifed", dto.getMessageNotifed(), "e.messageNotifed", "String", dto.getMessageNotifedParam(), param, index, locale));
			}
			if (dto.getNbrePointActuelle()!= null && dto.getNbrePointActuelle() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointActuelle", dto.getNbrePointActuelle(), "e.nbrePointActuelle", "Integer", dto.getNbrePointActuelleParam(), param, index, locale));
			}
			if (dto.getNbrePointPrecedent()!= null && dto.getNbrePointPrecedent() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbrePointPrecedent", dto.getNbrePointPrecedent(), "e.nbrePointPrecedent", "Integer", dto.getNbrePointPrecedentParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSouscription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSouscription", dto.getDateSouscription(), "e.dateSouscription", "Date", dto.getDateSouscriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
