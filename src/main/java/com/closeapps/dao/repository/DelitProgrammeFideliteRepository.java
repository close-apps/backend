
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._DelitProgrammeFideliteRepository;

/**
 * Repository : DelitProgrammeFidelite.
 */
@Repository
public interface DelitProgrammeFideliteRepository extends JpaRepository<DelitProgrammeFidelite, Integer>, _DelitProgrammeFideliteRepository {
	/**
	 * Finds DelitProgrammeFidelite by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object DelitProgrammeFidelite whose id is equals to the given id. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.id = :id and e.isDeleted = :isDeleted")
	DelitProgrammeFidelite findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DelitProgrammeFidelite by using dateDelit as a search criteria.
	 *
	 * @param dateDelit
	 * @return An Object DelitProgrammeFidelite whose dateDelit is equals to the given dateDelit. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.dateDelit = :dateDelit and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByDateDelit(@Param("dateDelit")Date dateDelit, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using heureDelit as a search criteria.
	 *
	 * @param heureDelit
	 * @return An Object DelitProgrammeFidelite whose heureDelit is equals to the given heureDelit. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.heureDelit = :heureDelit and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByHeureDelit(@Param("heureDelit")Date heureDelit, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using numeroDelit as a search criteria.
	 *
	 * @param numeroDelit
	 * @return An Object DelitProgrammeFidelite whose numeroDelit is equals to the given numeroDelit. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.numeroDelit = :numeroDelit and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByNumeroDelit(@Param("numeroDelit")String numeroDelit, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using isNotifed as a search criteria.
	 *
	 * @param isNotifed
	 * @return An Object DelitProgrammeFidelite whose isNotifed is equals to the given isNotifed. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.isNotifed = :isNotifed and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByIsNotifed(@Param("isNotifed")Boolean isNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using messageNotifed as a search criteria.
	 *
	 * @param messageNotifed
	 * @return An Object DelitProgrammeFidelite whose messageNotifed is equals to the given messageNotifed. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.messageNotifed = :messageNotifed and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByMessageNotifed(@Param("messageNotifed")String messageNotifed, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using messageLocked as a search criteria.
	 *
	 * @param messageLocked
	 * @return An Object DelitProgrammeFidelite whose messageLocked is equals to the given messageLocked. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.messageLocked = :messageLocked and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByMessageLocked(@Param("messageLocked")String messageLocked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using isApplicateSanction as a search criteria.
	 *
	 * @param isApplicateSanction
	 * @return An Object DelitProgrammeFidelite whose isApplicateSanction is equals to the given isApplicateSanction. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.isApplicateSanction = :isApplicateSanction and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByIsApplicateSanction(@Param("isApplicateSanction")Boolean isApplicateSanction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object DelitProgrammeFidelite whose createdBy is equals to the given createdBy. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object DelitProgrammeFidelite whose createdAt is equals to the given createdAt. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object DelitProgrammeFidelite whose updatedBy is equals to the given updatedBy. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object DelitProgrammeFidelite whose updatedAt is equals to the given updatedAt. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object DelitProgrammeFidelite whose deletedBy is equals to the given deletedBy. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object DelitProgrammeFidelite whose deletedAt is equals to the given deletedAt. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds DelitProgrammeFidelite by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object DelitProgrammeFidelite whose isDeleted is equals to the given isDeleted. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds DelitProgrammeFidelite by using informationRegleSecuriteTamponId as a search criteria.
	 *
	 * @param informationRegleSecuriteTamponId
	 * @return A list of Object DelitProgrammeFidelite whose informationRegleSecuriteTamponId is equals to the given informationRegleSecuriteTamponId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.informationRegleSecuriteTampon.id = :informationRegleSecuriteTamponId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByInformationRegleSecuriteTamponId(@Param("informationRegleSecuriteTamponId")Integer informationRegleSecuriteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DelitProgrammeFidelite by using informationRegleSecuriteTamponId as a search criteria.
   *
   * @param informationRegleSecuriteTamponId
   * @return An Object DelitProgrammeFidelite whose informationRegleSecuriteTamponId is equals to the given informationRegleSecuriteTamponId. If
   *         no DelitProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from DelitProgrammeFidelite e where e.informationRegleSecuriteTampon.id = :informationRegleSecuriteTamponId and e.isDeleted = :isDeleted")
  DelitProgrammeFidelite findDelitProgrammeFideliteByInformationRegleSecuriteTamponId(@Param("informationRegleSecuriteTamponId")Integer informationRegleSecuriteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DelitProgrammeFidelite by using informationRegleSecuriteCarteId as a search criteria.
	 *
	 * @param informationRegleSecuriteCarteId
	 * @return A list of Object DelitProgrammeFidelite whose informationRegleSecuriteCarteId is equals to the given informationRegleSecuriteCarteId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.informationRegleSecuriteCarte.id = :informationRegleSecuriteCarteId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByInformationRegleSecuriteCarteId(@Param("informationRegleSecuriteCarteId")Integer informationRegleSecuriteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DelitProgrammeFidelite by using informationRegleSecuriteCarteId as a search criteria.
   *
   * @param informationRegleSecuriteCarteId
   * @return An Object DelitProgrammeFidelite whose informationRegleSecuriteCarteId is equals to the given informationRegleSecuriteCarteId. If
   *         no DelitProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from DelitProgrammeFidelite e where e.informationRegleSecuriteCarte.id = :informationRegleSecuriteCarteId and e.isDeleted = :isDeleted")
  DelitProgrammeFidelite findDelitProgrammeFideliteByInformationRegleSecuriteCarteId(@Param("informationRegleSecuriteCarteId")Integer informationRegleSecuriteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DelitProgrammeFidelite by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object DelitProgrammeFidelite whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DelitProgrammeFidelite by using pointDeVenteId as a search criteria.
   *
   * @param pointDeVenteId
   * @return An Object DelitProgrammeFidelite whose pointDeVenteId is equals to the given pointDeVenteId. If
   *         no DelitProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from DelitProgrammeFidelite e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
  DelitProgrammeFidelite findDelitProgrammeFideliteByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds DelitProgrammeFidelite by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object DelitProgrammeFidelite whose carteId is equals to the given carteId. If
	 *         no DelitProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from DelitProgrammeFidelite e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<DelitProgrammeFidelite> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one DelitProgrammeFidelite by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object DelitProgrammeFidelite whose carteId is equals to the given carteId. If
   *         no DelitProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from DelitProgrammeFidelite e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  DelitProgrammeFidelite findDelitProgrammeFideliteByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of DelitProgrammeFidelite by using delitProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of DelitProgrammeFidelite
	 * @throws DataAccessException,ParseException
	 */
	public default List<DelitProgrammeFidelite> getByCriteria(Request<DelitProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from DelitProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<DelitProgrammeFidelite> query = em.createQuery(req, DelitProgrammeFidelite.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of DelitProgrammeFidelite by using delitProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of DelitProgrammeFidelite
	 *
	 */
	public default Long count(Request<DelitProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from DelitProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DelitProgrammeFideliteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		DelitProgrammeFideliteDto dto = request.getData() != null ? request.getData() : new DelitProgrammeFideliteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DelitProgrammeFideliteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DelitProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateDelit", dto.getDateDelit(), "e.dateDelit", "Date", dto.getDateDelitParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getHeureDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("heureDelit", dto.getHeureDelit(), "e.heureDelit", "Date", dto.getHeureDelitParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumeroDelit())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numeroDelit", dto.getNumeroDelit(), "e.numeroDelit", "String", dto.getNumeroDelitParam(), param, index, locale));
			}
			if (dto.getIsNotifed()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isNotifed", dto.getIsNotifed(), "e.isNotifed", "Boolean", dto.getIsNotifedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMessageNotifed())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("messageNotifed", dto.getMessageNotifed(), "e.messageNotifed", "String", dto.getMessageNotifedParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMessageLocked())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("messageLocked", dto.getMessageLocked(), "e.messageLocked", "String", dto.getMessageLockedParam(), param, index, locale));
			}
			if (dto.getIsApplicateSanction()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isApplicateSanction", dto.getIsApplicateSanction(), "e.isApplicateSanction", "Boolean", dto.getIsApplicateSanctionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getInformationRegleSecuriteTamponId()!= null && dto.getInformationRegleSecuriteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("informationRegleSecuriteTamponId", dto.getInformationRegleSecuriteTamponId(), "e.informationRegleSecuriteTampon.id", "Integer", dto.getInformationRegleSecuriteTamponIdParam(), param, index, locale));
			}
			if (dto.getInformationRegleSecuriteCarteId()!= null && dto.getInformationRegleSecuriteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("informationRegleSecuriteCarteId", dto.getInformationRegleSecuriteCarteId(), "e.informationRegleSecuriteCarte.id", "Integer", dto.getInformationRegleSecuriteCarteIdParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
