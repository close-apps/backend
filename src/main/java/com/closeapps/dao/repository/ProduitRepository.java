
package com.closeapps.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._ProduitRepository;

/**
 * Repository : Produit.
 */
@Repository
public interface ProduitRepository extends JpaRepository<Produit, Integer>, _ProduitRepository {
	/**
	 * Finds Produit by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object Produit whose id is equals to the given id. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.id = :id and e.isDeleted = :isDeleted")
	Produit findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Produit by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object Produit whose libelle is equals to the given libelle. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Produit findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using urlImage as a search criteria.
	 *
	 * @param urlImage
	 * @return An Object Produit whose urlImage is equals to the given urlImage. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.urlImage = :urlImage and e.isDeleted = :isDeleted")
	List<Produit> findByUrlImage(@Param("urlImage")String urlImage, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using description as a search criteria.
	 *
	 * @param description
	 * @return An Object Produit whose description is equals to the given description. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.description = :description and e.isDeleted = :isDeleted")
	List<Produit> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object Produit whose createdBy is equals to the given createdBy. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Produit> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object Produit whose createdAt is equals to the given createdAt. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Produit> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object Produit whose updatedBy is equals to the given updatedBy. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Produit> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object Produit whose updatedAt is equals to the given updatedAt. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Produit> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object Produit whose deletedBy is equals to the given deletedBy. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Produit> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object Produit whose deletedAt is equals to the given deletedAt. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Produit> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Produit by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object Produit whose isDeleted is equals to the given isDeleted. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.isDeleted = :isDeleted")
	List<Produit> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Produit by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object Produit whose enseigneId is equals to the given enseigneId. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<Produit> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Produit by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object Produit whose enseigneId is equals to the given enseigneId. If
   *         no Produit is found, this method returns null.
   */
  @Query("select e from Produit e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  Produit findProduitByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of Produit by using produitDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of Produit
	 * @throws DataAccessException,ParseException
	 */
	public default List<Produit> getByCriteria(Request<ProduitDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Produit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Produit> query = em.createQuery(req, Produit.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Produit by using produitDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of Produit
	 *
	 */
	public default Long count(Request<ProduitDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Produit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ProduitDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		ProduitDto dto = request.getData() != null ? request.getData() : new ProduitDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ProduitDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ProduitDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUrlImage())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("urlImage", dto.getUrlImage(), "e.urlImage", "String", dto.getUrlImageParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDescription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
