
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._UserEnseignePointDeVenteRepository;

/**
 * Repository : UserEnseignePointDeVente.
 */
@Repository
public interface UserEnseignePointDeVenteRepository extends JpaRepository<UserEnseignePointDeVente, Integer>, _UserEnseignePointDeVenteRepository {
	/**
	 * Finds UserEnseignePointDeVente by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object UserEnseignePointDeVente whose id is equals to the given id. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.id = :id and e.isDeleted = :isDeleted")
	UserEnseignePointDeVente findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds UserEnseignePointDeVente by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object UserEnseignePointDeVente whose createdBy is equals to the given createdBy. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object UserEnseignePointDeVente whose createdAt is equals to the given createdAt. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object UserEnseignePointDeVente whose updatedBy is equals to the given updatedBy. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object UserEnseignePointDeVente whose updatedAt is equals to the given updatedAt. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object UserEnseignePointDeVente whose deletedBy is equals to the given deletedBy. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object UserEnseignePointDeVente whose deletedAt is equals to the given deletedAt. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserEnseignePointDeVente by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object UserEnseignePointDeVente whose isDeleted is equals to the given isDeleted. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds UserEnseignePointDeVente by using pointDeVenteId as a search criteria.
	 *
	 * @param pointDeVenteId
	 * @return A list of Object UserEnseignePointDeVente whose pointDeVenteId is equals to the given pointDeVenteId. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one UserEnseignePointDeVente by using pointDeVenteId as a search criteria.
   *
   * @param pointDeVenteId
   * @return An Object UserEnseignePointDeVente whose pointDeVenteId is equals to the given pointDeVenteId. If
   *         no UserEnseignePointDeVente is found, this method returns null.
   */
  @Query("select e from UserEnseignePointDeVente e where e.pointDeVente.id = :pointDeVenteId and e.isDeleted = :isDeleted")
  UserEnseignePointDeVente findUserEnseignePointDeVenteByPointDeVenteId(@Param("pointDeVenteId")Integer pointDeVenteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds UserEnseignePointDeVente by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object UserEnseignePointDeVente whose userId is equals to the given userId. If
	 *         no UserEnseignePointDeVente is found, this method returns null.
	 */
	@Query("select e from UserEnseignePointDeVente e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<UserEnseignePointDeVente> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one UserEnseignePointDeVente by using userId as a search criteria.
   *
   * @param userId
   * @return An Object UserEnseignePointDeVente whose userId is equals to the given userId. If
   *         no UserEnseignePointDeVente is found, this method returns null.
   */
  @Query("select e from UserEnseignePointDeVente e where e.user.id = :userId and e.isDeleted = :isDeleted")
  UserEnseignePointDeVente findUserEnseignePointDeVenteByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of UserEnseignePointDeVente by using userEnseignePointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of UserEnseignePointDeVente
	 * @throws DataAccessException,ParseException
	 */
	public default List<UserEnseignePointDeVente> getByCriteria(Request<UserEnseignePointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from UserEnseignePointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<UserEnseignePointDeVente> query = em.createQuery(req, UserEnseignePointDeVente.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of UserEnseignePointDeVente by using userEnseignePointDeVenteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of UserEnseignePointDeVente
	 *
	 */
	public default Long count(Request<UserEnseignePointDeVenteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from UserEnseignePointDeVente e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<UserEnseignePointDeVenteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		UserEnseignePointDeVenteDto dto = request.getData() != null ? request.getData() : new UserEnseignePointDeVenteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (UserEnseignePointDeVenteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(UserEnseignePointDeVenteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getPointDeVenteId()!= null && dto.getPointDeVenteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteId", dto.getPointDeVenteId(), "e.pointDeVente.id", "Integer", dto.getPointDeVenteIdParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteNom", dto.getPointDeVenteNom(), "e.pointDeVente.nom", "String", dto.getPointDeVenteNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPointDeVenteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("pointDeVenteCode", dto.getPointDeVenteCode(), "e.pointDeVente.code", "String", dto.getPointDeVenteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
