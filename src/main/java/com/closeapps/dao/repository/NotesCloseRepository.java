
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._NotesCloseRepository;

/**
 * Repository : NotesClose.
 */
@Repository
public interface NotesCloseRepository extends JpaRepository<NotesClose, Integer>, _NotesCloseRepository {
	/**
	 * Finds NotesClose by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object NotesClose whose id is equals to the given id. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.id = :id and e.isDeleted = :isDeleted")
	NotesClose findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NotesClose by using commentaire as a search criteria.
	 *
	 * @param commentaire
	 * @return An Object NotesClose whose commentaire is equals to the given commentaire. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.commentaire = :commentaire and e.isDeleted = :isDeleted")
	List<NotesClose> findByCommentaire(@Param("commentaire")String commentaire, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using isValiderByClose as a search criteria.
	 *
	 * @param isValiderByClose
	 * @return An Object NotesClose whose isValiderByClose is equals to the given isValiderByClose. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.isValiderByClose = :isValiderByClose and e.isDeleted = :isDeleted")
	List<NotesClose> findByIsValiderByClose(@Param("isValiderByClose")Boolean isValiderByClose, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using retourClose as a search criteria.
	 *
	 * @param retourClose
	 * @return An Object NotesClose whose retourClose is equals to the given retourClose. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.retourClose = :retourClose and e.isDeleted = :isDeleted")
	List<NotesClose> findByRetourClose(@Param("retourClose")String retourClose, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using nbreEtoile as a search criteria.
	 *
	 * @param nbreEtoile
	 * @return An Object NotesClose whose nbreEtoile is equals to the given nbreEtoile. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.nbreEtoile = :nbreEtoile and e.isDeleted = :isDeleted")
	List<NotesClose> findByNbreEtoile(@Param("nbreEtoile")Integer nbreEtoile, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object NotesClose whose createdBy is equals to the given createdBy. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<NotesClose> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object NotesClose whose createdAt is equals to the given createdAt. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<NotesClose> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object NotesClose whose updatedBy is equals to the given updatedBy. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<NotesClose> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object NotesClose whose updatedAt is equals to the given updatedAt. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<NotesClose> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object NotesClose whose deletedBy is equals to the given deletedBy. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<NotesClose> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object NotesClose whose deletedAt is equals to the given deletedAt. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<NotesClose> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds NotesClose by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object NotesClose whose isDeleted is equals to the given isDeleted. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.isDeleted = :isDeleted")
	List<NotesClose> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds NotesClose by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object NotesClose whose carteId is equals to the given carteId. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<NotesClose> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotesClose by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object NotesClose whose carteId is equals to the given carteId. If
   *         no NotesClose is found, this method returns null.
   */
  @Query("select e from NotesClose e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  NotesClose findNotesCloseByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds NotesClose by using userId as a search criteria.
	 *
	 * @param userId
	 * @return A list of Object NotesClose whose userId is equals to the given userId. If
	 *         no NotesClose is found, this method returns null.
	 */
	@Query("select e from NotesClose e where e.user.id = :userId and e.isDeleted = :isDeleted")
	List<NotesClose> findByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one NotesClose by using userId as a search criteria.
   *
   * @param userId
   * @return An Object NotesClose whose userId is equals to the given userId. If
   *         no NotesClose is found, this method returns null.
   */
  @Query("select e from NotesClose e where e.user.id = :userId and e.isDeleted = :isDeleted")
  NotesClose findNotesCloseByUserId(@Param("userId")Integer userId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of NotesClose by using notesCloseDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of NotesClose
	 * @throws DataAccessException,ParseException
	 */
	public default List<NotesClose> getByCriteria(Request<NotesCloseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from NotesClose e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<NotesClose> query = em.createQuery(req, NotesClose.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of NotesClose by using notesCloseDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of NotesClose
	 *
	 */
	public default Long count(Request<NotesCloseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from NotesClose e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<NotesCloseDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		NotesCloseDto dto = request.getData() != null ? request.getData() : new NotesCloseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (NotesCloseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(NotesCloseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCommentaire())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("commentaire", dto.getCommentaire(), "e.commentaire", "String", dto.getCommentaireParam(), param, index, locale));
			}
			if (dto.getIsValiderByClose()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValiderByClose", dto.getIsValiderByClose(), "e.isValiderByClose", "Boolean", dto.getIsValiderByCloseParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getRetourClose())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("retourClose", dto.getRetourClose(), "e.retourClose", "String", dto.getRetourCloseParam(), param, index, locale));
			}
			if (dto.getNbreEtoile()!= null && dto.getNbreEtoile() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nbreEtoile", dto.getNbreEtoile(), "e.nbreEtoile", "Integer", dto.getNbreEtoileParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getUserId()!= null && dto.getUserId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userId", dto.getUserId(), "e.user.id", "Integer", dto.getUserIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserPrenoms())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenoms", dto.getUserPrenoms(), "e.user.prenoms", "String", dto.getUserPrenomsParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUserLogin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
