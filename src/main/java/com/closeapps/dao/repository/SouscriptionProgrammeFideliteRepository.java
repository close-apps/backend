
package com.closeapps.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.closeapps.helper.dto.*;
import com.closeapps.helper.contract.*;
import com.closeapps.helper.contract.Request;
import com.closeapps.helper.contract.Response;
import com.closeapps.helper.*;
import com.closeapps.dao.entity.*;
import com.closeapps.dao.repository.customize._SouscriptionProgrammeFideliteRepository;

/**
 * Repository : SouscriptionProgrammeFidelite.
 */
@Repository
public interface SouscriptionProgrammeFideliteRepository extends JpaRepository<SouscriptionProgrammeFidelite, Integer>, _SouscriptionProgrammeFideliteRepository {
	/**
	 * Finds SouscriptionProgrammeFidelite by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object SouscriptionProgrammeFidelite whose id is equals to the given id. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.id = :id and e.isDeleted = :isDeleted")
	SouscriptionProgrammeFidelite findById(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFidelite by using dateSouscription as a search criteria.
	 *
	 * @param dateSouscription
	 * @return An Object SouscriptionProgrammeFidelite whose dateSouscription is equals to the given dateSouscription. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.dateSouscription = :dateSouscription and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByDateSouscription(@Param("dateSouscription")Date dateSouscription, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object SouscriptionProgrammeFidelite whose createdBy is equals to the given createdBy. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using createdAt as a search criteria.
	 *
	 * @param createdAt
	 * @return An Object SouscriptionProgrammeFidelite whose createdAt is equals to the given createdAt. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using updatedBy as a search criteria.
	 *
	 * @param updatedBy
	 * @return An Object SouscriptionProgrammeFidelite whose updatedBy is equals to the given updatedBy. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using updatedAt as a search criteria.
	 *
	 * @param updatedAt
	 * @return An Object SouscriptionProgrammeFidelite whose updatedAt is equals to the given updatedAt. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using deletedBy as a search criteria.
	 *
	 * @param deletedBy
	 * @return An Object SouscriptionProgrammeFidelite whose deletedBy is equals to the given deletedBy. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using deletedAt as a search criteria.
	 *
	 * @param deletedAt
	 * @return An Object SouscriptionProgrammeFidelite whose deletedAt is equals to the given deletedAt. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SouscriptionProgrammeFidelite by using isDeleted as a search criteria.
	 *
	 * @param isDeleted
	 * @return An Object SouscriptionProgrammeFidelite whose isDeleted is equals to the given isDeleted. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SouscriptionProgrammeFidelite by using carteId as a search criteria.
	 *
	 * @param carteId
	 * @return A list of Object SouscriptionProgrammeFidelite whose carteId is equals to the given carteId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFidelite by using carteId as a search criteria.
   *
   * @param carteId
   * @return An Object SouscriptionProgrammeFidelite whose carteId is equals to the given carteId. If
   *         no SouscriptionProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFidelite e where e.carte.id = :carteId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFidelite findSouscriptionProgrammeFideliteByCarteId(@Param("carteId")Integer carteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFidelite by using programmeFideliteTamponId as a search criteria.
	 *
	 * @param programmeFideliteTamponId
	 * @return A list of Object SouscriptionProgrammeFidelite whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFidelite by using programmeFideliteTamponId as a search criteria.
   *
   * @param programmeFideliteTamponId
   * @return An Object SouscriptionProgrammeFidelite whose programmeFideliteTamponId is equals to the given programmeFideliteTamponId. If
   *         no SouscriptionProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFidelite e where e.programmeFideliteTampon.id = :programmeFideliteTamponId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFidelite findSouscriptionProgrammeFideliteByProgrammeFideliteTamponId(@Param("programmeFideliteTamponId")Integer programmeFideliteTamponId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFidelite by using enseigneId as a search criteria.
	 *
	 * @param enseigneId
	 * @return A list of Object SouscriptionProgrammeFidelite whose enseigneId is equals to the given enseigneId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFidelite by using enseigneId as a search criteria.
   *
   * @param enseigneId
   * @return An Object SouscriptionProgrammeFidelite whose enseigneId is equals to the given enseigneId. If
   *         no SouscriptionProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFidelite e where e.enseigne.id = :enseigneId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFidelite findSouscriptionProgrammeFideliteByEnseigneId(@Param("enseigneId")Integer enseigneId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds SouscriptionProgrammeFidelite by using programmeFideliteCarteId as a search criteria.
	 *
	 * @param programmeFideliteCarteId
	 * @return A list of Object SouscriptionProgrammeFidelite whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
	 *         no SouscriptionProgrammeFidelite is found, this method returns null.
	 */
	@Query("select e from SouscriptionProgrammeFidelite e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
	List<SouscriptionProgrammeFidelite> findByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one SouscriptionProgrammeFidelite by using programmeFideliteCarteId as a search criteria.
   *
   * @param programmeFideliteCarteId
   * @return An Object SouscriptionProgrammeFidelite whose programmeFideliteCarteId is equals to the given programmeFideliteCarteId. If
   *         no SouscriptionProgrammeFidelite is found, this method returns null.
   */
  @Query("select e from SouscriptionProgrammeFidelite e where e.programmeFideliteCarte.id = :programmeFideliteCarteId and e.isDeleted = :isDeleted")
  SouscriptionProgrammeFidelite findSouscriptionProgrammeFideliteByProgrammeFideliteCarteId(@Param("programmeFideliteCarteId")Integer programmeFideliteCarteId, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds List of SouscriptionProgrammeFidelite by using souscriptionProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of SouscriptionProgrammeFidelite
	 * @throws DataAccessException,ParseException
	 */
	public default List<SouscriptionProgrammeFidelite> getByCriteria(Request<SouscriptionProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SouscriptionProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<SouscriptionProgrammeFidelite> query = em.createQuery(req, SouscriptionProgrammeFidelite.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	

	/**
	 * Finds count of SouscriptionProgrammeFidelite by using souscriptionProgrammeFideliteDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of SouscriptionProgrammeFidelite
	 *
	 */
	public default Long count(Request<SouscriptionProgrammeFideliteDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SouscriptionProgrammeFidelite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscriptionProgrammeFideliteDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		SouscriptionProgrammeFideliteDto dto = request.getData() != null ? request.getData() : new SouscriptionProgrammeFideliteDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscriptionProgrammeFideliteDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscriptionProgrammeFideliteDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSouscription())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSouscription", dto.getDateSouscription(), "e.dateSouscription", "Date", dto.getDateSouscriptionParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getDeletedBy()!= null && dto.getDeletedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getCarteId()!= null && dto.getCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteId", dto.getCarteId(), "e.carte.id", "Integer", dto.getCarteIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteTamponId()!= null && dto.getProgrammeFideliteTamponId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponId", dto.getProgrammeFideliteTamponId(), "e.programmeFideliteTampon.id", "Integer", dto.getProgrammeFideliteTamponIdParam(), param, index, locale));
			}
			if (dto.getEnseigneId()!= null && dto.getEnseigneId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneId", dto.getEnseigneId(), "e.enseigne.id", "Integer", dto.getEnseigneIdParam(), param, index, locale));
			}
			if (dto.getProgrammeFideliteCarteId()!= null && dto.getProgrammeFideliteCarteId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteId", dto.getProgrammeFideliteCarteId(), "e.programmeFideliteCarte.id", "Integer", dto.getProgrammeFideliteCarteIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCarteCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("carteCode", dto.getCarteCode(), "e.carte.code", "String", dto.getCarteCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteTamponLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteTamponLibelle", dto.getProgrammeFideliteTamponLibelle(), "e.programmeFideliteTampon.libelle", "String", dto.getProgrammeFideliteTamponLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEnseigneNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enseigneNom", dto.getEnseigneNom(), "e.enseigne.nom", "String", dto.getEnseigneNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getProgrammeFideliteCarteLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("programmeFideliteCarteLibelle", dto.getProgrammeFideliteCarteLibelle(), "e.programmeFideliteCarte.libelle", "String", dto.getProgrammeFideliteCarteLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
