package com.closeapps;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.closeapps.helper.Utilities;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

@SpringBootApplication
public class CloseAppsApplication {	

	public static void main(String[] args) throws ParseException {
		SpringApplication.run(CloseAppsApplication.class, args);

		//String test = "test.png" ;

		//		String hkd []  = test.split("\\.") ;
		//		System.out.println(hkd.length);

		//		System.out.println(System.currentTimeMillis());
		//		  System.out.println(new java.util.Date().getTime());
		//		  System.out.println(Calendar.getInstance().getTimeInMillis() );
		//		  System.out.println(Calendar.getInstance().getTime().getTime ()) ;
		//		int a = 5 ;
		//		double b = 4 ;
		//		double c = a/b ;
		//		DecimalFormat df = new DecimalFormat("#");
		//
		//		System.out.println(c);		
		//		System.out.println(df.format(c));		


		//		 try {
		//	            generateQRCodeImage("1 2 CREDITER 1300", 350, 350, QR_CODE_IMAGE_PATH);
		//	            generateQRCodeImage("This is my first QR Code 11", 350, 350, QR_CODE_IMAGE_PATH_1);
		//	            generateQRCodeImage("This is my first QR Code 22", 350, 350, QR_CODE_IMAGE_PATH_2);
		//	            generateQRCodeImage("This is my first QR Code 33", 350, 350, QR_CODE_IMAGE_PATH_3);
		//	            System.out.println("Succes !!!");
		//	        } catch (WriterException e) {
		//	            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
		//	        } catch (IOException e) {
		//	            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
		//	        }


	}


	private static final String QR_CODE_IMAGE_PATH = "./MyQRCode.png";
	private static final String QR_CODE_IMAGE_PATH_1 = "./MyQRCode1.png";
	private static final String QR_CODE_IMAGE_PATH_2 = "./MyQRCode2.png";
	private static final String QR_CODE_IMAGE_PATH_3 = "./MyQRCode3.png";

	private static void generateQRCodeImage(String text, int width, int height, String filePath)
			throws WriterException, IOException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

		Path path = FileSystems.getDefault().getPath(filePath);
		System.out.println(bitMatrix) ;
		MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
	}


	//This method takes the text to be encoded, the width and height of the QR Code, 
	//and returns the QR Code in the form of a byte array.

	private byte[] getQRCodeImage(String text, int width, int height) throws WriterException, IOException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

		ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
		byte[] pngData = pngOutputStream.toByteArray(); 
		return pngData;
	}



}

@RestController
class HelloController {

	@GetMapping("/")
	String hello() {
		return "Ma premiere application deployée sur HEROKU !!!" ;
	}
}
