
#FROM openjdk:8-jdk-alpine
FROM openjdk:8-jre-alpine

RUN mkdir /allFiles

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar



ENTRYPOINT ["java","-jar","/app.jar","--spring.profiles.active=prod"]
# ENTRYPOINT ["java","-jar","/app.jar","--spring.profiles.active=local"]